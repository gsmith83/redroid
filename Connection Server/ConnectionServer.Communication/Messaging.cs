﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using ConnectionServer.Utilities;
using Newtonsoft.Json;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using System.Configuration;

namespace ConnectionServer.Communication
{
	public class Messaging
	{
		// Twilio Texting Constants
		private static readonly string TwilioAccountSid = ConfigurationManager.AppSettings["TwilioAccountSid"];
		private static readonly string TwilioAuthToken = ConfigurationManager.AppSettings["TwilioAuthToken"];
		private static readonly string TwilioPhoneNumber = ConfigurationManager.AppSettings["TwilioPhoneNumber"];


		// Email Constants from redroid.uofu@gmail.com
		private static readonly string FromEmailAddress = ConfigurationManager.AppSettings["FromEmailAddress"];
		private static readonly string FromEmailPassword = ConfigurationManager.AppSettings["FromEmailPassword"];
		private static readonly string EmailSubject = ConfigurationManager.AppSettings["EmailSubject"];

		// Google Firebase push notifications constants
		private static readonly string FirebaseUrl = ConfigurationManager.AppSettings["FirebaseUrl"];
		private static readonly string FirebaseSenderId = ConfigurationManager.AppSettings["FirebaseSenderId"];
		private static readonly string FirebaseServerKey = ConfigurationManager.AppSettings["FirebaseServerKey"];



		/// <summary>
		/// Sends an sms message using the redroid twilio account.
		/// </summary>
		/// <param name="phoneNumber">The phone number to send the sms to.</param>
		/// <param name="messageContent">The content of the message.</param>
		public static void SendText(string phoneNumber, string messageContent)
		{
			if(TwilioAccountSid == null || TwilioAuthToken == null || TwilioPhoneNumber == null)
			{
				throw new InvalidOperationException("No values for TwilioAccountSid, TwilioAuthToken, or TwilioPhoneNumber and are required for sending text messages.");
			}
		    return;
			TwilioClient.Init(TwilioAccountSid, TwilioAuthToken);

			var message = MessageResource.Create(
				to: new PhoneNumber(phoneNumber),
				from: new PhoneNumber(TwilioPhoneNumber),
				body: messageContent);

		}


		/// <summary>
		/// Sends an email from the redroid email address to the given email address with the given content.
		/// </summary>
		/// <param name="to">The email address to send to.</param>
		/// <param name="htmlContent">The html content of the email.</param>
		public static void SendEmail(string to, string htmlContent)
		{
			if(FromEmailAddress == null || FromEmailPassword == null || EmailSubject == null)
			{
				throw new InvalidOperationException(
					"No values for FromEmailAddress, FromEmailPassword, or EmailSubject and are required for sending email.");
			}
			var fromAddress = new MailAddress(FromEmailAddress, "Redroid");
			var toAddress = new MailAddress(to);

			var smtp = new SmtpClient
			{
				Host = "smtp.gmail.com",
				Port = 587,
				EnableSsl = true,
				DeliveryMethod = SmtpDeliveryMethod.Network,
				UseDefaultCredentials = false,
				Credentials = new NetworkCredential(fromAddress.Address, FromEmailPassword)
			};

			using (var mailMessage = new MailMessage(fromAddress, toAddress)
			{
				Subject = EmailSubject,
				IsBodyHtml = true,
				Body = htmlContent
			})
			{
				smtp.Send(mailMessage);
				Utilities.Utilities.Log(LogType.NotificationSent, $"Email sent to {to}: {htmlContent}");
			}
		}


		/// <summary>
		/// Sends a push notification to the android device identified by token.
		/// </summary>
		/// <param name="token">The unique Firebase Token belonging to the app instance that caused the rule.</param>
		/// <param name="configId">The Config ID that caused this notification to be sent.</param>
		/// <param name="ruleId">The Rule ID that caused this notification to be sent.</param>
		/// <param name="title">The short title of the notification.</param>
		/// <param name="content">The body of the notification.</param>
		/// <param name="imageUrl">Optional. The url of the image to link to.</param>
		public static void SendPushNotification(string token, string configId, string ruleId, string title, string content, string imageUrl = "")
		{
			if(FirebaseUrl == null || FirebaseSenderId == null || FirebaseServerKey == null)
			{
				throw new InvalidOperationException("No values for FirebaseUrl, FirebaseSenderId, or FirebaseServerKey and are required for sending push notifications.");
			}

			var request = WebRequest.Create(FirebaseUrl);
			request.Method = "POST";
			request.ContentType = "application/json";

			object data; // don't include the image if we weren't given one.
			if (string.IsNullOrEmpty(imageUrl))
			{
				data = new
				{
					configId,
					ruleId,
				};
			}
			else
			{
				data = new
				{
					configId,
					ruleId,
					image = imageUrl
				};
			}

			var notification = new
			{
				body = content,
				title,
				sound = "default"
			};
			var json = JsonConvert.SerializeObject(new
			{
				to = token,
				priority = "high",
				notification,
				data
			});

			var jsonBytes = Encoding.UTF8.GetBytes(json);
			request.Headers.Add($"Authorization: key={FirebaseServerKey}");
			request.Headers.Add($"Sender: id={FirebaseSenderId}");
			request.ContentLength = jsonBytes.Length;

			using (var dataStream = request.GetRequestStream())
			{
				dataStream.Write(jsonBytes, 0, jsonBytes.Length);

				using (var tResponse = request.GetResponse())
				using (var dataStreamResponse = tResponse.GetResponseStream())
				using (var tReader = new StreamReader(dataStreamResponse))
				{
					string sResponseFromServer = tReader.ReadToEnd();

					Utilities.Utilities.Log(LogType.NotificationSent, $"Notification was sent. Includes {imageUrl}.\n{sResponseFromServer}");
				}
			}
		}
	}
}