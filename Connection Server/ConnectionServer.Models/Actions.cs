﻿using Newtonsoft.Json;

namespace ConnectionServer.Models
{
    
    class Actions
    {
        /// <summary>
        /// Whether a notification should be send to this phone if the rule is triggered.
        /// </summary>
        [JsonProperty("notification")]
        public bool Notification { get; set; }

        /// <summary>
        /// The phone number to send the text to. Null if no text is requested.
        /// </summary>
        [JsonProperty("text")]
        public string Text { get; set; }

        /// <summary>
        /// The email address to send the email to. Null if no email is requested.
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }
    }
}