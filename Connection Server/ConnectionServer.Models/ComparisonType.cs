﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ConnectionServer.Models
{
    /// <summary>
    /// Comparison of a value. Only time is allowed to be Between currently.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ComparisonType
    {
        Above,
        Below,
        Between
    }
}