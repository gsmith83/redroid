﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ConnectionServer.Models
{


    public class Connection
    {
        /// <summary>
        /// Whether this connection belongs to a master or peripheral device.
        /// </summary>
        public ConnectionType Type;


        private RedroidSocket _apiSocket;

        /// <summary>
        /// The 
        /// </summary>
        public IPEndPoint ComSocketEndPoint;

        /// <summary>
        /// The API socket of this connection. Traditionally flows from master to server, or from server to peripheral.
        /// </summary>
        public RedroidSocket ApiSocket
        {
            get {return _apiSocket; }
            set
            {
                this.ComSocketEndPoint = value.EndPoint;
                this._apiSocket = value;
            }
        }

        /// <summary>
        /// The Event socket of this connection. Traditionally flows from server to master, or from peripheral to server.
        /// </summary>
        public RedroidSocket EventSocket;
    }

    /// <summary>
    /// The type of device.
    /// </summary>
    public enum ConnectionType
    {
        Master,
        Peripheral
    }

    /// <summary>
    /// A socket wrapper with an internal byte buffer containing bytes waiting to be read into the CurrentMessage stringbuilder. 
    /// </summary>
    public class RedroidSocket
    {
        /// <summary>
        /// The buffer to be given to the underlying socket to read from.
        /// </summary>
        public byte[] Buffer;

        /// <summary>
        /// The message read from the buffer so far. May include multiple messages or partial messages.
        /// </summary>
        public StringBuilder CurrentMessage = new StringBuilder();

        /// <summary>
        /// This RedroidSocket's underlying socket
        /// </summary>
        public Socket Socket
        {
            get {return _socket; }
            set
            {
                this.EndPoint = (IPEndPoint)value.RemoteEndPoint;
                this._socket = value;
            }
        }

        private Socket _socket;
        
        public IPEndPoint EndPoint;

        /// <summary>
        /// The Remote IPAddress this connection is connected to.
        /// </summary>
        public IPAddress Address => EndPoint.Address;

        /// <summary>
        /// The Remote Port this connection's API socket is connected to.
        /// </summary>
        public int Port => EndPoint.Port;

        /// <summary>
        /// The Connection this RedroidSocket is associated with
        /// </summary>
        public Connection Connection;

        /// <summary>
        /// Whether this socket is used for API communication or Events
        /// </summary>
        public SocketType Type { get; set; }

        public override string ToString() => $"{Connection?.Type.ToString() ?? "No connection"} {Type} {Address}:{Port}";

        /// <summary>
        /// The callback queue to call when a message is received. If queue is empty, default routes should be used.
        /// </summary>
        public Queue<Func<Message, Connection, bool>> MessageReceivedCallbacks = new Queue<Func<Message, Connection, bool>>();
    }


    public enum SocketType
    {
        Api,
        Event
    }

    /// <summary>
    /// 
    /// </summary>
    public class MasterConnection : Connection 
    {
        /// <summary>
        /// The device id of the peripheral this master is connected to.
        /// </summary>
        public string PeripheralDeviceId { get; set; } 

        /// <summary>
        /// The session id assigned to this master.
        /// </summary>
        public string SessionId { get; set; }

        // The device this master is associated with.
        public Master Device { get; set; }
    }


    public class PeripheralConnection : Connection
    {
        /// <summary>
        /// This peripheral's security token.
        /// </summary>
        public string SecurityToken { get; set; }

        /// <summary>
        /// Tis peripheral's device id.
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// The device this peripheral connection is associated with.
        /// </summary>
        public Peripheral Device { get; set; }
    }
}