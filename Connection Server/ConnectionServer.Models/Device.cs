﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConnectionServer.Models
{
    public class Master
    {
        
    }


    public class Peripheral
    {
        /// <summary>
        /// The rules currently running on this peripheral device.
        /// </summary>
        public ISet<Rule> Rules;

        /// <summary>
        /// Returns the set of all TriggerTypes in use by all rules of this peripheral.
        /// </summary>
        /// <returns></returns>
        public ISet<TriggerType> GetRuleSensors()
        {
            var result = new HashSet<TriggerType>();

            foreach (var rule in Rules)
            {
                result.UnionWith(rule.RequiredSensors());
            }
            return result;
        }


        public Peripheral()
        {
            Rules = new HashSet<Rule>();
        }


    }
}
