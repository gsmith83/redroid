﻿using Newtonsoft.Json;

namespace ConnectionServer.Models
{
    public class SensorEvent
    {
        [JsonProperty("accuracy")]
        public int Accuracy { get; set; }

        [JsonProperty("sensor_type")]
        public TriggerType TriggerType { get; set; }

        [JsonProperty("time_stamp")]
        public long TimeStamp { get; set; }

        [JsonProperty("values")]
        public double[] Values { get; set; }
    }

    public class LocationEvent
    {
        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        [JsonProperty("longitude")]
        public double Longitude { get; set; }

        [JsonProperty("altitude")]
        public double Altitude { get; set; }

        [JsonProperty("bearing")]
        public double Bearing { get; set; }

        [JsonProperty("speed")]
        public double Speed { get; set; }

        [JsonProperty("time_stamp")]
        public long TimeStamp { get; set; }
    }

    public class MotionEvent
    {
        [JsonProperty("image")]
        public string ImageUrl { get; set; }

    }
}