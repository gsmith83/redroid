﻿using System.Linq;
using System.Net;
using System.Net.WebSockets;

namespace ConnectionServer.Models
{
    public enum MessageType
    {
        Request,
        Response
    }

    public class Message
    {
        public string Url { get; set; }

        /// <summary>
        /// Returns the string following the final '/' in the url
        /// </summary>
        public string UrlTrailingParam => Url.Split('/').Last();

        public string Body { get; set; }

        public string HttpMethod { get; set; }

        public HttpStatusCode StatusCode { get; set; }

        public SocketType SocketType { get; set; }

        public string FullText { get; set; }

        public MessageType MessageType;


        public override string ToString()
        {
            switch (MessageType)
            {
                case MessageType.Request:
                    return $"Url: {Url} \nBody: {Body} \nMethod: {HttpMethod} \n";

                case MessageType.Response:
                    return $"Body: {Body} \nStatusCode: {StatusCode} \n";

                default:
                    return $"Url: {Url} \nBody: {Body} \nMethod: {HttpMethod} \nStatusCode: {StatusCode} \nType: {SocketType} \n";

            }
        }
    }
}