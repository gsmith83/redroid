﻿using System.Collections.Generic;
using ConnectionServer.Communication;
using Newtonsoft.Json;

namespace ConnectionServer.Models
{

    public class Rule
    {
        [JsonProperty("token")]
        private string FirebaseToken { get; set; }

        [JsonProperty("device_id")]
        private string DeviceId { get; set; }

        [JsonProperty("config_id")]
        private string ConfigId { get; set; }

        [JsonProperty("rule_id")]
        public string RuleId { get; set; }

        [JsonProperty("rule_name")]
        private string RuleName { get; set; }

        [JsonProperty("triggers")]
        private Trigger[] Triggers { get; set; }

        [JsonProperty("action")]
        private Actions Actions { get; set; }

        public ISet<TriggerType> RequiredSensors()
        {
            var result = new HashSet<TriggerType>();

            foreach (var trigger in Triggers)
            {
                var type = trigger.TriggerType;

                // fake that we need location for any sensor that consumes a location event.
                if (type == TriggerType.Speed)
                    result.Add(TriggerType.Location);

                result.Add(type);
            }

            return result;
        }

        /// <summary>
        /// Adds the given sensorEvent's data to this rule and evaluates the trigger, if necessary.
        /// </summary>
        /// <returns>Whether this rule's actions were performed.</returns>
        public bool AddSensorEvent(SensorEvent sensorEvent)
        {
            foreach (var trigger in Triggers)
            {
                if (trigger.TriggerType == sensorEvent.TriggerType)
                {
                    // TODO: don't assume the first value is the correct one for all sensors.
                    trigger.AddDataPoint(sensorEvent.Values[0]);
                }
            }

            if (EvaluateTriggers())
            {
                PerformActions();
                return true;
            }

            return false;
        }

        /// <summary>
        /// To be called whenever a peripheral detects motion.
        /// </summary>
        /// <param name="imageUrl">The image url of image that caused motion detection.</param>
        /// <returns></returns>
        public bool MotionDetected(string imageUrl)
        {
            // make sure this rule cares about motion detection
            if (!RequiredSensors().Contains(TriggerType.Video))
                return false;

            foreach (var trigger in Triggers)
            {
                if (trigger.TriggerType == TriggerType.Video)
                {
                    trigger.MotionDetected();
                }

            }

            if (EvaluateTriggers())
            {
                PerformActions(imageUrl);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Evaluates all triggers that is part of this rule.
        /// </summary>
        /// <returns>True if all triggers have evaluated to true.</returns>
        private bool EvaluateTriggers()
        {
            foreach (var trigger in Triggers)
            {
                // see if the trigger is true, may use new data point we just added or its previous value
                if (!trigger.Evaluate())
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Performs all actions to be taken based on the values in the Actions parameter.
        /// </summary>
        /// <param name="imageUrl"></param>
        private void PerformActions(string imageUrl = "")
        {
            var title = $"Rule \"{RuleName}\" has been triggered.";

            if (Actions.Notification)
            {
                if (RequiredSensors().Contains(TriggerType.Video))
                    Messaging.SendPushNotification(FirebaseToken, ConfigId, RuleId, "Redroid", title, imageUrl);
                else
                    Messaging.SendPushNotification(FirebaseToken, ConfigId, RuleId, "Redroid", title);

            }

            if (!string.IsNullOrWhiteSpace(Actions.Email))
            {
                var imageTag = string.IsNullOrWhiteSpace(imageUrl)
                    ? ""
                    : $"<img src =\"{imageUrl}\">";
                var content = $@"<!doctype html> <html> <head> <meta name=""viewport"" content=""width=device-width"" /> <meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"" /> <style> /* ------------------------------------- GLOBAL RESETS ------------------------------------- */ img {{border: none; -ms-interpolation-mode: bicubic; max-width: 100%; }} body {{background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; }} table {{border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; }} table td {{font-family: sans-serif; font-size: 14px; vertical-align: top; }} /* ------------------------------------- BODY & CONTAINER ------------------------------------- */ .body {{background-color: #f6f6f6; width: 100%; }} /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */ .container {{display: block; Margin: 0 auto !important; /* makes it centered */ max-width: 580px; padding: 10px; width: 580px; }} /* This should also be a block element, so that it will fill 100% of the .container */ .content {{box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; }} /* ------------------------------------- HEADER, FOOTER, MAIN ------------------------------------- */ .main {{background: #fff; border-radius: 3px; width: 100%; }} .wrapper {{box-sizing: border-box; padding: 20px; }} .footer {{clear: both; padding-top: 10px; text-align: center; width: 100%; }} .footer td, .footer p, .footer span, .footer a {{color: #999999; font-size: 12px; text-align: center; }} /* ------------------------------------- TYPOGRAPHY ------------------------------------- */ h1, h2, h3, h4 {{color: #000000; font-family: sans-serif; font-weight: 400; line-height: 1.4; margin: 0; Margin-bottom: 30px; }} h1 {{font-size: 35px; font-weight: 300; text-align: center; text-transform: capitalize; }} p, ul, ol {{font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px; }} p li, ul li, ol li {{list-style-position: inside; margin-left: 5px; }} a {{color: #3498db; text-decoration: underline; }} /* ------------------------------------- BUTTONS ------------------------------------- */ .btn {{box-sizing: border-box; width: 100%; }} .btn>tbody>tr>td {{padding-bottom: 15px; }} .btn table {{width: auto; }} .btn table td {{background-color: #ffffff; border-radius: 5px; text-align: center; }} .btn a {{background-color: #ffffff; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; color: #3498db; cursor: pointer; display: inline-block; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-decoration: none; text-transform: capitalize; }} .btn-primary table td {{background-color: #3498db; }} .btn-primary a {{background-color: #3498db; border-color: #3498db; color: #ffffff; }} /* ------------------------------------- OTHER STYLES THAT MIGHT BE USEFUL ------------------------------------- */ .last {{margin-bottom: 0; }} .first {{margin-top: 0; }} .align-center {{text-align: center; }} .align-right {{text-align: right; }} .align-left {{text-align: left; }} .clear {{clear: both; }} .mt0 {{margin-top: 0; }} .mb0 {{margin-bottom: 0; }} .preheader {{color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0; }} .powered-by a {{text-decoration: none; }} hr {{border: 0; border-bottom: 1px solid #f6f6f6; Margin: 20px 0; }} /* ------------------------------------- RESPONSIVE AND MOBILE FRIENDLY STYLES ------------------------------------- */ @media only screen and (max-width: 620px) {{table[class=body] h1 {{font-size: 28px !important; margin-bottom: 10px !important; }} table[class=body] p, table[class=body] ul, table[class=body] ol, table[class=body] td, table[class=body] span, table[class=body] a {{font-size: 16px !important; }} table[class=body] .wrapper, table[class=body] .article {{padding: 10px !important; }} table[class=body] .content {{padding: 0 !important; }} table[class=body] .container {{padding: 0 !important; width: 100% !important; }} table[class=body] .main {{border-left-width: 0 !important; border-radius: 0 !important; border-right-width: 0 !important; }} table[class=body] .btn table {{width: 100% !important; }} table[class=body] .btn a {{width: 100% !important; }} table[class=body] .img-responsive {{height: auto !important; max-width: 100% !important; width: auto !important; }} }} /* ------------------------------------- PRESERVE THESE STYLES IN THE HEAD ------------------------------------- */ @media all {{.ExternalClass {{width: 100%; }} .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {{line-height: 100%; }} .apple-link a {{color: inherit !important; font-family: inherit !important; font-size: inherit !important; font-weight: inherit !important; line-height: inherit !important; text-decoration: none !important; }} .btn-primary table td:hover {{background-color: #34495e !important; }} .btn-primary a:hover {{background-color: #34495e !important; border-color: #34495e !important; }} }} </style> </head> <body class=""""> <table border=""0"" cellpadding=""0"" cellspacing=""0"" class=""body""> <tr> <td>&nbsp;</td> <td class=""container""> <div class=""content""> <!-- START CENTERED WHITE CONTAINER --><table class=""main""> <!-- START MAIN CONTENT AREA --> <tr> <td class=""wrapper""> <table border=""0"" cellpadding=""0"" cellspacing=""0""> <tr> <td> <p>Rule ""{RuleName}"" has been triggered.</p> <table border=""0"" cellpadding=""0"" cellspacing=""0""> <tbody> <tr> <td align=""left""> <table border=""0"" cellpadding=""0"" cellspacing=""0""> <tbody> <tr>{imageTag}</tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </table> </td> </tr> <!-- END MAIN CONTENT AREA --> </table> <!-- START FOOTER --> <div class=""footer""> <table border=""0"" cellpadding=""0"" cellspacing=""0""> <tr> <td class=""content-block""> </td> </tr> <tr> <td class=""content-block powered-by""> Powered by <a href=""http://redroid.net""> <img src=""http://redroid.net/assets/logo_slate_sky-f2f98512230037c1626e038a2d24dc57da7829ef7f498119d682a062bec3841e.png""height=""30px"" align=""middle""> </a> </td> </tr> </table> </div> <!-- END FOOTER --> <!-- END CENTERED WHITE CONTAINER --> </div> </td> <td>&nbsp;</td> </tr> </table> </body> </html>";

                Messaging.SendEmail(Actions.Email, content);
            }

            if (!string.IsNullOrWhiteSpace(Actions.Text))
            {
                Messaging.SendText(Actions.Text, title);
            }
        }


        /// <summary>
        /// To be called whenever a location event is received.
        /// </summary>
        /// <param name="locationEvent"></param>
        /// <returns></returns>
        public bool AddLocationEvent(LocationEvent locationEvent)
        {
            return false;
        }

    }
}
