using System;
using ConnectionServer.Utilities;
using Newtonsoft.Json;

namespace ConnectionServer.Models
{
    public class Trigger
    {
        // So that json.net can easily deserialize to it. 
        [JsonProperty("comparison")]
        private string _comparisonType;

        [JsonIgnore]
        public ComparisonType ComparisonType
        {
            get { return (ComparisonType) Enum.Parse(typeof(ComparisonType), _comparisonType, true); }
            set { _comparisonType = value.ToString(); }
        }

        // So that json.net can easily deserialize to it. 
        [JsonProperty("sensor_type")]
        private string _sensorType;

        [JsonIgnore]
        public TriggerType TriggerType
        {
            get { return (TriggerType)Enum.Parse(typeof(TriggerType), _sensorType, true); }
            set { _sensorType = value.ToString(); }
        }

        [JsonProperty("value")]
        public double Value { get; set; }

        [JsonProperty("ref_start_time")]
        private string RefStartTime
        {
            set { StartTime = new Time(value); }
        }

        [JsonProperty("ref_end_time")]
        private string RefEndTime
        {
            set { EndTime = new Time(value); }
        }

        [JsonIgnore]
        public Time StartTime { get; set; }

        [JsonIgnore]
        public Time EndTime { get; set; }

        [JsonIgnore]
        public Time End { get; set; }

        [JsonProperty("ref_lat")]
        public double Latitude { get; set; }

        [JsonProperty("ref_lng")]
        public double Longitude { get; set; }


        [JsonIgnore]
        private double? _lastDataPoint = null;

        [JsonIgnore]
        private bool _motionDetected = false;

        public void AddDataPoint(double dataPoint)
        {
            _lastDataPoint = dataPoint;
        }

        public void MotionDetected()
        {
            _motionDetected = true;
        }

        /// <summary>
        /// Returns whether this trigger is true.
        /// Starts as false until AddDataPoint, MotionDetected, or AddLocation is called.
        /// </summary>
        /// <returns></returns>
        public bool Evaluate()
        {
            switch (TriggerType)
            {
                case TriggerType.Type_Linear_Acceleration:
                case TriggerType.Type_Magnetic_Field:
                case TriggerType.Type_Proximity:
                case TriggerType.Type_Accelerometer:
                case TriggerType.Type_Light:
                    // these all depend on _lastDataPoint
                    // which will be null if we've never received a point for it.
                    if (_lastDataPoint == null)
                        return false;
                    switch (ComparisonType)
                    {
                        case ComparisonType.Above:
                            return _lastDataPoint >= Value;
                        case ComparisonType.Below:
                            return _lastDataPoint <= Value;
                        default:
                            throw new ArgumentException("These sensors can't be between.");
                    }

                case TriggerType.Time:
                    var now = new Time(DateTime.Now.ToString("HH:mm"));
                    return StartTime < now && now < EndTime;

                case TriggerType.Video:
                    return _motionDetected;

                default:
                    // The rest are unimplemented for now. Pretend they're always true.
                    return true;
            }
        }
       
    }
}