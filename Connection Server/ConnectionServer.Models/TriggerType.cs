﻿// ReSharper disable InconsistentNaming
namespace ConnectionServer.Models
{
    public enum TriggerType
    {
        Type_Accelerometer = 1,
        Type_Magnetic_Field = 2,
        Type_Orientation = 3,
        Type_Gyroscope = 4,
        Type_Light = 5,
        Type_Pressure = 6,
        Type_Temperature = 7,
        Type_Proximity = 8,
        Type_Gravity = 9,
        Type_Linear_Acceleration = 10,
        Type_Rotation_Vector = 11,
        Type_Relative_Humidity = 12,
        Type_Ambient_Temperature = 13,
        Type_Magnetic_Field_Uncalibrated = 14,
        Type_Game_Rotation_Vector = 15,
        Type_Gyroscope_Uncalibrated = 16,
        Type_Significant_Motion = 17,
        Type_Step_Detector = 18,
        Type_Step_Counter = 19,
        Type_Geomagnetic_Rotation_Vector = 20,
        Type_Heart_Rate = 21,
        Type_Tilt_Detector = 22,
        Type_Wake_Gesture = 23,
        Type_Glance_Gesture = 24,
        Type_Pick_Up_Gesture = 25,
        Type_Wrist_Tilt_Gesture = 26,
        Type_Device_Orientation = 27,
        Type_Pose_6dof = 28,
        Type_Stationary_Detect = 29,
        Type_Motion_Detect = 30,
        Type_Heart_Beat = 31,
        Type_Dynamic_Sensor_Meta = 32,
        Time = 100,
        Speed = 101,

        // When in a rule, means geofencing. 
        // When in RuleSensors, means anything that consumes data from location event
        // as we don't know what data we need from the event until we evaluate the rule.
        Location = 102, 

        // Motion Detection
        Video = 103
    }
}