﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using ConnectionServer.Models;


namespace ConnectionServer.Network
{
    public sealed class ConnectionManager
    {
        private static readonly ConnectionManager _instance = new ConnectionManager();

        private int _sessionId = 10;

        /// <summary>
        /// Gets the next session id to use
        /// </summary>
        public string NextSessionId => _sessionId++.ToString();

        public static ConnectionManager Instance => _instance;


        private readonly Dictionary<IPEndPoint, MasterConnection> Masters;

        private readonly Dictionary<IPEndPoint, PeripheralConnection> Peripherals;

        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static ConnectionManager() { }

        private ConnectionManager()
        {
            Masters = new Dictionary<IPEndPoint, MasterConnection>();
            Peripherals = new Dictionary<IPEndPoint, PeripheralConnection>();
            _sessionId = 0;
        }

        public void AddMaster(IPEndPoint endpoint, MasterConnection conn)
        {
            if (Masters.ContainsKey(endpoint))
            {
                throw new Exception($"Connection Manager already has an entry for {endpoint}");
            }
            Masters.Add(endpoint, conn);
        }

        public void AddPeripheral(IPEndPoint endpoint, PeripheralConnection conn)
        {
            if (Peripherals.ContainsKey(endpoint))
            {
                throw new Exception($"Connection Manager already has an entry for {endpoint}");
            }
            Peripherals.Add(endpoint, conn);
        }

        public IList<MasterConnection> GetMasterConnectionByPeripheralId(string id)
        {
            return Masters.Values.Where(m => m.PeripheralDeviceId == id).ToList();
        }

        public PeripheralConnection GetPeripheralConnection(IPEndPoint endpoint)
        {
            PeripheralConnection result;
            if (Peripherals.TryGetValue(endpoint, out result))
            {
                return result;
            }

            throw new ArgumentException($"No connection with the peripheral {endpoint}");
        }

        public MasterConnection GetMasterConnection(IPEndPoint endpoint)
        {
            MasterConnection result;
            if (Masters.TryGetValue(endpoint, out result))
            {
                return result;
            }
            
            throw new ArgumentException($"No connection with endpoint {endpoint}");
        }

        /// <summary>
        /// Removes the connection.
        /// </summary>
        public void Remove(Connection conn)
        {
            var endpoint = conn.ComSocketEndPoint;
            
            if(Masters.ContainsKey(endpoint))
                Masters.Remove(endpoint);
            else if (Peripherals.ContainsKey(endpoint))
                Peripherals.Remove(endpoint);
        }


        /// <summary>
        /// Returns the connection from either peripheral or master
        /// </summary>
        public Connection Get(IPEndPoint endpoint)
        {
            if (Masters.ContainsKey(endpoint))
                return Masters[endpoint];
            else if (Peripherals.ContainsKey(endpoint))
                return Peripherals[endpoint];

            return null;
            //throw new ArgumentException($"Endpoint not found. {endpoint}");
        }

        public bool Contains(IPEndPoint endpoint)
        {
            return Masters.ContainsKey(endpoint)
                   || Peripherals.ContainsKey(endpoint);
        }

        public MasterConnection GetConnectionBySessionId(string sessionId)
        {
            var res = Masters.Values.Where(m => m.SessionId == sessionId).ToList();


            if (res.Count() != 1)
            {
                throw new ArgumentException($"Should have found exactly one id. But instead found {res.Count()}");
            }
            
            return res.First();
        }

        public PeripheralConnection GetPeripheralById(string peripheralId)
        {
            return Peripherals.Values.FirstOrDefault(p => p.DeviceId == peripheralId);
        }

        public MasterConnection GetMasterBySessionId(string sessionId)
        {
            return Masters.Values.FirstOrDefault(m => m.SessionId == sessionId);
        }
    }
}
