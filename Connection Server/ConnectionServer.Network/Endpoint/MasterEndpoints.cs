﻿using System.Linq;
using System.Net;
using System.Threading;
using ConnectionServer.Models;
using ConnectionServer.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ConnectionServer.Network.Endpoint
{
    partial class Endpoints
    {
        //private static int _currentId;
        private static ConnectionManager connectionManager = ConnectionManager.Instance;


        #region Handshake


        /// <summary>
        /// 001 Master Handshake and Credential Exchange: Part 1 of 2: Indicate peripheral device id and get a session token
        /// </summary>
        public static void MasterHandshake(Message message, MasterConnection conn)
        {
            var rs = conn.ApiSocket;
            var requestJson = JObject.Parse(message.Body);
            string deviceId = (string)requestJson["device_id"];

            if (deviceId != null)
            {
                var sessionId = connectionManager.NextSessionId;

                conn.PeripheralDeviceId = deviceId;
                conn.SessionId = sessionId;
                conn.Type = ConnectionType.Master;

                connectionManager.AddMaster(conn.ComSocketEndPoint, conn);

                var response = new { session_id = sessionId };

                RedroidServer.Instance.SendJsonResponse(conn.ApiSocket, HttpStatusCode.OK, response);
            }
            else
            {
                var response = new
                {
                    msg = "Request does not contain a 'device_id'"
                };

                RedroidServer.Instance.SendJsonResponse(rs, HttpStatusCode.BadRequest, response);
            }
        }


        /// <summary>
        /// 002 Master Handshake and Credential Exchange: Part 2 of 2: Validate security token using the session id
        /// </summary>
        public static void MasterAuth(Message message, MasterConnection conn)
        {
            var requestJson = JObject.Parse(message.Body);
            string sessionId = (string)requestJson["session_id"];
            string securityToken = (string)requestJson["security_token"];

            if (sessionId != conn.SessionId)
            {
                var msg = new
                {
                    msg = "Invalid sessionId"
                };

                RedroidServer.Instance.SendJsonResponse(conn.ApiSocket, HttpStatusCode.BadRequest, msg);
            }


            RedroidServer.Instance.SendHttpResponse(conn.ApiSocket, HttpStatusCode.OK);
        }

        /// <summary>
        /// 003 [Optional*] "SendCommunication me event notifications at this port"
        /// </summary>
        public static void SetEventPort(Message message, MasterConnection conn)
        {
            // Do nothing. Just needed to avoid default case.
            RedroidServer.Instance.SendHttpResponse(conn.EventSocket, HttpStatusCode.OK);
        }

        #endregion


        /// <summary>
        /// 101 "Start an Mjpeg video stream and tell me the url"
        /// </summary>
        public static void StartStreaming(Message message, MasterConnection conn)
        {
            var peripheralConn = connectionManager.GetPeripheralById(conn.PeripheralDeviceId);
            if (peripheralConn != null)
            {
                // send request to the peripheral, register that we're waiting for a response
                peripheralConn.ApiSocket.MessageReceivedCallbacks.Enqueue((m, c) =>
                    RedroidServer.Instance.Send(conn.ApiSocket, m.FullText));
                RedroidServer.Instance.Send(peripheralConn.ApiSocket, message.FullText);
            }
        }

        ///// <summary>
        ///// 401 "Begin sending me sensor events for [sensor_type]"
        ///// </summary>
        //public static void RegisterSensorEvent(Message message, MasterConnection conn)
        //{
        //    var requestJson = JObject.Parse(message.Body);
        //    var requestId = (int)requestJson["request_id"];
        //    var sampingPeriod = (int)requestJson["sampling_period"];
        //    var onChangeOnly = (int) requestJson["on_change_only"];
            
        //    // extract sensor type from url.
        //    TriggerType type;
        //    if (!TriggerType.TryParse(message.Url.Split('/').Last(), true, out type))
        //    {
        //        RedroidServer.Instance.SendHttpResponse(conn.ApiSocket, HttpStatusCode.BadRequest);
        //        return;
        //    }
        //    var peripheralConn = connectionManager.GetPeripheralById(conn.PeripheralDeviceId);
        //    if (peripheralConn != null)
        //    {
        //        peripheralConn.Device.ActiveSensors.Add(type);

        //        SendToConnectedPeripheralForwardResponseToMaster(message, conn, peripheralConn);
        //    }
        //}

        /// <summary>
        /// Sends the request to the connected peripheral and registers a callback to send the eventual response back to the master.
        /// </summary>
        /// <param name="message">The message to send</param>
        /// <param name="masterConn">The master connection the message was received from.</param>
        /// <param name="peripheralConn">Optional. Peripheral connection this master is connected to, if already known.</param>
        public static void SendToConnectedPeripheralForwardResponseToMaster(Message message, MasterConnection masterConn, PeripheralConnection peripheralConn = null)
        {
            if(peripheralConn == null)
                peripheralConn = connectionManager.GetPeripheralById(masterConn.PeripheralDeviceId);

            if (peripheralConn != null)
            {
                // send request to the peripheral, register that we're waiting for a response
                peripheralConn.ApiSocket.MessageReceivedCallbacks.Enqueue((m, c) =>
                    RedroidServer.Instance.Send(masterConn.ApiSocket, m.FullText));

                RedroidServer.Instance.Send(peripheralConn.ApiSocket, message.FullText);
            }
        }

        /// <summary>
        /// Sends a request to the peripheral and returns the response to the connected master.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="conn"></param>
        public static void DiscreteRequest(Message message, MasterConnection conn)
        {
            SendToConnectedPeripheralForwardResponseToMaster(message, conn);
        }

        /// <summary>
        /// Responds to a master directly without forwarding the ping to the peripheral.
        /// </summary>
        public static void Ping(Message message, MasterConnection conn)
        {
            RedroidServer.Instance.SendHttpResponse(conn.ApiSocket, HttpStatusCode.OK);
        }

        /// <summary>
        /// Forwards a message to the connected peripheral over the same socket the message was received on.
        /// </summary>
        public static void DefaultMasterEndpoint(Message message, MasterConnection conn)
        {
            // Don't forward 200 OK responses to the peripheral
            if (message.MessageType == MessageType.Response)
                return;

            var peripheralConn = connectionManager.GetPeripheralById(conn.PeripheralDeviceId);

            Utilities.Utilities.Log(LogType.Forwarding, $"Forwarding message to {(peripheralConn == null ? 0 : 1)} Peripherals. {message.FullText}");

            if (peripheralConn?.ApiSocket != null)
            {
                if(message.SocketType == SocketType.Api)   
                    RedroidServer.Instance.Send(peripheralConn.ApiSocket, message.FullText);
                else if(message.SocketType == SocketType.Event)
                    RedroidServer.Instance.Send(peripheralConn.EventSocket, message.FullText);
            }
        }

        /// <summary>
        /// Creates a rule and starts its evaluation.
        /// </summary>
        public static void CreateRule(Message message, MasterConnection conn)
        {
            var rule = JsonConvert.DeserializeObject<Rule>(message.Body);
            var peripheralConn = connectionManager.GetPeripheralById(conn.PeripheralDeviceId);

            if (peripheralConn == null)
            {
                RedroidServer.Instance.SendHttpResponse(conn.ApiSocket, HttpStatusCode.BadRequest);
            }
            else
            {
                peripheralConn.Device.Rules.Add(rule);

                // Request motion detection from the peripheral if the rule has it
                if (rule.RequiredSensors().Contains(TriggerType.Video))
                {
                    // send the request to the peripheral
                    var msg = new
                    {
                        request_id = 100 // request ids don't matter to the server, nothing does
                    };
                    // Do nothing with the response. 
                    peripheralConn.ApiSocket.MessageReceivedCallbacks.Enqueue((m, c) => true);
                    

                    RedroidServer.Instance.SendJsonRequest(peripheralConn.ApiSocket, "PUT",
                        "/redroid/server/register/motion_detection", msg); 

                }
                RedroidServer.Instance.SendHttpResponse(conn.ApiSocket, HttpStatusCode.OK);
            }
        }


        public static void DeleteRule(Message message, MasterConnection conn)
        {
            var ruleId = message.UrlTrailingParam;
            var peripheralConn = connectionManager.GetPeripheralById(conn.PeripheralDeviceId);

            var ruleToDelete = peripheralConn.Device.Rules.FirstOrDefault(r => r.RuleId == ruleId);

            if (ruleToDelete != null)
            {
                peripheralConn.Device.Rules.Remove(ruleToDelete);
            }

            RedroidServer.Instance.SendHttpResponse(conn.ApiSocket, HttpStatusCode.OK);
        }
    }
}