﻿using System;
using System.Collections.Generic;
using System.Net;
using ConnectionServer.Models;
using ConnectionServer.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Configuration;

namespace ConnectionServer.Network.Endpoint
{

    partial class Endpoints
    {
        /// <summary>
        /// 004 "Peripheral Server Connection Handshake"
        /// </summary>
        public static void PeripheralConnectApi(Message message, PeripheralConnection conn, string streamingServerUrl)
        {
            var rs = conn.ApiSocket;
            var requestJson = JObject.Parse(message.Body);
            string deviceId = (string)requestJson["device_id"];
            string securityToken = (string)requestJson["security_token"];

            if (deviceId == null || securityToken == null)
            {
                var errorMsg = "Request does not contain a 'device_id' or 'security_token'";
                var errorJson = new
                {
                    msg = errorMsg
                };
                Utilities.Utilities.Log(LogType.Error, errorMsg);

                RedroidServer.Instance.SendJsonResponse(rs, HttpStatusCode.BadRequest, errorJson);

                return;
            }

            conn.DeviceId = deviceId;
            conn.SecurityToken = securityToken;

            Endpoints.connectionManager.AddPeripheral(conn.ComSocketEndPoint, conn);

            var response = new { rtmp_server_url =  streamingServerUrl};

            RedroidServer.Instance.SendJsonResponse(conn.ApiSocket, HttpStatusCode.OK, response);

        }

        public static void PeripheralConnectEvent(Message message, PeripheralConnection conn)
        {
            // Here so we can avoid the default case
            RedroidServer.Instance.SendHttpResponse(conn.EventSocket, HttpStatusCode.OK);
        }

        public static void SensorEventReceived(Message message, PeripheralConnection conn)
        {
            lock (conn)
            {

                // respond directly to the peripheral, forward onto master if necessary
                var sensorEvent = JsonConvert.DeserializeObject<SensorEvent>(message.Body);

                EventReceived(message, conn, sensorEvent.TriggerType);
                var toRemove = new List<Rule>();

                // Evaluate a rule, if there is one.
                foreach (var rule in conn.Device.Rules)
                {
                    if (rule.AddSensorEvent(sensorEvent))
                    {
                        toRemove.Add(rule);   
                    }
                }


                // Remove the ones we executed
                foreach (var rule in toRemove)
                {
                    conn.Device.Rules.Remove(rule);
                    if (rule.RequiredSensors().Contains(TriggerType.Video))
                    {
                        //If the rule required motion but is now going to be removed, deregister the event.
                        conn.ApiSocket.MessageReceivedCallbacks.Enqueue((m, c) => true);


                        RedroidServer.Instance.SendHttpRequest(conn.ApiSocket, "DELETE",
                            "/redroid/server/register/motion_detection");
                    }
                }
            }
        }


        public static void LocationEventReceived(Message message, PeripheralConnection conn)
        {
            lock (conn)
            {
                // respond directly to the peripheral, forward onto master if necessary
                var locationEvent = JsonConvert.DeserializeObject<LocationEvent>(message.Body);

                // trigger type must be location as we don't know what a connected rule will actually use
                EventReceived(message, conn, TriggerType.Location);

                var toRemove = new List<Rule>();

                // Evaluate a rule, if there is one.
                foreach (var rule in conn.Device.Rules)
                {
                    if (rule.AddLocationEvent(locationEvent))
                        toRemove.Add(rule);
                }

                // Remove the ones we executed
                foreach (var rule in toRemove)
                {
                    conn.Device.Rules.Remove(rule);
                }
            }
        }

        /// <summary>
        /// 503 "I have detected motion (Peripheral -> Server)"
        /// </summary>
        public static void MotionDetectionReceived(Message message, PeripheralConnection conn)
        {
            // respond first, to eliminate peripheral delay
            RedroidServer.Instance.SendHttpResponse(conn.EventSocket, HttpStatusCode.OK);
            var requestJson = JObject.Parse(message.Body);
            var imageUrl = (string)requestJson["image"];

            var toRemove = new List<Rule>();

            foreach (var rule in conn.Device.Rules)
            {
                if (rule.MotionDetected(imageUrl))
                    toRemove.Add(rule);
            }

            // Remove the rules that were true and executed
            foreach (var rule in toRemove)
            {
                conn.Device.Rules.Remove(rule);
            }

            // Respond back to peripheral
        }


        private static void EventReceived(Message message, PeripheralConnection conn, TriggerType sensorType)
        {
            var connectedMasters = Endpoints.connectionManager.GetMasterConnectionByPeripheralId(conn.DeviceId);
            if (connectedMasters.Count > 0)
            {
                // if master is connected, respond 200 ok to peripheral and send response to master.
                RedroidServer.Instance.SendHttpResponse(conn.EventSocket, HttpStatusCode.OK);
                foreach (var masterConnection in connectedMasters)
                {
                    RedroidServer.Instance.Send(masterConnection.EventSocket, message.FullText);
                }
            }
            else if (conn.Device.GetRuleSensors().Contains(sensorType))
            {
                // if no master, but sensor event is involved in rule, respond 200 ok to peripheral
                RedroidServer.Instance.SendHttpResponse(conn.EventSocket, HttpStatusCode.OK);
            }
            else
            {
                // no master, no rule, respond with 400 bad request to unregister event
                RedroidServer.Instance.SendHttpResponse(conn.EventSocket, HttpStatusCode.BadRequest);
                Utilities.Utilities.Log(LogType.Default, $"Sensor event {sensorType} unregistered from {conn}.");
            }
        }

        public static void ForwardRequest(Message message, PeripheralConnection conn)
        {
            DefaultPeripheralEndpoint(message, conn);
        }


        /// <summary>
        /// Forwards a message to all connected masters
        /// </summary>
        public static void DefaultPeripheralEndpoint(Message message, PeripheralConnection conn)
        {
            // Forward to all masters that are wanting data from the peripheral
            // TODO: Keep track of which masters are subscribed to which events. Notify peripheral appropriately
            var connectedMasters = Endpoints.connectionManager.GetMasterConnectionByPeripheralId(conn.DeviceId);

            Utilities.Utilities.Log(LogType.Forwarding, $"Forwarding message to {connectedMasters.Count} Masters. {message.FullText}");

            foreach (var master in connectedMasters)
            {
                if (message.SocketType == SocketType.Api)
                {
                    if (master.ApiSocket != null)
                        RedroidServer.Instance.Send(master.ApiSocket, message.FullText);
                    else
                        throw new Exception("Data came over peripheral api socket but connected master has no event socket.");
                }
                else if (message.SocketType == SocketType.Event)
                {
                    if (master.EventSocket != null)
                        RedroidServer.Instance.Send(master.EventSocket, message.FullText);
                    else
                        throw new Exception("Data came over the peripheral's event socket but master has no event socket.");
                }
            }
        }
    }
}
