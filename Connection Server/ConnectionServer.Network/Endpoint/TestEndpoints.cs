﻿using System.Linq;
using System.Net;
using ConnectionServer.Communication;
using ConnectionServer.Models;
using Newtonsoft.Json.Linq;
using ConnectionServer.Utilities;
using Newtonsoft.Json;
using Rule = ConnectionServer.Models.Rule;

namespace ConnectionServer.Network.Endpoint
{
    partial class Endpoints
    {
        public static void RouteTest(Message message, RedroidSocket rs)
        {
            if (message.Url.StartsWith("/test/test"))
                Test(message, rs);
            else if (message.Url.StartsWith("/test/text"))
                SendText(message, rs);
            else if (message.Url.StartsWith("/test/email"))
                SendEmail(message, rs);
            else if (message.Url.StartsWith("/test/notification"))
                SendNotification(message, rs);
            else if (message.Url.StartsWith("/test/rule"))
                TestRule(message, rs);
        }


        public static void Test(Message message, RedroidSocket rs)
        {
            RedroidServer.Instance.SendJsonResponse(rs, HttpStatusCode.OK, new { msg = "Test Successful." });
        }

        public static void SendText(Message message, RedroidSocket rs)
        {
            var requestJson = JObject.Parse(message.Body);

            var content = (string)requestJson["content"];
            var phoneNumber = (string)requestJson["phone_number"];

            if (content != null && phoneNumber != null)
            {
                Messaging.SendText(phoneNumber, content);

                RedroidServer.Instance.SendJsonResponse(rs, HttpStatusCode.OK, new { msg = "Message sent." });
                Utilities.Utilities.Log(LogType.NotificationSent, $"Text sent to {phoneNumber}: {content}");
            }
            else
            {
                RedroidServer.Instance.SendJsonResponse(rs, HttpStatusCode.BadRequest, new { msg = "Need content and phone number." });
            }
        }


        public static void SendEmail(Message message, RedroidSocket rs)
        {
            var requestJson = JObject.Parse(message.Body);

            var to = (string)requestJson["email_address"];
            var content = (string)requestJson["content"];

            if (content == null || to == null)
            {
                RedroidServer.Instance.SendJsonResponse(rs, HttpStatusCode.BadRequest, new { msg = "Need content and email_address." });
            }

            Messaging.SendEmail(to, content);
        }


        public static void SendNotification(Message message, RedroidSocket rs)
        {
            var rule = JsonConvert.DeserializeObject<Rule>(message.Body);

            
            //if (rule.RequiredSensors().Contains(TriggerType.Video))
            //    Messaging.SendPushNotification("Redroid", rule.title, rule.FirebaseToken, rule.ConfigId, rule.RuleId, rule.imageUrl);
            //else
            //    Messaging.SendPushNotification("Redroid", rule.title, rule.FirebaseToken, rule.ConfigId, rule.RuleId);

            RedroidServer.Instance.SendJsonResponse(rs, HttpStatusCode.OK, new {msg = "Notification  sent."});
        }

        public static void TestRule(Message message, RedroidSocket rs)
        {
            var rule = JsonConvert.DeserializeObject<Rule>(message.Body);

            RedroidServer.Instance.SendJsonResponse(rs, HttpStatusCode.OK, new {msg = "Rule received."});

        }
    }
}