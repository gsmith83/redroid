﻿using System;
using ConnectionServer.Models;
using ConnectionServer.Utilities;
using Newtonsoft.Json.Linq;
using Endpoints = ConnectionServer.Network.Endpoint.Endpoints;

namespace ConnectionServer.Network
{
    public class ListeningServer
    {
        private readonly int _port;
        private readonly string _streamingServerUrl;
        private readonly RedroidServer _server;

        private readonly ConnectionManager _connectionManager;

        public ListeningServer(int port, string streamingServerUrl)
        {
            this._port = port;
            this._streamingServerUrl = streamingServerUrl;
            _server = RedroidServer.Instance;
            _connectionManager = ConnectionManager.Instance;

        }


        public void Start()
        {
            _server.Start(_port, RouteRequest, SocketDisconnected);
        }


        public void Stop()
        {
            //_server.Stop();
        }


        /// <summary>
        /// Routes the given message and connection pair to the route
        /// </summary>
        /// <param name="message"></param>
        /// <param name="conn"></param>
        /// <returns></returns>
        public bool RouteRequest(Message message, RedroidSocket rs)
        {
            if (message.MessageType == MessageType.Request)
            {
                // For testing 
                if (message.Url.StartsWith("/test"))
                {
                    Endpoints.RouteTest(message, rs);
                    return true;
                }

                // Connection will be null if this socket is connected for the first time.
                if (rs.Connection == null)
                {
                    if (ConnectSocket(message, rs) == null)
                    {
                        return false;
                    }
                }

                Connection conn = rs.Connection;

                // We must know the socket type by this point, tell the message which socket it came over.
                message.SocketType = rs.Type;


                if (message.Url.StartsWith("/redroid/master", StringComparison.InvariantCultureIgnoreCase))
                {
                    var masterConn = conn as MasterConnection;
                    if (masterConn == null)
                        throw new ArgumentException("Must be a peripheral connection to hit a peripheral endpoint.");

                    RouteMaster(message, masterConn);
                }
                else if (message.Url.StartsWith("/redroid/peripheral", StringComparison.InvariantCultureIgnoreCase))
                {
                    var peripheralConn = conn as PeripheralConnection;
                    if (peripheralConn == null)
                        throw new ArgumentException("Must be a peripheral connection to hit a peripheral endpoint.");

                    RoutePeripheral(message, peripheralConn);
                }
                else
                {
                    throw new ArgumentException("Url must start with either /redroid/master or /redroid/peripheral");
                }
            }
            else if (message.MessageType == MessageType.Response)
            {
                var conn = rs.Connection;
                if (conn == null)
                    throw new ArgumentException("Redroid Socket must have a connection to receive a response.");

                if (conn.Type == ConnectionType.Master)
                {
                    Endpoints.DefaultMasterEndpoint(message, conn as MasterConnection);
                }
                else if (conn.Type == ConnectionType.Peripheral)
                {
                    Endpoints.DefaultPeripheralEndpoint(message, conn as PeripheralConnection);
                }
            }

            // have to return something here because it's used as a func
            return true;

        }

        /// <summary>
        /// Routes the given message to the appropriate function.
        /// Register new Master endpoints here.
        /// </summary>
        /// <param name="message">The message that was received</param>
        /// <param name="conn">The master connection the message was received over.</param>
        private void RouteMaster(Message message, MasterConnection conn)
        {
            if (message.Url.StartsWith("/redroid/master/connect/api"))
                Endpoints.MasterHandshake(message, conn);

            else if (message.Url.StartsWith("/redroid/master/auth"))
                Endpoints.MasterAuth(message, conn);

            else if (message.Url.StartsWith("/redroid/master/connect/event"))
                Endpoints.SetEventPort(message, conn);

            else if (message.Url.StartsWith("/redroid/master/ping"))
                Endpoints.Ping(message, conn);

            //else if (message.Url.StartsWith("/redroid/master/register/sensor_events/"))
            //    Endpoints.RegisterSensorEvent(message, conn);

            else if (message.Url.StartsWith("/redroid/master/rule") 
                && string.Equals(message.HttpMethod, "PUT", StringComparison.InvariantCultureIgnoreCase))
                Endpoints.CreateRule(message, conn);

            else if (message.Url.StartsWith("/redroid/master/rule") 
                && string.Equals(message.HttpMethod, "DELETE", StringComparison.InvariantCultureIgnoreCase))
                Endpoints.DeleteRule(message, conn);

            else if (message.Url.StartsWith("/redroid/master/rtmp")
                || message.Url.StartsWith("/redroid/master/request")
                || message.Url.StartsWith("/redroid/master/location_events")
                || message.Url.StartsWith("/redroid/master/streaming")
                || message.Url.StartsWith("/redroid/master/register/sensor_events/"))
                Endpoints.DiscreteRequest(message, conn);

            else
                Endpoints.DefaultMasterEndpoint(message, conn);
        }

        /// <summary>
        /// Routes the given message to the appropriate function.
        /// Register new Peripheral endpoints here.
        /// </summary>
        /// <param name="message">The message that was received</param>
        /// <param name="conn">The peripheral connection the message was received over.</param>
        private void RoutePeripheral(Message message, PeripheralConnection conn)
        {
            if (message.Url.StartsWith("/redroid/peripheral/connect/api"))
                Endpoints.PeripheralConnectApi(message, conn, _streamingServerUrl);
            else if (message.Url.StartsWith("/redroid/peripheral/connect/event"))
                Endpoints.PeripheralConnectEvent(message, conn);
            else if (message.Url.StartsWith("/redroid/peripheral/sensor_event/"))
                Endpoints.SensorEventReceived(message, conn);
            else if (message.Url.StartsWith("/redroid/peripheral/location_event"))
                Endpoints.LocationEventReceived(message, conn);
            else if (message.Url.StartsWith("/redroid/peripheral/motion_detection"))
                Endpoints.MotionDetectionReceived(message, conn);
            else
                Endpoints.DefaultPeripheralEndpoint(message, conn);
        }

        /// <summary>
        /// Creates or retrieves the connection this redroid socket belongs to. 
        /// Sets redroid socket's connection if it has none.
        /// </summary>
        /// <param name="message">The message received. The url must be one of the connect endpoints.</param>
        /// <param name="rs">The RedroidSocket the message was received over.</param>
        /// <returns>The connection this redroid socket is connected to.</returns>
        private Connection ConnectSocket(Message message, RedroidSocket rs)
        {
            Connection conn = null;

            if (message.Url.StartsWith("/redroid/master/connect/", StringComparison.InvariantCultureIgnoreCase))
            {
                if (message.Url.Contains("api"))
                {
                    rs.Type = SocketType.Api;

                    conn = new MasterConnection()
                    {
                        ApiSocket = rs,
                        Type = ConnectionType.Master
                    };

                }
                else if (message.Url.Contains("event"))
                {
                    var requestJson = JObject.Parse(message.Body);
                    var deviceId = (string)requestJson["session_id"];

                    conn = ConnectionManager.Instance.GetMasterBySessionId(deviceId);

                    rs.Type = SocketType.Event;
                    conn.EventSocket = rs;
                }
                else
                    throw new ArgumentException("Endpoint does not have either api or event.");
            }
            else if (message.Url.StartsWith("/redroid/peripheral/connect/", StringComparison.InvariantCultureIgnoreCase))
            {
                if (message.Url.Contains("api"))
                {
                    rs.Type = SocketType.Api;
                    conn = new PeripheralConnection()
                    {
                        ApiSocket = rs,
                        Type = ConnectionType.Peripheral,
                        Device = new Peripheral()
                    };
                }
                else if (message.Url.Contains("event"))
                {
                    var requestJson = JObject.Parse(message.Body);
                    var deviceId = (string)requestJson["device_id"];

                    conn = ConnectionManager.Instance.GetPeripheralById(deviceId);

                    rs.Type = SocketType.Event;
                    conn.EventSocket = rs;
                }
                else
                    throw new ArgumentException("Endpoint does not have either api or event.");
            }
            else
                return null;
            //throw new ArgumentException("Method only valid if socket is connecting for the first time.");

            rs.Connection = conn;
            Console.WriteLine($"New {conn.Type} {rs.Type} from {rs}.");
            return conn;
        }


        /// <summary>
        /// To be called when a client's socket has been disconnected.
        /// Removes socket from connection and connection manager, if necessary.
        /// </summary>
        public bool SocketDisconnected(RedroidSocket rs)
        {
            // remove the socket from the connection manager
            var conn = rs.Connection;

            if (conn == null)
                return true;

            if (rs.Type == SocketType.Event)
            {
                conn.EventSocket = null;
            }
            else
            {
                _connectionManager.Remove(conn);
            }

            Console.WriteLine($"{conn.Type} {rs.Type} Socket disconnected from {rs}");
            return true;
        }
    }
}
