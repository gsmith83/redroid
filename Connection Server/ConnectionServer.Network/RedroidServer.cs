﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using ConnectionServer.Models;
using ConnectionServer.Utilities;
using Newtonsoft.Json;

namespace ConnectionServer.Network
{
    public sealed class RedroidServer
    {
        private static readonly RedroidServer _instance = new RedroidServer();
        public static RedroidServer Instance => _instance;

        private List<RedroidSocket> _sockets;
        private System.Net.Sockets.Socket _serverSocket;
        private event Func<Message, RedroidSocket, bool> MessageReceived;
        private event Func<RedroidSocket, bool> SocketDisconnected;
        private int _port;
        private int _backlog;
        private int _bufferSize;


        public bool Start(int port, Func<Message, RedroidSocket, bool> messageReceivedCallback, Func<RedroidSocket, bool> socketDisconnected)
        {
            // Initialize fields
            this._port = port;
            this.MessageReceived = messageReceivedCallback;
            this.SocketDisconnected = socketDisconnected;
            this._backlog = 100;
            this._bufferSize = 4096;
            this._sockets = new List<RedroidSocket>();


            IPAddress[] ipv4Addresses = Array.FindAll(Dns.GetHostEntry(string.Empty).AddressList,
                a => a.AddressFamily == AddressFamily.InterNetwork);// && !a.ToString().Contains("10."));

            // Returns ipv6 addresses as well
            //System.Net.IPHostEntry localhost = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
            System.Net.IPEndPoint serverEndPoint;
            try
            {
                serverEndPoint = new System.Net.IPEndPoint(ipv4Addresses[0], _port);
            }
            catch (System.ArgumentOutOfRangeException e)
            {
                throw new ArgumentOutOfRangeException("Port number entered would seem to be invalid, should be between 1024 and 65000", e);
            }
            try
            {
                _serverSocket = new System.Net.Sockets.Socket(serverEndPoint.Address.AddressFamily, System.Net.Sockets.SocketType.Stream, ProtocolType.Tcp);
            }
            catch (System.Net.Sockets.SocketException e)
            {
                throw new ApplicationException("Could not create CommunicationSocket, check to make sure not duplicating port", e);
            }
            Console.WriteLine("Server started on {0}:{1}", serverEndPoint.Address, serverEndPoint.Port);
            try
            {
                _serverSocket.Bind(serverEndPoint);
                _serverSocket.Listen(_backlog);
            }
            catch (Exception e)
            {
                throw new ApplicationException("Error occured while binding CommunicationSocket, check inner exception", e);
            }
            try
            {
                //warning, only call this once, this is a bug in .net 2.0 that breaks if 
                // you're running multiple asynch accepts, this bug may be fixed, but
                // it was a major pain in the ass previously, so make sure there is only one
                //BeginAccept running
                _serverSocket.BeginAccept(new AsyncCallback(acceptCallback), _serverSocket);
            }
            catch (Exception e)
            {
                throw new ApplicationException("Error occured starting listeners, check inner exception", e);
            }
            return true;
        }

        private void acceptCallback(IAsyncResult result)
        {
            var conn = new RedroidSocket();
            try
            {
                //Finish accepting the connection
                System.Net.Sockets.Socket s = (System.Net.Sockets.Socket)result.AsyncState;
                conn.Socket = s.EndAccept(result);
                conn.Buffer = new byte[_bufferSize];

                lock (_sockets)
                {
                    _sockets.Add(conn);
                }
                //Queue recieving of data from the connection
                conn.Socket.BeginReceive(conn.Buffer, 0, conn.Buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), conn);
                //Queue the accept of the next incomming connection
                _serverSocket.BeginAccept(new AsyncCallback(acceptCallback), _serverSocket);
            }
            catch (SocketException)
            {
                if (conn.Socket != null)
                {
                    lock (_sockets)
                    {
                        _sockets.Remove(conn);
                        SocketDisconnected?.Invoke(conn);
                    }
                    conn.Socket.Close();
                }
                //Queue the next accept, think this should be here, stop attacks based on killing the waiting listeners
                _serverSocket.BeginAccept(new AsyncCallback(acceptCallback), _serverSocket);
            }
            catch (Exception)
            {
                if (conn.Socket != null)
                {
                    lock (_sockets)
                    {
                        _sockets.Remove(conn);
                        SocketDisconnected?.Invoke(conn);
                    }
                    conn.Socket.Close();
                }
                //Queue the next accept, think this should be here, stop attacks based on killing the waiting listeners
                _serverSocket.BeginAccept(new AsyncCallback(acceptCallback), _serverSocket);
            }
        }




        private void ReceiveCallback(IAsyncResult result)
        {
            //get our connection from the callback
            var conn = (RedroidSocket)result.AsyncState;
            //catch any errors, we'd better not have any
            try
            {
                //todo: place a try catch around endreceive
                //Grab our communicationBuffer and count the number of bytes receives
                int bytesRead = conn.Socket.EndReceive(result);
                //make sure we've read something, if we haven't it supposedly means that the client disconnected
                if (bytesRead > 0)
                {

                    TempMessage(conn, bytesRead);

                    try
                    {
                        //Queue the next receive
                        conn.Socket.BeginReceive(conn.Buffer, 0, conn.Buffer.Length, SocketFlags.None,
                            new AsyncCallback(ReceiveCallback), conn);
                    }
                    catch
                    {
                        SocketDisconnected?.Invoke(conn);
                    }
                }
                else
                {
                    //Callback run but no data, close the connection
                    //supposedly means a disconnect
                    //and we still have to close the CommunicationSocket, even though we throw the event later
                    lock (_sockets)
                    {
                        _sockets.Remove(conn);
                        SocketDisconnected?.Invoke(conn);
                    }
                    conn.Socket.Close();
                }
            }
            catch (SocketException e)
            {
                //Something went terribly wrong
                //which shouldn't have happened
                Utilities.Utilities.Log(LogType.Error, $"Reading from a CommunicationSocket failed {e}");
                if (conn.Socket != null)
                {
                    lock (_sockets)
                    {
                        _sockets.Remove(conn);
                        SocketDisconnected?.Invoke(conn);
                    }
                    conn.Socket.Close();
                }
            }
        }

        #region Sending



        public bool SendJsonResponse(RedroidSocket conn, HttpStatusCode statusCode, object obj)
        {
            var json = JsonConvert.SerializeObject(obj, Formatting.Indented);
            return SendHttpResponse(conn, statusCode, json);
        }

        public bool SendJsonRequest(RedroidSocket conn, string method, string url, object payload)
        {
            var json = JsonConvert.SerializeObject(payload, Formatting.Indented);
            return SendHttpRequest(conn, method, url, json);
        }

        public bool SendHttpResponse(RedroidSocket conn, HttpStatusCode statusCode, string payload = "")
        {
            var response = $"HTTP/1.1 {(int)statusCode} {GetStatusDescription(statusCode)}\r\n"
                           + $"Date: {DateTime.Now:R}\r\n"
                           + $"Content-Type: application/json\r\n"
                           + $"Content-Length: {payload.Length}\r\n\r\n";

            if (!string.IsNullOrWhiteSpace(payload))
                response += $"{payload}";

            return Send(conn, response);
        }

        public bool SendHttpRequest(RedroidSocket conn, string method, string url, string payload = "")
        {
            var endpoint = (IPEndPoint)conn.Socket.RemoteEndPoint;

            String request = $"{method} {url} HTTP/1.1\r\n" +
                                "Content-Type: application/json\r\n" +
                                "User-Agent: Redroid\r\n" +
                                $"Host: {endpoint.Address}:{endpoint.Port}\r\n" +
                                "Connection: Keep-Alive\r\n" +
                                $"Content-Length: {payload.Length}\r\n\r\n" +
                                $"{payload}";

            return Send(conn, request);
        }

        private string GetStatusDescription(HttpStatusCode statusCode)
        {
            switch (statusCode)
            {
                case HttpStatusCode.OK:
                    return "OK";
                case HttpStatusCode.BadRequest:
                    return "Bad Request";
                default:
                    return "OK";

            }
        }

        public bool Send(RedroidSocket conn, string message)
        {
            Utilities.Utilities.Log(LogType.MessageSent, $"Sending to {conn}:\n{message}");

            return Send(Encoding.ASCII.GetBytes(message), conn);
        }

        /// <summary>
        /// Sends a byte array message over the given connection's Api Socket
        /// </summary>
        /// <param name="message">The message to send</param>
        /// <param name="conn">The connection to send the message over</param>
        /// <returns>Whether the message was successfully sent</returns>
        public bool Send(byte[] message, RedroidSocket conn)
        {

            if (conn != null && conn.Socket.Connected)
            {
                lock (conn)
                {
                    try
                    {
                        //we use a blocking mode send, no async on the outgoing
                        //since this is primarily a multithreaded application, shouldn't cause problems to send in blocking mode
                        conn.Socket.Send(message, message.Length, SocketFlags.None);
                    }
                    catch
                    {
                        SocketDisconnected?.Invoke(conn);
                        return false;
                    }
                }
            }
            else
                return false;
            return true;
        }


        #endregion


        #region Message Handling


        /// <summary>
        /// If an entire message has been received, parses it and hands it off to the router.
        /// Does nothing if a message has not been received in its entirety.
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="bytesRead"></param>
        private void TempMessage(RedroidSocket conn, int bytesRead)
        {

            var newText = Encoding.ASCII.GetString(conn.Buffer, 0, bytesRead);
            conn.CurrentMessage.Append(newText);
            int messagesReceived = 0;

            while (conn.CurrentMessage.Length > 10)
            {
                if(++messagesReceived != 1)
                    Console.WriteLine();

                var reader = new StringReader(conn.CurrentMessage.ToString());
                conn.CurrentMessage.Clear();


                var currentMessage = new StringBuilder();
                int contentLength = 0;
                var line = reader.ReadLine();

                while (!string.IsNullOrEmpty(line))
                {
                    currentMessage.Append(line);
                    currentMessage.Append("\r\n");
                    if (line.StartsWith("Content-Length: ", StringComparison.InvariantCultureIgnoreCase))
                    {
                        contentLength = int.Parse(line.Substring("Content-Length: ".Length));
                    }

                    line = reader.ReadLine();
                }
                currentMessage.Append("\r\n");

                // done with headers, and have contentLength
                var buffer = new char[contentLength];
                var payloadBytesRead = reader.Read(buffer, 0, contentLength);

                if (payloadBytesRead != contentLength)
                {
                    conn.CurrentMessage.Append(currentMessage.ToString());
                    conn.CurrentMessage.Append(new string(buffer));
                    break;
                }
                // have entire payload, append to message
                currentMessage.Append(new string(buffer));

                HandleMessage(conn, currentMessage.ToString());

                conn.CurrentMessage.Append(reader.ReadToEnd());
            }
        }

        private void HandleMessage(RedroidSocket conn, string httpMessage)
        {
            var message = ParseMessage(httpMessage);
            message.SocketType = conn.Type; // may be default value, if it's the first time we've seen the socket

            conn.CurrentMessage.Clear();

            Utilities.Utilities.Log(LogType.MessageReceived, $"Message request received from {conn}:\n{message}\n");


            if (conn.MessageReceivedCallbacks.Count > 0)
            {
                if (conn.Connection == null)
                    throw new InvalidOperationException("Redroid Socket has a callback but does not have a connection");

                conn.MessageReceivedCallbacks.Dequeue().Invoke(message, conn.Connection);
            }
            else
            {
                MessageReceived?.Invoke(message, conn);
            }
        }


        private Message ParseMessage(string message)
        {
            if (message.StartsWith("HTTP"))
                return ParseHttpResponse(message); //Response to a query we made
            else
                return ParseHttpRequest(message); // Request to this server
        }

        private Message ParseHttpResponse(string response)
        {
            var headerAndBody = response.Split(new string[] { "\r\n\r\n" }, 2, StringSplitOptions.RemoveEmptyEntries);
            var headerLines = headerAndBody[0].Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            var statusLine = headerLines[0].Split(' ');

            HttpStatusCode statusCode;
            HttpStatusCode.TryParse(statusLine[1], out statusCode);

            var body = headerAndBody.Length > 1 ? headerAndBody[1] : "";

            return new Message()
            {
                Body = body,
                StatusCode = statusCode,
                FullText = response,
                MessageType = MessageType.Response
            };
        }

        /// <summary>
        /// Parses a request and returns the message it encodes
        /// </summary>
        private Message ParseHttpRequest(string request)
        {
            //if (request.StartsWith(" /"))
            //    request = "PUT" + request;

            var headerAndBody = request.Split(new string[] { "\r\n\r\n" }, 2, StringSplitOptions.RemoveEmptyEntries);

            var headerLines = headerAndBody[0].Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            if (headerLines.Length < 1)
                throw new ArgumentException($"Request does not contain an http request. {request}.");

            var firstLine = headerLines[0].Split(' ');

            if (firstLine.Length != 3)
                throw new ArgumentException(
                    $"Request does not contain a valid http method, url, or version. {firstLine}");

            var body = headerAndBody.Length > 1 ? headerAndBody[1] : "";

            return new Message()
            {
                HttpMethod = firstLine[0].ToUpper(),
                Url = firstLine[1],
                Body = body.ToString(),
                FullText = request,
                MessageType = MessageType.Request
            };
        }

        #endregion

    }
}
