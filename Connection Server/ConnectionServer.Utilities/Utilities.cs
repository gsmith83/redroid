﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace ConnectionServer.Utilities
{
    public enum LogType
    {
        Default,
        MessageReceived,
        MessageSent,
        Forwarding,
        NotificationSent,
        Error
    }
    public static class Utilities
    {

        private static readonly object Spinlock = new object();

        public static void Log(LogType type, string text)
        {
            switch (type)
            {
                case LogType.MessageReceived:
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    break;
                case LogType.MessageSent:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;

                case LogType.Forwarding:
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    break;

                case LogType.NotificationSent:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    break;

                case LogType.Error:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;

                case LogType.Default:
                    break;

            }

            Console.WriteLine(text);
            Console.ResetColor();
        }

        //public static class NonBlockingConsole
        //{
        //    private static BlockingCollection<string> m_Queue = new BlockingCollection<string>();

        //    static NonBlockingConsole()
        //    {
        //        var thread = new Thread(
        //          () =>
        //          {
        //              while (true) Console.WriteLine(m_Queue.Take());
        //          });
        //        thread.IsBackground = true;
        //        thread.Start();
        //    }

        //    public static void WriteLine(string value)
        //    {
        //        m_Queue.Add(value);
        //    }
        //}
    }
}