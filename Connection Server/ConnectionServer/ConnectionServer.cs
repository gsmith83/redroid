﻿using System;
using ConnectionServer.Network;
using System.Configuration;

namespace ConnectionServer
{
    class ConnectionServer
    {
       

        static void Main(string[] args)
        {
            int port;
            var streamingServerUrl = ConfigurationManager.AppSettings["StreamingServerUrl"];
            if(!int.TryParse(ConfigurationManager.AppSettings["Port"], out port) || streamingServerUrl == null)
            {
                Console.WriteLine("Configuration File incorrect. Connection Server requires a Port and StreamingServerUrl.");
                Console.ReadLine();
                return;
            }

            while (true)
            {
                try
                {
                    Console.WriteLine($"Starting connection server.");

                    var listeningServer = new ListeningServer(port, streamingServerUrl);
                    listeningServer.Start();

                    Console.ReadLine();
                    listeningServer.Stop();
                    // If we get here, the user actually pressed the button to stop the server
                    break;

                }
                catch (Exception e)
                {
                    Console.WriteLine($"Connection Server crashed.\n{e}");
                }
            }
            Console.WriteLine("Connection server stopped.");
            Console.WriteLine();
        }
    }
}
