using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using ConnectionServer.Network;
using Microsoft.Azure;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace ConnectionServerWorker
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        public override void Run()
        {
            Trace.TraceInformation("ConnectionServerWorker is running");

            int port;
            var streamingServerUrl = CloudConfigurationManager.GetSetting("StreamingServerUrl");
            if (!int.TryParse(CloudConfigurationManager.GetSetting("Port"), out port) || streamingServerUrl == null)
            {
                Console.WriteLine("Configuration File incorrect. Connection Server requires a Port and StreamingServerUrl.");
                Console.ReadLine();
                return;
            }
            try
            {
                while (true)
                {
                    try
                    {
                        Console.WriteLine($"Starting connection server.");

                        var listeningServer = new ListeningServer(port, streamingServerUrl);
                        listeningServer.Start();

                        Thread.Sleep(Timeout.Infinite);

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Connection Server crashed.\n{e}");
                    }
                }
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 1;

            // For information on handling configuration changes
            // see the MSDN topic at https://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation("ConnectionServerWorker has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("ConnectionServerWorker is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("ConnectionServerWorker has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("Working");
                await Task.Delay(1000);
            }
        }
    }
}
