# Redroid Connection Server

This is the connection server that serves as a multiplexing proxy and Rules Engine for the Redroid Master and Peripheral Android applications.

It can be deployed locally on a windows machine or run on an Azure Worker Role depending on the intended startup project.

For certain features to work, two app.config folders will need to have relevant keys set.

> **ConnectionServer\ConnectionServer.Communication\app.config**
> ```xml
>  </configuration>
> 	...
>  	<appSettings>
>     	<add key="TwilioAccountSid" value=""/>
>     	<add key="TwilioAuthToken" value=""/>
>     	<add key="TwilioPhoneNumber" value=""/>
>     	<add key="FromEmailAddress" value=""/>
>     	<add key="FromEmailPassword" value=""/>
>     	<add key="EmailSubject" value=""/>
>     	<add key="FirebaseUrl" value=""/>
>     	<add key="FirebaseSenderId" value=""/>
>     	<add key="FirebaseServerKey" value=""/>
> 	</appSettings>
> </configuration>
> ```



> **ConnectionServer\ConnectionServer\App.config**  *when running locally*
>
> ​		or
>
> **ConnectionServer\ConnectionServerWorker\app.config** *when running in Azure*
>
> ```xml
> </configuration>
> 	...
> 	<appSettings>
>     	<add key="port" value=""/>
>     	<add key="streamingServerUrl" value=""/>
> 	</appSettings>
> </configuration>
> ```



To run the Redroid Connection Server, open the `ConnectionServer.sln` in Visual Studio and press the `start`. The necessary NuGet packages should be automatically downloaded though it might be necessary to right click on the solution in Visual Studio and select `Restore NuGet Packages`. It will open a console and print the IP Address and Port the Connection Server is running on. It needs access through the windows firewall  through the port you've selected and may require that this command be run in an elevated command prompt.

`netsh http add urlacl url=http://+:{desired port}/ user=EVERYONE`