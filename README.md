# Redroid

## Overview
Redroid is a software platform that puts retired Android devices back to work performing useful tasks for other devices. 

This repository contains the source code for the two Redroid applications: Redroid Peripheral, and Redroid Master (branded as "Redroid"), as well as the Connection Server and the Web Portal.

**Redroid Peripheral** makes available to other devices many of the hardware features of an Android device via network.

**Redroid Master** is a full featured Android phone designed to make use of the features of a device running Redroid Peripheral.

## Installation

1) Clone [this repo](https://teamredroid.visualstudio.com/Redroid/_git/RedroidApps)

2) Create an Android Studio project at the root of the cloned repo

3) Sync the Gradle Files and run Build on the project

4) Run the module Redroid Peripheral on an Android device running Android 4.1 or higher.

5) Run the module Redroid Master on an Android device running Android 


## Library Modules
The project contains one Redroid library module, called *RedroidCoreLibrary.* 

In addition, there a handful of third-party modules in the project. These are modified open-source projects not meant to be installed directly, and contain no launcher activities, but serve merely as library packages for various Redroid functions. They are:

* [*AndroidMotionDetection*:](https://github.com/appsthatmatter/android-motion-detection) A small video motion-detection library employed by Redroid Rules.

* [*Peepers*:](https://github.com/foxdog-studios/peepers) An Mjpeg streamer used by Redroid Peripheral to broadcast the bike cam feed.

* [*MjpegView*:](https://github.com/michogar/MjpegView) A simple view for used by Redroid Master to display the bike cam's Mjpeg feed.

* [*Yasea*:](https://github.com/begeekmyfriend/yasea) An RTMP streamer library used by Redroid Peripheral to broadcast Redroid video.

* [*Vitamio*:](https://github.com/yixia/VitamioBundle) An RTMP stream viewer used by Redroid Master to view Redroid video.

# Redroid Connection Server

This is the connection server that serves as a multiplexing proxy and Rules Engine for the Redroid Master and Peripheral Android applications.

It can be deployed locally on a windows machine or run on an Azure Worker Role depending on the intended startup project.

For certain features to work, two app.config folders will need to have relevant keys set.

> **ConnectionServer\ConnectionServer.Communication\app.config**
> ```xml
>  </configuration>
> 	...
>  	<appSettings>
>     	<add key="TwilioAccountSid" value=""/>
>     	<add key="TwilioAuthToken" value=""/>
>     	<add key="TwilioPhoneNumber" value=""/>
>     	<add key="FromEmailAddress" value=""/>
>     	<add key="FromEmailPassword" value=""/>
>     	<add key="EmailSubject" value=""/>
>     	<add key="FirebaseUrl" value=""/>
>     	<add key="FirebaseSenderId" value=""/>
>     	<add key="FirebaseServerKey" value=""/>
> 	</appSettings>
> </configuration>
> ```



> **ConnectionServer\ConnectionServer\App.config**  *when running locally*
>
> ​		or
>
> **ConnectionServer\ConnectionServerWorker\app.config** *when running in Azure*
>
> ```xml
> </configuration>
> 	...
> 	<appSettings>
>     	<add key="port" value=""/>
>     	<add key="streamingServerUrl" value=""/>
> 	</appSettings>
> </configuration>
> ```



To run the Redroid Connection Server, open the `ConnectionServer.sln` in Visual Studio and press the `start`. The necessary NuGet packages should be automatically downloaded though it might be necessary to right click on the solution in Visual Studio and select `Restore NuGet Packages`. It will open a console and print the IP Address and Port the Connection Server is running on. It needs access through the windows firewall  through the port you've selected and may require that this command be run in an elevated command prompt.

`netsh http add urlacl url=http://+:{desired port}/ user=EVERYONE`

# Redroid Video Streaming Server

The Redroid Video Streaming Server handles all video stream publishing and stored video management for the Redroid project.

### Technologies

  - Linux
  - Ruby on Rails
  - [NGINX + RTMP module](https://github.com/arut/nginx-rtmp-module)

### Thoughts on Security
All you need is the Peripheral Device ID to get to all your stored videos on the website. There is no login and password. This streaming server was originally intended to be run at your home for you to create your own personal security camera system using old Android phones via Redroid. With that said, we strongly suggest that you run this at your home on a Raspberry pi or something similar, and NOT open it up to the big scary internet.
  
### Installation Instructions
----
##### Note: There are plans in the future to make deploying this MUCH EASIER than the manual steps listed here. Probably with Docker. We may also provide a ready to go raspberry pi OS image that you can simply write to an sd card and you'll be ready to go. Until then, however, this is how you deploy the Redroid Video Streaming Server.
-----

#### You will need:
1. A linux box (this guide uses Ubuntu Server 16.04)
    - Can run on Raspberry pi! It runs surprisingly well. 
2. Some shell experience and router configuration knowledge
3. A lot of patience

### Step 1 -- Initial Setup
 Open terminal.
 Create new user redroid and set the password to whatever you want.
 ```sh 
 $ sudo useradd redroid
 $ sudo passwd redroid
 ```
 We need to add some folders in redroid home directory and set permissions to completely open.
 
 ```sh
 $ su redroid
 $ mkdir ~/motion_detection_images ~/video_saves && chmod 777 ~/motion_detection_images ~/video_saves
 $ mkdir ~/video_saves/vod_links && chmod 777 ~/video_saves/vod_links
 ```
 
 ### Step 2 --  NGINX + RTMP module
To complete this step, first follow the instructions outlined [here](https://www.vultr.com/docs/setup-nginx-rtmp-on-ubuntu-14-04).

We made changes to the nginx config file ``` /usr/local/nginx/conf/nginx.conf ``` in the tutorial. We need to change some things to make this config file work for Redroid. Replace the file with the following:

```sh
#user  nobody;
worker_processes  1;
#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;
#pid        logs/nginx.pid;

events {
    worker_connections  1024;
}

http {
        include       mime.types;
        default_type  application/octet-stream;
        sendfile        on;
        #keepalive_timeout  0;
        keepalive_timeout  65;

        upstream upstream_server {
            server localhost:3000;
        }
        
        server {
            listen       80;
            server_name  my_site.local;

            location / {
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $http_host;
                proxy_redirect off;
                proxy_pass http://upstream_server;
                break;
            }
        }
    }

rtmp {
    server {
            listen 1935;
            chunk_size 4096;

            application live_no_record {
                    live on;
                    record off;
                    exec ffmpeg -i rtmp://localhost/live/$name -threads 1 -c:v libx264 -profile:v baseline -b:v 350K -s 640x360 -f flv -c:a aac -ac 1 -strict -2 -b:a 56k rtmp://localhost/live360p/$name;
            }
            application live {
                    live on;
                    record all;
                    record_path /home/redroid/video_saves/;
                    record_suffix -%d-%b-%y-%T.flv;
                    drop_idle_publisher 5s;
                    exec ffmpeg -i rtmp://localhost/live/$name -threads 1 -c:v libx264 -profile:v baseline -b:v 350K -s 640x360 -f flv -c:a aac -ac 1 -strict -2 -b:a 56k rtmp://localhost/live360p/$name;
            }
            application live360p {
                    live on;
                    record off;
            }
            application vod {
                    play /home/redroid/video_saves/vod_links/;
            }
    }
}
```
Note: This file can be found in this project under the ```redroid-install``` folder.

This config file sets up nginx to do a number of things..
    - Automatically record all video streams to ```~/video_saves```
    - Reroute traffic recieved on port 80 to a rails app running internally on port 3000
    - Serve stored videos located in the ```vod_links``` folder.

Once that file is updated, restart nginx.
```sh
$ sudo service nginx restart
```
### Step 3 -- Video Indexer
#### What is it?
The video indexer is a python script whose job is to monitor the ```~/video_saves``` folder for new streaming videos. It completes the following:
  - Waits until a stream has stopped and injects metadata information into the ```.flv``` file. This is needed to enable video seeking to different times using the slider.
  - Moves video files into their corresponding peripheral folder.
    - Stored videos are sorted by the Peripheral device ID. i.e. ``` ~/video_saves/[DEVICE_ID]/video.flv```
- Creates symlinks in ```~/video_saves/vod_links``` . These are needed for NGINX to serve videos in the Redroid Video Portal.

#### To install
First make sure you have python 2.7 installed. 

Copy project folder ```redroid-fs-monitor``` into ```~/video_saves```

Install watchdog
```sh
$ sudo pip install watchdog
```

We will use supervisor to set up the indexer as a service.

```sh
$ sudo apt-get install supervisor
```

Copy file ```~/video_saves/redroid-fs-monitor/fs_monitor_supervisor.conf``` into ```/etc/supervisor/conf.d/```, update and start.

```sh
$ sudo cp ~/video_saves/redroid-fs-monitor/fs_monitor_supervisor.conf /etc/supervisor/conf.d/
$ sudo supervisor update
$ sudo supervisorctl start redroid-fs-monitor
```

You should then see ``` redroid-fs-monitor: started``` if everything was successful!


 ### Step 4 - Video Portal
 
 Follow the guide [here](https://gorails.com/setup/ubuntu/16.10 ) to set up Ruby on Rails (This may take a while).
 
 Once that is finished, navigate to ```~/redroid-webportal``` and start up the server
 
 ```sh
 $ cd ~/redroid-webportal
 $ bundle install
 $ rails db:migrate
 $ rails s
```
You should then (hopefully) see the server fire up. 

To back out use ``` Ctrl-C ``` to stop the server.

To make the server run in the background and detach from terminal, I like to use the ```screen``` utility.

```sh
$ cd ~/redroid-webportal
$ screen
$ rails s
```

Once the rails server is running type ```Ctrl-a``` and then ```d```. This will detach you from the terminal and have the server run in the background indefinitely. To reattach to the server in the terminal use

```sh
$ screen -r
```