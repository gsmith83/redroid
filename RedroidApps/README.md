# Redroid

## Overview
Redroid is a software platform that puts retired Android devices back to work performing useful tasks for other devices. 

This repository contains the source code for the two Redroid applications: Redroid Peripheral, and Redroid Master (branded as "Redroid"). 

**Redroid Peripheral** makes available to other devices many of the hardware features of an Android device via network.

**Redroid Master** is a full featured Android phone designed to make use of the features of a device running Redroid Peripheral.

## Installation

1) Clone [this repo](https://teamredroid.visualstudio.com/Redroid/_git/RedroidApps)

2) Create an Android Studio project at the root of the cloned repo

3) Sync the Gradle Files and run Build on the project

4) Run the module Redroid Peripheral on an Android device running Android 4.1 or higher.

5) Run the module Redroid Master on an Android device running Android 


## Library Modules
The project contains one Redroid library module, called *RedroidCoreLibrary.* 

In addition, there a handful of third-party modules in the project. These are modified open-source projects not meant to be installed directly, and contain no launcher activities, but serve merely as library packages for various Redroid functions. They are:

* [*AndroidMotionDetection*:](https://github.com/appsthatmatter/android-motion-detection) A small video motion-detection library employed by Redroid Rules.

* [*Peepers*:](https://github.com/foxdog-studios/peepers) An Mjpeg streamer used by Redroid Peripheral to broadcast the bike cam feed.

* [*MjpegView*:](https://github.com/michogar/MjpegView) A simple view for used by Redroid Master to display the bike cam's Mjpeg feed.

* [*Yasea*:](https://github.com/begeekmyfriend/yasea) An RTMP streamer library used by Redroid Peripheral to broadcast Redroid video.

* [*Vitamio*:](https://github.com/yixia/VitamioBundle) An RTMP stream viewer used by Redroid Master to view Redroid video.



