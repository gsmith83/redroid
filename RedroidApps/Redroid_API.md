# Redroid API

## Overview
Data requests and notifications between devices are sent via _HTTP requests_. Data payloads are in _JSON_ format. The general use of the HTTP methods are as follows:

- __GET__ to request data from the remote device (Types 1, 2, and 7)
- __POST__ to perform session management or set state on the remote device (Types 0 and 3)
- __PUT__ to register notification requests with the remote device (Type 4) or perform a requested notification (Type 5)
- __DELETE__ to de-register notification requests with the remote device (Type 6)

Responses status codes:
- __200__ Request Succeeded (with _JSON_ response in payload, if relevant)
- __4XX__ Bad request. _JSON_ response in payload contains message:
```
{
	"msg":STRING
}
```

## **Type 0 (__POST__): _"Start a session and authenticate_**

### 001 _Master Handshake and Credential Exchange: Part 1 of 2: Indicate peripheral device id and get a session token_

__Request__
```
POST /redroid/master/connect/api
{
    "device_id" : STRING
}
```
__Response__
```
{
    "session_id" : INT
}
```

### 002 _Master Handshake and Credential Exchange: Part 2 of 2: Validate security token using the session id_

__Request__
```
POST /redroid/master/auth
{
    "session_id" : STRING,
    "security_token" : STRING
}
```
__Response__
```
[No response body required for success -- response code indicates success status]
```

### 003 [Optional*] _"Send me event notifications at this port"_

__Request__
```
POST /redroid/master/connect/event
{
    "session_id" : STRING
}
```
__Response__
```
[No response body required for success -- response code indicates success status]
```
```
*This is for use by Master devices behind a NAT router.
The server or peripheral would use the source port of this HTTP request as the designated port for events rather than the default

```

### 004 _"Peripheral Server Connection Handshake"_

__Request__
```
POST /redroid/peripheral/connect/api
{
    "device_id" : STRING,
    "security_token" : STRING
}
```
__Response__
```
{
    "rtmp_server_url" : STRING
}
```

### 005 _"Peripheral Server Connection Event Socket Handshake"_

__Request__
```
POST /redroid/peripheral/connect/event
{
      "device_id" : STRING
}
```
__Response__
```
[No response body required for success -- response code indicates success status]
```



## Type 1 (__GET__): _“Send me some streaming data on another channel”_**

### 101 _"Start an Mjpeg video stream and tell me the url"_

__Request__
```
GET /redroid/master/streaming/mjpeg
```
__Response__
```
{
    "url" : "http://..."
}
```

### 102 _"Start an RTMP video+audio stream and tell me the url"_

 Should this be a type 2?

__Request__
```
GET /redroid/master/streaming/rtmp/{video | audio | video_audio}
```
__Response__
```
{
    "url" : "rtmp://..."
}
```




## **Type 2 (__GET__): _“Send me some discrete state data (in your response)”_**

### 201 _"Send me a list of all available sensors"_
__Request__
```
GET /redroid/master/request/sensor_list
```
__Response__
```
{
	"sensors" : [
		{
            "type" : "TYPE_ACCELEROMETER",
			"max_range": FLOAT
		},
		{
			"type" : "TYPE_MAGNETIC_FIELD",
			"max_range": FLOAT
		},
		{
			"type" : "TYPE_ORIENTATION",
			"max_range": FLOAT
		},
		{
			"type" : "TYPE_GYROSCOPE",
			"max_range": FLOAT
		},
		{
			"type" : "TYPE_LIGHT",
			"max_range": FLOAT
		},
		{
			"type" : "TYPE_PRESSURE",
			"max_range": FLOAT
		},
		{
			"type" : "TYPE_TEMPERATURE",
			"max_range": FLOAT
		},
		{
			"type" : "TYPE_PROXIMITY",
			"max_range": FLOAT
		},
		{
			"type" : "TYPE_GRAVITY",
			"max_range": FLOAT
		},
		{
			"type" : "TYPE_LINEAR_ACCELERATION",
			"max_range": FLOAT
		},
		{
			"type" : "TYPE_ROTATION_VECTOR",
			"max_range": FLOAT
		},
		{
			"type" : "TYPE_RELATIVE_HUMIDITY",
			"max_range": FLOAT
		},
		{
			"type" : "TYPE_AMBIENT_TEMPERATURE",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_MAGNETIC_FIELD_UNCALIBRATED",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_GAME_ROTATION_VECTOR",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_GYROSCOPE_UNCALIBRATED",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_SIGNIFICANT_MOTION",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_STEP_DETECTOR",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_STEP_COUNTER",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_GEOMAGNETIC_ROTATION_VECTOR",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_HEART_RATE",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_TILT_DETECTOR",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_WAKE_GESTURE",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_GLANCE_GESTURE",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_PICK_UP_GESTURE",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_WRIST_TILT_GESTURE",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_DEVICE_ORIENTATION",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_POSE_6DOF",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_STATIONARY_DETECT",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_MOTION_DETECT",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_HEART_BEAT",
			"max_range" : FLOAT
		},
		{
			"type" : "TYPE_DYNAMIC_SENSOR_META",
			"max_range" : FLOAT
		}
	]
}
```
__NOTE:__ The above list shows all possible values for sensor types. An actual response should contain only those sensors which are found in the device, with only one entry (the default) for each type.
See <https://developer.android.com/reference/android/hardware/Sensor.html> for descriptions of each type.

### 202 _"Send me a list of available camera and location features"_
__Request__
```
GET /redroid/master/request/feature_list
```
__Response__
```
{
	"FEATURE_CAMERA" : BOOLEAN,
    "FEATURE_CAMERA_FRONT" : BOOLEAN,
    "FEATURE_CAMERA_FLASH" :  BOOLEAN,
    "FEATURE_MICROPHONE" : BOOLEAN,
    "FEATURE_LOCATION" : BOOLEAN,
    "FEATURE_LOCATION_GPS" : BOOLEAN,
    "FEATURE_LOCATION_NETWORK" : BOOLEAN
}
```

### 203 _"Send me specs for the given camera"_
__Request__
```
GET /redroid/master/request/camera_specs/{FEATURE_CAMERA | FEATURE_CAMERA_FRONT}
```

__Response__
```
{
	"resolutions" : [
		{
			"height": INT,
			"width": INT
		},
		{
			"height": INT,
			"width": INT
		},
        ...
	]
}
```

### 204 _"Send me your last-known location data"_
__Request__
```
GET /redroid/master/request/last_location
```
__Response__
```
{
    "latitude" : FLOAT,
    "longitude" : FLOAT,
    "altitude" : DOUBLE,  //m
    "bearing" : FLOAT,    //degrees
    "speed" : FLOAT,      //m/s
    "accuracy" : FLOAT,   //meters
    "time" : LONG         //ms since January 1, 1970 UTC
}
// A negative value for altitude, bearing, or speed indicates that this value is not available.
```

### Bike Cam Calls


### 205 _"Send me your bike cam flasher state"_
__Request__
```
GET /redroid/bikecam/state/flasher
```
__Response__
```
{
    "is_on" : BOOLEAN
}
```

### 206 _"Send me your bike cam turn-signal state"_
__Request__
```
GET /redroid/bikecam/state/signal
```
__Response__
```
{
    "state" : LEFT|RIGHT|OFF
}
```
### 206 _"Send me your bike cam turn-signal state"_
__Request__
```
GET /redroid/master/request/battery
```
__Response__
```
{
    "level" : FLOAT,
	"charging" : BOOLEAN
}
```


## **Type 3 (__POST__): _"Set some state on yourself"_**


### 301 _"Start your bike cam activity"_
__Request__
```
POST /redroid/bikecam/activity/{bikecam}
{}
```
__Response__
```
[No response body required for success -- response code indicates success status]
```

### 302 _"Toggle your bike came flasher"_

__Request__
```
POST /redroid/bikecam/state/flasher
{
    "is_on" : BOOLEAN
}
```
__Response__
```
{
    "is_on" : BOOLEAN
}
```

### 303 _"Turn your bike cam left|right turn signal on"_
__Request__
```
POST /redroid/bikecam/state/signal
{
    "signal_state" : LEFT|RIGHT|OFF
}
```
__Response__
```
{
    "signal_state" : LEFT|RIGHT|OFF
}
```

### 304 _"Toggle your LED flashlight"_

__Request__
```
POST /redroid/bikecam/state/flashlight/
{
    "is_on" : BOOLEAN
}
```

__Reponse__
```
[No response body required for success -- response code indicates success status]
```

### 305 _"Set your streaming video resolution to the specified value"_
__Request__
```
POST /redroid/bikecam/state/videoresolution
{
    "resolution" :  STRING
}
```

__Reponse__
```
[No response body required for success -- response code indicates success status]
```

### 306 _"Start or stop your video stream"_
__Request__
```
POST /redroid/master/rtmp/state
{
    "is_streaming" : BOOLEAN
}
```

__Reponse__
```
[No response body required for success -- response code indicates success status]
```

## **Type 4 (__PUT__): _"Register an event or rule notification request"_**

### 401 _"Begin sending me sensor events for [sensor_type]"_
__Request__
```
PUT /redroid/master/register/sensor_events/{sensor_type}
{
    "request_id" : INT,
    "sampling_period" : INT,
    "on_change_only : BOOLEAN
}
```

```
*Possible values for [sensor_type]:*
TYPE_ACCELEROMETER
TYPE_MAGNETIC_FIELD
TYPE_ORIENTATION
TYPE_GYROSCOPE
TYPE_LIGHT
TYPE_PRESSURE
TYPE_TEMPERATURE
TYPE_PROXIMITY
TYPE_GRAVITY
TYPE_LINEAR_ACCELERATION
TYPE_ROTATION_VECTOR
TYPE_RELATIVE_HUMIDITY
TYPE_AMBIENT_TEMPERATURE
TYPE_MAGNETIC_FIELD_UNCALIBRATED
TYPE_GAME_ROTATION_VECTOR
TYPE_GYROSCOPE_UNCALIBRATED
TYPE_SIGNIFICANT_MOTION
TYPE_STEP_DETECTOR
TYPE_STEP_COUNTER
TYPE_GEOMAGNETIC_ROTATION_VECTOR
TYPE_HEART_RATE
TYPE_TILT_DETECTOR
TYPE_WAKE_GESTURE
TYPE_GLANCE_GESTURE
TYPE_PICK_UP_GESTURE
TYPE_WRIST_TILT_GESTURE
TYPE_DEVICE_ORIENTATION
TYPE_POSE_6DOF
TYPE_STATIONARY_DETECT
TYPE_MOTION_DETECT
TYPE_HEART_BEAT
TYPE_DYNAMIC_SENSOR_META
```

```
NOTE: sampling_period is in microseconds.
If on_change_only is true, response will come no more frequently than the specified resolution
```

__Response__
```
[No response body required for success -- response code indicates success status]
```

### 402 _"Begin sending me location events"_
__Request__
```
PUT /redroid/master/register/location_events
{
    "request_id" : INT,
    "sampling_period" : INT,
    "on_change_only : BOOLEAN
}
```

```
NOTE: sampling_period is in microseconds.
If on_change_only is true, response will come no more frequently than the specified resolution
```

__Response__
```
[No response body required for success -- response code indicates success status]
```

### 403 _"Begin sending me motion detection events (Server -> Peripheral)"_
__Request__
```
PUT /redroid/server/register/motion_detection
{
	request_id: INT
}
```
__Response__
```
[No response body required for success -- response code indicates success status]
```

### 404 _"Set the following rule"_
__Request__
```
PUT /redroid/master/rule
{
	"token" :	{STRING},  //firebase token
	"device_id": {STRING}, 
	"config_id" : {STRING},
	"rule_id" : {STRING},
	"rule_name" : {STRING}
	"triggers" : [
		{
			"sensor_type" : {SENSOR_TYPE},
			"comparison" : {ABOVE | BELOW | BETWEEN},
			"value" : {DOUBLE},
			"ref_start_time" : {STRING},
			"ref_end_time" : {STRING}
			"ref_lat" : {DOUBLE},
			"ref_lng" : {DOUBLE}
		},
		...
	],
	"actions" : 
	{
		"notification" : {BOOLEAN},
		"text" : "8015608425",
		"email" : "derekj627@gmail.com"
	}
}
```

__Response__
```
[No response body required for success -- response code indicates success status]
```

## **Type 5 (__PUT__): From Peripheral: Event Notifications**

Note that this is the only type that originates from the peripheral device rather than the master.

### 501 _"Here is a  sensor event for [request_id]"_
__Request__
```
PUT /redroid/peripheral/sensor_event/{request_id}
{
    "accuracy" : INT,
    "sensor_type" : INT,
    "time_stamp" : LONG,
    "values" : [ FLOAT, FLOAT, ...]  //
}

```

__Response__
```
[No response body required for success -- response code indicates success status]
```


### API 502: _Send updated location data_

__Request__
```
PUT /redroid/peripheral/location_event/{request_id}
{
    "latitude" : FLOAT,
    "longitude" : FLOAT,
    "altitude" : FLOAT,
    "bearing" : FLOAT,
	"speed" : FLOAT,
	"time_stamp" : LONG
}

```

__Response__
```
[No response body required for success -- response code indicates success status]
```


### 503 _"I have detected motion (Peripheral -> Server)"_
__Request__
```
PUT /redroid/peripheral/motion_detection
{
	"image":URL_STRING
}

```

__Response__
```
[No response body required for success -- response code indicates success status]
```



## **Type 6 (__DELETE__): _"De-register an event notification request"_**

### 601 _"Stop sending me sensor events for [sensor_type]"_

__Request__
```
DELETE /redroid/master/register/sensor_events/sensor_type/{request_id}
```

__Response__
```
[No response body required for success -- response code indicates success status]
```

### 602 _"Stop sending me location events"_

__Request__
```
DELETE /redroid/master/register/location_events/{request_id}
```

__Response__
```
[No response body required for success -- response code indicates success status]
```

### 603 _"Stop sending me motion detection events (Server -> Peripheral"_
__Request__
```
DELETE /redroid/server/register/motion_detection
```
__Response__
```
[No response body required for success -- response code indicates success status]
```

### 604 _"Unset the following rule"_
__Request__
```
DELETE /redroid/master/rule/{rule_id}
```
__Response__
```
[No response body required for success -- response code indicates success status]
```


## **Type 7 (__GET__): _PING_**
### 701 _"PING"_

__Request__
```
GET /redroid/master/ping
```

__Response__
```
[No response body required for success -- response code indicates success status]
```
