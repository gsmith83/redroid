See https://source.android.com/devices/sensors/sensor-types.html

# Base sensors
1.	Accelerometer   |   TYPE_ACCELEROMETER | 1   
2.	Ambient temperature  |  TYPE_AMBIENT_TEMPERATURE  |  13
3.	Magnetic field sensor  |  TYPE_MAGNETIC_FIELD  |  2
4.	Gyroscope  | TYPE_GYROSCOPE  |  4
5.	Heart Rate  | TYPE_HEART_RATE  | 21
6.	Light  |  TYPE_LIGHT  | 5
7.	Proximity  |  TYPE_PROXIMITY  |  8
8.	Pressure  |  TYPE_PRESSURE  |  6
9.	Relative humidity  |  TYPE_RELATIVE_HUMIDITY  |  12

#  Composite sensor types
##  Activity composite sensors
1.	Linear acceleration  |  TYPE_LINEAR_ACCELERATION  |  10
2.	Significant motion  |  TYPE_SIGNIFICANT_MOTION  |  17
3.	Step detector  |  TYPE_STEP_DETECTOR  |  18
4.	Step counter  |  TYPE_STEP_COUNTER  |  19
5.	Tilt detector  |  TYPE_TILT_DETECTOR  |  22

##  Attitude composite sensors
1.	Rotation vector  |  TYPE_ROTATION_VECTOR  |  11
2.	Game rotation vector  |  TYPE_GAME_ROTATION_VECTOR  |15  
3.	Gravity  |  TYPE_GRAVITY  |  9
4.	Geomagnetic rotation vector  |  TYPE_GEOMAGNETIC ROTATION VECTOR  |20  
5.	Orientation (deprecated)

##  Uncalibrated sensors
1.	Gyroscope uncalibrated  |  TYPE_GYROSCOPE_UNCALIBRATED  |  16
2.	Magnetic field uncalibrated  |  TYPE_MAGNETIC_FIELD_UNCALIBRATED  |14  
 
##  Interaction composite sensors
1.	Wake up gesture  |  TYPE_WAKE_GESTURE  |  23
2.	Pick up gesture  |  TYPE_PICK_UP_GESTURE  |  25 
3.	Glance gesture  |  TYPE_GLANCE_GESTURE  |  24

## Other composite sensors (not listed at URL above)
1. Wrist tilt gesture  |  TYPE_WRIST_TILT_GESTURE  |  26
2. Device orientation  |  TYPE_DEVICE_ORIENTATION  |  27
3. Stationary detect  |  TYPE_STATIONARY_DETECT  |  29
4. Motion detect  |  TYPE_MOTION_DETECT  |  30