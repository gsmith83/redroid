package com.jwetherell.motion_detection;


import android.graphics.Bitmap;

public interface OnMotionDetectionListener {
    public void onMotionDetected(byte[] byteArrayJpeg);
}
