package net.redroid.redroidcorelibrary;

import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

import static android.content.ContentValues.TAG;


/**
 * An abstract class defining API methods common to both Master and Peripheral sensors. Contains
 * subclasses and helper methods used by methods in the concrete API classes of both the master
 * and peripheral Redroid apps.
 */
public abstract class API {


    public enum Method {
        GET, POST, PUT, DELETE
    }
    //devine a "sensor type" value for location, just to make it easier to use with sensor frameworks
    protected final int TYPE_LOCATION = 100;




    /**
     * A generic helper method for making a POST-type API call. This is used by a variety of
     * specific POST-type API calls to do the work of fetching a response.
     * @param url the url for the POST request
     * @param jsonPayload the sensors payload for the POST request
     * @param apiResponseListener the listener for returning results to caller
     */
    @Deprecated
    protected static void getHttpResponseAsync(final String url,
                                               final API.Method method,
                                               final String jsonPayload,
                                               final ApiResponseListener apiResponseListener) {
        AsyncTask<Object, Void, Void> task = new AsyncTask<Object, Void, Void>() {
            @Nullable
            @Override
            protected Void doInBackground(Object[] params) {

                int responseCode;
                String jsonResponse;
                JSONObject jsonObject;

                //transact with server
                Pair<Integer, String> response =  Network.getHttpResponse(url, method,
                        jsonPayload);
                responseCode = response.first;
                jsonResponse = response.second;


                //create the return object from the response string
                jsonObject = null;
                try {
                    if (!jsonResponse.equals("") && jsonResponse != null) {
                        jsonObject = new JSONObject(jsonResponse);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "JSON exception: " + e);
                    try {
                        jsonObject = new JSONObject("{\"msg\": \"unknown failure\"}");
                    } catch (JSONException j) {}
                }

                //call the listener and pass the results
                if (apiResponseListener != null) {
                    apiResponseListener.onApiResponse(responseCode, jsonObject);
                }
                return null;
            }
        };
        task.execute();
    }


    /**
     * A helper method for converting a JSONArray object, such retreived from an API notification,
     * to a float array.
     * @param jsonArray the string array
     * @return a float array
     */
    public static float[] getFloatArray(JSONArray jsonArray) {
        float[] floatArray = new float[jsonArray.length()];
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                floatArray[i] = (float) jsonArray.getLong(i);
            }
        }
        catch (JSONException e){
            Log.d(TAG, "getFloatArray: " + e.getMessage());
            return null;
        }
        return floatArray;
    }


    /**
     * A helper method for generating a JSONArray object from a float[].
     * @param array the array of float values
     * @return a JSONArray
     */
    protected static JSONArray getJSONArray(float[] array) {
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < array.length; i++) {
                jsonArray.put((double)array[i]);
            }
            return jsonArray;
        }
        catch (JSONException e) {
            Log.d(TAG, "getJSONArray(float[]) " + e.getMessage());
            return null;
        }

    }


    public class APIException extends Exception {

    }


    /**
     * A listener for handling incoming responses to an API request
     * (primarily used by a master device for handling immediate responses to API requests)
     */
    public interface ApiResponseListener  {
        /**
         * the callback for an API response
         * @param responseCode the HTTP response code
         * @param response a JSONObject containing the response payload
         */
        void onApiResponse(final int responseCode, final JSONObject response);
    }
}
