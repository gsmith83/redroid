package net.redroid.redroidcorelibrary;

/**
 * A simple class for containing the information in a Redroid API response returned by an
 * ApiRequestListener to the Network module
 * responsCode: the HTTP response code associated with the responses
 * json: the JSON string containing the response payload
 */
public class ApiResponse {
    int responseCode = 200;
    String json = null;

    public ApiResponse() {
    }

    public ApiResponse(int responseCode, String json) {
        this.responseCode = responseCode;
        this.json = json;
    }

    public int getCode() {
        return responseCode;
    }

    public String getBody() {
        return json;
    }

    public void setCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public void setBody(String json) {
        this.json = json;
    }
}