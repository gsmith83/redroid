package net.redroid.redroidcorelibrary;

import android.app.Application;
import android.content.Context;

/**
 * Custom version of application class.
 * Allows any class to obtain application context by exposing it publicly.
 */

//see http://munchpress.com/singleton-vs-applicationcontext/

public class App extends Application {

    private static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
    }

    /**
     * returns an instance of the application context
     * @return returns the application context
     */
    public static Context getAppContext() {
        return appContext;
    }
}
