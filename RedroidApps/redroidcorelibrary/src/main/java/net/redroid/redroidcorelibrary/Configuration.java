package net.redroid.redroidcorelibrary;

import net.redroid.redroidcorelibrary.rules.Rule;
import net.redroid.redroidcorelibrary.sensor.SensorList;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a single configuration (connection of a given type with a given remote
 * peripheral) from a master device's perspective.
 */
public class Configuration {

     /**
     * ENUMERATION type describing a configuration type - one of several preset, or CUSTOM
     */
    public enum ConfigType {
        BIKE_CAM,
        PET_CAM,
        SECURITY_CAM,
        BABY_MONITOR,
        CUSTOM
    }

    /**
     * ENUMERATION type describing the network topology for a configuration
     */
    public enum NetworkMode {
        WIFI,
        P2P,
        SERVER,
        WIFI_AND_SERVER
    }

    /** Custom name for his configuration */
    private String name;

    /** One of the pre-set networking topologies associated with this configuration */
    private NetworkMode networkMode;

    /**One of the pre-set configuration types, or CUSTOM */
    private ConfigType configType;

    /** Unique ID for the peripheral device associated with this configuration*/
    private String deviceId;

    /** The secret security code found the peripheral device display */
    private String securityToken;

    /** The wifi address of the peripheral device display */
    private String wifiIpAddress;

    /** A unique ID for identifying and retrieving this config from a Firebase rules notification    */
    private String configId;

    /** Filepath of a thumbnail image associated with this configuration.
     * This will be set programmatically. If null, a system default image will be used. */
    private String thumbnailPath;

    /** the default image resource to use when a custom thumbnail is not set **/
    private int iconImageResource;

    /**
     * object to store the available sensors on the peripheral device
     */
    public SensorList sensorList;

    /**
     * object to store the available features on the peripheral device
     */
    public FeatureList featureList;

    /**
     * The store of rules for this config
     */
    public List<Rule> rulesList;

    /**
     * Small object to hold custom options info
     */
    private Options options = new Options();

    public class Options {
        public boolean streamingMedia = false;
        public boolean mjpeg = false;
        public boolean rtmp = false;
        public boolean video = false;
        public boolean audio = false;
        public boolean backCamera = false;
        public boolean frontCamera = false;
        public boolean location = false;
        public boolean map = false;
        public boolean coordinateFields = false;
        public boolean linearAcceleration = false;
        public boolean light = false;
        public boolean proximity = false;
        public boolean temperature = false;
        public boolean magneticField = false;
        public boolean stepCounting = false;
        public boolean tiltDetection = false;
        public boolean motionDetection = false;
    }

    /**
     * Constructor
     * @param name - the custom name for hte configuration
     * @param networkMode - the networking topology associated with this configuration
     * @param configType - One of the pre-set configuration types or CUSTOM
     * @param deviceId - unique public ID of the associated peripheral device
     * @param securityToken - unique security code of the associated peripheral device
     */
    public Configuration(String name, NetworkMode networkMode, ConfigType configType,
                         String deviceId, String securityToken) {

        this.name = name;
        this.networkMode = networkMode;
        this.configType = configType;
        this.deviceId = deviceId;
        this.securityToken = securityToken;
        this.thumbnailPath = null;
        this.configId = RedroidUtil.generateSecurityToken();
        this.rulesList = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public NetworkMode getNetworkMode() {
        return networkMode;
    }

    public ConfigType getConfigType() {
        return configType;
    }

    public String getSecurityToken() {
        return securityToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getConfigId() {
        return configId;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public int getIconImageResource() {
        return iconImageResource;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNetworkMode(NetworkMode networkMode) {
        this.networkMode = networkMode;
    }

    public void setConfigType(ConfigType configType) {
        this.configType = configType;
    }

    public void setSecurityToken(String securityToken) {
        this.securityToken = securityToken;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }

    public void setIconImageResource(int iconImageResource) {
        this.iconImageResource = iconImageResource;
    }

    public void setSensorList(SensorList sensorList) {
        this.sensorList = sensorList;
    }

    public void setWifiIpAddress(String wifiIpAddress) {
        this.wifiIpAddress = wifiIpAddress;
    }

    public String getWifiIpAddress() {
        return wifiIpAddress;
    }

    public Options getOptions() {
        return options;
    }

    public void addRule(Rule rule) {
        rulesList.add(rule);
    }

    public void deleteRule(Rule rule) {
        rulesList.remove(rule);
    }

    public void deleteRule(int position) {
        rulesList.remove(position);
    }

    /**
     * Sets the sensorList to an object whose fields have all the same value (mostly for testing
     * purpose)
     * @param value the boolean value to set across the board -- all true or all false
     */
//    //public void setSensorList(boolean value) {
//        this.sensorList = new SensorList(value);
//    }


    /**
     * A static utility method for easily fetching a string label for a configuration type
     * @param type the value of a ConfigType enum
     * @return a string representing the particular ConfigType value passed in
     */
    public static String getConfigTypeString(ConfigType type) {
        switch (type) {
            case BIKE_CAM:
                return "Bike Cam";
            case PET_CAM:
                return "Pet Cam";
            case SECURITY_CAM:
                return "Security Camera";
            case BABY_MONITOR:
                return "Baby Monitor";
            case CUSTOM:
                return "Custom";
            default:
                return "CONFIG TYPE STRING LOOKUP ERROR";
        }
    }

    /**
     * A static utility method for easily fetching a string label for a network mode
     * @param type the value of a NetworkMode enum
     * @return a string representing the particular NetworkMode value passed in
     */
    public static String getNetworkModeString(NetworkMode type) {
        switch (type) {
            case P2P:
                return "P2P";
            case SERVER:
                return "Server";
            case WIFI:
                return "Wi-Fi";
            case WIFI_AND_SERVER:
                return "Wi-Fi and Server";
            default:
                return "NETWORK MODE STRING LOOKUP ERROR";
        }
    }
}
