package net.redroid.redroidcorelibrary;

import java.util.ArrayList;

/**
 * This class represents a collection of configurations (instances of Configuration class) on
 * a master device. It contains methods for adding and removing configurations, as well as for
 * retrieving references to individual configurations and sensors about the collection as a whole.
 */

public class ConfigurationStore {

    /**
     * The backing store.
     */
    private ArrayList<Configuration> store = new ArrayList<Configuration>();

    public ConfigurationStore() {
        /**
        this.addConfiguration(new Configuration("Julia Cam",
                Configuration.NetworkType.WIFI,
                Configuration.ConfigType.BABY_MONITOR,
                "Redroid_qwer123",
                "JKL785"));
        this.addConfiguration(new Configuration("Rufus Cam",
                Configuration.NetworkType.SERVER,
                Configuration.ConfigType.PET_CAM,
                "Redroid_tyui456",
                "Y89FDS"));
        this.addConfiguration(new Configuration("Front Door",
                Configuration.NetworkType.SERVER,
                Configuration.ConfigType.SECURITY_CAM,
                "Redroid_opas789",
                "Y89964"));
         */
    }

    /**
     * Fetches the instance of Configuration at the specified index
     * @param index The index of the instance to be fetched
     * @return the instance of Configuration at the specified index. Returns null if the index is
     * out of bounds.
     */
    public Configuration getConfig(int index) {
        try {
            return store.get(index);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public Configuration getConfig(String configId) {
        for (Configuration config : store) {
            if (config.getConfigId().equals(configId))
                return config;
        }
        return null;
    }

    /**
     * Fetches the last instance of Configuration in the store
     * @return the last Configuration
     */
    public Configuration getLastConfig() {
        if (store.size() == 0)
            return null;
        return store.get(store.size()-1);
    }

    /**
     * Gets the size of the store
     * @return the NUMBER of items in the store
     */
    public int getSize() {
        return store.size();
    }

    /**
     * For adding an instance of Configuration to the collection
     * @param config The instance of Configuration to add to the store
     * @return A boolean value indicating whether the Configuration was successfully added
     */
    public Boolean addConfiguration(Configuration config) {
        return store.add(config);
    }

    /**
     * For removing an instance of Configuration from the collection by reference
     * @param config a reference to the actual Configuration instance to be removed
     * @return A boolean value indicating whether the given Configuration was successfully removed.
     * Returns false if the instance is not found in the collection.
     */
    public Boolean removeConfiguration(Configuration config) {
        return store.remove(config);
    }

    /**
     * Removes an instance of Configuration at the specified position
     * @param index the position of the Configuration instance to be removed
     * @return A the instance of Configuration that was removed. Returns null if the index is out
     * of bounds.
     */
    public Configuration removeConfiguration(int index) {
        try {
            return store.remove(index);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }
}