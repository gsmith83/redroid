package net.redroid.redroidcorelibrary;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import static android.content.ContentValues.TAG;


/**
 * Parent for a Master or Peripheral data model class
 */
public abstract class DataModel {
    public static final boolean DEBUG = true;

    protected String serverUrl = "";

    public Network network;


    public String getConnectionServerUrl() {
        if (serverUrl == null)
            return "";
        else
            return serverUrl;
    }



    public Network getNetwork() {
        return network;
    }

    public void setConnectionServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
        saveConnectionServerUrlToFile();
    }


    /**
     * Method for saving the server url to file
     */
    protected void saveConnectionServerUrlToFile() {
        BufferedWriter writer;
        try {
            File serverUrlFile = new File(App.getAppContext().getFilesDir(), "server_url.dat");
            writer = new BufferedWriter(new FileWriter(serverUrlFile, false));
            writer.write(serverUrl);
            writer.newLine();
            writer.flush();
            writer.close();
        } catch (IOException e) {
            String msg = "An error occurred when writing the server URL to file: " +
                    e.getMessage();
            Log.e(TAG, msg);
            Toast.makeText(App.getAppContext(), msg, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Method for refreshing the config store from file
     */
    protected void loadConnectionServerUrlFromFile() {
        try {
            File serverUrlFile = new File(App.getAppContext().getFilesDir(), "server_url.dat");
            if (serverUrlFile.exists()) {
                BufferedReader reader = new BufferedReader(new FileReader(serverUrlFile));
                this.serverUrl = reader.readLine();
                reader.close();
            }
        }
        catch (IOException e) {
            String msg = "An error occurred when loading the server URL from from file: " +
                    e.getMessage();
            Toast.makeText(App.getAppContext(), msg, Toast.LENGTH_LONG).show();
        }
    }
}
