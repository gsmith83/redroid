package net.redroid.redroidcorelibrary;

import android.content.pm.PackageManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;

/**
 * A simple class representing the presence of certain featuers on the device
 */
public class FeatureList {
    public boolean FEATURE_CAMERA = false;
    public boolean FEATURE_CAMERA_FRONT = false; 
    public boolean FEATURE_CAMERA_FLASH = false;
    public boolean FEATURE_MICROPHONE = false; 
    public boolean FEATURE_LOCATION = false; 
    public boolean FEATURE_LOCATION_GPS = false;
    public boolean FEATURE_LOCATION_NETWORK = false;

    /**
     * Constructs a featurelist object based on the features in the current device
     */
    public FeatureList() {
        PackageManager pm = App.getAppContext().getPackageManager();
        this.FEATURE_CAMERA = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
        this.FEATURE_CAMERA_FRONT = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT);
        this.FEATURE_CAMERA_FLASH = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        this.FEATURE_MICROPHONE = pm.hasSystemFeature(PackageManager.FEATURE_MICROPHONE);
        this.FEATURE_LOCATION = pm.hasSystemFeature(PackageManager.FEATURE_LOCATION);
        this.FEATURE_LOCATION_GPS = pm.hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS);
        this.FEATURE_LOCATION_NETWORK = pm.hasSystemFeature(PackageManager.FEATURE_LOCATION_NETWORK);
    }


    /**
     * Returns a FeatureList made from a JSON string, such as that exported using toJson()
     * @param json the JSON string
     * @return a FeatureList object based on the input JSON
     */
    public static FeatureList fromJson(String json) {
        Type featureListType = new TypeToken<FeatureList>(){}.getType();
        return new Gson().fromJson(json, featureListType);
    }


    /**
     * Exports a representation of this object in JSON
     * @return a JSON string representing this object
     */
    public String toJson(){
        return new Gson().toJson(this);
    }
}
