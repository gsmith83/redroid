package net.redroid.redroidcorelibrary;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.wifi.WifiManager;
import android.support.v4.util.Pair;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.concurrent.TimeoutException;

import static android.content.ContentValues.TAG;
import static android.content.Context.WIFI_SERVICE;
import static net.redroid.redroidcorelibrary.API.Method.POST;
import static net.redroid.redroidcorelibrary.API.Method.PUT;

/**
 * Contains general purpiose networking methods useful to Redroid tasks
 */
public abstract class Network {

    //APP LEVEL NETWORKING CONSTANTS

    /**ports**/
    public static final int SERVER_API_PORT = 11100;
    public static final int SERVER_EVENT_PORT = 11100;

    /**timeouts**/
    public static final int PING_INTERVAL = 3000;
    public static final int SOCKET_CONNECTION_TIMEOUT = 7000;
    public static final int SOCKET_READ_TIMEOUT = 5000;



    public String localIpAddress;
    protected String serverIpAddress;

    public abstract void init() throws TimeoutException, ConnectionException;
    public abstract void teardown();
    private static URL url = null;


    public String getRemoteIpAddress() {
        return serverIpAddress;
    }

    /**
     * A custom implementation for executing a simple, synchronous, HTTP request with a JSON
     * payload. Can be used by any application -- peripheral, or otherwise.
     * used by
     * @param remoteHost ipAddress:port of remote host
     * @param inputReader a BufferedReader object wrapping an existing socket's input stream
     * @param outputWriter a BufferedWriter object wrapping an existing socket's output stream
     * @param method the HTTP Method to use, as defined in the Redroid API
     * @param url the url string (not including host)
     * @param requestBody  the JSON payload
     * @return a Pair object containing the HTTP response code and response body (if any)
     */
    public synchronized static Pair<Integer, String> getHttpResponse(String remoteHost,
                                                  BufferedReader inputReader,
                                                  BufferedWriter outputWriter,
                                                  API.Method method,
                                                  String url,
                                                  String requestBody) throws ConnectionException {

        int responseCode = -2;
        String responseBody = null;

        if (requestBody == null)
            requestBody = "";

        //MAKE THE REQUEST
        // method, url, remotehost, port, content-length, payload
        String template =
                "%s %s HTTP/1.1\r\n" +
                        "Content-Type: application/json\r\n" +
                        "User-Agent: Redroid\r\n" +
                        "Host: %s\r\n" +
                        "Connection: Keep-Alive\r\n" +
                        "Content-Length: %d\r\n\r\n" +
                        "%s";

        // form the request
        int contentLength = requestBody.length();
        String request = String.format(template, method.name(), url, remoteHost,
                contentLength, requestBody);

        //send the request
        try {
            outputWriter.write(request);
            outputWriter.flush();
            if (url.contains("sensor_event")) {
                Log.d("SensorEvents", "Peripheral sent event:\r\n " + request);
            }
            else if (url.contains("location_event")) {
                Log.d("LocationEvents", "Peripheral sent location:\r\n " + request);
            }
        }
        catch (IOException e){
            Log.e("url", "IO exception in getHttpResponse: " + e);
            responseCode = -1;
            responseBody = "{\"msg\":\"Failed to connect\"}";
            return new Pair<>(responseCode, responseBody);
        }

        //FETCH THE RESPONSE
        try {
            String[] responseParams = getHttpResponseParams(inputReader);
            responseCode = Integer.parseInt(responseParams[0]);
            responseBody = responseParams[1];
        }
        catch (IOException | NullPointerException e) {
            Log.e(TAG, "getHttpResponse: " + e.getMessage());
            responseCode = -1;
            String msg = String.format("Connection failure: %s", e instanceof SocketTimeoutException? "socket timeout" : e.getMessage());
            responseBody = String.format("{\"msg\":\"%s\"}", msg);
        }
        catch (NumberFormatException e) {
            Log.e(TAG, "getHttpResponse: " + e.getMessage());
            responseCode = -1;
            responseBody = "{\"msg\":\"Response parse failure\"}";
        }

        return new Pair<>(responseCode, responseBody);
    }


    /**
     * A custom implementation for executing a simple, synchronous, HTTP request with a JPEG
     * payload. Can be used by any application -- peripheral, or otherwise.
     * used by
     * @param remoteHost ipAddress:port of remote host
     * @param inputReader a BufferedReader object wrapping an existing socket's input stream
     * @param outputStream a socket's output stream
     * @param url the url string (not including host)
     * @param jpegImageData  jpeg image byte array
     * @return a Pair object containing the HTTP response code and response body (if any)
     */
    public synchronized static Pair<Integer, String> getHttpResponseJpeg(String remoteHost,
                                                                         BufferedReader inputReader,
                                                                         OutputStream outputStream,
                                                                         String url,
//                                                                         byte[] rawImageData
                                                                         byte[] jpegImageData

    ) throws ConnectionException {

        int responseCode = -2;
        String responseBody = null;
        final int JPEG_QUALITY = 90;

        /** get a jpeg byte array from the raw image byte array **/
//        //generate a Bitmap from the image data
//        Bitmap bmp = BitmapFactory.decodeByteArray(rawImageData, 0, rawImageData.length);
//
//        //stream the bmp as a jpeg into an output stream
//        ByteArrayOutputStream jpegOutStream = new ByteArrayOutputStream();
//        bmp.compress(Bitmap.CompressFormat.JPEG, JPEG_QUALITY, jpegOutStream);
//
//        //get a byte array for the jpeg
//        byte[] jpegImageData = jpegOutStream.toByteArray();

        //MAKE THE REQUEST
        // method, url, remotehost, port, content-length, payload
        String headerTemplate =
                "%s %s HTTP/1.1\r\n" +
                "Content-Type: image/jpeg\r\n" +
                "User-Agent: Redroid\r\n" +
                "Host: %s\r\n" +
                "Connection: Keep-Alive\r\n" +
                "Content-Length: %d\r\n\r\n";

        String header = String.format(headerTemplate, PUT, url, remoteHost,
                jpegImageData.length);


        //send the request
        try {
            //send the header
            BufferedWriter outputWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
            outputWriter.write(header);
            outputWriter.flush();

            //send the jpeg byte array
            outputStream.write(jpegImageData);
        }
        catch (IOException e){
            Log.e("url", "IO exception in getHttpResponse: " + e);
            responseCode = -1;
            responseBody = "{\"msg\":\"Failed to connect\"}";
            return new Pair<>(responseCode, responseBody);
        }

        //FETCH THE RESPONSE
        try {
            String[] responseParams = getHttpResponseParams(inputReader);
            responseCode = Integer.parseInt(responseParams[0]);
            responseBody = responseParams[1];
        }
        catch (IOException | NullPointerException e) {
            Log.e(TAG, "getHttpResponse: " + e.getMessage());
            responseCode = -1;
            responseBody = "{\"msg\":\"Connection failure\"}";
        }
        catch (NumberFormatException e) {
            Log.e(TAG, "getHttpResponse: " + e.getMessage());
            responseCode = -1;
            responseBody = "{\"msg\":\"Response parse failure\"}";
        }

        return new Pair<>(responseCode, responseBody);
    }

    /**
     * Fetches on HTTP response from the specified URL. A multi-purpose method for any HTTP request
     * method. Deprecated.
     * @param method the HTTP method to be used
     * @param payload the JSON payload; ignored for GET requests, so may be null or blank
     * @param urlString the URL to request a response from
     * @return a Pair containing the response code and response string
     */

    @Deprecated
    static Pair<Integer, String> getHttpResponse(String urlString, API.Method method,
                                                 String payload){
        Integer retInt = -2;
        String retString = "";
        try {

            //open the connection
            URL url = new URL("http://" + urlString);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestProperty("Accept-Encoding", "identity");


            //extra work for PUT and POST methods
            if (method == POST || method == PUT) {

                //set the request properties for a payload-bearing request
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestMethod(method.name());

                //get an output stream and write the JSON payload to the request
                BufferedOutputStream outStream = new BufferedOutputStream(
                        connection.getOutputStream());
                RedroidUtil.writeString(outStream, payload);

            }
            /// get the response code
            retInt = connection.getResponseCode();

            //failure to connect?
            if (retInt == -1) {
                retString = "{\"msg\":\"Failed to connect\"}";
                return new Pair<>(retInt, retString);
            }

            // get the response JSON payload
            BufferedInputStream stream;
            if (retInt == 200) {
                stream = new BufferedInputStream(connection.getInputStream());
            }
            else {
                stream = new BufferedInputStream(connection.getErrorStream());
            }

            retString = RedroidUtil.readString(stream, 4096, false);


            //close out the connection properly to guarantee that keep-alive will work.
            // see http://stackoverflow.com/a/29546799/7407559
//            int left;
//            do {
//                left = stream.read();
//            } while (left != -1);
//            stream.close();
//            connection.disconnect();

            //return the response
            return new Pair<>(retInt, retString);
        }

        catch (MalformedURLException e) {
            Log.e("url", "malformed url exception in getHttpResponse: " + e);
            return new Pair<>(retInt, retString);
        }
        catch (IOException e) {
            Log.e("url", "IO exception in getHttpResponse: " + e);
            if (retInt == -2) {
                retString = "{\"msg\":\"Failed to connect\"}";
            }
            return new Pair<>(retInt, retString);

        }
    }

    /**
     * Gets the parameteres of a HTTP request from a socket's input stream (wrapped in a
     * BufferedReader) and returns them as an array of strings: method, url, and body, in that
     * order.
     * @param reader
     * @return an array of http request params: method (index 0), url (index 1), body (index 2)
     * @throws NumberFormatException if the Content Length value cannot be parsed as an int
     */
    protected static String[] getHttpRequestParams(BufferedReader reader) throws NumberFormatException{
        StringBuilder sb = new StringBuilder();
        String requestHeader, requestBody;
        try {
            String contentLengthHeader = "";
            String line = reader.readLine();
            while (!line.equals("")) {
                sb.append(line);
                if (line.toLowerCase().startsWith("content-length"))
                    contentLengthHeader = line;
                sb.append("\r\n");
                line = reader.readLine();
            }
            requestHeader = sb.toString();

            int contentLength = -1;

            String[] s = contentLengthHeader.split(" ");
            if (s.length < 2) {
                return null;
            }
            contentLength = Integer.parseInt(s[1]);

            // get the body
            sb = new StringBuilder();
            for(int i = 0; i < contentLength; i++){
                sb.append((char)reader.read());
            }
            requestBody = sb.toString();


            String[] strings = requestHeader.split("\\s");
            String method = strings[0];
            String url = strings[1];
            return new String[]{method, url, requestBody};
        }

        catch(IOException e){
            Log.e(TAG, "getHttpRequestParams: " + e.getMessage());
            return null;
        }
    }

    /**
     * Gets the parameteres of an HTTP response from a socket's input stream (wrapped in a
     * BufferedReader) and returns them as an array of strings: code, body
     * @param reader
     * @return an array of http response params: code (index 0), body (index 1)
     * @throws NumberFormatException if the Content Length value cannot be parsed as an int
     */
    protected static String[] getHttpResponseParams(BufferedReader reader) throws IOException,
            NumberFormatException {
        StringBuilder sb = new StringBuilder();
        String responseHeader, responseBody;
        String contentLengthHeader = "";
        String line = reader.readLine();
        if (line == null) {
            return new String[]{"-1", null};
        }
        while (!line.equals("")) {
            sb.append(line);
            if (line.toLowerCase().startsWith("content-length"))
                contentLengthHeader = line;
            sb.append("\r\n");
            line = reader.readLine();
        }
        responseHeader = sb.toString();

        //get the responseCode
        String[] strings = responseHeader.split("\\s");
        String responseCode = "-1";
        if (strings.length > 1)
            responseCode = strings[1];
        else
            return null;

        //get the body
        int contentLength = -1;
        String[] s = contentLengthHeader.split(" ");
        if (s.length > 1)
            contentLength = Integer.parseInt(s[1]);

        sb = new StringBuilder();
        for(int i = 0; i < contentLength; i++){
            sb.append((char)reader.read());
        }
        responseBody = sb.toString();

        return new String[]{responseCode, responseBody};
    }


    /**
     * Generates a proper HTTP response, adequate to the redroid API, when supplied with a response
     * code and body
     * @param responseCode the HTTP response code
     * @param responseBody the JSON payload
     * @return the complete HTTP response string
     */
    protected String getHttpResponseString(int responseCode, String responseBody){
        String template =
                "%s\r\n" +
                        "Date: %s\r\n" + "" +
                        "Server: Redroid\r\n" +
                        "Content-Type: application/json\r\n" +
                        "Content-Length: %d\r\n\r\n" +
                        "%s";

        String date = RedroidUtil.getServerTime();

        String responseHeader;
        if(responseCode == 200)
            responseHeader = "HTTP/1.1 200 OK";
        else if (responseCode == 401)
            responseHeader = "HTTP/1.1 401 Unauthorized";
        else if (responseCode == 404)
            responseHeader = "HTTP/1.1 404 Not Found";
        else if (responseCode == 503)
            responseHeader = "HTTP/1.1 503 Service Unavailable";
        else
            responseHeader = "HTTP/1.1 400 Bad Request";
        int contentLength;
        if (responseBody == null) {
            contentLength = 0;
        }
        else {
            contentLength = responseBody.length();
        }

        String response = String.format(template, responseHeader, date,
                contentLength, responseBody);

        return response;
    }


    /**
     *Network initialization tasks common to WiFi networks, both master and peripheral
     * @return the IP address
     * @throws ConnectionException
     * @throws TimeoutException
     */
    protected static String wifiInit() throws ConnectionException, TimeoutException {
        final int TIMEOUT = 8000;

        WifiManager wifi = (WifiManager)App.getAppContext().getSystemService(WIFI_SERVICE);
        boolean wifiEnabled = wifi.setWifiEnabled(true);
        if(!wifiEnabled)
            throw new ConnectionException("Failed to enable wifi.");
        int ipAddress = wifi.getConnectionInfo().getIpAddress();
        long start = System.currentTimeMillis();
        while(ipAddress == 0){
            if(System.currentTimeMillis() - start >= TIMEOUT)
                throw new TimeoutException("Failed to connect to wifi access point.");
            ipAddress = wifi.getConnectionInfo().getIpAddress();
        }
        return RedroidUtil.getIpAddressString(ipAddress);
    }


    public static class ConnectionException extends Exception{

        public ConnectionException(String s) {
            super(s);
        }
    }
}