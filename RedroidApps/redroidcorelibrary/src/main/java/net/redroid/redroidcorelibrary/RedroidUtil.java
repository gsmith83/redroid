package net.redroid.redroidcorelibrary;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.TimeZone;

/**
 * Static class to hold various utility methods for the Redroid peripheral and master apps. Such
 * methods should generally adhere to id or random NUMBER generation, translating encodings,
 * comparison or equality, and should not represent an object.
 */
public class RedroidUtil {
    private static final int AUTH_ID_LENGTH = 6;
    private static final String CHAR_ARRAY = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ{}";
    private static SecureRandom random = new SecureRandom();

    public RedroidUtil(){}

    /**
     * Converts a string of hex digits representing a mac address to a base 64 identifier.
     * @param text The string of hex digits
     * @return A string of base64 digits (0-9,a-z,A-Z,?,!)
     */
    public static String hexStringToBase64(String text){
        StringBuilder sb = new StringBuilder();
        text = text.toLowerCase();
//        String[] macOctets = text.split(":");
        String[] macOctets = {text.substring(0, 2), text.substring(2, 4), text.substring(4, 6), text.substring(6, 8), text.substring(8, 10), text.substring(10, 12)};

        for(int i = 0; i <= macOctets.length / 2; i += 3){
            // four bit digits
            int a = CHAR_ARRAY.indexOf(macOctets[i].charAt(0));
            int b = CHAR_ARRAY.indexOf(macOctets[i].charAt(1));
            int c = CHAR_ARRAY.indexOf(macOctets[i+1].charAt(0));
            int d = CHAR_ARRAY.indexOf(macOctets[i+1].charAt(1));
            int e = CHAR_ARRAY.indexOf(macOctets[i+2].charAt(0));
            int f = CHAR_ARRAY.indexOf(macOctets[i+2].charAt(1));

            // four six-digit values
            int newA = (a << 2) | (b >> 2);
            int newB = ((b&0x03) << 4) | c;
            int newC = (d << 2) | (e >> 2);
            int newD = ((e&0x03) << 4) | f;

            sb.append(CHAR_ARRAY.charAt(newA));
            sb.append(CHAR_ARRAY.charAt(newB));
            sb.append(CHAR_ARRAY.charAt(newC));
            sb.append(CHAR_ARRAY.charAt(newD));
        }
        return sb.toString();
    }

    public static String base64ToMacString(String text){
        StringBuilder sb = new StringBuilder();

        int[] intArr = new int[text.length()];

        for(int i = 0; i <= text.length() / 2; i+=4){
            // 6 bit digits
            int octetVal1 = CHAR_ARRAY.indexOf(text.charAt(i));
            int octetVal2 = CHAR_ARRAY.indexOf(text.charAt(i + 1));
            int octetVal3 = CHAR_ARRAY.indexOf(text.charAt(i + 2));
            int octetVal4 = CHAR_ARRAY.indexOf(text.charAt(i + 3));

            // six 4-bit values
            int newA = octetVal1 >> 2;
            int newB = ((octetVal1 & 0x03) << 2) | (octetVal2 >> 4);
            int newC = octetVal2 & 0xf;
            int newD = octetVal3 >> 2;
            int newE = ((octetVal3 & 0x03) << 2) | (octetVal4 >> 4);
            int newF = octetVal4 & 0xf;

            sb.append(CHAR_ARRAY.charAt(newA));
            sb.append(CHAR_ARRAY.charAt(newB));
            sb.append(":");
            sb.append(CHAR_ARRAY.charAt(newC));
            sb.append(CHAR_ARRAY.charAt(newD));
            sb.append(":");
            sb.append(CHAR_ARRAY.charAt(newE));
            sb.append(CHAR_ARRAY.charAt(newF));
            if(i == 0)
                sb.append(":");
        }

        return sb.toString();
    }

    public static String generateSecurityToken(){
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i  < AUTH_ID_LENGTH; i++){
            int randomInt = random.nextInt();
            if(randomInt < 0)
                randomInt *= -1;

            sb.append(CHAR_ARRAY.charAt(randomInt % CHAR_ARRAY.length()));
        }
        return sb.toString();
    }

    /**
     * Helper method to simplify reading a string from an input stream
     * @param stream the InputStream to read from
     * @param length the max NUMBER of bytes
     * @param close whether to close the stream when finished
     * @return the string that was read
     */
    public static String readString(InputStream stream, int length, boolean close) {
        Scanner scanner = new Scanner(stream).useDelimiter("\\A");
        try {
            if (scanner.hasNext()) {
                return scanner.next();
            }

            else {
                return "";
            }
        }
        finally {

            if (scanner != null && close) {
                scanner.close();
            }
        }
    }

    /**
     * Helper method to simplify writing a string to an output stream
     * @param stream the output stream
     * @param string the string to write
     */
    public static void writeString(OutputStream stream, String string) {
        OutputStreamWriter writer = new OutputStreamWriter(stream);
        try {
            writer.write(string);
            writer.flush();
            writer.close();
        }
        catch (IOException e) {
            Log.e("IO", "IO exception in writeString: " + e);
        }
    }

    /**
     * Helper method to simplify writing a string to an output stream
     * @param string the string to write
     */
    public static void writeStringToSocket  (BufferedWriter writer, String string)
            throws IOException {
        writer.write(string);
        writer.flush();
    }



    //this method borrowed from: http://stackoverflow.com/a/17789187/7407559
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * A list of type types arranged in order by constant int value as found in the Sensor
     * class (index = value - 1)
     *
     * e.g. Sensor.TYPE_ACCELEROMETER has a value of 1, and is indexed at 0 in the array
     */
    public static String[] sensorTypeArray = {
            "TYPE_ACCELEROMETER",               //1
            "TYPE_MAGNETIC_FIELD",              //2
            "TYPE_ORIENTATION",                 //3  deprecated -- see https://developer.android.com/guide/topics/sensors/sensors_position.html#sensors-pos-orient
            "TYPE_GYROSCOPE",                   //4
            "TYPE_LIGHT",                       //5
            "TYPE_PRESSURE",                    //6
            "TYPE_TEMPERATURE",                 //7 deprecated -- see 13
            "TYPE_PROXIMITY",                   //8
            "TYPE_GRAVITY",                     //9
            "TYPE_LINEAR_ACCELERATION",         //10
            "TYPE_ROTATION_VECTOR",             //11
            "TYPE_RELATIVE_HUMIDITY",           //12
            "TYPE_AMBIENT_TEMPERATURE",         //13
            "TYPE_MAGNETIC_FIELD_UNCALIBRATED", //14
            "TYPE_GAME_ROTATION_VECTOR",        //15
            "TYPE_GYROSCOPE_UNCALIBRATED",      //16
            "TYPE_SIGNIFICANT_MOTION",          //17
            "TYPE_STEP_DETECTOR",               //18
            "TYPE_STEP_COUNTER",                //19
            "TYPE_GEOMAGNETIC_ROTATION_VECTOR", //20
            "TYPE_HEART_RATE",                  //21
            "TYPE_TILT_DETECTOR",               //22
            "TYPE_WAKE_GESTURE",                //23
            "TYPE_GLANCE_GESTURE",              //24
            "TYPE_PICK_UP_GESTURE",             //25
            "TYPE_WRIST_TILT_GESTURE",          //26
            "TYPE_DEVICE_ORIENTATION",          //27
            "TYPE_POSE_6DOF",                   //28
            "TYPE_STATIONARY_DETECT",           //29
            "TYPE_MOTION_DETECT",               //30
            "TYPE_HEART_BEAT",                  //31
            "TYPE_DYNAMIC_SENSOR_META"          //32
    };

    /**
     * Lookup from type type integer constant to string (as found in the Sensor class).
     * This can be used to pass in the result of Sensor.getType() to obtain a string
     * representation of the pertinent type type.
     * @param i the value of the constant int as contained in the Sensor class
     * @return the string value (variable name) of this int value in the Sensor class
     *
     */
    public static String getSensorType(int i) {
        if (i > 32)
            return null;
        return sensorTypeArray[i-1];
    }

    /**
     * Reverse lookup from Sensor-type string to int value.
     * @param s the string argument representing a type type name
     * @return an the const int value of the given type type string. -1 for failure to find.
     */
    public static int getSensorTypeInt(String s) {
        for (int i = 0; i < sensorTypeArray.length; i++) {
            if (sensorTypeArray[i].equals(s.toUpperCase())) {
                return i + 1;
            }
        }
        return -1;
    }


    /**
     * citation: http://stackoverflow.com/a/8642463
     */
    public static String getServerTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormat.format(calendar.getTime());
    }


    public static String getIpAddressString(int ipaddress) {
       return String.format(Locale.US, "%d.%d.%d.%d", ipaddress & 0xff, ipaddress >> 8 & 0xff,
                ipaddress >> 16 & 0xff, ipaddress >> 24 & 0xff);
    }


    /**
     * Tests whether an ipv4 ip address string is valid0
     * @param ip the candidate ip address
     * @return true for valid IP address format, false otherwise
     */
    public static boolean isValidIpString(String ip) {
        String[] segments = ip.split("\\.");
        if (segments.length != 4) {
            return false;
        }
        for (String segment : segments) {
            try {
                int segInt = Integer.parseInt(segment);
                if (segInt > 255 || segInt < 0)
                    return false;
            } catch (NumberFormatException e) {
                return false;
            }
        }
        return true;
    }

    /**
     * Generic method for forming a jsimple son string from given parameters
     * @param keys An array of the keys for the JSON key-value pairs (if any)
     * @param values An array of the values for the JSON key-value pairs (if any), as objects.
     *               Any element not of underlying type String, Boolean, Double, Integer, or Long
     *               will be skipped.
     */
    public static String makeSimpleJson(String[] keys, Object[] values) throws JSONException {
        String jsonString = "";

        Set<Class> validClasses = new HashSet<>();
        validClasses.add(Boolean.class);
        validClasses.add(Double.class);
        validClasses.add(Integer.class);
        validClasses.add(Long.class);
        validClasses.add(String.class);

        //build the json request
        JSONObject jsonObject = new JSONObject();
        for (int i = 0; i < keys.length; i++) {
            if (i >= values.length) continue;
            if (!validClasses.contains(values[i].getClass())) {
                continue;
            }
            jsonObject.put(keys[i], values[i]);
        }
        jsonString = jsonObject.toString();
        return jsonString;
    }
     /**
     * Returns the class object given the full path of the class.
     * e.g. "net.redroid.redroidcorelibrary.type.SensorList" will return a SensorList class object.
     * @param className string path of the class
     * @return class object
     */
    public static Class getClassFromName(String className){
        Class result = null;
        try {
            result = Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Converts an object to a JSON string.
     * @param obj Object to serialize
     * @param classType Class object type of obj
     * @return a JSON string representing the object
     */
    public static String buildJSONString(Object obj, Class classType){
        GsonBuilder builder = new GsonBuilder();
        Gson gsonExt = builder.create();
        return gsonExt.toJson(obj, classType);
    }

    /**
     *
     * @param length
     * @return
     */
    public static String getRandomAlphaString(int length) {
        String base = "abcdefghijklmnopqrstuvwxyz";
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

    public class Constants{

        public static final String TYPE_ACCELEROMETER = "TYPE_ACCELEROMETER";
        public static final String TYPE_MAGNETIC_FIELD = "TYPE_MAGNETIC_FIELD";
        public static final String TYPE_ORIENTATION = "TYPE_ORIENTATION";
        public static final String TYPE_GYROSCOPE = "TYPE_GYROSCOPE";
        public static final String TYPE_LIGHT = "TYPE_LIGHT";
        public static final String TYPE_PRESSURE = "TYPE_PRESSURE";
        public static final String TYPE_TEMPERATURE = "TYPE_TEMPERATURE";
        public static final String TYPE_PROXIMITY = "TYPE_PROXIMITY";
        public static final String TYPE_GRAVITY = "TYPE_GRAVITY";
        public static final String TYPE_LINEAR_ACCELERATION = "TYPE_LINEAR_ACCELERATION";
        public static final String TYPE_ROTATION_VECTOR = "TYPE_ROTATION_VECTOR";
        public static final String TYPE_RELATIVE_HUMIDITY = "TYPE_RELATIVE_HUMIDITY";
        public static final String TYPE_AMBIENT_TEMPERATURE = "TYPE_AMBIENT_TEMPERATURE";
        public static final String TYPE_MAGNETIC_FIELD_UNCALIBRATED = "TYPE_MAGNETIC_FIELD_UNCALIBRATED";
        public static final String TYPE_GAME_ROTATION_VECTOR = "TYPE_GAME_ROTATION_VECTOR";
        public static final String TYPE_GYROSCOPE_UNCALIBRATED = "TYPE_GYROSCOPE_UNCALIBRATED";
        public static final String TYPE_SIGNIFICANT_MOTION = "TYPE_SIGNIFICANT_MOTION";
        public static final String TYPE_STEP_DETECTOR = "TYPE_STEP_DETECTOR";
        public static final String TYPE_STEP_COUNTER = "TYPE_STEP_COUNTER";
        public static final String TYPE_GEOMAGNETIC_ROTATION_VECTOR = "TYPE_GEOMAGNETIC_ROTATION_VECTOR";
        public static final String TYPE_HEART_RATE = "TYPE_HEART_RATE";
        public static final String TYPE_TILT_DETECTOR = "TYPE_TILT_DETECTOR";
        public static final String TYPE_WAKE_GESTURE = "TYPE_WAKE_GESTURE";
        public static final String TYPE_GLANCE_GESTURE = "TYPE_GLANCE_GESTURE";
        public static final String TYPE_PICK_UP_GESTURE = "TYPE_PICK_UP_GESTURE";
        public static final String TYPE_WRIST_TILT_GESTURE = "TYPE_WRIST_TILT_GESTURE";
        public static final String TYPE_DEVICE_ORIENTATION = "TYPE_DEVICE_ORIENTATION";
        public static final String TYPE_POSE_6DOF = "TYPE_POSE_6DOF";
        public static final String TYPE_STATIONARY_DETECT = "TYPE_STATIONARY_DETECT";
        public static final String TYPE_MOTION_DETECT = "TYPE_MOTION_DETECT";
        public static final String TYPE_HEART_BEAT = "TYPE_HEART_BEAT";
        public static final String TYPE_DYNAMIC_SENSOR_META = "TYPE_DYNAMIC_SENSOR_META";
        public static final String FEATURE_CAMERA = "FEATURE_CAMERA";
        public static final String FEATURE_CAMERA_FRONT = "FEATURE_CAMERA_FRONT";
        public static final String FEATURE_CAMERA_FLASH = "FEATURE_CAMERA_FLASH";
        public static final String FEATURE_MICROPHONE = "FEATURE_MICROPHONE";
        public static final String FEATURE_LOCATION = "FEATURE_LOCATION";
        public static final String FEATURE_LOCATION_GPS = "FEATURE_LOCATION_GPS";
        public static final String FEATURE_LOCATION_NETWORK = "FEATURE_LOCATION_NETWORK";

    }
}

