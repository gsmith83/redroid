package net.redroid.redroidcorelibrary.rules;

public class Action {
    public boolean notification;
    public String text, email;

    public Action(boolean notification, String text, String email) {
        this.notification = notification;
        this.text = text;
        this.email = email;
    }
}