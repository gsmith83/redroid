package net.redroid.redroidcorelibrary.rules;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

/**
 * For representing and storing a rules-engine rule
 */
public class Rule {

    public String token, device_id, config_id, rule_id, rule_name;
    public Trigger[] triggers;
    public Action action;
    public boolean is_set;


    public Rule(String token, String device_id, String config_id, String rule_id, String
            rule_name, Trigger[] triggers, Action action) {
        this.token = token;
        this.device_id = device_id;
        this.config_id = config_id;
        this.rule_id = rule_id;
        this.rule_name = rule_name;
        this.triggers = triggers;
        this.action = action;
        this.is_set = true;
    }

    public static Rule fromJson(String json) {
        Type RuleType = new TypeToken<Rule>(){}.getType();
        //read in the contents to a json object
        Rule rule = new Gson().fromJson(json, RuleType);
        return rule;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }
}




