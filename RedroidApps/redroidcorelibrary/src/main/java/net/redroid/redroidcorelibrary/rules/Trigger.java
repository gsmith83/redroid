package net.redroid.redroidcorelibrary.rules;

public class Trigger {
    public String sensor_type, comparison, ref_start_time, ref_end_time;
    public Double value, ref_lat, ref_lng;

    public Trigger(String sensor_type, String comparison, String ref_start_time, String
            ref_end_time, Double value, Double ref_lat, Double ref_lng) {
        this.sensor_type = sensor_type;
        this.comparison = comparison;
        this.ref_start_time = ref_start_time;
        this.ref_end_time = ref_end_time;
        this.value = value;
        this.ref_lat = ref_lat;
        this.ref_lng = ref_lng;
    }
}