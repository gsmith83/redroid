package net.redroid.redroidcorelibrary.sensor;


import android.hardware.Camera;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Scott Wells
 */

public class CameraMetaData{

    ArrayList<resolution> resolutions;

    public CameraMetaData(String cameraType){
        Camera camera;
        resolutions = new ArrayList<>();
        switch(cameraType){
            case "FEATURE_CAMERA_FRONT":
                camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
                break;
            case "FEATURE_CAMERA":
                camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
                break;
            default:
                camera = null;
        }
        List<Camera.Size> sizes = camera.getParameters().getSupportedPreviewSizes();
        for (Camera.Size size: sizes)
            resolutions.add(new resolution(size.width, size.height));
        // release the camera
        camera.release();
    }

    public static CameraMetaData fromJson(String jsonString) {
        Type cameraMetaDataType = new TypeToken<CameraMetaData>(){}.getType();
        return new Gson().fromJson(jsonString, cameraMetaDataType);
    }

    public String toJSONString(){
        return new Gson().toJson(this);
    }

    public class resolution{
        int width;
        int height;
        public resolution(int width, int height){
            this.width = width;
            this.height = height;
        }
    }
}
