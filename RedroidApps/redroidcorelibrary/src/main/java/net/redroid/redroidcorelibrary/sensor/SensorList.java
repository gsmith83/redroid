package net.redroid.redroidcorelibrary.sensor;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import net.redroid.redroidcorelibrary.App;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static android.content.Context.SENSOR_SERVICE;

/**
 * This class is used to make an object representing the sensors and features available
 * on a device.
 * Created by Scott Wells
 */
public class SensorList {

    public ArrayList<SensorMetaData> sensors;

    /**
     * Builds a new SensorList given an activity's context. Populates all needed hardware
     * metadata for the peripheral and packages in this list.
     */
    public SensorList() {
        SensorManager sm = (SensorManager) App.getAppContext().getSystemService(SENSOR_SERVICE);
        sensors = new ArrayList<>();

        // the default type for each type type found on the device is translated into a Redroid
        // sensorMetaData object
        for (int i = 0; i < 32; i++) {
            Sensor androidSensor = sm.getDefaultSensor(i+1);
            if (androidSensor == null)
                continue;
            sensors.add(new SensorMetaData(androidSensor));
        }
    }

    /**
     * Builds
     * new SensorList object given a JSON string built from toJSONString()
     * @param jsonString
     * @deprecated use SensorList.fromJson() instead
     */
    @Deprecated
    public SensorList(String jsonString){
        this.sensors = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        builder.disableHtmlEscaping();
        Gson gsonExt = builder.create();
        String[] gsonList = gsonExt.fromJson(jsonString, String[].class);
        for(String sensorJSON: gsonList){
            String description = "";
            try {
                // look into the JSON Object for the class name (to correctly cast)
                JSONObject obj = new JSONObject(sensorJSON);
                description = obj.getString("type");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Object gsonObj = gsonExt.fromJson(sensorJSON, getClassFromDescription(description));
            this.sensors.add((SensorMetaData) castToRuntimeMetaDataType(gsonObj, description));
        }
    }
    public static SensorList fromJson(String jsonString) {
        Type SensorListType = new TypeToken<SensorList>(){}.getType();
        return new Gson().fromJson(jsonString, SensorListType);
    }

    /**
     * Casts the object to the correct subclass of MetaData given the full class path name
     * @param obj Object to be casted
     * @param description Full path of the class
     * @return The casted MetaData object
     */
    private MetaData castToRuntimeMetaDataType(Object obj, String description){
        MetaData metadata = null;
        switch (description){
            case "TYPE_ACCELEROMETER":
            case "TYPE_MAGNETIC_FIELD":
            case "TYPE_ORIENTATION":
            case "TYPE_GYROSCOPE":
            case "TYPE_LIGHT":
            case "TYPE_PRESSURE":
            case "TYPE_TEMPERATURE":
            case "TYPE_PROXIMITY":
            case "TYPE_GRAVITY":
            case "TYPE_LINEAR_ACCELERATION":
            case "TYPE_ROTATION_VECTOR":
            case "TYPE_RELATIVE_HUMIDITY":
            case "TYPE_AMBIENT_TEMPERATURE":
            case "TYPE_MAGNETIC_FIELD_UNCALIBRATED":
            case "TYPE_GAME_ROTATION_VECTOR":
            case "TYPE_GYROSCOPE_UNCALIBRATED":
            case "TYPE_SIGNIFICANT_MOTION":
            case "TYPE_STEP_DETECTOR":
            case "TYPE_STEP_COUNTER":
            case "TYPE_GEOMAGNETIC_ROTATION_VECTOR":
            case "TYPE_HEART_RATE":
            case "TYPE_TILT_DETECTOR":
            case "TYPE_WAKE_GESTURE":
            case "TYPE_GLANCE_GESTURE":
            case "TYPE_PICK_UP_GESTURE":
            case "TYPE_WRIST_TILT_GESTURE":
            case "TYPE_DEVICE_ORIENTATION":
            case "TYPE_POSE_6DOF":
            case "TYPE_STATIONARY_DETECT":
            case "TYPE_MOTION_DETECT":
            case "TYPE_HEART_BEAT":
            case "TYPE_DYNAMIC_SENSOR_META":
                metadata = (SensorMetaData) obj;
                break;
            case "CameraMetaData":
                //metadata = (CameraMetaData) obj;
                break;
        }
        return metadata;
    }

    private Class getClassFromDescription (String description){
        switch (description){
            case "TYPE_ACCELEROMETER":
            case "TYPE_MAGNETIC_FIELD":
            case "TYPE_ORIENTATION":
            case "TYPE_GYROSCOPE":
            case "TYPE_LIGHT":
            case "TYPE_PRESSURE":
            case "TYPE_TEMPERATURE":
            case "TYPE_PROXIMITY":
            case "TYPE_GRAVITY":
            case "TYPE_LINEAR_ACCELERATION":
            case "TYPE_ROTATION_VECTOR":
            case "TYPE_RELATIVE_HUMIDITY":
            case "TYPE_AMBIENT_TEMPERATURE":
            case "TYPE_MAGNETIC_FIELD_UNCALIBRATED":
            case "TYPE_GAME_ROTATION_VECTOR":
            case "TYPE_GYROSCOPE_UNCALIBRATED":
            case "TYPE_SIGNIFICANT_MOTION":
            case "TYPE_STEP_DETECTOR":
            case "TYPE_STEP_COUNTER":
            case "TYPE_GEOMAGNETIC_ROTATION_VECTOR":
            case "TYPE_HEART_RATE":
            case "TYPE_TILT_DETECTOR":
            case "TYPE_WAKE_GESTURE":
            case "TYPE_GLANCE_GESTURE":
            case "TYPE_PICK_UP_GESTURE":
            case "TYPE_WRIST_TILT_GESTURE":
            case "TYPE_DEVICE_ORIENTATION":
            case "TYPE_POSE_6DOF":
            case "TYPE_STATIONARY_DETECT":
            case "TYPE_MOTION_DETECT":
            case "TYPE_HEART_BEAT":
            case "TYPE_DYNAMIC_SENSOR_META":
                return SensorMetaData.class;
            case "CameraMetaData":
                return CameraMetaData.class;
        }
        return null;
    }

    /**
     * Builds a JSON string representation of the SensorList object. Resulting
     * string can be used to build a copy of this SensorList object by calling SensorList.fromJson()
     * @return A JSON formatted string
     */
    public String toJSONString(){
        return new Gson().toJson(this);
//        String[] sensorJSONStrings = new String[this.sensors.size()];
//        for (int x = 0; x < sensorJSONStrings.length; x++)
//            sensorJSONStrings[x] = this.sensors.get(x).toJSONString();
//        return RedroidUtil.buildJSONString(sensorJSONStrings, String[].class);
    }

    public boolean has(String name){
        // check in each metadata object for the type
        for(MetaData metaData: sensors)
            if (metaData.type.equals(name))
                return true;
        return false;
    }

    public float getMaxRange(String name) {
        for (MetaData metaData: sensors) {
            if (metaData.type.equals(name)){
                //make sure it's a true android type with a maxrange
                if (metaData instanceof SensorMetaData) {
                    return ((SensorMetaData)metaData).max_range;
                }
                else
                    return -1f;
            }
        }
        return -1;
    }
}
