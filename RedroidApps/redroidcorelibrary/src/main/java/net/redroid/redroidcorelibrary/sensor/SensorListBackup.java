package net.redroid.redroidcorelibrary.sensor;

import java.util.List;
import java.util.Locale;


/**
 * This class is used to make an object representing the sensors and features available
 * on a device using public boolean constants.
 */
public class SensorListBackup {

    public final static int NUMBER = 39;

    public final boolean TYPE_ACCELEROMETER;
    public final boolean TYPE_MAGNETIC_FIELD;
    public final boolean TYPE_ORIENTATION;
    public final boolean TYPE_GYROSCOPE;
    public final boolean TYPE_LIGHT;
    public final boolean TYPE_PRESSURE;
    public final boolean TYPE_TEMPERATURE;
    public final boolean TYPE_PROXIMITY;
    public final boolean TYPE_GRAVITY;
    public final boolean TYPE_LINEAR_ACCELERATION;
    public final boolean TYPE_ROTATION_VECTOR;
    public final boolean TYPE_RELATIVE_HUMIDITY;
    public final boolean TYPE_AMBIENT_TEMPERATURE;
    public final boolean TYPE_MAGNETIC_FIELD_UNCALIBRATED;
    public final boolean TYPE_GAME_ROTATION_VECTOR;
    public final boolean TYPE_GYROSCOPE_UNCALIBRATED;
    public final boolean TYPE_SIGNIFICANT_MOTION;
    public final boolean TYPE_STEP_DETECTOR;
    public final boolean TYPE_STEP_COUNTER;
    public final boolean TYPE_GEOMAGNETIC_ROTATION_VECTOR;
    public final boolean TYPE_HEART_RATE;
    public final boolean TYPE_TILT_DETECTOR;
    public final boolean TYPE_WAKE_GESTURE;
    public final boolean TYPE_GLANCE_GESTURE;
    public final boolean TYPE_PICK_UP_GESTURE;
    public final boolean TYPE_WRIST_TILT_GESTURE;
    public final boolean TYPE_DEVICE_ORIENTATION;
    public final boolean TYPE_POSE_6DOF;
    public final boolean TYPE_STATIONARY_DETECT;
    public final boolean TYPE_MOTION_DETECT;
    public final boolean TYPE_HEART_BEAT;
    public final boolean TYPE_DYNAMIC_SENSOR_META;
    public final boolean FEATURE_CAMERA;
    public final boolean FEATURE_CAMERA_FRONT;
    public final boolean FEATURE_CAMERA_FLASH;
    public final boolean FEATURE_MICROPHONE;
    public final boolean FEATURE_LOCATION;
    public final boolean FEATURE_LOCATION_GPS;
    public final boolean FEATURE_LOCATION_NETWORK;


    /**
     * Constructor from list
     * @param list the List object containing the values
     * @throws IllegalArgumentException if the NUMBER of values in the list
     * does not exactly match the NUMBER of final variables to be set in the constructor.
     */
    public SensorListBackup(List<Boolean> list) throws IllegalArgumentException {
        if (list.size() != NUMBER) {
            throw new IllegalArgumentException(
                    String.format(Locale.US,
                            "FeatureVector constructor requires an argument with %d elements.",
                            NUMBER));
        }
        Boolean[] array = (Boolean[])list.toArray();
        TYPE_ACCELEROMETER = array[0];
        TYPE_MAGNETIC_FIELD = array[1];
        TYPE_ORIENTATION = array[2];
        TYPE_GYROSCOPE = array[3];
        TYPE_LIGHT = array[4];
        TYPE_PRESSURE = array[5];
        TYPE_TEMPERATURE = array[6];
        TYPE_PROXIMITY = array[7];
        TYPE_GRAVITY = array[8];
        TYPE_LINEAR_ACCELERATION = array[9];
        TYPE_ROTATION_VECTOR = array[10];
        TYPE_RELATIVE_HUMIDITY = array[11];
        TYPE_AMBIENT_TEMPERATURE = array[12];
        TYPE_MAGNETIC_FIELD_UNCALIBRATED = array[13];
        TYPE_GAME_ROTATION_VECTOR = array[14];
        TYPE_GYROSCOPE_UNCALIBRATED = array[15];
        TYPE_SIGNIFICANT_MOTION = array[16];
        TYPE_STEP_DETECTOR = array[17];
        TYPE_STEP_COUNTER = array[18];
        TYPE_GEOMAGNETIC_ROTATION_VECTOR = array[19];
        TYPE_HEART_RATE = array[20];
        TYPE_TILT_DETECTOR = array[21];
        TYPE_WAKE_GESTURE = array[22];
        TYPE_GLANCE_GESTURE = array[23];
        TYPE_PICK_UP_GESTURE = array[24];
        TYPE_WRIST_TILT_GESTURE = array[25];
        TYPE_DEVICE_ORIENTATION = array[26];
        TYPE_POSE_6DOF = array[27];
        TYPE_STATIONARY_DETECT = array[28];
        TYPE_MOTION_DETECT = array[29];
        TYPE_HEART_BEAT = array[30];
        TYPE_DYNAMIC_SENSOR_META = array[31];
        FEATURE_CAMERA = array[32];
        FEATURE_CAMERA_FRONT = array[33];
        FEATURE_CAMERA_FLASH = array[34];
        FEATURE_MICROPHONE = array[35];
        FEATURE_LOCATION = array[36];
        FEATURE_LOCATION_GPS = array[37];
        FEATURE_LOCATION_NETWORK = array[38];
    }


    /**
     * Constructor from array
     * @param array the array of containing the values
     * @throws IllegalArgumentException if the NUMBER of values in the array
     * does not exactly match the NUMBER of final variables to be set in the constructor.
     */
    public SensorListBackup(boolean[] array) {
        if (array.length != NUMBER) {
            throw new IllegalArgumentException(
                    String.format(Locale.US,
                            "FeatureVector constructor requires an argument with %d elements.",
                            NUMBER));
        }
        TYPE_ACCELEROMETER = array[0];
        TYPE_MAGNETIC_FIELD = array[1];
        TYPE_ORIENTATION = array[2];
        TYPE_GYROSCOPE = array[3];
        TYPE_LIGHT = array[4];
        TYPE_PRESSURE = array[5];
        TYPE_TEMPERATURE = array[6];
        TYPE_PROXIMITY = array[7];
        TYPE_GRAVITY = array[8];
        TYPE_LINEAR_ACCELERATION = array[9];
        TYPE_ROTATION_VECTOR = array[10];
        TYPE_RELATIVE_HUMIDITY = array[11];
        TYPE_AMBIENT_TEMPERATURE = array[12];
        TYPE_MAGNETIC_FIELD_UNCALIBRATED = array[13];
        TYPE_GAME_ROTATION_VECTOR = array[14];
        TYPE_GYROSCOPE_UNCALIBRATED = array[15];
        TYPE_SIGNIFICANT_MOTION = array[16];
        TYPE_STEP_DETECTOR = array[17];
        TYPE_STEP_COUNTER = array[18];
        TYPE_GEOMAGNETIC_ROTATION_VECTOR = array[19];
        TYPE_HEART_RATE = array[20];
        TYPE_TILT_DETECTOR = array[21];
        TYPE_WAKE_GESTURE = array[22];
        TYPE_GLANCE_GESTURE = array[23];
        TYPE_PICK_UP_GESTURE = array[24];
        TYPE_WRIST_TILT_GESTURE = array[25];
        TYPE_DEVICE_ORIENTATION = array[26];
        TYPE_POSE_6DOF = array[27];
        TYPE_STATIONARY_DETECT = array[28];
        TYPE_MOTION_DETECT = array[29];
        TYPE_HEART_BEAT = array[30];
        TYPE_DYNAMIC_SENSOR_META = array[31];
        FEATURE_CAMERA = array[32];
        FEATURE_CAMERA_FRONT = array[33];
        FEATURE_CAMERA_FLASH = array[34];
        FEATURE_MICROPHONE = array[35];
        FEATURE_LOCATION = array[36];
        FEATURE_LOCATION_GPS = array[37];
        FEATURE_LOCATION_NETWORK = array[38];
    }

    /**
     * Constructor which sets all variables to a single value
     * @param value the value to be set -- all false, or all true
     */
    public SensorListBackup(boolean value) {
        TYPE_ACCELEROMETER = value;
        TYPE_MAGNETIC_FIELD = value;
        TYPE_ORIENTATION = value;
        TYPE_GYROSCOPE = value;
        TYPE_LIGHT = value;
        TYPE_PRESSURE = value;
        TYPE_TEMPERATURE = value;
        TYPE_PROXIMITY = value;
        TYPE_GRAVITY = value;
        TYPE_LINEAR_ACCELERATION = value;
        TYPE_ROTATION_VECTOR = value;
        TYPE_RELATIVE_HUMIDITY = value;
        TYPE_AMBIENT_TEMPERATURE = value;
        TYPE_MAGNETIC_FIELD_UNCALIBRATED = value;
        TYPE_GAME_ROTATION_VECTOR = value;
        TYPE_GYROSCOPE_UNCALIBRATED = value;
        TYPE_SIGNIFICANT_MOTION = value;
        TYPE_STEP_DETECTOR = value;
        TYPE_STEP_COUNTER = value;
        TYPE_GEOMAGNETIC_ROTATION_VECTOR = value;
        TYPE_HEART_RATE = value;
        TYPE_TILT_DETECTOR = value;
        TYPE_WAKE_GESTURE = value;
        TYPE_GLANCE_GESTURE = value;
        TYPE_PICK_UP_GESTURE = value;
        TYPE_WRIST_TILT_GESTURE = value;
        TYPE_DEVICE_ORIENTATION = value;
        TYPE_POSE_6DOF = value;
        TYPE_STATIONARY_DETECT = value;
        TYPE_MOTION_DETECT = value;
        TYPE_HEART_BEAT = value;
        TYPE_DYNAMIC_SENSOR_META = value;
        FEATURE_CAMERA = value;
        FEATURE_CAMERA_FRONT = value;
        FEATURE_CAMERA_FLASH = value;
        FEATURE_MICROPHONE = value;
        FEATURE_LOCATION = value;
        FEATURE_LOCATION_GPS = value;
        FEATURE_LOCATION_NETWORK = value;
    }

    /**
     * Returns an array of booleans containing the field values of the object
     * @return a boolean array
     */
    public boolean[] toArray() {
        return new boolean[]{
                TYPE_ACCELEROMETER,
                TYPE_MAGNETIC_FIELD,
                TYPE_ORIENTATION,
                TYPE_GYROSCOPE,
                TYPE_LIGHT,
                TYPE_PRESSURE,
                TYPE_TEMPERATURE,
                TYPE_PROXIMITY,
                TYPE_GRAVITY,
                TYPE_LINEAR_ACCELERATION,
                TYPE_ROTATION_VECTOR,
                TYPE_RELATIVE_HUMIDITY,
                TYPE_AMBIENT_TEMPERATURE,
                TYPE_MAGNETIC_FIELD_UNCALIBRATED,
                TYPE_GAME_ROTATION_VECTOR,
                TYPE_GYROSCOPE_UNCALIBRATED,
                TYPE_SIGNIFICANT_MOTION,
                TYPE_STEP_DETECTOR,
                TYPE_STEP_COUNTER,
                TYPE_GEOMAGNETIC_ROTATION_VECTOR,
                TYPE_HEART_RATE,
                TYPE_TILT_DETECTOR,
                TYPE_WAKE_GESTURE,
                TYPE_GLANCE_GESTURE,
                TYPE_PICK_UP_GESTURE,
                TYPE_WRIST_TILT_GESTURE,
                TYPE_DEVICE_ORIENTATION,
                TYPE_POSE_6DOF,
                TYPE_STATIONARY_DETECT,
                TYPE_MOTION_DETECT,
                TYPE_HEART_BEAT,
                TYPE_DYNAMIC_SENSOR_META,
                FEATURE_CAMERA,
                FEATURE_CAMERA_FRONT,
                FEATURE_CAMERA_FLASH,
                FEATURE_MICROPHONE,
                FEATURE_LOCATION,
                FEATURE_LOCATION_GPS,
                FEATURE_LOCATION_NETWORK,
        };
    }

    /**
     * Returns a JSON string representing the object
     * @return
     */
    public String toJson() {
        String out = "{\n";
        out += String.format("\t\"TYPE_ACCELEROMETER\" : \"%s\",\n", TYPE_ACCELEROMETER);
        out += String.format("\t\"TYPE_MAGNETIC_FIELD\" : \"%s\",\n", TYPE_MAGNETIC_FIELD);
        out += String.format("\t\"TYPE_ORIENTATION\" : \"%s\",\n", TYPE_ORIENTATION);
        out += String.format("\t\"TYPE_GYROSCOPE\" : \"%s\",\n", TYPE_GYROSCOPE);
        out += String.format("\t\"TYPE_LIGHT\" : \"%s\",\n", TYPE_LIGHT);
        out += String.format("\t\"TYPE_PRESSURE\" : \"%s\",\n", TYPE_PRESSURE);
        out += String.format("\t\"TYPE_TEMPERATURE\" : \"%s\",\n", TYPE_TEMPERATURE);
        out += String.format("\t\"TYPE_PROXIMITY\" : \"%s\",\n", TYPE_PROXIMITY);
        out += String.format("\t\"TYPE_GRAVITY\" : \"%s\",\n", TYPE_GRAVITY);
        out += String.format("\t\"TYPE_LINEAR_ACCELERATION\" : \"%s\",\n", TYPE_LINEAR_ACCELERATION);
        out += String.format("\t\"TYPE_ROTATION_VECTOR\" : \"%s\",\n", TYPE_ROTATION_VECTOR);
        out += String.format("\t\"TYPE_RELATIVE_HUMIDITY\" : \"%s\",\n", TYPE_RELATIVE_HUMIDITY);
        out += String.format("\t\"TYPE_AMBIENT_TEMPERATURE\" : \"%s\",\n", TYPE_AMBIENT_TEMPERATURE);
        out += String.format("\t\"TYPE_MAGNETIC_FIELD_UNCALIBRATED\" : \"%s\",\n", TYPE_MAGNETIC_FIELD_UNCALIBRATED);
        out += String.format("\t\"TYPE_GAME_ROTATION_VECTOR\" : \"%s\",\n", TYPE_GAME_ROTATION_VECTOR);
        out += String.format("\t\"TYPE_GYROSCOPE_UNCALIBRATED\" : \"%s\",\n", TYPE_GYROSCOPE_UNCALIBRATED);
        out += String.format("\t\"TYPE_SIGNIFICANT_MOTION\" : \"%s\",\n", TYPE_SIGNIFICANT_MOTION);
        out += String.format("\t\"TYPE_STEP_DETECTOR\" : \"%s\",\n", TYPE_STEP_DETECTOR);
        out += String.format("\t\"TYPE_STEP_COUNTER\" : \"%s\",\n", TYPE_STEP_COUNTER);
        out += String.format("\t\"TYPE_GEOMAGNETIC_ROTATION_VECTOR\" : \"%s\",\n", TYPE_GEOMAGNETIC_ROTATION_VECTOR);
        out += String.format("\t\"TYPE_HEART_RATE\" : \"%s\",\n", TYPE_HEART_RATE);
        out += String.format("\t\"TYPE_TILT_DETECTOR\" : \"%s\",\n", TYPE_TILT_DETECTOR);
        out += String.format("\t\"TYPE_WAKE_GESTURE\" : \"%s\",\n", TYPE_WAKE_GESTURE);
        out += String.format("\t\"TYPE_GLANCE_GESTURE\" : \"%s\",\n", TYPE_GLANCE_GESTURE);
        out += String.format("\t\"TYPE_PICK_UP_GESTURE\" : \"%s\",\n", TYPE_PICK_UP_GESTURE);
        out += String.format("\t\"TYPE_WRIST_TILT_GESTURE\" : \"%s\",\n", TYPE_WRIST_TILT_GESTURE);
        out += String.format("\t\"TYPE_DEVICE_ORIENTATION\" : \"%s\",\n", TYPE_DEVICE_ORIENTATION);
        out += String.format("\t\"TYPE_POSE_6DOF\" : \"%s\",\n", TYPE_POSE_6DOF);
        out += String.format("\t\"TYPE_STATIONARY_DETECT\" : \"%s\",\n", TYPE_STATIONARY_DETECT);
        out += String.format("\t\"TYPE_MOTION_DETECT\" : \"%s\",\n", TYPE_MOTION_DETECT);
        out += String.format("\t\"TYPE_HEART_BEAT\" : \"%s\",\n", TYPE_HEART_BEAT);
        out += String.format("\t\"TYPE_DYNAMIC_SENSOR_META\" : \"%s\",\n", TYPE_DYNAMIC_SENSOR_META);
        out += String.format("\t\"FEATURE_CAMERA\" : \"%s\",\n", FEATURE_CAMERA);
        out += String.format("\t\"FEATURE_CAMERA_FRONT\" : \"%s\",\n", FEATURE_CAMERA_FRONT);
        out += String.format("\t\"FEATURE_CAMERA_FLASH\" : \"%s\",\n", FEATURE_CAMERA_FLASH);
        out += String.format("\t\"FEATURE_MICROPHONE\" : \"%s\",\n", FEATURE_MICROPHONE);
        out += String.format("\t\"FEATURE_LOCATION\" : \"%s\",\n", FEATURE_LOCATION);
        out += String.format("\t\"FEATURE_LOCATION_GPS\" : \"%s\",\n", FEATURE_LOCATION_GPS);
        out += String.format("\t\"FEATURE_LOCATION_NETWORK\" : \"%s\",\n", FEATURE_LOCATION_NETWORK);
        out += "}";
        return out;
    }

}
