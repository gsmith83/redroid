package net.redroid.redroidcorelibrary.sensor;

import net.redroid.redroidcorelibrary.RedroidUtil;

/**
 * A SensorMetaData object represents all of the needed type meta sensors needed by Redroid,
 * for most general sensors.
 */

public class SensorMetaData extends MetaData {

    public float max_range;

    /**
     * Creates a SensorMetaData object given the android API type identification
     * integer and the maximum range value of the type
     * @param type integer type
     * @param maxRange float maximum range
     */
    public SensorMetaData(String type, float maxRange){
        super(type);
        this.max_range = maxRange;
    }

    /**
     * Creates a SensorMetaData object given an android type
     * @param sensor android hardware type object
     *
     */
    public SensorMetaData(android.hardware.Sensor sensor){
        super(RedroidUtil.getSensorType(sensor.getType()));
        this.max_range = sensor.getMaximumRange();
    }

    public String toString(){
        return super.toString() +", MaxRange: " + max_range;
    }

    public String toJSONString(){
        return RedroidUtil.buildJSONString(this, SensorMetaData.class);
    }
}
