/**
 *
 */
package com.michogarcia.mjpegview;

import android.graphics.Bitmap;

/**
 * @author Micho Garcia
 *
 */
public interface ImjpegViewListener {

    void success();

    /**
     * Called by an error occurs
     * @param started indicates whether the stream started successfully
     *                before encountering the error
     */
    void error(Boolean started);

    void hasBitmap(Bitmap bm);
}
