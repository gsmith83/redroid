package net.redroid.redroidmaster;

import android.animation.ValueAnimator;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.View;
import android.widget.ImageView;

import com.michogarcia.mjpegview.ImjpegViewListener;
import com.michogarcia.mjpegview.MjpegView;

import net.redroid.redroidcorelibrary.API.ApiResponseListener;
import net.redroid.redroidcorelibrary.OnSwipeTouchListener;
import net.redroid.redroidmaster.MasterAPI.BLINKER_STATE;
import org.json.JSONObject;


/**
 * Display's the master-half of the bike cam
 */
public class BikeCamActivity extends MasterConfigActivity implements View.OnClickListener {

    private static final String TAG = "Master BikeCamActivity";
    MjpegView mjpegView;
    BikeCamActivity thisActivity = this;
    Boolean flasherOn;
    ImageView leftBlinker, rightBlinker, flasherButton, bellButton;
    ValueAnimator leftBlinkAnimator, rightBlinkAnimator;
    MediaPlayer bellMediaPlayer;
    MasterDataModel model = MasterDataModel.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setContentView(R.layout.activity_bike_cam);
        model.currentActivity = this;

        //start the peripheral bike cam activity
        model.api.requestPostStartBikeCam(new ApiResponseListener() {
            @Override
            public void onApiResponse(int responseCode, JSONObject response) {
                if (responseCode != 200) {
                    tearDown();
                    invokeHomeActivity("Peripheral bike cam failed to start");
                    Log.e(TAG, "Master: start_bike_cam call failed");
                }
                else {
                    Log.d(TAG, "Master: start_bike_cam call succeeded");
                }
            }
        });

        mjpegView = (MjpegView)findViewById(R.id.mjpeg_view);
        mjpegView.setListener(new ImjpegViewListener() {
            @Override
            public void hasBitmap(Bitmap bm) {}

            @Override
            public void success() {}

            @Override
            public void error(Boolean started) {
                if (thisActivity != null) {
                    if (started) {
                        Log.d(TAG, "ImjpegViewListener: bike cam stream terminated");
                        invokeHomeActivity("The bike cam video stream terminated.");
                    }
                    else {
                        Log.d(TAG, "ImjpegViewListener: bike cam stream not found");
                        invokeHomeActivity("The bike cam video stream was not found.");
                    }
                }
            }
        });

        //set initial flasher state
        flasherOn = false;

        //get handles to UI elements
        leftBlinker = (ImageView)findViewById(R.id.left_blinker);
        rightBlinker = (ImageView)findViewById(R.id.right_blinker);
        flasherButton = (ImageView)findViewById(R.id.flasher_button);
        bellButton = (ImageView)findViewById(R.id.bell_button);

        // set listeners
        flasherButton.setOnClickListener(this);
        bellButton.setOnClickListener(this);
        leftBlinker.setOnClickListener(this);
        rightBlinker.setOnClickListener(this);

        //set up the value animators for the blinkers
        leftBlinkAnimator = ValueAnimator.ofInt(1, 100);
        leftBlinkAnimator.setRepeatCount(ValueAnimator.INFINITE);
        leftBlinkAnimator.setRepeatMode(ValueAnimator.RESTART);
        leftBlinkAnimator.setDuration(1000);
        leftBlinkAnimator.addUpdateListener(new BlinkerUpdateListener());

        rightBlinkAnimator = ValueAnimator.ofInt(1, 100);
        rightBlinkAnimator.setRepeatCount(ValueAnimator.INFINITE);
        rightBlinkAnimator.setRepeatMode(ValueAnimator.RESTART);
        rightBlinkAnimator.setDuration(1000);
        rightBlinkAnimator.addUpdateListener(new BlinkerUpdateListener());

        findViewById(R.id.center_control_area).setOnTouchListener(
                new OnSwipeTouchListener(getApplicationContext()) {
                    @Override
                    public void onSwipeLeft() {
                        toggleBlinker(BLINKER_STATE.LEFT);
                    }
                    @Override
                    public void onSwipeRight() {
                        toggleBlinker(BLINKER_STATE.RIGHT);
                    }
                }
        );

        findViewById(R.id.center_control_area).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleBlinker(BLINKER_STATE.OFF);
            }
        });

        bellMediaPlayer = MediaPlayer.create(this, R.raw.bike_bell_sound);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mjpegView.setDisplayMode(MjpegView.SIZE_BEST_FIT);
        startStream();
    }

    @Override
    protected void onPause(){
        super.onPause();
    }


    public void onClick(View v) {

        if (v == flasherButton) {
            final ImageView view = (ImageView)v;
            view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);

            //flasher is on
            if (flasherOn) {
                //turn it off
                ((ImageView)v).setImageResource(R.drawable.flasher_white_50dp);
                flasherOn = false;

            }
            //flasher is off
            else {
                //turn it on
                ((ImageView)v).setImageResource(R.drawable.flasher_red_50dp);
                flasherOn = true;
            }
            //make the API call
            model.api.requestPostBikeCamStateFlasher(flasherOn, new ApiResponseListener() {
                @Override
                public void onApiResponse(int responseCode, JSONObject response) {
                    //TODO: reverse the action if API response is negative
                    Log.d(this.getClass().getName(),
                            "Received toggleBlinker response code {" + responseCode + "} json:  " +
                                    ((response == null)? "": response.toString()));
                }
            });
        }
        else if (v == bellButton) {
            // play at default sound levels
//            AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//            int prevVol = am.getStreamVolume(AudioManager.STREAM_MUSIC);
//            am.setStreamVolume(
//                    AudioManager.STREAM_MUSIC,
//                    am.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
//                    0);
            bellMediaPlayer.setVolume(1.0f, 1.0f);
            bellMediaPlayer.start();

//            am.setStreamVolume(AudioManager.STREAM_MUSIC, prevVol, 0);
        }
        else if (v == leftBlinker) {
            toggleBlinker(BLINKER_STATE.LEFT);
        }
        else if (v == rightBlinker) {
            toggleBlinker(BLINKER_STATE.RIGHT);
        }
    }

    /**
     * Sets the blinker state
     * @param state the stae of the blinkeer (LEFT, RIGHT, OFF)
     */
    private void toggleBlinker(BLINKER_STATE state) {

        BLINKER_STATE endState = BLINKER_STATE.OFF;

        switch(state) {
            case RIGHT:
                if (leftBlinkAnimator.isStarted()){
                    leftBlinkAnimator.cancel();
                    leftBlinker.setVisibility(View.INVISIBLE);
                }
                if(rightBlinkAnimator.isStarted()) {
                    rightBlinkAnimator.cancel();
                    rightBlinker.setVisibility(View.INVISIBLE);
                } else {
                    rightBlinkAnimator.start();
                    endState = BLINKER_STATE.RIGHT;
                }
                break;
            case LEFT:
                if (rightBlinkAnimator.isStarted()) {
                    rightBlinkAnimator.cancel();
                    rightBlinker.setVisibility(View.INVISIBLE);
                }
                if(leftBlinkAnimator.isStarted()) {
                    leftBlinkAnimator.cancel();
                    leftBlinker.setVisibility(View.INVISIBLE);
                } else {
                    leftBlinkAnimator.start();
                    endState = BLINKER_STATE.LEFT;
                }
                break;
            case OFF:
                if (rightBlinkAnimator.isStarted()) {
                    rightBlinkAnimator.cancel();
                    rightBlinker.setVisibility(View.INVISIBLE);
                }
                if (leftBlinkAnimator.isStarted()){
                    leftBlinkAnimator.cancel();
                    leftBlinker.setVisibility(View.INVISIBLE);
                }
                break;
        }
        //make the API call
        model.api.requestPostBikeCamSignal(endState, new ApiResponseListener() {
            @Override
            public void onApiResponse(int responseCode, JSONObject response) {
                //TODO: reverse if API call fails
                Log.d(this.getClass().getName(),
                        "Received toggleBlinker response code {" + responseCode + "} json:  " +
                                ((response == null)? "": response.toString()));
            }
        });
    }

    /**
     * A listener for managing the animation of the left and right blinker indicators.
     * To be registered with leftBlinkerAnimator and righBlinkerAnimator using addListener.
     */
    private class BlinkerUpdateListener implements ValueAnimator.AnimatorUpdateListener {
        /**
         * Called whenever a registered ValueAnimator is updated
         * @param animator the animator making the call
         */
        @Override
        public void onAnimationUpdate(ValueAnimator animator) {
            int value = (Integer) animator.getAnimatedValue();
            ImageView blinker;

            //which blinker is being updated?
            if (animator == leftBlinkAnimator) {
                blinker = leftBlinker;
            }
            else {
                blinker = rightBlinker;
            }

            //what state should the blinker be in?
            if (value < 51) {
                blinker.setVisibility(View.INVISIBLE);
            }
            else {
                blinker.setVisibility(View.VISIBLE);
            }
        }
    }


    /**
     * Starts the MJPEG stream
     */
    private void startStream() {
        AsyncTask<Object, Void, Void> task = new AsyncTask<Object, Void, Void>() {
            @Nullable
            @Override
            protected Void doInBackground(Object[] params) {

                final String url = "http://" + model.network.getRemoteIpAddress() + ":8080";
                Log.d(this.getClass().getName(), "Attempting video stream from url " + url);
                synchronized (TAG) {
                    try {
                        TAG.wait(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mjpegView.setSource(url);
                    }
                });
                return null;
            }
        };
        task.execute();
    }
}
