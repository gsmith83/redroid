package net.redroid.redroidmaster;

import android.app.Fragment;
import android.hardware.Sensor;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import net.redroid.redroidcorelibrary.API;
import net.redroid.redroidcorelibrary.Configuration;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import net.redroid.redroidcorelibrary.sensor.SensorList;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * An activity for displaying the UI of a custom configuration
 */
public class CustomConfigActivity extends MasterConfigActivity implements OnMapReadyCallback {


    private static final String TAG = CustomConfigActivity.class.getSimpleName();
    MasterDataModel model = MasterDataModel.getInstance();
    Configuration config;
    float strideLengthFeet = 2.5f;
    int totalSteps = 0;
    Date currentTime;
    Date timeOfLastTilt;
    Date timeOfLastMotion;
    GoogleMap map;
    LatLng coords;
    Marker marker;

    //location data
    double lat, lng;

    // Video Params
    final String[] rtmpURL = {null};

    //UI views
    Fragment mapFragment;
    VideoViewFragment videoFragment;
    View videoViewFragmentFrame, mediaDivider, locationFields, latitudeViewGroup,
            longitudeViewGroup,
            altitudeViewGroup, bearingViewGroup, speedViewGroup, accuracyViewGroup,
            mapFragmentFrame, locationDivider, linearAccelerationViewGroup,
            linearAccelerationDivider, lightViewGroup, lightDivider, proximityViewGroup,
            proximityDivider, tempViewGroup, tempDivider, magneticFieldViewGroup,
            magneticFieldDivider, lastDivider, stepCountingViewGroup, stepCountingDivider,
            tiltDetectionViewGroup, tiltDetectionDivider, motionDetectionViewGroup,
            motionDetectionDivider;
    CheckBox muteCheckBox;
    SeekBar linearAccelerationSeekbar, lightSeekbar, proximitySeekbar, tempSeekbar,
            magneticFieldSeekbar;
    TextView mapOverlay, latitudeLabel, longitudeLabel, altitudeLabel, altitudeUnits,
            bearingLabel, bearingUnits, speedLabel, speedUnits, accuracyLabel, accuracyUnits,
            linearAccelerationHeader, linearAccelerationMax,
            linearAccelerationUnits, lightHeader, lightMax, lightUnits, proximityHeader,
            proximityMin, proximityMax, proximityUnits, tempHeader, tempMin, tempMax, tempUnits,
            magneticFieldHeader, magneticFieldMin, magneticFieldMax, magneticFieldUnits,
            stepCountingHeader,
            stepCountLabel, distanceLabel, tiltDetectionHeader, currentTimeTiltLabel, tiltTimeLabel,
            currentTimeMotionLabel, motionDetectionHeader, motionTimeLabel;
    EditText latitudeValue, longitudeValue, altitudeValue, bearingValue, speedValue, accuracyValue,
            linearAccelerationValue, lightValue, proximityValue, tempValue, magneticFieldValue,
            stepCountValue, distanceValue, currentTimeTiltValue, tiltTimeValue,
            currentTimeMotionValue, motionTimeValue;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_config);
        Toolbar toolbar = (Toolbar) findViewById(R.id.custom_ui_toolbar);
        setSupportActionBar(toolbar);
        model.currentActivity = this;

        final Configuration config = super.config;

        //capture and set the title of the activity in the title bar
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(config.getName());

        //get handles to UI elements
        videoViewFragmentFrame = findViewById(R.id.video_view_fragment_frame);
        mapOverlay = (TextView) findViewById(R.id.map_overlay);
        mediaDivider = findViewById(R.id.media_divider);


        mapFragmentFrame = findViewById(R.id.map_fragment_frame);
        mapFragment = getFragmentManager().findFragmentById(R.id.map_fragment);
        locationFields = findViewById(R.id.location_fields);
        locationDivider = findViewById(R.id.location_divider);

        linearAccelerationDivider = findViewById(R.id.linear_acceleration_divider);
        lightDivider = findViewById(R.id.light_divider);
        proximityDivider = findViewById(R.id.proximity_divider);
        tempDivider = findViewById(R.id.temp_divider);
        magneticFieldDivider = findViewById(R.id.magnetic_field_divider);
        stepCountingDivider = findViewById(R.id.step_counting_divider);
        tiltDetectionDivider = findViewById(R.id.tilt_detection_divider);
        motionDetectionDivider = findViewById(R.id.motion_detection_divider);

        latitudeViewGroup = findViewById(R.id.latitude_viewgroup);
        longitudeViewGroup = findViewById(R.id.longitude_viewgroup);
        altitudeViewGroup = findViewById(R.id.altitude_viewgroup);
        bearingViewGroup = findViewById(R.id.bearing_viewgroup);
        speedViewGroup = findViewById(R.id.speed_viewgroup);
        accuracyViewGroup = findViewById(R.id.accuracy_viewgroup);
        linearAccelerationViewGroup = findViewById(R.id.linear_acceleration_viewgroup);
        lightViewGroup = findViewById(R.id.light_viewgroup);
        proximityViewGroup = findViewById(R.id.proximity_viewgroup);
        tempViewGroup = findViewById(R.id.temp_viewgroup);
        magneticFieldViewGroup = findViewById(R.id.magnetic_field_viewgroup);
        stepCountingViewGroup = findViewById(R.id.step_count_viewgroup);
        tiltDetectionViewGroup = findViewById(R.id.tilt_detection_viewgroup);
        motionDetectionViewGroup = findViewById(R.id.motion_detection_viewgroup);

        linearAccelerationSeekbar = (SeekBar) linearAccelerationViewGroup
                .findViewById(R.id.seekbar);
        lightSeekbar = (SeekBar) lightViewGroup.findViewById(R.id.seekbar);
        proximitySeekbar = (SeekBar) proximityViewGroup.findViewById(R.id.seekbar);
        tempSeekbar = (SeekBar) tempViewGroup.findViewById(R.id.seekbar);
        magneticFieldSeekbar = (SeekBar) magneticFieldViewGroup.findViewById(R.id.seekbar);

        latitudeLabel = (TextView) latitudeViewGroup.findViewById(R.id.label);
        longitudeLabel = (TextView) longitudeViewGroup.findViewById(R.id.label);
        altitudeLabel = (TextView) altitudeViewGroup.findViewById(R.id.label);
        altitudeUnits = (TextView) altitudeViewGroup.findViewById(R.id.units);
        altitudeUnits.setVisibility(VISIBLE);
        bearingLabel = (TextView) bearingViewGroup.findViewById(R.id.label);
        bearingUnits = (TextView) bearingViewGroup.findViewById(R.id.units);
        bearingUnits.setVisibility(VISIBLE);
        speedLabel = (TextView) speedViewGroup.findViewById(R.id.label);
        speedUnits = (TextView) speedViewGroup.findViewById(R.id.units);
        speedUnits.setVisibility(VISIBLE);
        accuracyLabel = (TextView) accuracyViewGroup.findViewById(R.id.label);
        accuracyUnits = (TextView) accuracyViewGroup.findViewById(R.id.units);
        accuracyUnits.setVisibility(VISIBLE);

        linearAccelerationHeader = (TextView) findViewById(R.id.linear_acceleration_header);
        lightHeader = (TextView) findViewById(R.id.light_header);
        proximityHeader = (TextView) findViewById(R.id.proximity_header);
        tempHeader = (TextView) findViewById(R.id.temp_header);
        magneticFieldHeader = (TextView) findViewById(R.id.magnetic_field_header);
        stepCountingHeader = (TextView) findViewById(R.id.step_counting_header);
        tiltDetectionHeader = (TextView) findViewById(R.id.tilt_detection_header);
        motionDetectionHeader = (TextView) findViewById(R.id.motion_detection_header);


        linearAccelerationMax = (TextView) linearAccelerationViewGroup.findViewById(R.id.max);
        lightMax = (TextView) lightViewGroup.findViewById(R.id.max);
        proximityMin = (TextView) proximityViewGroup.findViewById(R.id.min);
        proximityMax = (TextView) proximityViewGroup.findViewById(R.id.max);
        tempMin = (TextView) tempViewGroup.findViewById(R.id.min);
        tempMax = (TextView) tempViewGroup.findViewById(R.id.max);
        magneticFieldMax = (TextView) magneticFieldViewGroup.findViewById(R.id.max);
        magneticFieldMin = (TextView) magneticFieldViewGroup.findViewById(R.id.min);
        stepCountLabel = (TextView) findViewById(R.id.step_count_label);
        distanceLabel = (TextView) findViewById(R.id.distance_label);
        currentTimeTiltLabel = (TextView) findViewById(R.id.current_time_tilt)
                .findViewById(R.id.label);
        currentTimeMotionLabel = (TextView) findViewById(R.id.current_time_motion)
                .findViewById(R.id.label);
        tiltTimeLabel = (TextView) findViewById(R.id.last_tilt_time).findViewById(R.id.label);
        motionTimeLabel = (TextView) findViewById(R.id.last_motion_time).findViewById(R.id.label);

        latitudeValue = (EditText) latitudeViewGroup.findViewById(R.id.value);
        longitudeValue = (EditText) longitudeViewGroup.findViewById(R.id.value);
        altitudeValue = (EditText) altitudeViewGroup.findViewById(R.id.value);
        bearingValue = (EditText) bearingViewGroup.findViewById(R.id.value);
        speedValue = (EditText) speedViewGroup.findViewById(R.id.value);
        accuracyValue = (EditText) accuracyViewGroup.findViewById(R.id.value);
        linearAccelerationValue = (EditText) linearAccelerationViewGroup.findViewById(R.id.value);
        lightValue = (EditText) lightViewGroup.findViewById(R.id.value);
        proximityValue = (EditText) proximityViewGroup.findViewById(R.id.value);
        tempValue = (EditText) tempViewGroup.findViewById(R.id.value);
        magneticFieldValue = (EditText) magneticFieldViewGroup.findViewById(R.id.value);
        stepCountValue = (EditText) findViewById(R.id.step_count_value);
        distanceValue = (EditText) findViewById(R.id.distance_value);
        currentTimeTiltValue = (EditText) findViewById(R.id.current_time_tilt)
                .findViewById(R.id.value);
        tiltTimeValue = (EditText) findViewById(R.id.last_tilt_time).findViewById(R.id.value);
        motionTimeValue = (EditText) findViewById(R.id.last_motion_time).findViewById(R.id.value);


        linearAccelerationUnits = (TextView) linearAccelerationViewGroup
                .findViewById(R.id.units);
        lightUnits = (TextView) lightViewGroup.findViewById(R.id.units);
        proximityUnits = (TextView) proximityViewGroup.findViewById(R.id.units);
        tempUnits = (TextView) tempViewGroup.findViewById(R.id.units);
        magneticFieldUnits = (TextView) magneticFieldViewGroup.findViewById(R.id.units);

        muteCheckBox = (CheckBox) findViewById(R.id.mute_check_box);

        //set visibility of elements from config options
        final Configuration.Options options = config.getOptions();
        final SensorList sensorList = config.sensorList;

        //start the listening socket
        if (options.location || options.magneticField || options.motionDetection ||
                options.tiltDetection || options.stepCounting || options.linearAcceleration ||
                options.light || options.proximity) {
            boolean success = model.network.startMasterEventServer();
            if (!success) {
                invokeHomeActivity("Master event server failed to start.");
            }
        }

        // STREAMING MEDIA?
        //moved to the end, because it is CPU intensive
        if (options.streamingMedia) {
            lastDivider = mediaDivider;
            mediaDivider.setVisibility(VISIBLE);
        }

        // LOCATION DATA?
        //moved near end, because is network intensive(??)
        if (options.location) {
            model.network.startMasterEventServer();
            lastDivider = locationDivider;
            locationDivider.setVisibility(VISIBLE);
        }

        //LINEAR ACCELERATION?
        if (options.linearAcceleration) {
            model.network.startMasterEventServer();
            //reveal the views and set the values
            linearAccelerationHeader.setVisibility(VISIBLE);
            linearAccelerationUnits.setText(R.string.m_per_s2);
            float maxRange = config.sensorList.getMaxRange("TYPE_LINEAR_ACCELERATION");
            linearAccelerationSeekbar.setMax(10 * (int) maxRange);
            linearAccelerationMax.setText(String.format("%.2f", maxRange));
            linearAccelerationSeekbar.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return true; //makes the view un-clickable
                }
            });
            linearAccelerationViewGroup.setVisibility(VISIBLE);
            linearAccelerationDivider.setVisibility(VISIBLE);
            lastDivider = linearAccelerationDivider;

            //make an API call to register for light type events
            model.api.requestPutRegisterSensorEvents(Sensor.TYPE_LINEAR_ACCELERATION, 5000000,
                    false, new MasterAPI.ApiResponseListener() {

                        public void onApiResponse(int responseCode, JSONObject jsonPayload) {
                            //what to do with the immediate response (success or failure)
                            if (responseCode != 200) {
                                Log.d(TAG, "onApiResponse: linear acceleration type event " +
                                        "registration failed");
                                Snackbar.make(CustomConfigActivity.this.findViewById(R.id
                                                .custom_ui_content),
                                        "Linear acceleration type event registration failed",
                                        Snackbar.LENGTH_LONG).show();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        linearAccelerationValue.setText("n/a");
                                        linearAccelerationSeekbar.setEnabled(false);
                                    }
                                });
                            }
                        }
                    },
                    new MasterAPI.ApiEventListener() {
                        @Override
                        public void onApiEvent(String jsonString) {
                            //what do do with an incoming type event

                            try {
                                //extract the values
                                JSONObject jsonObject = new JSONObject(jsonString);
                                JSONArray jsonValueArray = jsonObject.getJSONArray("values");
                                final float[] valueArray = API.getFloatArray(jsonValueArray);

                                //set the light value field to the returned value
                                if (valueArray != null) {
                                    final float value = valueArray[0];
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            linearAccelerationValue.setText(
                                                    String.format(Locale.US, "%.2f", value));
                                            linearAccelerationSeekbar.setProgress(
                                                    (int) (valueArray[0] * 10));
                                        }
                                    });

                                }
                                else {
                                    linearAccelerationValue.setText(String.format("%s", "err"));
                                }
                            }
                            catch (JSONException e) {
                                Log.d("API 13 TYPE_LIGHT_ACCEL", "onApiResponse: " + e.getMessage
                                        ());
                            }
                        }
                    });
        }

        //LIGHT?
        if (options.light) {
            model.network.startMasterEventServer();
            //reveal the views and set the values
            lightHeader.setVisibility(VISIBLE);
            lightUnits.setText(R.string.lux);
            float maxRange = config.sensorList.getMaxRange("TYPE_LIGHT");
            // todo: the following is temporary for a beta demonstration. Figure out why maxRange
            // is not reporting reliable data
//            lightSeekbar.setMax(10 * (int) maxRange);
//            lightMax.setText(String.format("%.0f", maxRange));
            lightSeekbar.setMax((int) 1e6);
            lightMax.setText(String.format("%.0f", 1e5));
            lightSeekbar.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return true; //makes the view un-clickable
                }
            });
            lightViewGroup.setVisibility(VISIBLE);
            lightDivider.setVisibility(VISIBLE);
            lastDivider = lightDivider;

            //make an API call to register for light type events
            model.api.requestPutRegisterSensorEvents(Sensor.TYPE_LIGHT, 5000000, false,
                    new MasterAPI.ApiResponseListener() {
                        public void onApiResponse(int responseCode, JSONObject jsonPayload) {
                            //what to do with the immediate response (success or failure)

                            if (responseCode != 200) {
                                Log.d(TAG, "onApiResponse: light type event registration failed");
                                Snackbar.make(CustomConfigActivity.this.findViewById(R.id
                                                .custom_ui_content),
                                        "Light type event registration failed",
                                        Snackbar.LENGTH_LONG).show();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        lightValue.setText("n/a");
                                        lightSeekbar.setEnabled(false);

                                    }
                                });
                            }
                        }
                    },
                    new MasterAPI.ApiEventListener() {
                        @Override
                        public void onApiEvent(String jsonString) {
                            //what do do with an incoming type event

                            try {
                                //extract the values
                                JSONObject jsonObject = new JSONObject(jsonString);
                                JSONArray jsonValueArray = jsonObject.getJSONArray("values");
                                final float[] valueArray = API.getFloatArray(jsonValueArray);

                                //set the light value field to the returned value
                                if (valueArray != null) {
                                    final float value = valueArray[0];
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            lightValue.setText(String.format(Locale.US, "%.0f",
                                                    value));
                                            lightSeekbar.setProgress((int) (valueArray[0] * 10));
                                        }
                                    });

                                }
                                else {
                                    lightValue.setText(String.format("%s", "err"));
                                }
                            }
                            catch (JSONException e) {
                                Log.d("API 13 TYPE_LIGHT", "onApiResponse: " + e.getMessage());
                            }
                        }
                    });
        }

        //PROXIMITY?
        if (options.proximity) {
            model.network.startMasterEventServer();
            //reveal the views and set the values
            proximityHeader.setVisibility(VISIBLE);
            proximityUnits.setText("");
            proximityMin.setText(R.string.near);
            proximityMax.setText(R.string.far);
            proximitySeekbar.setMax(1);
            proximitySeekbar.setProgress(1);
            proximityValue.setText(R.string.far);
            proximitySeekbar.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return true; //makes the view un-clickable
                }
            });
            proximityViewGroup.setVisibility(VISIBLE);
            proximityDivider.setVisibility(VISIBLE);
            lastDivider = proximityDivider;

            //make an API call to register for proximity type events
            model.api.requestPutRegisterSensorEvents(
                    Sensor.TYPE_PROXIMITY, 5000000,
                    false, new MasterAPI.ApiResponseListener() {

                        public void onApiResponse(int responseCode, JSONObject jsonPayload) {
                            //what to do with the immediate response (success or failure)
                            if (responseCode != 200) {
                                Log.d(TAG, "onApiResponse: proximity type event registration " +
                                        "failed");
                                Snackbar.make(CustomConfigActivity.this.findViewById(R.id
                                                .custom_ui_content),
                                        "Proximity sensor event registration failed",
                                        Snackbar.LENGTH_INDEFINITE)
                                        .setAction("Dismiss", null)
                                        .show();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        proximityValue.setText("n/a");
                                        proximitySeekbar.setEnabled(false);
                                    }
                                });
                            }
                        }
                    },
                    new MasterAPI.ApiEventListener() {
                        @Override
                        public void onApiEvent(String jsonString) {
                            //what do do with an incoming type event

                            try {
                                //extract the values
                                JSONObject jsonObject = new JSONObject(jsonString);
                                JSONArray jsonValueArray = jsonObject.getJSONArray("values");
                                final float[] valueArray = API.getFloatArray(jsonValueArray);

                                //set the proximity value field to the returned value
                                if (valueArray != null) {
                                    final float value = valueArray[0];
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            proximityValue.setText((value == 0) ? "near" : "far");
                                            proximitySeekbar.setProgress((int) (value));
                                        }
                                    });

                                }
                                else {
                                    proximityValue.setText(String.format("%s", "err"));
                                }
                            }
                            catch (JSONException e) {
                                Log.d("API 13 TYPE_PROXIMITY", "onApiResponse: " + e.getMessage());
                            }
                        }
                    });
        }

        //TEMPERATURE?
        if (options.temperature) {
            model.network.startMasterEventServer();
            //reveal the views and set the values
            tempHeader.setVisibility(VISIBLE);
            tempUnits.setText(R.string.degrees_C);
            tempMin.setText("-80.0"); //TODO: replacd with API call
            tempMax.setText("80.0"); //TODO: replace with API call
            tempSeekbar.setMax(800 * 2); //TODO: replace with API call
            tempSeekbar.setProgress(370 + 800); //TODO: replace with API call
            tempValue.setText("37.0"); //TODO: replace with API call
            tempSeekbar.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return true; //makes the view un-clickable
                }
            });
            tempViewGroup.setVisibility(VISIBLE);
            tempDivider.setVisibility(VISIBLE);
            lastDivider = tempDivider;
        }

        //MAGNETIC FIELD?
        if (options.magneticField) {
            model.network.startMasterEventServer();
            //reveal the views and set the values
            magneticFieldHeader.setVisibility(VISIBLE);
            magneticFieldUnits.setText(R.string.micro_tesla);
            float maxRange = config.sensorList.getMaxRange("TYPE_MAGNETIC_FIELD");
            magneticFieldSeekbar.setMax(10 * (int) maxRange);
            magneticFieldMax.setText(String.format("%.0f", maxRange / 2));
            magneticFieldMin.setText(String.format("%.0f", -1 * maxRange / 2));
            magneticFieldSeekbar.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return true; //makes the view un-clickable
                }
            });
            magneticFieldViewGroup.setVisibility(VISIBLE);
            magneticFieldDivider.setVisibility(VISIBLE);
            lastDivider = magneticFieldDivider;

            //make an API call to register for light type events
            model.api.requestPutRegisterSensorEvents(
                    Sensor.TYPE_MAGNETIC_FIELD, 5000000,
                    false, new MasterAPI.ApiResponseListener() {

                        public void onApiResponse(int responseCode, JSONObject jsonPayload) {
                            //what to do with the immediate response (success or failure)
                            if (responseCode != 200) {
                                Log.d(TAG, "onApiResponse: light type event registration failed");
                                Snackbar.make(CustomConfigActivity.this.findViewById(R.id
                                                .custom_ui_content),
                                        "Light type event registration failed",
                                        Snackbar.LENGTH_LONG).show();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        magneticFieldValue.setText("n/a");
                                        magneticFieldSeekbar.setEnabled(false);
                                    }
                                });
                            }
                        }
                    },
                    new MasterAPI.ApiEventListener() {
                        @Override
                        public void onApiEvent(String jsonString) {
                            //what do do with an incoming type event

                            try {
                                //extract the values
                                JSONObject jsonObject = new JSONObject(jsonString);
                                JSONArray jsonValueArray = jsonObject.getJSONArray("values");
                                final float[] valueArray = API.getFloatArray(jsonValueArray);

                                //set the magneticfield value field to the returned value
                                if (valueArray != null) {
                                    final float value = valueArray[0];
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            magneticFieldValue.setText(String
                                                    .format("%.0f", value));
                                            magneticFieldSeekbar.setProgress((int) (
                                                    magneticFieldSeekbar.getMax() / 2
                                                            + valueArray[0] * 10)
                                            );
                                        }
                                    });

                                }
                                else {
                                    magneticFieldValue.setText(String.format("%s", "err"));
                                }
                            }
                            catch (JSONException e) {
                                Log.d("API 13 TYPE_MAG", "onApiResponse: " + e.getMessage());
                            }
                        }
                    });
        }

        //STEP COUNTER
        if (options.stepCounting) {
            model.network.startMasterEventServer();
            //reveal the views and set the values
            stepCountingHeader.setVisibility(VISIBLE);
            stepCountingViewGroup.setVisibility(VISIBLE);
            totalSteps = 2017; //TODO: set dynamically via API
            stepCountValue.setText(String.format(Locale.US, "%d", totalSteps));
            distanceValue.setText(String.format(Locale.US, "%.1f",
                    totalSteps * strideLengthFeet / 5280));
            stepCountingDivider.setVisibility(VISIBLE);
            lastDivider = stepCountingDivider;
        }

        //TILT DETECTION
        if (options.tiltDetection) {
            model.network.startMasterEventServer();
            //reveal the views and set the values
            tiltDetectionHeader.setVisibility(VISIBLE);
            tiltDetectionViewGroup.setVisibility(VISIBLE);
            currentTimeTiltLabel.setText(R.string.current_time);
            tiltTimeLabel.setText(R.string.time_of_last_tilt);
            timeOfLastTilt = Calendar.getInstance().getTime(); //TODO: get this dynamically via
            // API call
            tiltDetectionDivider.setVisibility(VISIBLE);
            lastDivider = tiltDetectionDivider;
            String tiltTime = new SimpleDateFormat("h:mm:ss a (MM/dd)", Locale.US)
                    .format(timeOfLastTilt);
            tiltTimeValue.setText(tiltTime);

        }

        //MOTION DETECTION
        if (options.motionDetection) {
            model.network.startMasterEventServer();
            //reveal the views and set the values
            motionDetectionHeader.setVisibility(VISIBLE);
            motionDetectionViewGroup.setVisibility(VISIBLE);
            currentTimeMotionLabel.setText(R.string.current_time);
            motionTimeLabel.setText(R.string.time_of_last_motion_detected);
            timeOfLastMotion = Calendar.getInstance().getTime(); //TODO: get this dynamically via
            // API call
            motionDetectionDivider.setVisibility(VISIBLE);
            lastDivider = motionDetectionDivider;
            String motionTime = new SimpleDateFormat("hh:mm:ss a (MM/dd)", Locale.US)
                    .format(timeOfLastMotion);
            motionTimeValue.setText(motionTime);
        }

        // LOCATION?
        if (options.location) {
            // has location fields - reveal views and set field values
            if (options.coordinateFields) {
                latitudeLabel.setText(R.string.latitude_abbrev);
                latitudeValue.setText(R.string.empty_field_value);
                longitudeLabel.setText(R.string.longitude_abbrev);
                longitudeValue.setText(R.string.empty_field_value);
                altitudeLabel.setText(R.string.altitude_abbrev);
                altitudeValue.setText(R.string.empty_field_value);
                altitudeUnits.setText(R.string.meters_abbrev);
                bearingLabel.setText(R.string.bearing_abbrev);
                bearingValue.setText(R.string.empty_field_value);
                bearingUnits.setText("°");
                speedLabel.setText(R.string.speed_abbrev);
                speedValue.setText(R.string.empty_field_value);
                speedUnits.setText("mph");
                accuracyLabel.setText(R.string.accuracy_abbrev);
                accuracyValue.setText(R.string.empty_field_value);
                accuracyUnits.setText("ft");
                locationFields.setVisibility(VISIBLE);
            }

            // has a map
            if (options.map) {
                //make it visible
                mapFragmentFrame.setVisibility(VISIBLE);
                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().
                        findFragmentById(R.id.map_fragment);
                mapFragment.getMapAsync(this);
            }

            new Thread(new Runnable() {
                @Override
                public void run() {
                    //get last-known location
                    model.api.requestGetLastLocation(new API.ApiResponseListener() {
                        @Override
                        public void onApiResponse(int responseCode, final JSONObject response) {
                            if (responseCode == 200) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        setLocationData(response);
                                    }
                                });
                            }

                        } //onApiResponse
                    }); //requestGetLastLocation

                    //register for ongoing notifications
                    model.api.requestPutRegisterLocationEvent(5000000, false,
                            new MasterAPI.ApiResponseListener() {


                                public void onApiResponse(int responseCode, JSONObject
                                        jsonPayload) {
                                    //what to do with the immediate response (success or failure)
                                    if (responseCode != 200) {
                                        //hide the fields, put up an overlay on the map, and
                                        // provide a toast
                                        //message explaining about location unavailability
                                        String m = "";
                                        try {
                                            m = jsonPayload.getString("msg");
                                        }
                                        catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        final String responseMsg = m;
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                findViewById(R.id.location_not_available_group)
                                                        .setVisibility(VISIBLE);
                                                mapFragmentFrame.setVisibility(GONE);
                                                locationFields.setVisibility(GONE);
                                                String msg = getResources().getString(
                                                        R.string.location_not_available_dialog_text,
                                                        config.getDeviceId());
                                                new AlertDialog.Builder(CustomConfigActivity.this)
                                                        .setMessage(msg)
                                                        .setTitle(getResources().getString(R.string
                                                                .location_data_not_available))
                                                        .setPositiveButton(R.string.ok, null)
                                                        .create()
                                                        .show();
                                            } //run
                                        }); //runOnUiThread
                                    } //if
                                } //onApiResponse
                            }, //new ApiResponseListener
                            new MasterAPI.ApiEventListener() {
                                @Override
                                public void onApiEvent(String jsonString) {
                                    //what do do with an incoming type event

                                    try {
                                        //extract the values
                                        final JSONObject jsonObject = new JSONObject(jsonString);

                                        //set location data
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                setLocationData(jsonObject);
                                            }
                                        });

                                    }
                                    catch (JSONException e) {
                                        Log.e("API 402", "CustomConfigActivity " + e.getMessage());
                                        e.printStackTrace();
                                    }
                                }
                            });
                }
            }).start();
        }

        // STREAMING MEDIA?
        if (options.streamingMedia) {

            if (options.rtmp) {

                //instantiate the video fragment
                videoFragment = new VideoViewFragment();
                videoFragment.setConfig(config);

                 //set the fragment in its frame and set it to visible
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.video_view_fragment_frame,
                        videoFragment)
                        .commit();
                videoViewFragmentFrame.setVisibility(VISIBLE);
            }
        }
        //eliminate last divider view
        if (lastDivider != null)
            lastDivider.setVisibility(GONE);

    }


    @Override
    public void onBackPressed() {
        synchronized (videoViewFragmentFrame) {
            if (videoFragment != null && videoFragment.isPlaying()) {
                videoFragment.stopPlayback();
                videoFragment = null;
            }
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.battery) {
            model.api.requestGetBattery(new API.ApiResponseListener() {
                @Override
                public void onApiResponse(int responseCode, JSONObject response) {
                    if (responseCode == 200) {
                        float level;
                        boolean charging;
                        try {
                            level = (float) response.getLong("level");
                            charging = response.getBoolean("charging");
                            showBatteryAlert(level, charging);
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                            return;
                        }
                    }
                }
            });
        }
        return super.onOptionsItemSelected(item);
    }

    private void showBatteryAlert(float level, boolean charging) {
        new AlertDialog.Builder(CustomConfigActivity.this)
                .setMessage(String.format("Level: %.0f\n%s", level, charging ? "Charging" : "Not " +
                        "charging"))
                .setTitle("Battery Status")
                .setPositiveButton(R.string.ok, null)
                .create()
                .show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setZoomControlsEnabled(true);
//        updateMap(40.767815, -111.845342); //WEB
        updateMap();
    }

    private synchronized void setLocationData(JSONObject locationJson) {
        try {
            String empty = getResources().getString(R.string.empty_field_value);
            lat = locationJson.getDouble("latitude");
            latitudeValue.setText(String.format("%.5f", lat));
            lng = locationJson.getDouble("longitude");
            longitudeValue.setText(String.format("%.5f", lng));

            double altitude = locationJson.getDouble("altitude");
            if (altitude > 0) {
                altitudeValue.setText(String.format("%.0f", altitude));
                altitudeUnits.setVisibility(VISIBLE);
            }
            else {
                altitudeValue.setText(empty);
                altitudeUnits.setVisibility(GONE);
            }

            float bearing = (float) locationJson.getDouble("bearing");
            if (bearing > 0) {
                bearingValue.setText(String.format("%.0f", bearing));
                bearingUnits.setVisibility(VISIBLE);
            }
            else {
                bearingValue.setText(empty);
                bearingUnits.setVisibility(GONE);
            }

            float speed = 2.23694f * (float) locationJson.getDouble("speed");
            if (speed > 0) {
                speedValue.setText(String.format("%.0f", speed));
                speedUnits.setVisibility(VISIBLE);
            }
            else {
                speedValue.setText(empty);
                speedUnits.setVisibility(GONE);
            }

            float accuracy = 3.28084f * (float) locationJson.getDouble("accuracy");
            if (accuracy > 0) {
                accuracyValue.setText(String.format("%.0f", accuracy));
                accuracyUnits.setVisibility(VISIBLE);
            }
            else {
                accuracyValue.setText(empty);
                accuracyUnits.setVisibility(GONE);
            }
            updateMap();

        }
        catch (JSONException e) {
            Log.e(TAG, "setLocationData: " + e.getMessage());
            e.printStackTrace();
        }
        catch (NullPointerException n) {
        }
    }

    private synchronized void updateMap() {
        if (map != null && lat != 0.0 && lng != 0.0) {
            mapOverlay.setVisibility(GONE);
            coords = new LatLng(lat, lng);
            if (marker != null) {
                marker.remove();
            }
            marker = map.addMarker(new MarkerOptions().position(coords));
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(coords, 17.0f));
        }
    }
}
