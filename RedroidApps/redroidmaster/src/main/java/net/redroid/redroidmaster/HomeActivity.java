package net.redroid.redroidmaster;

import android.content.DialogInterface;
import android.database.DataSetObserver;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import net.redroid.redroidcorelibrary.Configuration;
import net.redroid.redroidcorelibrary.RedroidUtil;
import net.redroid.redroidmaster.rules.NewRuleActivity;
import net.redroid.redroidmaster.rules.RulesActivity;
import android.support.v7.app.AppCompatActivity;
import com.google.firebase.iid.FirebaseInstanceId;
import static net.redroid.redroidcorelibrary.Configuration.NetworkMode;

/**
 * The home screen for the Redroid Master. Contains a listview of config cards.
 */
public class HomeActivity extends AppCompatActivity implements ListAdapter, View.OnClickListener {

    public static final String TAG = "HomeActivity";

    //intent identifier for optional snackbar message to show when home activity is invoked
    public static final String SNACKBAR_MESSAGE = "home_activity_init_snackbar_message";
    public static final String CONFIG = "CONFIG";
    public final int INIT_ACTIVITY_REQUEST_CODE = 0;
    public final int MSG_REQUEST_CODE = 1; //for getting string message result from another activity
    public final static int SERVER_URL_UPDATE_REQUEST_CODE = 2;

    MasterDataModel model = MasterDataModel.getInstance();

    Activity thisActivity = this;
    ListView listView;
    TextView defaultText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        model.currentActivity = this;

        //get UI handles
        listView = (ListView)findViewById(R.id.home_config_list);
        defaultText = (TextView)findViewById(R.id.rule_list_default_text);
        listView.setAdapter(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.home_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                invokeWizardHomeActivity();
            }
        });

        // in case this activity was invoked by another, check for any snackbar messages to display
        String snackbarMessage = getIntent().getStringExtra(SNACKBAR_MESSAGE);
        if (snackbarMessage != null) {
            Snackbar.make(thisActivity.findViewById(R.id.activity_home),
                    snackbarMessage, Snackbar.LENGTH_LONG).show();
            //ensure that this isn't repeated
            getIntent().removeExtra(SNACKBAR_MESSAGE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        model = MasterDataModel.getInstance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.server_url) {
            Intent intent = new Intent(getBaseContext(), ServerUrlActivity.class);
            startActivityForResult(intent, MSG_REQUEST_CODE);
        }
        else if (id == R.id.delete_all) {
            makeDeleteAllConfirmationDialog();
        }
        else if (id == R.id.show_token) {
            String msg = FirebaseInstanceId.getInstance().getToken();
            Snackbar
                    .make(this.findViewById(R.id.activity_home), msg, Snackbar.LENGTH_INDEFINITE)
                    .setAction("DISMISS", null)
                    .show();
        }
        else if (id == R.id.exit_menu_item) {
            System.exit(0);
        }
        return super.onOptionsItemSelected(item);
    }

    /***************** ListAdapter Methods *********************************/
    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {}

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {}

    @Override
    public int getCount() {
        int count =  model.getConfigCount();
        //show default text if there are no configs to show
        if (count == 0) {
            defaultText.setVisibility(View.VISIBLE);
        }
        else {
            defaultText.setVisibility(View.INVISIBLE);
        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        return model.getConfig(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Configuration config = (Configuration)getItem(position);

        if (convertView == null) {
            convertView = getLayoutInflater().inflate(R.layout.viewgroup_config_list_item_card,
                    null);
        }

        //set the config icon
        ImageView configIcon = (ImageView)convertView.findViewById(R.id.config_icon);
        configIcon.setImageResource(config.getIconImageResource());

        //set the configuration name in the view based on name in the Configuration object
        TextView name = (TextView)convertView.findViewById(R.id.config_name);
        name.setText(config.getName());

        TextView deviceId = (TextView)convertView.findViewById(R.id.device_id);
        deviceId.setText(config.getDeviceId());

        //set configuration type label in the view based on config type in the Configuration object
        TextView type = (TextView)convertView.findViewById(R.id.config_type);
        final Configuration.ConfigType configType = config.getConfigType();
        switch (configType) {
            case BABY_MONITOR:
                type.setText(getResources().getText(R.string.baby_monitor));
                break;
            case PET_CAM:
                type.setText(getResources().getText(R.string.pet_cam));
                break;
            case SECURITY_CAM:
                type.setText(getResources().getText(R.string.security_cam));
                break;
            case BIKE_CAM:
                type.setText(getResources().getText(R.string.bike_cam));
                break;
            case CUSTOM:
                type.setText(getResources().getText(R.string.custom));
                break;
        }

        //set networkType label in the view based on network type in the Configuration object
        TextView network = (TextView)convertView.findViewById(R.id.config_network_topology);
        final Configuration.NetworkMode networkMode = config.getNetworkMode();
        switch (networkMode) {
            case WIFI:
                network.setText(getResources().getText(R.string.wifi));
                break;
            case P2P:
                network.setText(getResources().getText(R.string.p2p));
                break;
            case SERVER:
                network.setText(getResources().getText(R.string.server));
                break;
            case WIFI_AND_SERVER:
                network.setText(getResources().getText(R.string.wifi_and_server));
                break;
        }

        //set gesture listeners
        // config card menu
        Toolbar configToolbar = (Toolbar)convertView.findViewById(R.id.config_menu_toolbar);
        configToolbar.getMenu().clear(); //necessary to avoid duplicated menu items from convertView
        configToolbar.inflateMenu(R.menu.menu_config_card);
        //hide the rules stuff
        configToolbar.getMenu().findItem(R.id.rules).setVisible(false);
        configToolbar.getMenu().findItem(R.id.new_rule).setVisible(false);
        //set the listener
        configToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.delete_config) {
                    makeDeleteConfirmationDialog(position);
                }
                else if (id == R.id.change_wifi_ip) {
                    showChangeIpAddressAlertDialog(config);
                }
                else if (id == R.id.change_name) {
                    showChangeNameAlertDialog(config);
                }
                else if (id == R.id.get_config_id) {
                    String msg = config.getConfigId();
                    if (msg == null) return false;
                    Snackbar.make(findViewById(R.id.activity_home),
                            msg, Snackbar.LENGTH_INDEFINITE).show();
                    Log.d(TAG, "Config ID: " + msg);
                    Log.d(TAG, "Token: " + model.firebaseToken);
                }
                else if (id == R.id.new_rule) {
                    Intent intent = new Intent(getBaseContext(), NewRuleActivity.class);
                    intent.putExtra("configId", config.getConfigId());
                    startActivity(intent);
                }
                else if (id == R.id.rules) {
                    Intent intent = new Intent(getBaseContext(), RulesActivity.class);
                    intent.putExtra("configId", config.getConfigId());
                    startActivity(intent);
                }
                else if (id == R.id.red) {
                    config.setIconImageResource(R.drawable.config_bot_red);
                    model.saveConfigsToFile();
                    listView.invalidateViews();
                }
                else if (id == R.id.green) {
                    config.setIconImageResource(R.drawable.config_bot_green);
                    model.saveConfigsToFile();
                    listView.invalidateViews();
                }
                else if (id == R.id.blue) {
                    config.setIconImageResource(R.drawable.config_bot_blue);
                    model.saveConfigsToFile();
                    listView.invalidateViews();
                }
                else if (id == R.id.yellow) {
                    config.setIconImageResource(R.drawable.config_bot_yellow);
                    model.saveConfigsToFile();
                    listView.invalidateViews();
                }
                else if (id == R.id.purple) {
                    config.setIconImageResource(R.drawable.config_bot_purple);
                    model.saveConfigsToFile();
                    listView.invalidateViews();
                }
                else if (id == R.id.orange) {
                    config.setIconImageResource(R.drawable.config_bot_orange);
                    model.saveConfigsToFile();
                    listView.invalidateViews();
                }
                return false;
            }
        });

        //enable the change IP menu option for WiFi
        if (networkMode == NetworkMode.WIFI) {
            configToolbar.getMenu().findItem(R.id.change_wifi_ip).setVisible(true);
        }
        else {
            configToolbar.getMenu().findItem(R.id.change_wifi_ip).setVisible(false);
        }


        View clickableBody = convertView.findViewById(R.id.clickable_body);
        clickableBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if server, check valid url
                if (networkMode == NetworkMode.SERVER && model.getConnectionServerUrl().equals("")) {
                    divertToServerUrlActivity();
                    return;
                }
                invokeInitActivity(config);
            }
        });
        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    /***** end ListAdapter methods **********************************************/

    @Override
    public void onClick(View v) {}

    protected void invokeBikeCamActivity()  {
        Intent intent = new Intent(getBaseContext(), BikeCamActivity.class);
        startActivityForResult(intent, MSG_REQUEST_CODE);
    }

    protected void invokeWizardHomeActivity()  {
        Intent intent = new Intent(getBaseContext(), WizardHomeActivity.class);
        startActivity(intent);
    }

    private void invokeInitActivity(Configuration config) {
        Intent intent = new Intent(getBaseContext(), InitActivity.class);
        intent.putExtra("configId", config.getConfigId());
        startActivityForResult(intent, INIT_ACTIVITY_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultIntent) {
        super.onActivityResult(requestCode, resultCode, resultIntent);
        if (requestCode == MSG_REQUEST_CODE) {
            Snackbar.make(this.findViewById(R.id.activity_home),
                    getResources().getString(R.string.url_updated),
                    Snackbar.LENGTH_LONG).show();
        }
        else if (requestCode == SERVER_URL_UPDATE_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                return;
            }
            Snackbar.make(this.findViewById(R.id.activity_home),
                    getResources().getString(R.string.url_updated),
                    Snackbar.LENGTH_LONG).show();
        }
        //failed network initialization encountered when invoking a config activity
        else if (requestCode == INIT_ACTIVITY_REQUEST_CODE
                && resultCode == RESULT_CANCELED) {
            if (resultIntent != null) {
                String message = resultIntent.getStringExtra(SNACKBAR_MESSAGE);
                Snackbar.make(this.findViewById(R.id.activity_home),
                        message, Snackbar.LENGTH_LONG).show();
            }
        }
    }

    //code to a confirmation dialog for deleting a configuration from the list
    private void makeDeleteConfirmationDialog(int position) {
        //make a confirmation alert dialog before deleting
        final Configuration config = (Configuration)getItem(position);
        final String name = config.getName();
        AlertDialog.Builder builder =  new AlertDialog.Builder(HomeActivity.this);
        builder.setMessage(getResources().getString(R.string.delete_config_confirm_msg, name))
                .setTitle(getResources().getString(R.string.confirm_delete));
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int id) {
            }
        });
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                model.removeConfiguration(config);
                listView.invalidateViews();
                Snackbar.make(HomeActivity.this.findViewById(R.id.activity_home),
                        getResources().getString(R.string.config_deleted, name),
                        Snackbar.LENGTH_LONG).show();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    //code for making a confirmation dialog for deleting a configuration from the list
    private void makeDeleteAllConfirmationDialog() {
        //make a confirmation alert dialog before deleting
        AlertDialog.Builder builder =  new AlertDialog.Builder(HomeActivity.this);
        builder.setMessage(getResources().getString(R.string.delete_all_configs_confirm_msg))
                .setTitle(getResources().getString(R.string.confirm_delete_all));
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int id) {
            }
        });
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                model.clearConfigs();
                listView.invalidateViews();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * The purpose of this method is in case the server url has not been set yet,
     * the user will be forced to the server url activity to set it. If they return having set it
     * successfully, this will trigger a snackbar message informing the user that the url was
     * updated. (see onActivityResult)
     */
    private void divertToServerUrlActivity() {
        //make a confirmation alert dialog before invokeing the activity
        AlertDialog.Builder builder =  new AlertDialog.Builder(HomeActivity.this);
        builder.setMessage(getResources().getString(R.string.update_server_url_msg))
                .setTitle(getResources().getString(R.string.server_url_not_set_dialog_title));
        builder.setPositiveButton(R.string.set_server_url, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(getBaseContext(),
                        ServerUrlActivity.class);
                startActivityForResult(intent, SERVER_URL_UPDATE_REQUEST_CODE);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Shows a dialog for changing a config's IP address
     * @param config the config in question
     */
    private void showChangeIpAddressAlertDialog(final Configuration config) {

        final EditText ipField = new EditText(this);
        ipField.setText(config.getWifiIpAddress());
        AlertDialog.Builder builder =  new AlertDialog.Builder(HomeActivity.this);
        builder.setView(ipField);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                String ip = ipField.getText().toString();
                if (RedroidUtil.isValidIpString(ip)) {
                    config.setWifiIpAddress(ip);
                    model.saveConfigsToFile();
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Shows a dialog for changing a config's name
     * @param config the config in question
     */
    private void showChangeNameAlertDialog(final Configuration config) {

        final EditText nameField = new EditText(this);
        nameField.setText(config.getName());
        //make an alert
        AlertDialog.Builder builder =  new AlertDialog.Builder(HomeActivity.this);
        builder.setView(nameField);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                String name = nameField.getText().toString();
                if (name != null && !name.isEmpty()) {
                    config.setName(name);
                    model.saveConfigsToFile();
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}