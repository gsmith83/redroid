package net.redroid.redroidmaster;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import net.redroid.redroidcorelibrary.App;
import net.redroid.redroidcorelibrary.Configuration;
import net.redroid.redroidcorelibrary.Network;
import net.redroid.redroidmaster.network.MasterNetwork;
import net.redroid.redroidmaster.network.MasterP2PNetwork;
import net.redroid.redroidmaster.network.MasterServerNetwork;
import net.redroid.redroidmaster.network.MasterWiFiNetwork;

import java.util.Locale;
import java.util.concurrent.TimeoutException;

import static net.redroid.redroidcorelibrary.Network.SERVER_API_PORT;

/**
 * An activity which runs only momentarily before opening a config. It initializes the proper network
 * object, performs connection handshakes,  and starts the API server before moving on or retreating
 */
public class InitActivity extends AppCompatActivity {

    private static final String TAG = "InitActivity";
    final static String CLASS = "CLASS";
    final static String TARGET_ACTIVITY = "TARGET_ACTIVITY";

    MasterDataModel model = MasterDataModel.getInstance();
    String returnFailureMessage = null;
    Configuration config;
    MasterNetwork network;
    String remoteHost;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init);

        //find the relevant config
        String configId = getIntent().getStringExtra("configId");
        if (configId == null) {
            returnFailureMessage = getResources().getString(R.string.error_retrieving_config);
            returnFailureResult();
        }
        config = model.getConfig(configId);

        if (config == null) {
            returnFailureMessage = getResources().getString(R.string.requested_config_not_found);
            returnFailureResult();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        if (!hasFocus)
            return;

        new Thread(new Runnable() {
            @Override
            public void run() {
                //p2p
                if (config.getNetworkMode() == Configuration.NetworkMode.P2P) {
                    p2pTest();
                }
                else {
                    test();
                }
            }
        }).start();

    }

    /**
     * alternate version for p2p because p2p network intialization requires an asynchronous approach
     */
    private void p2pTest() {
        network = new MasterP2PNetwork(config.getDeviceId(), config.getSecurityToken());
        //set remoteHost
        remoteHost = String.format(Locale.US, "%s:%d", network.getRemoteIpAddress(), model.SERVER_PORT);

        //initialize the network
        try {
            model.initializeP2PNetwork((MasterP2PNetwork) network, new MasterP2PNetwork.InitSuccessListener() {
                @Override
                public void onInitSuccess() {
                    //try to perform a Redriod handshake with the remote host
                    String handshakeResult = model.api.performHandshake(config.getDeviceId(),
                            config.getSecurityToken(), model.network.getRemoteIpAddress() + ":"
                                    + SERVER_API_PORT);
                    if (handshakeResult == null) {
                        invokeTargetActivity();
                    }
                    else {
                        returnFailureMessage = handshakeResult;
                        returnFailureResult();
                    }
                    invokeTargetActivity();
                }

                @Override
                public void onInitFailure(String msg) {
                    returnFailureMessage = msg;
                    returnFailureResult();
                }
            });
        } catch (TimeoutException e) {
            returnFailureMessage = getResources().getString(R.string.peer_discovery_timed_out);
            returnFailureResult();

        } catch (Network.ConnectionException e) {
            returnFailureMessage = e.getMessage();
            returnFailureResult();

        }
    }


    /**
     * Tests the connection
     */
    private void test() {

        //choose the right network type
        network = new MasterWiFiNetwork(config.getWifiIpAddress()); //todo: temp - delete after completing switch statement
        switch (config.getNetworkMode()) {
            case WIFI:
                network = new MasterWiFiNetwork(config.getWifiIpAddress());
                break;
            case SERVER:
                network = new MasterServerNetwork();
                break;
            case P2P:
                network = new MasterP2PNetwork(config.getDeviceId(), config.getSecurityToken());
                break;
        }

        //set remoteHost
        remoteHost = String.format(Locale.US, "%s:%d", network.getRemoteIpAddress(), model.SERVER_PORT);

        //initialize the network
        try {
            model.initializeNetwork(network);
        } catch (TimeoutException e) {
            if (network instanceof MasterWiFiNetwork)
                returnFailureMessage = getResources().getString(R.string.wifi_not_connected_msg);
            else
                returnFailureMessage = getResources().getString(R.string.server_not_reachable);

        } catch (Network.ConnectionException e) {
            if (network instanceof MasterWiFiNetwork)
                returnFailureMessage = getResources().getString(R.string.wifi_error);
            else
                returnFailureMessage = getResources().getString(R.string.server_connection_error);

        }

        //perform connection handshake
        String handshakeResult = model.api.performHandshake(config.getDeviceId(),
                config.getSecurityToken(), remoteHost);

        if (handshakeResult != null) {
            returnFailureMessage = handshakeResult;
        }

        //start the target activity if initialization succeeded
        if (returnFailureMessage == null) {
            invokeTargetActivity();
        }
        //or go back to the calling activity if initialization failed
        else {
            returnFailureResult();
        }
    }

    /**
     * for advancing to the target class when network initialization and auth succeed
     */
    private void invokeTargetActivity() {
        //determine the intended class from config data
        Configuration.ConfigType configType = config.getConfigType();
        Class targetClass = null;

        try {
            if (configType == Configuration.ConfigType.CUSTOM) {
                targetClass = Class.forName("net.redroid.redroidmaster.CustomConfigActivity");
            } else if (configType == Configuration.ConfigType.BIKE_CAM) {
                targetClass = Class.forName("net.redroid.redroidmaster.BikeCamActivity");
            } else if (configType == Configuration.ConfigType.SECURITY_CAM || configType == Configuration.ConfigType.PET_CAM) {
                targetClass = Class.forName("net.redroid.redroidmaster.SecurityPetCamConfigActivity");
            }
            if (targetClass == null)
                throw new ClassNotFoundException("the config's ConfigType was not one of the expected values");
        } catch (ClassNotFoundException e) {
            Log.e(TAG, "invokeTargetActivity: " + e);
            returnFailureMessage = "Runtime error: " + e;
            //abort and notify user of runtime error
            returnFailureResult();
        }
        Intent intent = new Intent(App.getAppContext(), targetClass);

        //pass along any extras packaged with the original intent
        intent.putExtras(getIntent().getExtras());

        //start the activity and clear this one from the back stack
        startActivity(intent);
        finish();
    }


    /**
     * For returning to the invoking class when network initialization and/or auth fails
     */
    private void returnFailureResult() {
        //clear the config object stored in the model, as it is not needed
        model.clearStoredObject();

        //teardown the network
        model.teardownNetwork();

        //was this invoked from elsewhere (not for result), such as the RedroidFirebaseMessagingService?
        if (getCallingActivity() == null) {
            Intent intent = new Intent(this, HomeActivity.class);
            intent.putExtra(HomeActivity.SNACKBAR_MESSAGE, returnFailureMessage);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        //if not, return a result to the Home Activity that called it
        else {
            //return a result
            Intent failureIntent = new Intent();
            failureIntent.putExtra(HomeActivity.SNACKBAR_MESSAGE, returnFailureMessage);
            setResult(RESULT_CANCELED, failureIntent);
            finish();
        }
    }
}
