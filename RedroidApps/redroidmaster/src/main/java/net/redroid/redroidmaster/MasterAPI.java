package net.redroid.redroidmaster;

import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.util.Log;
import android.util.SparseArray;
import net.redroid.redroidcorelibrary.API;
import net.redroid.redroidcorelibrary.ApiResponse;
import net.redroid.redroidcorelibrary.Network;
import net.redroid.redroidcorelibrary.RedroidUtil;
import net.redroid.redroidcorelibrary.rules.Rule;
import net.redroid.redroidmaster.network.MasterNetwork;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import static android.content.ContentValues.TAG;
import static java.lang.Math.max;
import static net.redroid.redroidcorelibrary.API.Method.DELETE;
import static net.redroid.redroidcorelibrary.API.Method.GET;
import static net.redroid.redroidcorelibrary.API.Method.POST;
import static net.redroid.redroidcorelibrary.API.Method.PUT;
import static net.redroid.redroidcorelibrary.Network.SERVER_API_PORT;


/**
 * Contains methods for performing redroid network communication API calls on a redroid Master.
 * Extends the AbstractAPI class in the Redroid Core Library, which contains methods common to
 * both Master and Peripheral, including helper methods utilized by both.
 */
public class MasterAPI extends API {


    private SparseArray<ApiEventListener> apiEventListenerMap; //for storing API callbacks
    private SparseArray<Integer> requestIdsBySensor; //for storing API callbacks
    private int requestId = 0;  //for obtaining a unique callback id for the notificationMap
    public boolean handshakeSuccess = false;


    /**
     * A listener for handling incoming ApiNotifications initiated by a peripheral
     * (primarily used by a master device to handle requested notifications)
     */
    public interface ApiEventListener {

        /**
         * For handling incoming API notifications.
         *
         * @param jsonString the JSON string payload of the incoming API command
         * @return An ApiResponse object containing the response code and JSON string
         * @throws JSONException if there are any errors caused by packing or unpacking a JSONObject
         */
        void onApiEvent(String jsonString);
    }


    public MasterAPI() {
        this.apiEventListenerMap = new SparseArray<ApiEventListener>();
        this.requestIdsBySensor = new SparseArray<Integer>();
    }


    /**
     * A blocking method which performs the two master handshake steps for connecting with a
     * peripheral or server. If no response is received from the server it will time out after a
     * given interval and return. Returns a message string on failure, or null on success.
     *
     * @param deviceId      the id of the peripheral to connect with
     * @param securityToken the security token shown on the screen of the peripheral
     *                      * @param remoteHost the ip address + port of API request receiver (peripheral or server)
     * @return a string containing a failure message, or null if successful.
     */
    public String performHandshake(final String deviceId, final String securityToken,
                                   final String remoteHost) {

        final Object monitorObject = new Object();
        final int TIMEOUT_INTERVAL = Network.SOCKET_READ_TIMEOUT;
        this.handshakeSuccess = false;
        final String[] out = {null};
        final String[] sessionId = {null};
        final MasterNetwork network = MasterDataModel.getInstance().network;

        //flags to track completion and success of the handshake phases
        final Boolean[] successFlags = {null, null};


        //STEP 1
        requestPostConnectApi(deviceId, remoteHost, new ApiResponseListener() {
            @Override
            public void onApiResponse(int responseCode, JSONObject response) {
                //check for failure and notify user of reason
                if (responseCode != 200) {
                    String msg;
                    try {
                        if (response != null)
                            msg = (String) response.get("msg");
                        else
                            msg = "unspecified cause";
                    } catch (JSONException e) {
                        msg = null;
                    }
                    Log.e(TAG, "handshake response contained failure code: " + responseCode);
                    out[0] = ((msg == null) ? "code " + responseCode : msg);

                    //notify parent thread of completion
                    successFlags[0] = false;
                } else {
                    try {
                        //extract the session id)
                        if (response != null) {
                            sessionId[0] = response.getString("session_id");
                        }
                        successFlags[0] = true;
                    } catch (JSONException e) {
                        //no session id? Something is wrong.
                        Log.e(TAG, "onApiResponse: handshake response contained 200 OK, but no session ID: "
                                + e.getMessage());
                        out[0] = "Connection handshake failed: runtime error.";
                        successFlags[0] = false;
                    }
                }
                //notify parent thread of callback completion
                synchronized (monitorObject) {
                    monitorObject.notify();
                }
            }
        });

        //BLOCK UNTIL STEP 1 COMPLETES
        //time out after TIMEOUT_INTERVAL milliseconds with no response
        long startTime = System.currentTimeMillis();
        long elapsedTime = 0;
        while (successFlags[0] == null) {
            synchronized (monitorObject) {
                try {
                    //sleep for the remainder of the timeout interval
                    monitorObject.wait(max(TIMEOUT_INTERVAL - elapsedTime, 0));
                } catch (InterruptedException e) {
                }
            }
            //return early on a timeout
            elapsedTime = System.currentTimeMillis() - startTime;
            if (elapsedTime >= TIMEOUT_INTERVAL && successFlags[0] == null) {
                out[0] = "Redroid handshake timed out";
                return out[0];
            }
        }

        //if step 1 failed, just return now
        if (successFlags[0] == false) {
            return out[0];
        }

        //STEP 2
        requestPostRedroidMasterAuth(sessionId[0], securityToken, new ApiResponseListener() {
            @Override
            public void onApiResponse(int responseCode, JSONObject response) {
                //check for failure and notify user of reason
                if (responseCode != 200) {
                    String msg;
                    try {
                        if (response != null) {
                            msg = (String) response.get("msg");
                        }
                        else {
                            msg = responseCode +" garblegarble";
                        }
                    } catch (JSONException e) {
                        msg = null;
                    }
                    Log.e(TAG, "handshake response contained failure code: " + responseCode);
                    out[0] = ((msg == null) ? "code " + responseCode : msg);
                    successFlags[1] = false;
                }
                // step 2 succeeeded
                else {
                    successFlags[1] = true;
                    network.sessionId = sessionId[0];
                }
                synchronized (monitorObject) {
                    monitorObject.notify();
                }
            }
        });


        //BLOCK UNTIL STEP 2 COMPLETES
        //time out after TIMEOUT_INTERVAL milliseconds with no response
        startTime = System.currentTimeMillis();
        elapsedTime = 0;
        while (successFlags[1] == null) {
            synchronized (monitorObject) {
                try {
                    //sleep for the remainder of the timeout interval
                    monitorObject.wait(max(TIMEOUT_INTERVAL - elapsedTime, 0));
                } catch (InterruptedException e) {
                }
            }
            //return early on a timeout
            elapsedTime = System.currentTimeMillis() - startTime;
            if (elapsedTime >= TIMEOUT_INTERVAL && successFlags[1] == null) {
                out[0] = "Handshake timed out on step 1";
                return out[0];
            }
        }
        return out[0];
    }


    /**
     * Generic method for making any API request for a redroid Master device.
     *
     * @param keys     An array of the keys for the JSON key-value pairs (if any)
     * @param values   An array of the values for the JSON key-value pairs (if any), as objects.
     *                 Any element not of underlying type String, Boolean, Double, Integer, or Long
     *                 will be skipped.
     * @param method   the API.Method, either GET, POST, PUT, or DELETE
     * @param listener the ApiResponseListener containing code to be executed upon receipt of an API
     *                 response
     */
    public void makeGenericApiRequest(final Method method, String url, String[] keys,
                                      Object[] values,
                                      final ApiResponseListener listener) throws JSONException {

        String jsonRequest = null;
        if (keys != null && values != null)
            jsonRequest = RedroidUtil.makeSimpleJson(keys, values);
        getHttpResponseAsync(method, url, jsonRequest, listener); //uses custom http class; located in MasterAPI
    }

    /**
     * Blocking version of the API 701:PING to test connection with peripheral
     *
     * @return an ApiReponse object.
     * @throws IOException
     * @throws Network.ConnectionException
     */
    public ApiResponse requestGetPingBlocking() throws IOException, Network.ConnectionException {
        MasterNetwork network = MasterDataModel.getInstance().network;
        Pair<Integer, String> response = network.getHttpResponse(GET, "/redroid/master/ping", null);
        return new ApiResponse(response.first, response.second);
    }

    /**
     * API 201: Fetch a list of available sensors
     * @param listener the response listener
     */
    public void requestGetSensorList(ApiResponseListener listener) {
        String url = "/redroid/master/request/sensor_list";
        try {
            makeGenericApiRequest(GET, url, null, null, listener);
        } catch (JSONException e) {
            Log.e(TAG, "requestGetSensorList: " + e.getMessage());
        }
    }

    /**
     * API 202: Fetch a list of available features
     * @param listener the response listener
     */
    public void requestGetFeatureList(ApiResponseListener listener) {
        String url = "/redroid/master/request/feature_list";
        try {
            makeGenericApiRequest(GET, url, null, null, listener);
        } catch (JSONException e) {
            Log.e(TAG, "requestGetFeatureList: " + e.getMessage());
        }
    }


    /**
     * API 001
     * Performs handshake step 1: hello and get session id
     *
     * @param deviceId   the deivice id of the peripheral to connect with
     * @param remoteHost the ip address + port of the API request receiver (peripheral or server)
     * @param listener   contains a callback to be executed upon receipt of a response
     */
    public void requestPostConnectApi(final String deviceId, final String remoteHost,
                                      final ApiResponseListener listener) {

        String[] keys = {"device_id"};
        String[] values = {deviceId};
        String url = "/redroid/master/connect/api";
        try {
            makeGenericApiRequest(POST, url, keys, values, listener);
        } catch (JSONException e) {
            Log.e(TAG, "001 requestPostConnectApi: " + e.getMessage());
        }
    }


    /**
     * API 002
     * Performs handshake step 2: authorization/authentication
     *
     * @param sessionId     the session id provided by the remoteHost in step 1
     * @param securityToken the security token of the peripheral being connected to
     * @param listener      contains a callback to be executed upon receipt of a response
     */
    public void requestPostRedroidMasterAuth(final String sessionId, final String securityToken,
                                             final ApiResponseListener listener) {

        String[] keys = {"session_id", "security_token"};
        String[] values = {sessionId, securityToken};
        String urlString = "/redroid/master/auth";
        try {
            makeGenericApiRequest(POST, urlString, keys, values, listener);
        } catch (JSONException e) {
            Log.e(TAG, "002 requestPostRedroidMasterAuth: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * API 003
     * Performs handshake step 3: set event port (optional) for Masters behind NAT with a server
     *
     * @param sessionId the session id provided by the remoteHost in step 1
     * @param listener  contains a callback to be executed upon receipt of a response
     */
    public void requestPostSetEventPort(final String sessionId, final ApiResponseListener listener) {

        final String url = "/redroid/master/connect/event";
        final API.Method method = POST;
        final String body = String.format("{\"session_id\":\"%s\"}", sessionId);

        AsyncTask<Object, Void, Void> task = new AsyncTask<Object, Void, Void>() {
            @Nullable
            @Override
            protected Void doInBackground(Object[] params) {

                int responseCode = -1;
                String jsonResponse = null;
                JSONObject jsonObject;
                MasterNetwork network = MasterDataModel.getInstance().network;
                Pair<Integer, String> response = null;

                //create the event socket and its readers
                network.eventSocket = new Socket();
                network.eventSocketReader = null;
                network.eventSocketWriter = null;
                try {
                    network.eventSocket.setReuseAddress(true);
                    network.eventSocket.setSoTimeout(Network.SOCKET_READ_TIMEOUT);
                    network.eventSocket.connect(new InetSocketAddress(network.getRemoteIpAddress(),
                            SERVER_API_PORT), Network.SOCKET_CONNECTION_TIMEOUT);
                    network.eventSocketReader = new BufferedReader(
                            new InputStreamReader(network.eventSocket.getInputStream()));
                    network.eventSocketWriter = new BufferedWriter(
                            new OutputStreamWriter(network.eventSocket.getOutputStream()));
                } catch (IOException e) {
                    Log.e(TAG, "requestPostSetEventPort: eventSocket: " + e.getMessage());
                    network.eventSocket = null;
                }
                //check for success
                if (network.eventSocket == null)
                    return null;

                //transact with server
                try {
                    SocketAddress address = network.eventSocket.getRemoteSocketAddress();
                    String remoteHost = address.toString().replace("/", "");
                    response = Network.getHttpResponse(remoteHost, network.eventSocketReader,
                            network.eventSocketWriter, method, url, body);

                } catch (Network.ConnectionException | NullPointerException e) {
                    network.teardown();
                }

                //evaluate the response
                if (response != null) {
                    responseCode = response.first;
                    jsonResponse = response.second;
                } else {
                    responseCode = -1;
                    jsonResponse = "{\"msg\":\"Device not available.\"}";
                }

                //create the JSON object from the response string
                jsonObject = null;
                try {
                    if (jsonResponse != null && !jsonResponse.equals("")) {
                        jsonObject = new JSONObject(jsonResponse);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "JSON exception: " + e);
                    try {
                        jsonObject = new JSONObject("{\"msg\": \"malformed JSON\"}");
                    } catch (JSONException j) {
                    }
                }

                //call the listener and pass the results
                if (listener != null) {
                    listener.onApiResponse(responseCode, jsonObject);
                }
                return null;
            }
        };
        task.execute();
    }

    /**
     * API 102
     * Asks for to start the RTMP server
     * @param audio whether to include audio
     * @param video whether to include video
     * @param listener ApiResponeListener
     */
    public void requestGetRTMP(boolean audio, boolean video, final ApiResponseListener listener) {
        String urlString = "";
        if(audio && video){
            urlString = "/redroid/master/streaming/rtmp/video_audio";
        }
        else if(video){
            urlString = "/redroid/master/streaming/rtmp/video";
        }
        else if(audio){
            urlString = "/redroid/master/streaming/rtmp/audio";
        }

        if (urlString.isEmpty())
            throw new RuntimeException("Cannot instantiate video stream without audio and/or video");

        try {
            makeGenericApiRequest(GET, urlString, null, null, listener);
        } catch (JSONException e) {
            Log.e(TAG, "101 - 103 requestGetRTMP: " + e.getMessage());
        }
    }

    /**
     * API 204
     * Gets data about the peripheral's last known location
     */
    public void requestGetLastLocation(final ApiResponseListener listener) {
        String url = "/redroid/master/request/last_location";
        try {
            makeGenericApiRequest(GET, url, null, null, listener);
        } catch (JSONException e) {
            Log.e(TAG, "requestGetLastLocation: " + e.getMessage());
            e.printStackTrace();
        }
    }



    /**
     * API 205
     * Asyncronously sens an HTTP GET request to obtain the current state of a remote bike cam's
     * flasher
     *
     * @param listener the listener for returning results to caller
     */
    public void requestGetBikeCamFlasher(final ApiResponseListener listener) {
        String urlString =  "/redroid/bikecam/state/flasher";
        try {
            makeGenericApiRequest(GET, urlString, null, null, listener);
        } catch (JSONException e) {
            Log.e(TAG, "205 requestGetBikeCamFlahser: " + e.getMessage());
        }
    }

    /**
     * API 206
     * Asynchronously sends an HTTP GET request to obtain the current state of a remote bike cam's
     * turn signal
     *
     * @param remoteHost the URL (ip address or hostname) of the remote bike cam
     * @param listener   the listener for returning results to caller
     */
    public void requestGetBikeCamSignal(final String remoteHost,
                                        final ApiResponseListener listener) {
        String urlString =  "/redroid/bikecam/state/signal";
        try {
            makeGenericApiRequest(GET, urlString, null, null, listener);
        } catch (JSONException e) {
            Log.e(TAG, "206 requestGetBikeCameSignal: " + e.getMessage());
        }
    }

    /**
     * API 207
     * Asynchronously sends an HTTP GET request to obtain the current state of the battery
     * @param listener the listener for returning results to caller
     */
    public void requestGetBattery(final ApiResponseListener listener) {
        String urlString =  "/redroid/master/request/bike";
        try {
            makeGenericApiRequest(GET, urlString, null, null, listener);
        } catch (JSONException e) {
            Log.e(TAG, "207 requestGetBattery: " + e.getMessage());
        }
    }

    /**
     * API 301
     * Sends the command for the peripheral to start its Bike Cam Activity
     *
     * @param listener the listener for resturning results to caller
     */
    public void requestPostStartBikeCam(final ApiResponseListener listener) {

        //build the URL
        String urlString =  "/redroid/bikecam/activity/bikecam";

        try {
            makeGenericApiRequest(POST, urlString, null, null, listener);
        } catch (JSONException e) {
            Log.e(TAG, "302 requestPostStartBikeCam: " + e.getMessage());
        }
    }

    /**
     * API 302
     * Asyncronously sends an HTTP POST request to set the flasher state on a remote bike cam
     *
     * @param is_on    true to turn flasher on, false to turn it off
     * @param listener the listener for resturning results to caller
     */
    public void requestPostBikeCamStateFlasher(final boolean is_on, final ApiResponseListener listener) {

        String urlString =  "/redroid/bikecam/state/flasher";
        String[] keys = new String[] {"is_on"};
        Boolean[] values = new Boolean[] {is_on};
        try {
            makeGenericApiRequest(POST, urlString, keys, values, listener);
        } catch (JSONException e) {
            Log.e(TAG, "302 requestPostBikeCamStateFlasher: " + e.getMessage());
        }
    }

    /**
     * the enum defining possible blinker states
     */
    public enum BLINKER_STATE {
        LEFT, RIGHT, OFF
    }

    /**
     * API 303
     * Asynchronously sends an HTTP POST request to set the turn signal state on a remote bike cam
     *
     * @param state    either LEFT, RIGHT, or OFF
     * @param listener the listener for returning results to caller
     */
    public void requestPostBikeCamSignal(final BLINKER_STATE state,   final ApiResponseListener listener) {
        String urlString =  "/redroid/bikecam/state/signal";
        String[] keys = new String[] {"signal_state"};
        String[] values = new String[] {state.name()};
        try {
            makeGenericApiRequest(POST, urlString, keys, values, listener);
        } catch (JSONException e) {
            Log.e(TAG, "303 requestPostBikeCamSignal: " + e.getMessage());
        }
    }

    /**
     * API 306
     * Asynchronously sends an HTTP POST request to set RTMP stream state on peripheral device
     * @param isStreaming whether the stream should be started (true) or stopped (false)
     * @param listener the listener for returning results to caller
     */
    public void requestPostRtmpState(final boolean isStreaming, final ApiResponseListener listener) {
        String urlString =  "/redroid/master/rtmp/state";
        String[] keys = new String[] {"is_streaming"};
        Object[] values = new Boolean[] {isStreaming};
        try {
            makeGenericApiRequest(POST, urlString, keys, values, listener);
        } catch (JSONException e) {
            Log.e(TAG, "306 requestPostRtmpState: " + e.getMessage());
        }
    }

    /**
     * API 401
     * Asyncronously sends an HTTP PUT request to register to receive events from a particular
     * sensor
     *
     * @param sensorType       the type of sensor, as an integer, as defined in the Android Sensor API
     * @param samplingPeriod   minimum of microseconds between updates
     * @param onChangeOnly     whether or not updates should only be send when changed
     * @param responseListener the listener for returning results (success or failure) to the caller
     * @param eventListener    the listener for hapndling incoming sensor notifications
     */
    public void requestPutRegisterSensorEvents(final int sensorType,
                                               final int samplingPeriod,
                                               final boolean onChangeOnly,
                                               final ApiResponseListener responseListener,
                                               final ApiEventListener eventListener) {


        //register the event listener and get the request id
        int requestId = registerApiEventListener(sensorType, eventListener);

        String[] keys = {"request_id", "sampling_period", "on_change_only"};
        Object[] values = {requestId, samplingPeriod, onChangeOnly};
        String url = String.format("/redroid/master/register/sensor_events/%s",
                RedroidUtil.getSensorType(sensorType));
        try {
            makeGenericApiRequest(PUT, url, keys, values, responseListener);
        } catch (JSONException e) {
            Log.e(TAG, "401 requestPutRegisterSensorEvents: " + e.getMessage());
        }
    }

    /**
     * API 402
     * Sends an HTTP PUT request to register to receive location events
     *
     * @param samplingPeriod   minimum of microseconds between updates
     * @param onChangeOnly     whether or not updates should only be send when changed
     * @param responseListener the listener for returning results (success or failure) to the caller
     * @param eventListener    the listener for hapndling incoming sensor notifications
     */
    public void requestPutRegisterLocationEvent(final int samplingPeriod,
                                                final boolean onChangeOnly,
                                                final ApiResponseListener responseListener,
                                                final ApiEventListener eventListener) {


        //register the event listener and get the request id
        int requestId = registerApiEventListener(TYPE_LOCATION, eventListener);

        String[] keys = {"request_id", "sampling_period", "on_change_only"};
        Object[] values = {requestId, samplingPeriod, onChangeOnly};
        String url = "/redroid/master/register/location_events";
        try {
            makeGenericApiRequest(PUT, url, keys, values, responseListener);
        } catch (JSONException e) {
            Log.e(TAG, "402 requestPutRegisterSensorEvents: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * API 404
     * Sends an HTTP PUT request to register a rule with the peripheral.
     *
     * @param rule the Rule object describing the rule to register
     * @param responseListener the response listener
     */
    public void requestPutRule(final Rule rule,
                               final ApiResponseListener responseListener) {

        String url = "/redroid/master/rule";
        String ruleJson = rule.toJson();
        getHttpResponseAsync(PUT, url, ruleJson, responseListener);
    }



    /**
     * API 601
     * Asyncronously sends an HTTP DELETE request to unregister receipt of events from a particular
     * sensor
     *
     * @param sensorType       the type of sensor previously registered
     * @param responseListener the ApiResponse listener to trigger when done
     */
    public void requestDeleteSensorEventRegistration(final String sensorType,
                                                     final ApiResponseListener responseListener) {
        int sensorTypeInt = RedroidUtil.getSensorTypeInt(sensorType);
        String urlTemplate = "/redroid/master/register/sensor_events/%s/%d";
        int requestId = requestIdsBySensor.get(sensorTypeInt);
        String url = String.format(urlTemplate, sensorType, requestId);
        try {
            makeGenericApiRequest(DELETE, url, null, null, responseListener);
            requestIdsBySensor.delete(sensorTypeInt);
        } catch (JSONException e) {
            Log.e(TAG, "requestPostRedroidMasterAuth: " + e.getMessage());
        }
    }

    /**
     * API 604
     * Sends an HTTP DELETE request to unset a rule with the server
     *
     * @param ruleId the Rule ID associated with the rule to unset
     * @param responseListener the response listener
     */
    public void requestDeleteRule(final String ruleId, final ApiResponseListener responseListener) {

        String url = "/redroid/master/rule/" + ruleId;
        getHttpResponseAsync(DELETE, url, null, responseListener);
    }


    private synchronized int registerApiEventListener(int sensorType, MasterAPI.ApiEventListener listener) {
        apiEventListenerMap.put(requestId, listener);
        requestIdsBySensor.put(sensorType, requestId);
        Log.d("SensorEvents", "Master registered new event callback with callback id: " + requestId);
        return requestId++;
    }


    /**
     * returns the ApiEventListener mapped to the given requestId.
     *
     * @param requestId
     * @return an ApiEventListener, or null of if not found
     */
    public MasterAPI.ApiEventListener getApiEventListener(int requestId) {
        return apiEventListenerMap.get(requestId);
    }


    /**
     * A generic helper method for making an API call from a Redroid master. This is used by a
     * variety of specific API calls to do the work of fetching a response asynchronously
     *
     * @param method              the HTTP method, as defined in the API
     * @param url                 the API url
     * @param requestBody         the JSON for the request
     * @param apiResponseListener the listener containing code to execute on response receipt
     */
    public void getHttpResponseAsync(final API.Method method,
                                     final String url,
                                     final String requestBody,
                                     final ApiResponseListener apiResponseListener) {


        AsyncTask<Object, Void, Void> task = new AsyncTask<Object, Void, Void>() {
            @Nullable
            @Override
            protected Void doInBackground(Object[] params) {

                int responseCode = -1;
                String jsonResponse = null;
                JSONObject jsonObject;
                MasterNetwork network = MasterDataModel.getInstance().network;

                //transact with server
                Pair<Integer, String> response = null;
                try {
                    response = network.getHttpResponse(method, url,
                            requestBody);
                } catch (Network.ConnectionException | NullPointerException e) {
                    if (network != null)
                        network.teardown();
                }
                if (response != null) {
                    responseCode = response.first;
                    jsonResponse = response.second;
                } else {
                    responseCode = -1;
                    jsonResponse = "{\"msg\":\"Device not available.\"}";
                }

                //create the return object from the response string

                jsonObject = null;
                try {
                    if (jsonResponse != null && !jsonResponse.equals("")) {
                        jsonObject = new JSONObject(jsonResponse);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "JSON exception: " + e);
                    try {
                        jsonObject = new JSONObject("{\"msg\": \"" +
                                "malformed JSON\"}");
                    } catch (JSONException j) {}
                }

                //call the listener and pass the results
                if (apiResponseListener != null) {
                    apiResponseListener.onApiResponse(responseCode, jsonObject);
                }
                return null;
            }
        };
        task.execute();
    }
}
