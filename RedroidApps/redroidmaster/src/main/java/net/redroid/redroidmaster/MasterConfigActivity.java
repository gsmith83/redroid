package net.redroid.redroidmaster;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import net.redroid.redroidcorelibrary.Configuration;
import net.redroid.redroidmaster.rules.NewRuleActivity;
import net.redroid.redroidmaster.rules.RulesActivity;


/**
 * Abstract parent class for all config activities on the Master
 */
public abstract class MasterConfigActivity extends AppCompatActivity {

    MasterDataModel model = MasterDataModel.getInstance();
    protected Configuration config;

    /**
     * Invokes HomeActivity upon completion of this activity
     * @param message an optional message to dispaly in a snackbar on return to Home
     */
    boolean invoked = false;
    protected synchronized void invokeHomeActivity(String message)  {
        if (invoked)
            return;
        invoked = true;
        Intent intent = new Intent(getBaseContext(), HomeActivity.class);
        intent.putExtra(HomeActivity.SNACKBAR_MESSAGE, message);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //find the relevant config
        String configId = getIntent().getStringExtra("configId");
        if (configId == null) {
            invokeHomeActivity(getResources().getString(R.string.error_retrieving_config));
            finish();
            return;
        }
        config = model.getConfig(configId);

        if (config == null) {
            invokeHomeActivity(getResources().getString(R.string.requested_config_not_found));
            finish();
            return;
        }

        // set up the back button in the title bar
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        //receive broadcast notification that the network has disconnected; return home
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                tearDown();
                invokeHomeActivity(getResources().getString(R.string.connection_disconnected));
            }
        }, new IntentFilter("disconnected"));
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        tearDown();
        invokeHomeActivity(null);
    }

    /**
     * Tears down the network
     */
    protected void tearDown() {
        //deregister event requests
        //teardown the network
        if (model.network != null) {
            model.network.teardown();
            model.network = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_config, menu);
        if (config.getNetworkMode() != Configuration.NetworkMode.SERVER) {
            menu.findItem(R.id.rules_menu_item).setVisible(false);
            menu.findItem(R.id.new_rule_menu_item).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        switch (menuItem.getItemId()) {
            //user pressed back button in action bar
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.new_rule_menu_item:
                Intent newRuleIntent = new Intent(getBaseContext(), NewRuleActivity.class);
                newRuleIntent.putExtra("configId", config.getConfigId());
                startActivity(newRuleIntent);
                break;
            case R.id.rules_menu_item:
                Intent rulesIntent = new Intent(getBaseContext(), RulesActivity.class);
                rulesIntent.putExtra("configId", config.getConfigId());
                startActivity(rulesIntent);
                break;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
        return true;
    }
}
