package net.redroid.redroidmaster;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import net.redroid.redroidcorelibrary.*;
import net.redroid.redroidmaster.network.MasterNetwork;
import net.redroid.redroidmaster.network.MasterP2PNetwork;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.concurrent.TimeoutException;
import io.vov.vitamio.widget.VideoView;

/**
 * A singleton which serves as the data model for an instance of Redroid Master
 */
public class MasterDataModel extends DataModel {

    //TODO: set this to false for production code
    static final public boolean DEBUG = true;

    final int SERVER_PORT = 11100;
    private static final String TAG = "Master DataModel";
    private static MasterDataModel instance;
    private ConfigurationStore configStore;
    private Object tempObject; //for storing objects momentarily to pass between activities
    public VideoView videoView; //for storing a video view while rotating
    public MasterAPI api = new MasterAPI();
    public MasterNetwork network;
    public String firebaseToken;
    public AutoCompleteLists autoCompleteLists;
    public Activity currentActivity;


    /**
     * Singleton accessor
     *
     * @return the singleton
     */
    public synchronized static MasterDataModel getInstance() {
        if (instance == null) {
            instance = new MasterDataModel();
        }
        return instance;
    }

    /**
     * Private constructor
     */
    private MasterDataModel() {
        loadConfigsFromFile();
        loadConnectionServerUrlFromFile();
        loadAutoCompleteListsFromFile();
        firebaseToken = FirebaseInstanceId.getInstance().getToken();
    }

    /**
     * Method for adding an instance of Configuration to the config store
     *
     * @param config The instance of Configuration to add to the store
     * @return A boolean value indicating whether the Configuration was successfully added
     */
    boolean addConfiguration(Configuration config) {
        //add a random icon drawable
        int imageResource = -1;
        int lastConfigImageResource = -2;
        if (configStore.getLastConfig() != null)
            lastConfigImageResource = configStore.getLastConfig().getIconImageResource();
        do {
            int iconNum = (int) (Math.random() * 6);
            switch (iconNum) {
                case 0:
                    imageResource = net.redroid.redroidcorelibrary.R.drawable.config_bot_red;
                    break;
                case 1:
                    imageResource = net.redroid.redroidcorelibrary.R.drawable.config_bot_green;
                    break;
                case 2:
                    imageResource = net.redroid.redroidcorelibrary.R.drawable.config_bot_blue;
                    break;
                case 3:
                    imageResource = net.redroid.redroidcorelibrary.R.drawable.config_bot_yellow;
                    break;
                case 4:
                    imageResource = net.redroid.redroidcorelibrary.R.drawable.config_bot_purple;
                    break;
                case 5:
                    imageResource = net.redroid.redroidcorelibrary.R.drawable.config_bot_orange;
                    break;
            }
            //make sure it isn't the same as the current last one
        } while (imageResource == lastConfigImageResource);
        config.setIconImageResource(imageResource);

        //add it
        boolean success = configStore.addConfiguration(config);
        if (success)
            saveConfigsToFile();
        //update the auto complete lists
        addAutoCompleteEntries(config.getName(), config.getDeviceId(), config.getSecurityToken(),
                config.getWifiIpAddress(), null, null);
        return success;
    }

    /**
     * Method for saving the config store to file
     */
    public void saveConfigsToFile() {
        Gson gson = new Gson();
        String json = gson.toJson(configStore);
        try {
            File configFile = new File(App.getAppContext().getFilesDir(), "configs.dat");
            BufferedWriter writer = new BufferedWriter(new FileWriter(configFile, false));
            writer.write(json);
            writer.newLine();
            writer.flush();
            writer.close();
        }
        catch (IOException e) {
            String msg = "An error occurred when writing the config store to file: " +
                    e.getMessage();
            Log.e(TAG, msg);
            Toast.makeText(App.getAppContext(), msg, Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Method for refreshing the config store from file
     */
    private void loadConfigsFromFile() {

        try {
            //open the file and set a reader
            File configFile = new File(App.getAppContext().getFilesDir(), "configs.dat");
            if (!configFile.exists()) {
                this.configStore = new ConfigurationStore();
            }
            else {
                BufferedReader reader = new BufferedReader(new FileReader(configFile));
                Type ConfigStoreType = new TypeToken<ConfigurationStore>() {
                }.getType();
                //read in the contents to a json object
                String jsonConfigs = reader.readLine();
                Gson gson = new Gson();
                this.configStore = gson.fromJson(jsonConfigs, ConfigStoreType);
                reader.close();
            }

        }
        catch (IOException e) {
            String msg = "An error occurred when loading the config store from from file: " +
                    e.getMessage();
            Toast.makeText(App.getAppContext(), msg, Toast.LENGTH_LONG).show();
            if (configStore == null) {
                this.configStore = new ConfigurationStore();
            }
        }
    }


    /**
     * Method for saving the auto complete lists to file
     */
    private void saveAutoCompleteListsToFile() {
        Gson gson = new Gson();
        if (autoCompleteLists == null)
            return;
        String json = gson.toJson(autoCompleteLists);

        try {
            File listsFile = new File(App.getAppContext().getFilesDir(), "ac_lists.dat");
            BufferedWriter writer = new BufferedWriter(new FileWriter(listsFile, false));
            writer.write(json);
            writer.newLine();
            writer.flush();
            writer.close();

        }
        catch (IOException e) {
            String msg = "An error occurred when writing the auto complete lists to file: " +
                    e.getMessage();
            Log.e(TAG, msg);
            Toast.makeText(App.getAppContext(), msg, Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Method for refreshing auto complete string lists from file
     */
    private void loadAutoCompleteListsFromFile() {

        try {
            //open the file and set a reader
            File listsFile = new File(App.getAppContext().getFilesDir(), "ac_lists.dat");
            if (!listsFile.exists()) {
                this.autoCompleteLists = new AutoCompleteLists();
            }
            else {
                BufferedReader reader = new BufferedReader(new FileReader(listsFile));
                Type AutoCompleteListType = new TypeToken<AutoCompleteLists>() {
                }.getType();
                //read in the contents to a json object
                String json = reader.readLine();
                this.autoCompleteLists = new Gson().fromJson(json, AutoCompleteListType);
                reader.close();
            }
        }
        catch (IOException e) {
            String msg = "An error occurred when loading the auto complete lists from from file: " +
                    e.getMessage();
            Toast.makeText(App.getAppContext(), msg, Toast.LENGTH_LONG).show();
            if (autoCompleteLists == null) {
                this.autoCompleteLists = new AutoCompleteLists();
            }
        }
    }

    /**
     * For adding strings to the auto complete lists
     *
     * @param name          a new device name
     * @param deviceId      a new deviceId
     * @param securityToken a new securityToken
     * @param wifiAddress   a new wifiAddress
     */
    private void addAutoCompleteEntries(String name, String deviceId, String securityToken,
                                        String wifiAddress, String phoneNumber, String email) {
        boolean updated = false; //were any of the sets actually updated?
        if (name != null && !name.isEmpty())
            updated = autoCompleteLists.names.add(name);
        if (deviceId != null && !deviceId.isEmpty())
            updated = autoCompleteLists.deviceIds.add(deviceId) || updated;
        if (securityToken != null && !securityToken.isEmpty())
            updated = autoCompleteLists.securityTokens.add(securityToken) || updated;
        if (wifiAddress != null && !wifiAddress.isEmpty())
            updated = autoCompleteLists.wifiAddresses.add(wifiAddress) || updated;
        if (phoneNumber != null && !phoneNumber.isEmpty())
            updated = autoCompleteLists.phoneNumbers.add(phoneNumber) || updated;
        if (email != null && !email.isEmpty())
            updated = autoCompleteLists.emailAddresses.add(email) || updated;
        if (updated)
            saveAutoCompleteListsToFile();
    }

    /**
     * For adding phone and email autocomplete entries from the new rule activity
     *
     * @param phone
     * @param email
     */
    public void addRuleAutoCompleteEntries(String phone, String email) {
        addAutoCompleteEntries(null, null, null, null, phone, email);
    }


    /**
     * Gets the NUMBER of configs in the config store
     *
     * @return the number; -1 if configStore is null
     */
    int getConfigCount() {
        if (configStore != null) {
            return configStore.getSize();
        }
        else {
            return -1;
        }
    }

    /**
     * Gets a configuration from the store at a given position
     *
     * @param position the index of the config in the store
     * @return the Configuration object; null if configStore is null
     */
    Configuration getConfig(int position) {
        if (configStore != null) {
            return configStore.getConfig(position);
        }
        else {
            return null;
        }
    }


    /**
     * Gets a configuration from the store at a given position
     *
     * @param configId the configId of the config in the store
     * @return the Configuration object; null if configStore is null
     */
    public Configuration getConfig(String configId) {
        if (configStore != null) {
            return configStore.getConfig(configId);
        }
        else {
            return null;
        }
    }

    /**
     * Method for removing a specific configuration
     *
     * @param config the configuration to remove
     */
    void removeConfiguration(Configuration config) {
        if (configStore == null)
            return;
        if (configStore.removeConfiguration(config)) {
            saveConfigsToFile();
        }
    }

    /**
     * Clears the configstore and starts fresh with an empty one
     */
    void clearConfigs() {
        configStore = new ConfigurationStore();
        saveConfigsToFile();
    }

    /**
     * Stores an object temporarily
     */
    synchronized void storeObject(Object o) {
        tempObject = o;
    }

    /**
     * Retrieves the stored object. tempObject is cleared upon completion to avoid memory leaks.
     *
     * @return the object
     */
    synchronized Object retrieveStoredObject() {
        Object out = tempObject;
        clearStoredObject();
        return out;
    }

    /**
     * Gets the stored object without clearing it.
     *
     * @return the object
     */
    synchronized Object peekStoredObject() {
        return tempObject;
    }


    /**
     * Retrieves the stored object. tempObject is nullified upon completion to avoid memory leaks.
     *
     * @return the object
     */
    synchronized void clearStoredObject() {
        tempObject = null;
    }

    /**
     * Gets the type (class) of the stored object
     *
     * @return a Class object representing the type of the stored object
     */
    synchronized Class getStoredObjectType() {
        if (tempObject != null) {
            return tempObject.getClass();
        }
        else {
            return null;
        }
    }

    /**
     * Method for tearing down the currently running network object
     */
    public void teardownNetwork() {
        if (network != null) {
            network.teardown();
            network = null;
        }
    }

    /**
     * Method for intializing a new network object
     *
     * @param newNetwork the new network object
     * @throws TimeoutException            if connection takes too long
     * @throws Network.ConnectionException if initialization fails for another reason
     */
    void initializeNetwork(MasterNetwork newNetwork) throws TimeoutException,
            Network.ConnectionException {

        //clean up the old one
        teardownNetwork();

        //update with the new one
        network = newNetwork;

        // initialize
        try {
            network.init();
        }
        catch (TimeoutException | Network.ConnectionException e) {
            teardownNetwork(); //reverse the work done so far
            throw e; //pass the exception up to the caller
        }
    }

    /**
     * The P2P version of initialize Network, as that object's init() method is asynchronous and
     * takes a listener object
     * @param newNetwork the network object
     * @param listener the async listener
     * @throws TimeoutException if init takes too long
     * @throws Network.ConnectionException if fails otherwise
     */
    void initializeP2PNetwork(MasterP2PNetwork newNetwork,
                              MasterP2PNetwork.InitSuccessListener listener)
            throws TimeoutException,
            Network.ConnectionException {

        teardownNetwork();
        network = newNetwork;
        try {
            newNetwork.init(listener);
        }
        catch (TimeoutException | Network.ConnectionException e) {
            teardownNetwork(); //reverse the work done so far
            throw e; //pass the exception up to the caller
        }
    }

    /**
     * For storing lists of auto-complete entries for the wizard text fields
     */
    public class AutoCompleteLists {
        public HashSet<String> names = new HashSet<>();
        public HashSet<String> deviceIds = new HashSet<>();
        public HashSet<String> securityTokens = new HashSet<>();
        public HashSet<String> wifiAddresses = new HashSet<>();
        public HashSet<String> phoneNumbers = new HashSet<>();
        public HashSet<String> emailAddresses = new HashSet<>();
    }

}