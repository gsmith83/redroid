package net.redroid.redroidmaster;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import static android.content.ContentValues.TAG;

/**
 * Refreshes the firebase token, if updated
 */
public class RedroidFirebaseInstanceIdService extends FirebaseInstanceIdService {
    MasterDataModel model = MasterDataModel.getInstance();
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        if (model != null) {
            String token = FirebaseInstanceId.getInstance().getToken();
            Log.d(TAG, "Refreshed Token: " + token);
        }
    }
}
