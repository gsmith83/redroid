package net.redroid.redroidmaster;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import net.redroid.redroidcorelibrary.Configuration;
import net.redroid.redroidmaster.network.MasterP2PNetwork;
import net.redroid.redroidmaster.network.MasterServerNetwork;
import net.redroid.redroidmaster.network.MasterWiFiNetwork;
import java.net.URL;
import java.net.URLConnection;

/**
 * Handles incoming Redroid rules notifications. If an image url is included, it downloads the image
 * and adds it to the notification.
 */
public class RedroidFirebaseMessagingService extends FirebaseMessagingService {

    final static String TAG = "FirebaseMessagingSvc";
    MasterDataModel model;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        super.onMessageReceived(remoteMessage);
        model = MasterDataModel.getInstance();
        Bitmap bigPic = null;

        //unpack the data values
        String configId = remoteMessage.getData().get("configId");
        String imageUrl = remoteMessage.getData().get("image");

        if (imageUrl != null && !imageUrl.isEmpty()) {
            //download the image
            try {

                URL url = new URL(imageUrl);
                URLConnection conn = url.openConnection();
                bigPic = BitmapFactory.decodeStream(conn.getInputStream());
            }
            catch (Exception e) {
                Log.d(TAG, "onMessageReceived: failed to fetch notification image: " + e.getMessage());

                e.printStackTrace();
            }
        }

        Configuration targetConfig = model.getConfig(configId);
        if (targetConfig == null)
            return;
        Configuration.ConfigType targetConfigType = targetConfig.getConfigType();

        //determine target activity based on type
        Class targetActivity = null;
        try {
            if (targetConfigType == Configuration.ConfigType.CUSTOM) {
                targetActivity = Class.forName("net.redroid.redroidmaster.CustomConfigActivity");
            }
            else if (targetConfigType == Configuration.ConfigType.BIKE_CAM) {
                targetActivity = Class.forName("net.redroid.redroidmaster.BikeCamActivity");
            }
            else if (targetConfigType == Configuration.ConfigType.SECURITY_CAM ||
                    targetConfigType == Configuration.ConfigType.PET_CAM) {
                targetActivity = Class.forName("net.redroid.redroidmaster" +
                        ".SecurityPetCamConfigActivity");
            }
            if (targetActivity == null)
                throw new ClassNotFoundException("Failed to set targetActivity from configType");
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
            Log.e(TAG, "invokeTargetActivity: " + e);
            return;
        }

        //determine target activity network mode
        Configuration.NetworkMode targetNetworkMode = targetConfig.getNetworkMode();
        Class targetNetworkClass = null;
        if (targetNetworkMode == Configuration.NetworkMode.WIFI) {
            targetNetworkClass = MasterWiFiNetwork.class;
        }
        else if (targetNetworkMode == Configuration.NetworkMode.SERVER) {
            targetNetworkClass = MasterServerNetwork.class;
        }
        else if (targetNetworkMode == Configuration.NetworkMode.P2P) {
            targetNetworkClass = MasterP2PNetwork.class;
        }

        //make the intent
        Intent intent;

        //check if the current network matches that of the target activity (and is not in the middle
        // of init) and a set the intent to it, if so
        if (model.network != null && model.network.getClass() == targetNetworkClass && !(model
                .currentActivity instanceof InitActivity) && !(model.currentActivity instanceof
                TestConfigActivity)) {
            intent = new Intent(this, targetActivity);
        }
        // otherwise, go via the InitActivity to initialize the network and open it
        else {
            intent = new Intent(this, InitActivity.class);
        }

        //make sure the notification contains a data payload and pass the relevant fields into
        // the intent
        if (remoteMessage.getData().size() > 0) {
            intent.putExtra("configId", remoteMessage.getData().get("configId"));
            intent.putExtra("ruleId", remoteMessage.getData().get("ruleId"));
            intent.putExtra("targetActivity", targetActivity.getCanonicalName());
        }

        //prepare the pending intent
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent
                .FLAG_ONE_SHOT);

        //prepare the trigger the notification
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setContentTitle("Redroid Notification")
                .setContentText(remoteMessage.getNotification().getBody())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setSmallIcon(R.drawable.bot_notif_icon_white)
                .setColor(getResources().getColor(R.color.colorAccent))
                .setStyle(bigPic == null? null:new NotificationCompat.BigPictureStyle().bigPicture(bigPic))
                .setContentIntent(pendingIntent);
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(0,
                notificationBuilder.build());
    }
}