package net.redroid.redroidmaster;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import net.redroid.redroidcorelibrary.Configuration;


/**
 * An activity for displaying a Pet Cam or Security Cam config. Includes an embedded
 * ViewViewFragment (Vitamio) in full-screen landscape.
 */
public class SecurityPetCamConfigActivity extends MasterConfigActivity {


    private static final String TAG = "CustomUIActivity";
    MasterDataModel model = MasterDataModel.getInstance();
    Configuration config;

    // Video Params
    final String[] rtmpURL = {null};

    //UI views
    VideoViewFragment videoFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security_pet_cam_config);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        model.currentActivity = this;

        //capture and set the title of the activity in the title bar
        getSupportActionBar().setTitle("");

        //get the config
        final Configuration config = super.config;

        //instantiate the video fragment
        videoFragment = new VideoViewFragment();
        videoFragment.setConfig(config);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.video_view_fragment_frame, videoFragment)
                .commit();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        return super.onCreateOptionsMenu(menu);
//    }

    @Override
    public void onBackPressed() {
        videoFragment.stopPlayback();
        super.onBackPressed();
    }
}
