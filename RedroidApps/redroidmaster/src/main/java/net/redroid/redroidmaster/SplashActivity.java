package net.redroid.redroidmaster;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import net.redroid.redroidmaster.debug.DebugMenuActivity;
import net.redroid.redroidmaster.debug.TestVideoViewActivity2;

/**
 * Activity for showing the Redroid logo on boot
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        MasterDataModel.getInstance().currentActivity = this;

        //make a countdown timer for splash timeout
        //long TIME = (MasterDataModel.DEBUG)?  400 : 1500;

        long TIME = 2000;

        final CountDownTimer timer = new CountDownTimer(TIME, TIME) {
            public void onFinish() {
                invokeHomeActivity();
            }

            @Override
            public void onTick(long millisUntilFinished) {}
        };

        //start the timer
        timer.start();

        //set a listener to close the splash screen when touched
        LinearLayout view = (LinearLayout)findViewById(R.id.activity_splash);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                invokeHomeActivity();
            }
        });
        Button debugMenuButton = (Button)findViewById(R.id.debug_menu);
        debugMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                invokeVideoStreamingActivity();
            }
        });

    }

    private void invokeDebugMenuActivity(){
        startActivity(new Intent(getBaseContext(), DebugMenuActivity.class));
        finish();
    }

    private void invokeHomeActivity()  {
        Intent textReadingIntent = new Intent(getBaseContext(), HomeActivity.class);
        startActivity(textReadingIntent);
        finish(); //so you can't go back to the splash screen
    }

    public void invokeVideoStreamingActivity(){
        startActivity(new Intent(getBaseContext(), TestVideoViewActivity2.class));
        finish();
    }
}
