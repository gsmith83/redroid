package net.redroid.redroidmaster;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import net.redroid.redroidcorelibrary.API;
import net.redroid.redroidcorelibrary.Configuration;
import net.redroid.redroidcorelibrary.FeatureList;
import net.redroid.redroidcorelibrary.Network;
import net.redroid.redroidcorelibrary.sensor.SensorList;
import net.redroid.redroidmaster.network.MasterNetwork;
import net.redroid.redroidmaster.network.MasterP2PNetwork;
import net.redroid.redroidmaster.network.MasterServerNetwork;
import net.redroid.redroidmaster.network.MasterWiFiNetwork;
import org.json.JSONObject;
import java.util.concurrent.TimeoutException;


/**
 * An activity for testing a newly-created config to make sure it is valid. Contacts the
 * peripheral and performs a two-step handshake to validate tokens, then fetches the sensor and
 * features lists.
 */
public class TestConfigActivity extends AppCompatActivity {

    //for putting string extras in the intent for the next activity
    public final static String CONFIG = "CONFIG";
    MasterDataModel model = MasterDataModel.getInstance();
    Configuration config;
    Button finishButton;
    String configTypeString;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_config);
        model.currentActivity = this;

        //retrieve the config object from the model
        if (model.getStoredObjectType() == Configuration.class)
            config = (Configuration) model.retrieveStoredObject();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Thread(new Runnable() {
            @Override
            public void run() {
                test();
            }
        }).start();
    }

    private void test() {
        //detect and prepare proper network mode
        final Configuration.NetworkMode networkMode = config.getNetworkMode();
        MasterNetwork network = null;
        switch (networkMode) {
            case WIFI:
                network = new MasterWiFiNetwork(config.getWifiIpAddress());
                break;
            case P2P:
                network = new MasterP2PNetwork(config.getDeviceId(), config.getSecurityToken());
                break;
            case SERVER:
                network = new MasterServerNetwork();
                break;
            default:
                returnTestFailure("invalid network mode or network mode not implemented yet");
                break;
        }

        //do network initialization
        try {
            if (network instanceof MasterP2PNetwork) {
                //async version required for p2p initialization
                model.initializeP2PNetwork((MasterP2PNetwork) network, new MasterP2PNetwork
                        .InitSuccessListener() {
                    @Override
                    public void onInitSuccess() {
                        postInit(networkMode);
                    }

                    @Override
                    public void onInitFailure(String msg) {
                        Log.e("TestConfigActivity", "onInitFailure: initializeP2PNetwork" + msg);
                        returnTestFailure(msg);
                    }
                });
            }
            else {
                //synchronous version
                model.initializeNetwork(network);
                postInit(networkMode);
            }
        }
        catch (TimeoutException | Network.ConnectionException e) {
            returnTestFailure(e.toString());
            return;
        }
    }

    /**
     * Tasks to be peformed following network object init
     * @param networkMode the network mode specified in the config
     */
    private void postInit(Configuration.NetworkMode networkMode) {
        // prepare remoteHost string
        String remoteHost = "xxx";
        switch (networkMode) {
            case WIFI:
                remoteHost = String.format("%s:%s", config.getWifiIpAddress(),
                        Network.SERVER_API_PORT);
                break;
            case P2P:
                remoteHost = String.format("%s:%s", model.network.getRemoteIpAddress(),
                        Network.SERVER_API_PORT);
                break;
            case SERVER:
                remoteHost = String.format("%s:%s", model.getConnectionServerUrl(), Network
                        .SERVER_API_PORT);
                break;
            default:
                returnTestFailure("invalid network mode or network mode not implemented yet");
                return;
        }

        //now do the handshake
        String handshakeResult = model.api.performHandshake(config.getDeviceId(), config
                        .getSecurityToken(),
                remoteHost);
        if (handshakeResult != null) {
            returnTestFailure(handshakeResult);
            return;
        }

        //success! Now fetch the sensor and feature lists
        //sensor list first:
        model.api.requestGetSensorList(new API.ApiResponseListener() {
            @Override
            public void onApiResponse(int responseCode, JSONObject response) {
                if (response != null && responseCode == 200) {
                    String sensorListJson = response.toString();
                    config.sensorList = SensorList.fromJson(sensorListJson);

                    //success! now feature list:
                    model.api.requestGetFeatureList(new API.ApiResponseListener() {
                        @Override
                        public void onApiResponse(int responseCode, JSONObject response) {
                            if (response != null && responseCode == 200) {
                                String featureListJson = response.toString();
                                config.featureList = FeatureList.fromJson(featureListJson);

                                //all done with testing the config - tear down the network and
                                // move on
                                model.teardownNetwork();
                                invokeWizardSuccessActivity(config);
                            }
                            else {
                                returnTestFailure("Could not parse type list JSON, or JSON not " +
                                        "found");
                            }
                        }
                    });
                }
                else {
                    returnTestFailure("Could not parse type list JSON, or JSON not found");
                }
            }
        });
    }


    /**
     * Advances to the wizard success screen on config validaiton sucess
     *
     * @param config the configuration that was just validated
     */
    private void invokeWizardSuccessActivity(Configuration config) {
        Intent intent = new Intent(getBaseContext(), WizardSuccessActivity.class);
        model.storeObject(config);
        startActivity(intent);
        finish(); //remove from back stack
    }


    /**
     * Returns to the calling activity upon failure to validate the config
     *
     * @param msg a message describing the reason for failure
     */
    private void returnTestFailure(String msg) {
        model.teardownNetwork();
        Intent resultIntent = new Intent();
        resultIntent.putExtra(WizardConfigActivity.TEST_CONFIG_FAILURE_MSG, msg);
        setResult(RESULT_CANCELED, resultIntent);
        finish();
    }

    /**
     * for testing/development
     **/
    private boolean pretendValidation() {
        super.onResume();
        long TIME = 1000;
        final CountDownTimer timer = new CountDownTimer(TIME, TIME) {
            public void onFinish() {
                invokeWizardSuccessActivity(config);
            }

            @Override
            public void onTick(long millisUntilFinished) {
            }
        };

        //start the timer
        timer.start();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        returnTestFailure("aborted");
    }

}
