package net.redroid.redroidmaster;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import net.redroid.redroidcorelibrary.API;
import net.redroid.redroidcorelibrary.App;
import net.redroid.redroidcorelibrary.Configuration;
import net.redroid.redroidcorelibrary.sensor.SensorList;
import org.json.JSONException;
import org.json.JSONObject;
import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.Vitamio;
import io.vov.vitamio.widget.MediaController;
import io.vov.vitamio.widget.VideoView;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static net.redroid.redroidcorelibrary.RedroidUtil.Constants.FEATURE_CAMERA;
import static net.redroid.redroidcorelibrary.RedroidUtil.Constants.FEATURE_CAMERA_FLASH;
import static net.redroid.redroidcorelibrary.RedroidUtil.Constants.FEATURE_CAMERA_FRONT;


/**
 * Adapted from the Yasea VideoViewActivity for use in embedding
 */
public class VideoViewFragment extends Fragment {


    private static final String TAG = "CustomUIActivity";
    MasterDataModel model = MasterDataModel.getInstance();
    Configuration config;

    // Video Params
    final String[] rtmpURL = {null};

    //UI views
    VideoView videoStreamSurface;
    View videoFrame;
    View videoControls, videoIconOverlay, fetchingStreamOverlay, frontCameraSwitchViewGroup, flashSwitchViewGroup;
    Switch frontCameraSwitch, flashSwitch;
    ImageView muteIcon, stopButton;
    TextView flashSwitchLabel, frontCameraSwitchLabel;
    boolean isMuted = false;

    /**
     * This should always be set immediately after instantiating the fragment
     * @param config
     */
    public void setConfig(Configuration config) {
        this.config = config;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_view, container, false);



        //get handles to UI elements
        videoFrame = view.findViewById(R.id.video_view_frame_layout_wrapper);
        videoStreamSurface = (VideoView)view.findViewById(R.id.video_surface_view);
        videoControls = view.findViewById(R.id.video_controls);
        videoIconOverlay = view.findViewById(R.id.video_icon_overlay_group);
        fetchingStreamOverlay = view.findViewById(R.id.fetching_video_overlay);
        frontCameraSwitchViewGroup = view.findViewById(R.id.front_cam_switch_viewgroup);
        frontCameraSwitch = (Switch) frontCameraSwitchViewGroup
                .findViewById(R.id.switch_thing);
        frontCameraSwitchLabel = (TextView) frontCameraSwitchViewGroup.findViewById(R.id.label);
        flashSwitchViewGroup = view.findViewById(R.id.flash_switch_viewgroup);
        flashSwitch = (Switch) view.findViewById(R.id.switch_thing);
        flashSwitchLabel = (TextView) flashSwitchViewGroup.findViewById(R.id.label);
        muteIcon = (ImageView)view.findViewById(R.id.mute_icon);
        muteIcon.setVisibility(View.GONE);
        stopButton = (ImageView)view.findViewById(R.id.stop_button);

        //set visibility of elements from config options
        final SensorList sensorList = config.sensorList;

        frontCameraSwitchLabel.setText(R.string.front_camera);
        flashSwitchLabel.setText(R.string.flashlight);

        //less than 2 cameras - hide camera switch
        if (!sensorList.has(FEATURE_CAMERA) || !sensorList.has(FEATURE_CAMERA_FRONT)) {
            frontCameraSwitchViewGroup.setVisibility(GONE);
        }

        //no flash present - hide flash switch
        if (!sensorList.has(FEATURE_CAMERA_FLASH)) {
            flashSwitchViewGroup.setVisibility(GONE);
        }

        muteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isMuted) {
                    isMuted = false;
                    videoStreamSurface.setVolume(1f, 1f);
                    muteIcon.setImageResource(R.drawable.ic_volume_up_white_24dp);
                }
                else {
                    isMuted = true;
                    videoStreamSurface.setVolume(0f, 0f);
                    muteIcon.setImageResource(R.drawable.ic_volume_off_white_24dp);
                }
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRtmpStream();
            }
        });


        //try to fetch rtmp url
        new Thread(new Runnable() {
            @Override
            public void run() {
                //startRtmpViewer();
                model.api.requestGetRTMP(true, true, new API.ApiResponseListener() {
                    @Override
                    public void onApiResponse(int responseCode, JSONObject response) {
                        try {
                            if (responseCode == 200 && response != null) {
                                rtmpURL[0] = response.getString("url");
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        startRtmpViewer();
                                    }
                                });
                            } else { //404 not found
                                updateNotStreamingVideoUI();
                            }

                        } catch (JSONException e) {
                            Log.e(TAG, "onApiResponse: requestGetRTMP: Invalid JSON received for RTMP URL.");
                        }
                    }
                });
            }
        }).start();
        return view;
    }


    private void startRtmpStream() {
        //make the api call
        model.api.requestPostRtmpState(true, new API.ApiResponseListener() {
            @Override
            public void onApiResponse(int responseCode, JSONObject response) {
                if (responseCode == 200) {
                    try {
                        //extract url from json
                        rtmpURL[0] = response.getString("url");
                        //start the stream view
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startRtmpViewer();
                            }
                        });
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                        throw new RuntimeException("somebody blew it");
                    }
                }
                else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(), "Video stream failed to start.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            }
        });
    }

    private void stopRtmpStream() {
        //make the api call
        model.api.requestPostRtmpState(false, new API.ApiResponseListener() {
            @Override
            public void onApiResponse(int responseCode, JSONObject response) {
                if (responseCode == 200) {
                        //revert the stream view
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateNotStreamingVideoUI();
                            }
                        });
                }
                else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(), "Video stream failed to stop.",
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }

    private void updateNotStreamingVideoUI() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                stopButton.setVisibility(GONE);
                muteIcon.setVisibility(GONE);
                videoIconOverlay.setVisibility(VISIBLE);
                fetchingStreamOverlay.setVisibility(GONE);
                videoFrame.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startRtmpStream();
                    }
                });
            }
        });
    }

    private void updateStreamingVideoUI() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                stopButton.setVisibility(VISIBLE);
                muteIcon.setVisibility(VISIBLE);
                videoIconOverlay.setVisibility(GONE);
                fetchingStreamOverlay.setVisibility(GONE);
                videoFrame.setOnClickListener(null);
            }
        });
    }

    private void updateFetchingStreamUI() {
        if (isPlaying())
            return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                stopButton.setVisibility(GONE);
                muteIcon.setVisibility(GONE);
                fetchingStreamOverlay.setVisibility(VISIBLE);
                videoIconOverlay.setVisibility(GONE);
                videoFrame.setOnClickListener(null);
            }
        });
    }

    private void startRtmpViewer(){
        // TODO change to api call to get proper url
        synchronized (videoStreamSurface) {
            Vitamio.isInitialized(getActivity());
            updateFetchingStreamUI();
            videoStreamSurface.setVideoPath(rtmpURL[0]);
            //videoStreamSurface.setVideoPath("rtmp://24.2.102.119/live/testvideo");
            videoStreamSurface.setMediaController(new MediaController(getActivity()));
            videoStreamSurface.requestFocus();
            videoStreamSurface.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    // optional need Vitamio 4.0
                    mediaPlayer.setPlaybackSpeed(1.0f);
                    updateStreamingVideoUI();
                }
            });
            videoStreamSurface.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    Toast.makeText(getActivity(), "video player error",
                            Toast.LENGTH_SHORT).show();
                    fetchingStreamOverlay.setVisibility(GONE);
                    return false;
                }
            });
            videoStreamSurface.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    new AlertDialog.Builder(App.getAppContext())
                            .setTitle("Stream terminated")
                            .setMessage("The remote device's stream was stopped.")
                            .show();
                }
            });
        }
    }

    public void stopPlayback() {
        synchronized (videoStreamSurface) {
            if (videoStreamSurface != null && videoStreamSurface.isPlaying()) {
                videoStreamSurface.stopPlayback();
                videoStreamSurface = null;
            }
        }
    }

    public boolean isPlaying() {
        return videoStreamSurface.isPlaying();
    }
}
