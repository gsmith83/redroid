package net.redroid.redroidmaster;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.varvet.barcodereadersample.barcode.BarcodeCaptureActivity;

import net.redroid.redroidcorelibrary.Configuration;
import net.redroid.redroidcorelibrary.RedroidUtil;

import static android.view.inputmethod.EditorInfo.IME_ACTION_DONE;
import static android.view.inputmethod.EditorInfo.IME_ACTION_NEXT;
import static net.redroid.redroidmaster.MasterDataModel.DEBUG;


/**
 * This activity is used in the configuration wizard to enter information about a Bike Cam
 * configuration. THe user is asked to choose a name and enter the Device ID and SEcurity Token
 * from the peripheral device.
 */
public class WizardConfigActivity extends AppCompatActivity {

    TextView nameLabel, deviceIdLabel, securityTokenLabel, wifiIpLabel;
    AutoCompleteTextView nameField, deviceIdField, securityTokenField, wifiIpField;
    View wifiIp;
    RadioGroup radioGroup;
    RadioButton wifiRadioButton, serverRadioButton, p2pRadioButton;
    Button qrButton, backButton, nextButton;
    String configTypeString;
    Configuration config;
    int selected; //the id of the selected radio button
    MasterDataModel model = MasterDataModel.getInstance();

    //for putting string extras in the intent for the next activity
    public final static String CONFIG = "CONFIG";
    public final static String CONFIG_TYPE = "CONFIG_TYPE";
    public final static int SERVER_URL_UPDATE_REQUEST = 1;
    public final static int TEST_CONFIG_REQUEST = 2;
    public final static int CAPTURE_QR_CODE = 3;
    public final static String TEST_CONFIG_FAILURE_MSG = "MSG";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wizard_config);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        model.currentActivity = this;

        //capture and set the title of the activity in the title bar
        configTypeString = getIntent().getStringExtra(WizardHomeActivity.CONFIG_TYPE);
        toolbar.setTitle(String.format("%s: %s",
                getResources().getString(R.string.new_configuration), configTypeString));

        // set up the back button in the title bar
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        //get the view handles
        nameLabel = (TextView) findViewById(R.id.name_label);
        nameField = (AutoCompleteTextView) findViewById(R.id.name_field);
        deviceIdLabel = (TextView) findViewById(R.id.device_id_label);
        deviceIdField = (AutoCompleteTextView) findViewById(R.id.device_id_field);
        securityTokenLabel = (TextView) findViewById(R.id.security_token_label);
        securityTokenField = (AutoCompleteTextView) findViewById(R.id.security_token_field);
        wifiIp = findViewById(R.id.wifi_ip);
        wifiIpLabel = (TextView) findViewById(R.id.wifi_ip_label);
        wifiIpField = (AutoCompleteTextView) findViewById(R.id.wifi_ip_field);
        radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        wifiRadioButton = (RadioButton) findViewById(R.id.wifi);
        serverRadioButton = (RadioButton) findViewById(R.id.server);
        p2pRadioButton = (RadioButton) findViewById(R.id.p2p);
        qrButton = (Button) findViewById(R.id.get_qr_button);
        backButton = (Button) findViewById(R.id.back_button);
        nextButton = (Button) findViewById(R.id.next_button);

        //force p2p for bike cam
        if (configTypeString.equals("Bike Cam")) {
//            wifiRadioButton.setEnabled(false);
            wifiRadioButton.setEnabled(true);
            serverRadioButton.setEnabled(false);
            p2pRadioButton.setChecked(true);
        }

        /*** temp   **/
        if (DEBUG) {
            nameField.setText(String.format("%s ", configTypeString));
            deviceIdField.setText("");
            securityTokenField.setText("");
            wifiIpField.setText("");
            wifiRadioButton.setChecked(true);
        }
        /*************/

        //set the auto-conmplete array adapters for the fields
        String[] names = (String[]) model.autoCompleteLists.names.toArray(new String[0]);
        ArrayAdapter<String> namesAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, names);
        nameField.setAdapter(namesAdapter);

        String[] deviceIds = (String[]) model.autoCompleteLists.deviceIds.toArray(new String[0]);
        ArrayAdapter<String> deviceIdsAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, deviceIds);
        deviceIdField.setAdapter(deviceIdsAdapter);

        String[] securityTokens = (String[]) model.autoCompleteLists.securityTokens.toArray(new String[0]);
        ArrayAdapter<String> securityTokensAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, securityTokens);
        securityTokenField.setAdapter(securityTokensAdapter);

        String[] wifiAddresss = (String[]) model.autoCompleteLists.wifiAddresses.toArray(new String[0]);
        ArrayAdapter<String> wifiAddresssAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, wifiAddresss);
        wifiIpField.setAdapter(wifiAddresssAdapter);

        //trigger the watcher once time at the outset to inspect any pre-set values
        textWatcher.afterTextChanged(nameField.getEditableText());

        //set the text field listeners
        nameField.addTextChangedListener(textWatcher);
        nameField.setSelection(nameField.getText().length());
        deviceIdField.addTextChangedListener(textWatcher);
        securityTokenField.addTextChangedListener(textWatcher);
        wifiIpField.addTextChangedListener(textWatcher);

        //call the callback once in case the fields are pre-populated
        //textWatcher.afterTextChanged(null);

        //set the button listeners
        qrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), BarcodeCaptureActivity.class);
                startActivityForResult(intent, CAPTURE_QR_CODE);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //close the current activity and go back to the previous
                finish();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameField.getText().toString();
                String deviceId = deviceIdField.getText().toString();
                String securityToken = securityTokenField.getText().toString();
                String ipAddress = wifiIpField.getText().toString();

                //check for valid server url if they chose Server
                if (selected == R.id.server && model.getConnectionServerUrl().equals("")) {
                    divertToServerUrlActivity();
                    return;
                }

                Configuration.NetworkMode networkMode;
                switch (selected) {
                    case R.id.wifi:
                        networkMode = Configuration.NetworkMode.WIFI;
                        break;
                    case R.id.server:
                        networkMode = Configuration.NetworkMode.SERVER;
                        break;
                    case R.id.p2p:
                        networkMode = Configuration.NetworkMode.P2P;
                        break;
                    default:
                        networkMode = Configuration.NetworkMode.WIFI_AND_SERVER;
                        break;
                }


                Configuration.ConfigType configType = null;
                switch (configTypeString) {
                    case "Baby Monitor":
                        configType = Configuration.ConfigType.BABY_MONITOR;
                        break;
                    case "Security Camera":
                        configType = Configuration.ConfigType.SECURITY_CAM;
                        break;
                    case "Pet Cam":
                        configType = Configuration.ConfigType.PET_CAM;
                        break;
                    case "Bike Cam":
                        configType = Configuration.ConfigType.BIKE_CAM;
                        break;
                    case "Custom":
                        configType = Configuration.ConfigType.CUSTOM;
                        break;
                }

                //construct a config object from the sensors and invoke the Test Config Activity
                config = new Configuration(name, networkMode, configType, deviceId, securityToken);
                config.setWifiIpAddress(ipAddress);
                invokeTestConfigActivity(config);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        // see if any radio button is already checked
        int checkedId = radioGroup.getCheckedRadioButtonId();

        // trigger the checked callback if so
        if (checkedId != -1) {
            onRadioButtonClicked(findViewById(checkedId));
        }
    }

    /**
     * Handles clicks to the network mode radio button group
     * @param view
     */
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // add or remove the IP address field for wifi
        if (view == wifiRadioButton) {
            wifiIp.setVisibility(View.VISIBLE);
            textWatcher.afterTextChanged(wifiIpField.getEditableText());
            securityTokenField.setImeOptions(IME_ACTION_NEXT);
        } else {
            wifiIp.setVisibility(View.GONE);
            textWatcher.afterTextChanged(wifiIpField.getEditableText());
            securityTokenField.setImeOptions(IME_ACTION_DONE);
        }

        //  capture the id of the selected radio button
        selected = view.getId();
    }


    /**
     * Invokes the activity for settings a configuration's network mode
     * @param config a Configuration object containing the sensors set by the user
     */
    protected void invokeTestConfigActivity(Configuration config) {
        Intent intent = new Intent(getBaseContext(),
                TestConfigActivity.class);
        model.storeObject(config);
        intent.putExtra(CONFIG_TYPE, configTypeString);
        startActivityForResult(intent, TEST_CONFIG_REQUEST);
    }

    /**
     * Invokes the activity for choosing custom configuration options
     * @param config a Configuration object containing the sensors set by the user
     */
    private void invokeCustomConfigActivity(Configuration config) {
        Intent intent = new Intent(getBaseContext(),
                WizardCustomConfigActivity.class);
        model.storeObject(config);
        intent.putExtra(CONFIG_TYPE, configTypeString);
        startActivity(intent);
    }

    /**
     * TextWatcher object to watch the three text fields in the activity and activate the
     * Finish button when the fields are properly populated
     */
    private final TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable s) {


            //check to make sure all three text fields are non-empty and ID's are proper length
            String name = nameField.getText().toString();
            String deviceId = deviceIdField.getText().toString();
            String securityToken = securityTokenField.getText().toString();
            String wifiIp = wifiIpField.getText().toString();

            //validate text field inputs before activating next button
            if (!wifiRadioButton.isChecked() && !name.equals("") && deviceId.length() == 8
                    && securityToken.length() == 6) {
                //enable the button
                nextButton.setEnabled(true);
            } else if (wifiRadioButton.isChecked() && !name.equals("") && deviceId.length() == 8
                    && securityToken.length() == 6 && RedroidUtil.isValidIpString(wifiIp)) {
                //enable the button
                nextButton.setEnabled(true);
            } else {
                nextButton.setEnabled(false);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };


    /**
     * The purpose of the following 2 methods is in case the server url has not been set yet,
     * the user will be forced to the server url activity to set it. If they return having set it
     * successfully, this will trigger a snackbar message informing the user that the url was
     * updated.
     */
    private void divertToServerUrlActivity() {
        //make a confirmation alert dialog before deleting
        AlertDialog.Builder builder = new AlertDialog.Builder(WizardConfigActivity.this);
        builder.setMessage(getResources().getString(R.string.update_server_url_msg))
                .setTitle(getResources().getString(R.string.server_url_not_set_dialog_title));
        builder.setPositiveButton(R.string.set_server_url, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(getBaseContext(),
                        ServerUrlActivity.class);
                startActivityForResult(intent, SERVER_URL_UPDATE_REQUEST);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAPTURE_QR_CODE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    String[] fields = barcode.displayValue.split("\n");
                    if (fields.length != 2 && fields.length != 3) {
                        playQrFailAlert();
                        return;
                    }
                    playQrSuccessAlert();
                    deviceIdField.setText(fields[0]);
                    securityTokenField.setText(fields[1]);
                    if (fields.length == 3) {
                        wifiIpField.setText(fields[2]);
                    }
                }
            }

            //when result comes from updating server url
            if (requestCode == SERVER_URL_UPDATE_REQUEST) {
                if (resultCode != RESULT_OK) {
                    return;
                }
                Snackbar.make(this.findViewById(R.id.activity_wizard_config),
                        getResources().getString(R.string.url_updated),
                        Snackbar.LENGTH_LONG).show();
            }

            //when result comes from testing a config
            if (requestCode == TEST_CONFIG_REQUEST) {
                if (resultCode != RESULT_OK) {
                    String msg = null;
                    if (data != null) {
                        msg = data.getStringExtra(TEST_CONFIG_FAILURE_MSG);
                    }
                    if (msg != null && !msg.isEmpty()) {
                        Snackbar.make(this.findViewById(R.id.activity_wizard_config),
                                getResources().getString(R.string.test_config_failed, msg),
                                Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        }
    }

    /**
     * Plays a success sound when QR scan is successful
     */
    private void playQrFailAlert() {
        MediaPlayer player = MediaPlayer.create(this, R.raw.qr_fail);
        player.start();
    }

    /**
     * Plays a failure sound when QR scan fails
     */
    private void playQrSuccessAlert() {
        MediaPlayer player = MediaPlayer.create(this, R.raw.qr_success);
        player.start();
    }
}
