package net.redroid.redroidmaster;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import net.redroid.redroidcorelibrary.Configuration;

import static net.redroid.redroidcorelibrary.RedroidUtil.Constants.FEATURE_CAMERA;
import static net.redroid.redroidcorelibrary.RedroidUtil.Constants.FEATURE_CAMERA_FRONT;
import static net.redroid.redroidcorelibrary.RedroidUtil.Constants.FEATURE_MICROPHONE;
import static net.redroid.redroidcorelibrary.RedroidUtil.Constants.TYPE_ACCELEROMETER;
import static net.redroid.redroidcorelibrary.RedroidUtil.Constants.TYPE_LIGHT;
import static net.redroid.redroidcorelibrary.RedroidUtil.Constants.TYPE_MAGNETIC_FIELD;
import static net.redroid.redroidcorelibrary.RedroidUtil.Constants.TYPE_PROXIMITY;
import static net.redroid.redroidcorelibrary.RedroidUtil.Constants.TYPE_SIGNIFICANT_MOTION;
import static net.redroid.redroidcorelibrary.RedroidUtil.Constants.TYPE_STEP_COUNTER;
import static net.redroid.redroidcorelibrary.RedroidUtil.Constants.TYPE_TEMPERATURE;
import static net.redroid.redroidcorelibrary.RedroidUtil.Constants.TYPE_TILT_DETECTOR;

/**
 * An activity for choosing the sensors, features, and settings to be included in a custom config.
 */
public class WizardCustomConfigActivity extends AppCompatActivity {


    MasterDataModel model = MasterDataModel.getInstance();
    CheckBox streamingMediaCheckBox, videoCheckBox, audioCheckBox, backCameraCheckBox,
            frontCameraCheckBox, locationCheckBox, mapCheckBox, coordinateFieldsCheckBox,
            linearAccelerationCheckBox, lightCheckBox, proximityCheckBox, temperatureCheckBox,
            magneticCheckBox, stepCountingCheckBox, tiltCheckBox, motionCheckBox;
    RadioGroup mediaRadioGroup;
    RadioButton rtmpRadioButton, mjpegRadioButton;
    View contentRoot, mediaTypeOptions, locationOptions;
    Button backButton, finishButton;
    Configuration config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wizard_custom_config);
        Toolbar toolbar = (Toolbar) findViewById(R.id.custom_config_toolbar);
        setSupportActionBar(toolbar);
        model.currentActivity = this;

        //extract the config object from the intent
        if (model.getStoredObjectType() == Configuration.class)
            config = (Configuration)model.retrieveStoredObject();

        //capture and set the title of the activity in the title bar
        getSupportActionBar().setTitle(String.format("%s: %s",
                getResources().getString(R.string.new_configuration), config.getName()));

        //fetch handles for views
        contentRoot = findViewById(R.id.custom_config_content);
        streamingMediaCheckBox = (CheckBox)findViewById(R.id.stream_media_checkbox);
        mediaTypeOptions = findViewById(R.id.streaming_media_options);
        mediaRadioGroup = (RadioGroup)findViewById((R.id.media_radio_group));
        rtmpRadioButton = (RadioButton)findViewById(R.id.rtmp_radio_button);
        mjpegRadioButton = (RadioButton)findViewById(R.id.mjpeg_radio_button);
        videoCheckBox = (CheckBox)findViewById(R.id.video_checkbox);
        audioCheckBox = (CheckBox)findViewById(R.id.audio_checkbox);
        backCameraCheckBox =(CheckBox)findViewById(R.id.back_camera_checkbox);
        frontCameraCheckBox = (CheckBox)findViewById(R.id.front_camera_checkbox);
        locationOptions = findViewById(R.id.location_options);
        locationCheckBox = (CheckBox)findViewById(R.id.location_checkbox);
        mapCheckBox = (CheckBox)findViewById(R.id.map_checkbox);
        coordinateFieldsCheckBox = (CheckBox)findViewById(R.id.coordinate_fields_checkbox);
        linearAccelerationCheckBox = (CheckBox)findViewById(R.id.linear_acceleration_checkbox);
        lightCheckBox = (CheckBox)findViewById(R.id.light_checkbox);
        proximityCheckBox = (CheckBox)findViewById(R.id.proximity_checkbox);
        temperatureCheckBox = (CheckBox)findViewById(R.id.temperature_checkbox);
        magneticCheckBox = (CheckBox)findViewById(R.id.magnetic_field_checkbox);
        stepCountingCheckBox = (CheckBox)findViewById(R.id.step_counting_checkbox);
        tiltCheckBox = (CheckBox)findViewById(R.id.tilt_detection_checkbox);
        motionCheckBox = (CheckBox)findViewById(R.id.motion_detection_checkbox);
        backButton = (Button)findViewById(R.id.custom_config_back_button);
        finishButton = (Button)findViewById(R.id.custom_config_finish_button);


        //set visibilities
        if (config.featureList.FEATURE_CAMERA || config.featureList.FEATURE_CAMERA_FRONT ||
                config.featureList.FEATURE_MICROPHONE) {

            streamingMediaCheckBox.setVisibility(View.VISIBLE);

            //enable the video option if there is a camera, and default to selected
            if (config.featureList.FEATURE_CAMERA || config.featureList.FEATURE_CAMERA_FRONT) {
                videoCheckBox.setEnabled(true);
                videoCheckBox.setChecked(true);
            }
            //otherwise, audio only: disable MJPEG as an option and force RMTP
            else {
                mjpegRadioButton.setEnabled(false);
                mjpegRadioButton.setChecked(false);
                rtmpRadioButton.setEnabled(true);
                rtmpRadioButton.setChecked(true);
            }
            //if a microphone is present, enable the audio option and default to checked
            if (config.featureList.FEATURE_MICROPHONE) {
                audioCheckBox.setEnabled(true);
                audioCheckBox.setChecked(true);
            }
            //if a back camera is present, enable the option and default to checked
            if (config.featureList.FEATURE_CAMERA) {
                backCameraCheckBox.setEnabled(true);
                backCameraCheckBox.setChecked(true);
            }
            //if a front camera is present, enabled the option and default to checked
            if (config.featureList.FEATURE_CAMERA_FRONT) {
                frontCameraCheckBox.setEnabled(true);
                frontCameraCheckBox.setChecked(true);
            }
        }
        if (config.featureList.FEATURE_LOCATION)
            locationCheckBox.setVisibility(View.VISIBLE);
        if (config.sensorList.has(TYPE_ACCELEROMETER))
            linearAccelerationCheckBox.setVisibility(View.VISIBLE);
        if (config.sensorList.has(TYPE_LIGHT))
            lightCheckBox.setVisibility(View.VISIBLE);
        if (config.sensorList.has(TYPE_PROXIMITY))
            proximityCheckBox.setVisibility(View.VISIBLE);
        if (config.sensorList.has(TYPE_TEMPERATURE))
            temperatureCheckBox.setVisibility(View.VISIBLE);
        if (config.sensorList.has(TYPE_MAGNETIC_FIELD))
            magneticCheckBox.setVisibility(View.VISIBLE);
//        if (config.sensorList.has(TYPE_STEP_COUNTER))
//            stepCountingCheckBox.setVisibility(View.VISIBLE);
//        if (config.sensorList.has(TYPE_TILT_DETECTOR))
//            tiltCheckBox.setVisibility(View.VISIBLE);
//        if (config.sensorList.has(TYPE_SIGNIFICANT_MOTION))
//            motionCheckBox.setVisibility(View.VISIBLE);



        //set finish and back listeners
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set the config's options variables based on checked boxes
                Configuration.Options options = config.getOptions();
                options.streamingMedia = streamingMediaCheckBox.isChecked();
                options.rtmp = options.streamingMedia && rtmpRadioButton.isChecked();
                options.mjpeg = options.streamingMedia && mjpegRadioButton.isChecked();
                options.video = options.streamingMedia && videoCheckBox.isChecked();
                options.audio = options.streamingMedia && audioCheckBox.isChecked();
                options.backCamera = options.streamingMedia && backCameraCheckBox.isChecked();
                options.frontCamera = options.streamingMedia && frontCameraCheckBox.isChecked();
                options.location = locationCheckBox.isChecked();
                options.map = options.location && mapCheckBox.isChecked();
                options.coordinateFields = options.location && coordinateFieldsCheckBox.isChecked();
                options.linearAcceleration = linearAccelerationCheckBox.isChecked();
                options.light = lightCheckBox.isChecked();
                options.proximity = proximityCheckBox.isChecked();
                options.temperature = temperatureCheckBox.isChecked();
                options.magneticField = magneticCheckBox.isChecked();
                options.stepCounting = stepCountingCheckBox.isChecked();
                options.tiltDetection = tiltCheckBox.isChecked();
                options.motionDetection = motionCheckBox.isChecked();

                //finalize and add the configuration to the store
                model.addConfiguration(config);
                invokeHomeActivity();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        streamingMediaVisibilityLogic();
        locationVisibilityLogic();
    }

    /**
     * Controls the visibility of the streaming media options
     */
    private void streamingMediaVisibilityLogic() {
        if (streamingMediaCheckBox.isChecked()) {
            mediaTypeOptions.setVisibility(View.GONE); //HIDDEN - was VISIBLE
        }
        else {
            mediaTypeOptions.setVisibility(View.GONE);
        }
    }

    /**
     * Controls the visibility of the location options
     */
    private void locationVisibilityLogic() {
        if (locationCheckBox.isChecked()) {
            locationOptions.setVisibility(View.VISIBLE);
        }
        else {
            locationOptions.setVisibility(View.GONE);
        }
    }

    /**
     * Manages dynamic view changes when check boxes are clicked
     * @param view
     */
    public void onCheckBoxClicked(View view) {
        finishButton.setEnabled(anyChecked());

        if (view == streamingMediaCheckBox) {
            streamingMediaVisibilityLogic();
        }

        else if (view == locationCheckBox) {
            locationVisibilityLogic();
        }

        else if (view == videoCheckBox) {
            //Video and Audio cannot both be unchecked at the same time
            if (!videoCheckBox.isChecked() && !audioCheckBox.isChecked()) {
                videoCheckBox.setChecked(true);
                showSnackbar(getResources().getString(R.string.audio_video_uncheck_msg));
            }
            //if video is unchecked, disable the front and back camera options
            if (!videoCheckBox.isChecked()) {
                backCameraCheckBox.setEnabled(false);
                backCameraCheckBox.setChecked(false);
                frontCameraCheckBox.setEnabled(false);
                frontCameraCheckBox.setChecked(false);
            }
            //otherwise it was just checked -- make sure camera options are enabled IF present
            else {
                if (config.sensorList.has(FEATURE_CAMERA)) {
                    backCameraCheckBox.setEnabled(true);
                    backCameraCheckBox.setChecked(true);
                }
                if (config.sensorList.has(FEATURE_CAMERA_FRONT)) {
                    frontCameraCheckBox.setEnabled(true);
                    frontCameraCheckBox.setChecked(true);
                }
            }
        }

        else if (view == audioCheckBox && !audioCheckBox.isChecked()) {
            //Video and Audio cannot both be unchecked at the same time
            if (!videoCheckBox.isChecked()) {
                audioCheckBox.setChecked(true);
                showSnackbar(getResources().getString(R.string.audio_video_uncheck_msg));
            }
        }

        //back and front camera cannot both be unchecked at the same time
        else if (view == backCameraCheckBox && !backCameraCheckBox.isChecked()) {
            if (!frontCameraCheckBox.isChecked()) {
                backCameraCheckBox.setChecked(true);
                showSnackbar(getResources().getString(R.string.back_front_camera_uncheck_msg));
            }

        }
        else if (view == frontCameraCheckBox && !frontCameraCheckBox.isChecked()) {
            if (!backCameraCheckBox.isChecked()) {
                frontCameraCheckBox.setChecked(true);
                showSnackbar(getResources().getString(R.string.back_front_camera_uncheck_msg));
            }
        }

        //map and coordinates cannot both be unchecked at the same time
        else if (view == mapCheckBox && !mapCheckBox.isChecked()) {
            if (!coordinateFieldsCheckBox.isChecked()) {
                mapCheckBox.setChecked(true);
                showSnackbar(getResources().getString(R.string.map_coordinates_uncheck_msg));
            }

        }
        else if (view == coordinateFieldsCheckBox && !coordinateFieldsCheckBox.isChecked()) {
            if (!mapCheckBox.isChecked()) {
                coordinateFieldsCheckBox.setChecked(true);
                showSnackbar(getResources().getString(R.string.map_coordinates_uncheck_msg));
            }
        }
    }

    /**
     * Handles clicks to radio button groups
     * @param view
     */
    public void onRadioButtonClicked(View view) {
        if (mjpegRadioButton.isChecked()) {
            //disable and uncheck them audio option
            if (config.sensorList.has(FEATURE_MICROPHONE)) {
                audioCheckBox.setEnabled(false);
                audioCheckBox.setChecked(false);
            }
            //force the video option to be checked
            if (videoCheckBox.isEnabled())
                videoCheckBox.setChecked(true);
        }
        //RTMP
        else if (config.sensorList.has(FEATURE_MICROPHONE)){
            audioCheckBox.setEnabled(true);
            audioCheckBox.setChecked(true);
        }
    }

    /**
     * Invokes HomeActivity upon completion of this activity
     */
    protected void invokeHomeActivity()  {
        Intent intent = new Intent(getBaseContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    /**
     * checks whether any top-level checkboxes are checked (for activating the next button)
     * @return true if any are checked, false otherwise
     */
    private boolean anyChecked() {
        return  streamingMediaCheckBox.isChecked() ||
                locationCheckBox.isChecked() ||
                linearAccelerationCheckBox.isChecked() ||
                lightCheckBox.isChecked() ||
                proximityCheckBox.isChecked() ||
                temperatureCheckBox.isChecked() ||
                magneticCheckBox.isChecked() ||
                stepCountingCheckBox.isChecked() ||
                tiltCheckBox.isChecked() ||
                motionCheckBox.isChecked();
    }

    /**
     * Helper method for showing snackbars in this activity
     * @param msg the message to show in the snackbar
     */
    private void showSnackbar(String msg) {
        Snackbar.make(contentRoot, msg, Snackbar.LENGTH_LONG).show();
    }
}
