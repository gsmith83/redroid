package net.redroid.redroidmaster;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.support.v7.app.AppCompatActivity;

import net.redroid.redroidcorelibrary.Configuration;


/**
 * The opening screen for a new config wizard. Allows the user to choose config type.
 */
public class WizardHomeActivity extends AppCompatActivity {

    RadioGroup radioGroup;
    RadioButton babyMonitorRadioButton;
    RadioButton securityCamRadioButton;
    RadioButton bikeCamRadioButton;
    RadioButton customRadioButton;
    Button nextButton;
    int selected; //the id of the selected radio button

    public final static String CONFIG_TYPE = "CONFIG_TYPE";
    MasterDataModel model = MasterDataModel.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wizard_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        model.currentActivity = this;

        //set the title of the activity in the title bar
        toolbar.setTitle("New Configuration Wizard");

        // set up the back button in the title bar
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        radioGroup = (RadioGroup)findViewById(R.id.radio_group);
        babyMonitorRadioButton = (RadioButton)findViewById(R.id.baby_monitor);
        securityCamRadioButton = (RadioButton)findViewById(R.id.security_cam);
        bikeCamRadioButton = (RadioButton)findViewById(R.id.bike_cam);
        customRadioButton = (RadioButton)findViewById(R.id.custom);
        nextButton = (Button)findViewById(R.id.next_button);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(selected) {
                    case R.id.baby_monitor:
                        invokeWizardConfigActivity("Baby Monitor");
                        break;
                    case R.id.security_cam:
                        invokeWizardConfigActivity("Security Camera");
                        break;
                    case R.id.pet_cam:
                        invokeWizardConfigActivity("Pet Cam");
                        break;
                    case R.id.bike_cam:
                        invokeWizardConfigActivity("Bike Cam");
                        break;
                    case R.id.custom:
                        invokeWizardConfigActivity("Custom");
                        break;
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        // see if any radio button is already checked
        int checkedId = radioGroup.getCheckedRadioButtonId();

        // trigger the checked callback if so
        if (checkedId != -1) {
            onRadioButtonClicked(findViewById(checkedId));
        }
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // enable the next button
        if (checked) {
            nextButton.setEnabled(true);
        }
        else {
            return;
        }

        //  capture the id of the selected radio button
        selected = view.getId();
    }

    /**
     * Invokes the activity for settings a configuration's name and IDs
     * @param configType a string value specifying which config type was chosen
     */
    protected void invokeWizardConfigActivity(String configType)  {
        Intent intent = new Intent(getBaseContext(),
                WizardConfigActivity.class);
        intent.putExtra(CONFIG_TYPE, configType);
        startActivity(intent);
    }


}
