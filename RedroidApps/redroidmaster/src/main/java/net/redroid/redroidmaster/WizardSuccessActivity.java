package net.redroid.redroidmaster;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import net.redroid.redroidcorelibrary.Configuration;


/**
 * This is the final activity in the activity is used in the configuration wizard to enter information about a Bike Cam
 * configuration. THe user is asked to choose a name and enter the Device ID and SEcurity Token
 * from the peripheral device.
 */

public class WizardSuccessActivity extends AppCompatActivity {

    public static final String CONFIG = "CUSTOM";
    MasterDataModel model = MasterDataModel.getInstance();
    TextView nameLabel, nameField, typeLabel, typeField, networkModeLabel, networkModeField,
            deviceIdLabel, deviceIdField, securityTokenLabel, securityTokenField;
    Button backButton, finishButton;
    String configTypeString;
    Configuration config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wizard_success);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        model.currentActivity = this;

        //extract the config object from the datamodel
        if (model.getStoredObjectType() == Configuration.class)
            config = (Configuration)model.retrieveStoredObject();

        //capture and set the title of the activity in the title bar
        toolbar.setTitle(String.format("%s: %s",
                getResources().getString(R.string.new_configuration), config.getName()));

        // set up the back button in the title bar
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        //get the view handles
        nameLabel = (TextView)findViewById(R.id.name).findViewById(R.id.label);
        nameField = (TextView)findViewById(R.id.name).findViewById(R.id.field);
        typeLabel = (TextView)findViewById(R.id.type).findViewById(R.id.label);
        typeField = (TextView)findViewById(R.id.type).findViewById(R.id.field);
        networkModeLabel = (TextView)findViewById(R.id.network_mode).findViewById(R.id.label);
        networkModeField = (TextView)findViewById(R.id.network_mode).findViewById(R.id.field);
        deviceIdLabel = (TextView)findViewById(R.id.device_id).findViewById(R.id.label);
        deviceIdField = (TextView)findViewById(R.id.device_id).findViewById(R.id.field);
        securityTokenLabel = (TextView)findViewById(R.id.security_token).findViewById(R.id.label);
        securityTokenField = (TextView)findViewById(R.id.security_token).findViewById(R.id.field);
        backButton = (Button)findViewById(R.id.back_button);
        finishButton = (Button)findViewById(R.id.next_button);

        //set finish button to next for custom
        if (config.getConfigType() == Configuration.ConfigType.CUSTOM) {
            finishButton.setText(R.string.next);
        }

        //set the field text from the config
        nameLabel.setText(getResources().getString(R.string.name));
        nameField.setText(config.getName());
        typeLabel.setText(getResources().getString(R.string.type));
        typeField.setText(Configuration.getConfigTypeString(config.getConfigType()));
        networkModeLabel.setText(getResources().getString(R.string.network_mode));
        networkModeField.setText(Configuration.getNetworkModeString(config.getNetworkMode()));
        deviceIdLabel.setText(getResources().getString(R.string.device_id));
        deviceIdField.setText(config.getDeviceId());
        securityTokenLabel.setText(getResources().getText(R.string.security_token));
        securityTokenField.setText(config.getSecurityToken());

        //set the button listeners
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //close the current activity and go back to the previous
                finish();
                //todo: go back to Wizard_Config -- skip the Test_config activity
            }
        });

        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (config.getConfigType() == Configuration.ConfigType.CUSTOM) {
                    invokeCustomConfigActivity(config);
                }
                else {

                    //add the new config to the config store in the sensors model
                    model.addConfiguration(config);

                    //go back home
                    invokeHomeActivity();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Invokes HomeActivity upon completion of this activity
     */
    protected void invokeHomeActivity()  {
        Intent intent = new Intent(getBaseContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    /**
     * Invokes the WizardCustomConfigActivity when a custom config is chosen
     * @param config the config object
     */
    private void invokeCustomConfigActivity(Configuration config)  {
        Intent intent = new Intent(getBaseContext(), WizardCustomConfigActivity.class);
        model.storeObject(config);
        startActivity(intent);
        finish(); //remove from back stack
    }
}
