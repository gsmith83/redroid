package net.redroid.redroidmaster.debug;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import net.redroid.redroidmaster.R;

public class DebugMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug_menu);
        Button startVideoButton = (Button) findViewById(R.id.open_video);

        startVideoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invokeVideoStreamingActivity();
            }
        });
    }

    public void invokeVideoStreamingActivity(){
        startActivity(new Intent(getBaseContext(), TestVideoViewActivity.class));
        finish();
    }
}
