package net.redroid.redroidmaster.network;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.util.Pair;
import android.util.Log;

import net.redroid.redroidcorelibrary.API;
import net.redroid.redroidcorelibrary.ApiResponse;
import net.redroid.redroidcorelibrary.App;
import net.redroid.redroidcorelibrary.Network;
import net.redroid.redroidcorelibrary.RedroidUtil;
import net.redroid.redroidmaster.MasterAPI;
import net.redroid.redroidmaster.MasterDataModel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.concurrent.TimeoutException;

import static android.content.ContentValues.TAG;


public class MasterNetwork extends Network {

    //used by methods in the MasterAPI class to do make API requests
    private Socket apiSocket;
    private BufferedWriter apiSocketWriter;
    private BufferedReader apiSocketReader;

    //used by the MasterEventServer (in this class) to fetch event notifications
    private int eventSocketPort = SERVER_EVENT_PORT; //can be changed for NAT sensors
    public Socket eventSocket;
    public BufferedWriter eventSocketWriter;
    public BufferedReader eventSocketReader;

    protected ServerSocket serverSocket;
    private MasterEventServer eventServer;
    protected MasterDataModel model = MasterDataModel.getInstance();

    public String sessionId;


    public MasterNetwork() {
    }

    @Override
    public void init() throws TimeoutException, ConnectionException {
    }

    @Override
    public synchronized void teardown() {
        try {
            if (apiSocketReader != null) {
                apiSocketReader.close();
                apiSocketReader = null;
            }
            if (apiSocketWriter != null) {
                apiSocketWriter.flush();
                apiSocketWriter.close();
                apiSocketWriter = null;
            }
            if (apiSocket != null) {
                apiSocket.close();
                apiSocket = null;
            }
            if (eventSocketReader != null) {
                eventSocketReader.close();
                eventSocketReader = null;
            }
            if (eventSocketWriter != null) {
                eventSocketWriter.flush();
                eventSocketWriter.close();
                eventSocketWriter = null;
            }
            if (eventSocket != null) {
                eventSocket.close();
                eventSocket = null;
            }
            if (serverSocket != null) {
                serverSocket.close();
                serverSocket = null;
            }

        } catch (IOException e) {
            Log.e(TAG, "teardown: " + e.getMessage());
            e.printStackTrace();
        }
    }


    /**
     * For setting the apiSocket with lazy initialization. Must be called from a worker
     * thread, as it performs networking tasks.
     *
     * @return the socket, or null if creation failed.
     */
    private synchronized void setMasterAPISocket() {
        if (apiSocket == null) {

            try {
                apiSocket = new Socket();
                apiSocket.setReuseAddress(true);
                apiSocket.setSoTimeout(Network.SOCKET_READ_TIMEOUT);
                apiSocket.connect(new InetSocketAddress(serverIpAddress, SERVER_API_PORT),
                        Network.SOCKET_CONNECTION_TIMEOUT);
                apiSocketReader = new BufferedReader(
                        new InputStreamReader(apiSocket.getInputStream()));
                apiSocketWriter = new BufferedWriter(
                        new OutputStreamWriter(apiSocket.getOutputStream()));

                //start a thread to monitor apiSocket state
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        testConnection();
                    }
                }).start();

            } catch (IOException e) {
                Log.e(TAG, "getApiSocket: " + e.getMessage());
                apiSocket = null;
            }
        }
    }

    /**
     * TO be run by the thread to monitor the state of the apiSocket and shut down if the PING
     * times out
     */
    private void testConnection() {

        synchronized (apiSocket) {
            while (apiSocket != null && !apiSocket.isClosed()) {
                try {
                    apiSocket.wait(Network.PING_INTERVAL);
                    ApiResponse response = model.api.requestGetPingBlocking();
                    if (response.getCode() != 200)
                        throw new ConnectionException("response code: " + response.getCode());
                } catch (InterruptedException e) {
                } catch (IOException | NullPointerException |
                        ConnectionException e) {
                    teardown();
                }
            }
            //socket closed: trigger onDisconnect no
            onDisconnect();
        }
    }

    /**
     * for fetching the apiSocket
     *
     * @return the socket, or null if creation failed.
     */
    public synchronized Socket getApiSocket() {
        setMasterAPISocket();
        return apiSocket;
    }

    /**
     * for fetching the apiSocket's ouput stream wrapped in a buffered writer
     *
     * @return the writer, or null if creation failed
     */
    private synchronized BufferedWriter getApiSocketWriter() {
        setMasterAPISocket();
        return apiSocketWriter;
    }

    /**
     * for fetching the apiSocket's input stream wrapped in a buttered reader
     *
     * @return the reader, or null if creation failed
     */
    private synchronized BufferedReader getApiSocketReader() {
        setMasterAPISocket();
        return apiSocketReader;
    }


    /**
     * A public accessor method for performing a blocking HTTP request and fetching the response.
     * This overloaded method is specifically for a Redroid Master, as it makes use of the
     * MasterAPISocket stored in the Network object. It simply unpacks the relevant variables
     * stored in the Network object and passes them into the generic getHttpResponse method.
     *
     * @param method the HTTP method as defined in the Redroid API
     * @param url    the url string
     * @param body   the HTTP request body
     * @return a Pair object containing the HTTP response code and response payload (JSON)
     */
    public synchronized Pair<Integer, String> getHttpResponse(API.Method method, String url, String body)
            throws ConnectionException {
        Socket apiSocket = getApiSocket();
        if (apiSocket == null)
            return null;
        SocketAddress address = apiSocket.getRemoteSocketAddress();
        String remoteHost = address.toString().replace("/", "");
        BufferedReader reader = getApiSocketReader();
        BufferedWriter writer = getApiSocketWriter();
        return getHttpResponse(remoteHost, reader, writer, method, url, body);
    }


    public boolean startMasterEventServer() {
        if (eventServer == null) {
            eventServer = new MasterEventServer();
            eventServer.start();
        }
        return true;
    }

    protected class MasterEventServer extends Thread {

        @Override
        public void run() {

            try {
                serverSocket = new ServerSocket();
                serverSocket.setReuseAddress(true);
                serverSocket.bind(new InetSocketAddress(eventSocketPort), 1);

                Log.d(TAG, "run: Master server listening on " + localIpAddress + ":" + SERVER_API_PORT);

                //get a connection
                eventSocket = serverSocket.accept();
                Log.d("Event Server", "Connection established to: " +
                        eventSocket.getInetAddress().toString());

                //build the reader and writer
                eventSocketReader = new BufferedReader(new InputStreamReader(
                        MasterNetwork.this.eventSocket.getInputStream()));
                eventSocketWriter = new BufferedWriter(new OutputStreamWriter(
                        MasterNetwork.this.eventSocket.getOutputStream()));

                /** the meat of the server code **/
                serverWhileLoop();
                /********************************/

                if (eventSocket != null && !eventSocket.isClosed()) {
                    eventSocket.close();
                } else {
                    onDisconnect();
                }
            } catch (IOException | NullPointerException e) {
                Log.e(TAG, "startMasterEventServer: " + ((e.getMessage() == null) ? "" : e.getMessage()));
                onDisconnect();
            } finally {
                try {
                    if (serverSocket != null)
                        serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void serverWhileLoop() throws IOException {
        while (eventSocket != null && !eventSocket.isClosed()) {
            int requestId;
            String requestHeader, requestBody;
            String responseHeader = "HTTP/1.1 200 OK";

//                    try {
            StringBuilder sb = new StringBuilder();

            //get the header fields
            String contentLengthHeader = "";
            String line;
            while (!(line = eventSocketReader.readLine()).equals("")) {
                sb.append(line);
                if (line.toLowerCase().startsWith("content-length"))
                    contentLengthHeader = line;
                sb.append("\r\n");
            }
            requestHeader = sb.toString();

            //get the content length
            int contentLength = -1;
            try {
                String[] s = contentLengthHeader.split(" ");
                contentLength = Integer.parseInt(s[1]);
            } catch (NumberFormatException n) {
                responseHeader = "HTTP/1.1 411 Length Required";
            }

            //get the body
            sb = new StringBuilder();
            for (int i = 0; i < contentLength; i++) {
                sb.append((char) eventSocketReader.read());
            }
            requestBody = sb.toString();

            //extract the method and url
            Log.d("SensorEvents /", "/LocationEvents : Master received event:\r\n" + requestHeader +
                    requestBody);
            String[] strings = requestHeader.split("\\s");
            String method = strings[0];
            String url = strings[1];

            //fetch the correct event listener and call its callback
            try {
                String[] urlParts = url.split("/");
                requestId = Integer.parseInt(urlParts[urlParts.length-1]);
                MasterAPI.ApiEventListener eventListener = model.api
                        .getApiEventListener(requestId);
                if (eventListener == null) {
                    responseHeader = "HTTP/1.1 404 Not Found";
                } else {
                    eventListener.onApiEvent(requestBody);
                }
            } catch (NumberFormatException e) {
                Log.d(TAG, "MasterAPIServer.run: " + e.getMessage());
                responseHeader = "HTTP/1.1 400 Bad Request";
            }

            String httpResponseFormattedString =
                    "%s\r\n" +
                            "Date: %s\r\n" + "" +
                            "Server: Redroid\r\n" +
                            "Content-Length: 0\r\n" +
                            "Content-Type: text/plain\r\n\r\n";
            String date = RedroidUtil.getServerTime();
            String response = String.format(httpResponseFormattedString,
                    responseHeader, date);

            //send back the response
            eventSocketWriter.write(response);
            eventSocketWriter.flush();
        }//while
    }

    //make sure only one thread triggers this method
    private boolean onDisconnectTriggered = false;

    protected synchronized void onDisconnect() {
        //get a stacktrace
        try {throw new Exception();}catch (Exception e) {e.printStackTrace();}
        if (!onDisconnectTriggered) {
            onDisconnectTriggered = true;
            Intent intent = new Intent("disconnected");
            LocalBroadcastManager.getInstance(App.getAppContext()).sendBroadcast(intent);
        }
    }
}
