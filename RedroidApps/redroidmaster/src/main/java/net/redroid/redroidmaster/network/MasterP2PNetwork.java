package net.redroid.redroidmaster.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

import net.redroid.redroidcorelibrary.API;
import net.redroid.redroidcorelibrary.App;
import net.redroid.redroidcorelibrary.Network;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeoutException;

import static android.content.Context.WIFI_SERVICE;

public class MasterP2PNetwork extends MasterWiFiNetwork {
    String TAG = this.getClass().getName();
    boolean success = false;
    final Object wifiCycleLock = new Object();
    final Object discoverPeersLock = new Object();
    final Object connectionRepeatLock = new Object();

    String deviceID, securityToken;
    WifiP2pManager manager;
    WifiP2pManager.Channel channel;
    MasterReceiver receiver;
    IntentFilter filter;
    InitSuccessListener initSuccessListener;


    public MasterP2PNetwork(String deviceID, String securityToken){
        super(null);
        this.deviceID = deviceID;
        this.securityToken = securityToken;
    }


    public void init(InitSuccessListener listener) throws TimeoutException, ConnectionException{
        super.init();

        initSuccessListener = listener;

        manager = (WifiP2pManager)App.getAppContext().getSystemService(Context.WIFI_P2P_SERVICE);
        channel = manager.initialize(App.getAppContext(), Looper.getMainLooper(), null);
        receiver = new MasterReceiver();

        cycleWifi();

        // intent filter
        filter = new IntentFilter();
        filter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        filter.addAction(WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION);
        filter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        filter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        filter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        // Create handler thread
        HandlerThread handlerThread = new HandlerThread("OnReceive Handler Thread");
        handlerThread.start();
        Looper looper = handlerThread.getLooper();
        Handler handler = new Handler(looper);

        // register receiver
        App.getAppContext().registerReceiver(receiver, filter, null, handler);

        final long MAX_WAIT = 10 * 1000;
        final long start = System.currentTimeMillis();

        final WifiP2pManager.ActionListener discoverPeersListener = new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Discovery initiated");
            }

            @Override
            public void onFailure(int reason) {
                Log.d(TAG, "Discover peers onFailure: " + reason);
                long elapsed = System.currentTimeMillis() - start;

                //trigger failure callback if MAX_WAIT exceeded
                if (elapsed > MAX_WAIT) {
                    initSuccessListener.onInitFailure("P2P framework timed out");
                }

                //wait max 500 ms and try again
                synchronized (discoverPeersLock) {
                    try {
                        discoverPeersLock.wait(Math.min(500, MAX_WAIT - elapsed));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                manager.discoverPeers(channel, this);
            }
        };

        manager.discoverPeers(channel, discoverPeersListener);
    }


    private void cycleWifi() {
        Log.d(TAG, "cycleWifi: cycling");
        final int WIFI_CYCLE_DELAY = 2000;
        WifiManager wifiManager = (WifiManager)App.getAppContext().getSystemService(WIFI_SERVICE);

        long start = System.currentTimeMillis();
        long elapsed = 0;
        //disabled wifi
        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
            boolean done = false;
            while (!done) {
                synchronized (wifiCycleLock) {
                    try {
                        wifiCycleLock.wait(WIFI_CYCLE_DELAY - elapsed);
                        elapsed = System.currentTimeMillis() - start;
                        if (elapsed >= WIFI_CYCLE_DELAY) {
                            done = true;
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace(); // todo: enforce this delay??
                    }
                }
            }
        }

        //enable it again
        wifiManager.setWifiEnabled(true);
//        while(wifiManager.getWifiState() != WifiManager.WIFI_STATE_ENABLED){} //todo: possible freeze point??
    }

    @Override
    public void teardown() {
        super.teardown();
        cycleWifi();
        try {
            App.getAppContext().unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class MasterReceiver extends BroadcastReceiver{
        final boolean[] peersRequested = {false};
        @Override
        public void onReceive(Context context, Intent intent){
            final String TAG = this.getClass().getName();
            String action = intent.getAction();

            Log.d(TAG, (action + "\n" + intent.toString()));

            switch(action){
                case WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION:
                    NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
                    Log.d(TAG, "P2P Connection changed. NetworkInfo: " + networkInfo.toString());

                    WifiP2pInfo info = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_INFO);
                    Log.d(TAG, "WifiP2PInfo: " + info.toString());

                    WifiP2pGroup group = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_GROUP);
                    if(group != null)
                        Log.d(TAG, "WifiP2PGroup: " + group.toString());

                    if (networkInfo.isConnected()) {
                        manager.requestConnectionInfo(channel, new WifiP2pManager.ConnectionInfoListener() {
                    @Override
                    public void onConnectionInfoAvailable(final WifiP2pInfo info) {
                        Log.d(TAG, "onConnectionInfoAvailable, WifiP2pInfo: " + info.toString());
                        serverIpAddress = info.groupOwnerAddress.toString().replace("/", "");
                        initSuccessListener.onInitSuccess();
                    }
                });
            }
                    break;
                case WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION:
                    int discoveryState = intent.getIntExtra(WifiP2pManager.EXTRA_DISCOVERY_STATE, -1);
                    Log.d(TAG, "Discovery changed. DiscoverState: " + (discoveryState == 2 ? "Started" : "Stopped"));

                    break;
                case WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION:
                    int numFailures[] = {0};
                    if (peersRequested[0])
                        break;
                    WifiP2pDeviceList deviceList = (WifiP2pDeviceList)intent.getParcelableExtra(WifiP2pManager.EXTRA_P2P_DEVICE_LIST);
                    Log.d(TAG, "onReceive: \n" + deviceList.toString());
                    WifiP2pDevice device = null;
                    for (WifiP2pDevice d: deviceList.getDeviceList()) {
                        if (d.deviceName.contains(deviceID)) {
                            device = d;
                        }
                    }

                    //device not in the list; try again
                    if (device == null) {
                        Log.d(TAG, "onReceive: I DIDN'T FIND THE DEVICE IN THE LIST");
                        break;
                    }
                    Log.d(TAG, "onReceive: I FOUND THE DEVICE!");

                    final WifiP2pConfig config = new WifiP2pConfig();
                    config.deviceAddress = device.deviceAddress;
                    boolean deviceFound = false;
                    final int[] numTries = {0};
                    final int MAX_TRIES = 15;

                    //set up the listener first
                    WifiP2pManager.ActionListener connectActionListener = new WifiP2pManager.ActionListener() {
                        @Override
                        public void onSuccess() {
                            Log.d(TAG, "Connect onSuccess: ");
                        }

                        @Override
                        public void onFailure(int reason) {
                            String reasonString = "unknown reason";
                            switch (reason) {
                                case 0:
                                    reasonString = "internal error";
                                    break;
                                case 2:
                                    reasonString = "busy";
                                    break;
                            }
                            Log.d(TAG, "Connect onFailure: " + reasonString);
                            if (numTries[0]++ < MAX_TRIES) {
                                //try to connect again
                                //wait max 500 ms and try again
                                synchronized (connectionRepeatLock) {
                                    try {
                                        connectionRepeatLock.wait(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                                Log.d(TAG, String.format("onFailure: attempt connection again, time %d/%d", numTries[0], MAX_TRIES));
                                manager.connect(channel, config, this);
                            }
                            else {
                                initSuccessListener.onInitFailure("p2p connection attempt failed: " + reasonString);
                            }
                        }
                    };

                    //try to connect
                    manager.connect(channel, config, connectActionListener);
                    break;
                case WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION:
                    int wifiState = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
                    Log.d(TAG, "State changed. wifi state: " + (wifiState == 2 ? "Enabled" : "Disabled"));

                    break;
                case WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION:
                    Log.d(TAG, "This device changed.");

                    break;
                default:
                    break;
            }
        }
    }

    public interface InitSuccessListener{
        void onInitSuccess();
        void onInitFailure(String msg);
    }
}
