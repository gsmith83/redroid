
package net.redroid.redroidmaster.network;

import android.net.wifi.WifiManager;
import android.util.Log;

import net.redroid.redroidcorelibrary.API;
import net.redroid.redroidcorelibrary.App;
import net.redroid.redroidcorelibrary.Network;
import net.redroid.redroidcorelibrary.RedroidUtil;
import net.redroid.redroidmaster.MasterAPI;
import net.redroid.redroidmaster.network.MasterNetwork;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.concurrent.TimeoutException;

import static android.content.ContentValues.TAG;
import static android.content.Context.WIFI_SERVICE;


public class MasterServerNetwork extends MasterNetwork {

    MasterEventServer masterEventServer;


    public MasterServerNetwork() {
        super();
        this.serverIpAddress = model.getConnectionServerUrl();
    }

    @Override
    public void init() throws TimeoutException, ConnectionException {
        super.init();
    }

    @Override
    public void teardown() {
        super.teardown();
    }

//    @Override
//    public boolean startMasterEventServer() {
//        if (masterEventServer != null) {
//            return true;
//        }
//
//        final boolean[] checkComplete = {false};
//        final boolean[] success = {true};
//        final Object LOCK = new Object();
//
//        //find out if local interface is wifi, and if so, if it is a NAT
//        WifiManager wifi = (WifiManager) App.getAppContext().getSystemService(WIFI_SERVICE);
//        if (wifi.isWifiEnabled()) {
//            int localIpAddress = wifi.getConnectionInfo().getIpAddress();
//            String ipAddressString = RedroidUtil.getIpAddressString(localIpAddress);
//            String prefix = ipAddressString.split(".")[0];
//
//            //check for private IP prefix
//            if (prefix.equals("192") || prefix.equals("10")) {
//
//                //send a set event port API msg to the server, which also sets the eventSocket
//                model.api.requestPostSetEventPort(sessionId, new API.ApiResponseListener() {
//                    @Override
//                    public void onApiResponse(int responseCode, JSONObject response) {
//                        if (responseCode == 200) {
//                            masterEventServer = new MasterEventServer();
//                            masterEventServer.start();
//                            success[0] = true;
//                        }
//                        else {
//                            success[0] = false;
//                        }
//
//                        checkComplete[0] = true;
//                        synchronized (LOCK) {
//                            LOCK.notify();
//                        }
//                    }
//                });
//            }
//        }
//        else {
//            checkComplete[0] = true;
//            synchronized (LOCK) {
//                LOCK.notify();
//            }
//        }
//
//        //wait for the check to complete
//        while (!checkComplete[0]) {
//            synchronized (LOCK) {
//                try {
//                    LOCK.wait();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        if (!success[0]) {
//            return false;
//        }
//        if (masterEventServer == null) {
//            super.startMasterEventServer();
//        }
//        return true;
//    }

    @Override
    public boolean startMasterEventServer() {
        if (masterEventServer != null) {
            return true;
        }

        final boolean[] checkComplete = {false};
        final Object LOCK = new Object();


        //send a set event port API msg to the server, which also sets the eventSocket
        model.api.requestPostSetEventPort(sessionId, new API.ApiResponseListener() {
            @Override
            public void onApiResponse(int responseCode, JSONObject response) {
                if (responseCode == 200) {
                    masterEventServer = new MasterEventServer();
                    masterEventServer.start();
                }
                checkComplete[0] = true;
                synchronized (LOCK) {
                    LOCK.notify();
                }
            }
        });

        //wait for the check to complete
        while (!checkComplete[0]) {
            synchronized (LOCK) {
                try {
                    LOCK.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return masterEventServer != null;
    }



    private class MasterEventServer extends Thread {

        @Override
        public void run() {

            try {
                /** the meat of the server code **/
                serverWhileLoop(); //found in parent MasterNetwork
                /********************************/

                if (eventSocket != null && !eventSocket.isClosed()) {
                    eventSocket.close();
                } else {
                    onDisconnect();
                }
            } catch (IOException | NullPointerException e) {
                Log.e(TAG, "MasterEventServer run(): " + ((e.getMessage() == null) ? "" : e.getMessage()));
                onDisconnect();
            } finally {
                try {
                    if (serverSocket != null)
                        serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
