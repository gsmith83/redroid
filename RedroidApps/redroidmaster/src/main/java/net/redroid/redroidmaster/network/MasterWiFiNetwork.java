
package net.redroid.redroidmaster.network;

import java.util.concurrent.TimeoutException;


public class MasterWiFiNetwork extends MasterNetwork {


    public MasterWiFiNetwork(String remoteIpAddress) {
        super();
        this.serverIpAddress = remoteIpAddress;
    }

    @Override
    public void init() throws TimeoutException, ConnectionException {
        super.init();
    }

    @Override
    public void teardown() {
        super.teardown();
    }
}
