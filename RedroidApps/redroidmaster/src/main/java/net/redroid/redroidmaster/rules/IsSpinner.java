package net.redroid.redroidmaster.rules;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.AppCompatSpinner;
import android.util.AttributeSet;

/**
 * A spinner to be used for the IS spinner in a rule
 */
public class IsSpinner extends AppCompatSpinner {
    public IsSpinner(Context context) {
        super(context);
    }

    public IsSpinner(Context context, int mode) {
        super(context, mode);
    }

    public IsSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public IsSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public IsSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode) {
        super(context, attrs, defStyleAttr, mode);
    }

    public IsSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode, Resources
            .Theme popupTheme) {
        super(context, attrs, defStyleAttr, mode, popupTheme);
    }
}
