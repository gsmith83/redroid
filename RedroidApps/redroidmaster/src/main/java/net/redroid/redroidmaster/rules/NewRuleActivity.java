package net.redroid.redroidmaster.rules;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.iid.FirebaseInstanceId;

import net.redroid.redroidcorelibrary.API;
import net.redroid.redroidcorelibrary.App;
import net.redroid.redroidcorelibrary.Configuration;
import net.redroid.redroidcorelibrary.RedroidUtil;
import net.redroid.redroidcorelibrary.rules.Action;
import net.redroid.redroidcorelibrary.rules.Rule;
import net.redroid.redroidcorelibrary.rules.Trigger;
import net.redroid.redroidmaster.HomeActivity;
import net.redroid.redroidmaster.MasterDataModel;
import net.redroid.redroidmaster.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;


/**
 * Activity containing dynamic UI for creating a new Redroid rule
 */
public class NewRuleActivity extends AppCompatActivity implements AdapterView
        .OnItemSelectedListener {

    MasterDataModel model = MasterDataModel.getInstance();
    List<View> whenBlockList = new ArrayList<>();
    EditText ruleNameField;
    AutoCompleteTextView textToField, emailToField;
    ViewGroup rootWhenBlockLayout;
    List<String> whenAttributeList;
    ArrayAdapter<String> whenSpinnerAdapter;
    CheckBox notificationCheckBox, textCheckBox, emailCheckBox;
    Button saveButton;
    String ruleId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_rule);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //find the relevant config
        String configId = getIntent().getStringExtra("configId");
        if (configId == null) {
            invokeHomeActivity(getResources().getString(R.string.error_retrieving_config));
            finish();
            return;
        }
        final Configuration config = model.getConfig(configId);

        if (config == null) {
            invokeHomeActivity(getResources().getString(R.string.requested_config_not_found));
            finish();
            return;
        }

        //generate the rule ID
        ruleId = RedroidUtil.generateSecurityToken();


        //set the WHEN attribute list, WHEN spinner adapter, and root WHEN ViewGroup
        whenAttributeList = getAttributeList(config);
        whenSpinnerAdapter = new ArrayAdapter<String>(NewRuleActivity.this, R.layout.spinner_item,
                whenAttributeList);
        rootWhenBlockLayout = (ViewGroup) findViewById(R.id.when_block_container);

        //capture and set the title of in the title bar
        toolbar.setTitle(String.format("New Rule: %s", config.getName()));

        //set the rule name stub
        ruleNameField = (EditText) findViewById(R.id.rule_name_field);
        ruleNameField.setText(String.format("Rule %d", config.rulesList.size() + 1));
        ruleNameField.setSelection(ruleNameField.getText().length());

        //THEN block elements
        notificationCheckBox = (CheckBox) findViewById(R.id.notification_checkbox);
        textCheckBox = (CheckBox) findViewById(R.id.text_message_checkbox);
        emailCheckBox = (CheckBox) findViewById(R.id.email_checkbox);
        textToField = (AutoCompleteTextView) findViewById(R.id.text_message_to_field);
        emailToField = (AutoCompleteTextView) findViewById(R.id.email_to_field);

        //set text and email list adapters
        if (model.autoCompleteLists.phoneNumbers == null)
            model.autoCompleteLists.phoneNumbers = new HashSet<>();
        if (model.autoCompleteLists.emailAddresses == null)
            model.autoCompleteLists.emailAddresses = new HashSet<>();

        String[] phoneNums = (String[]) model.autoCompleteLists.phoneNumbers.toArray(new String[0]);
        ArrayAdapter<String> phoneNumAdapter = new ArrayAdapter<>(this, android.R.layout
                .simple_list_item_1, phoneNums);
        textToField.setAdapter(phoneNumAdapter);

        String[] emails = (String[]) model.autoCompleteLists.emailAddresses.toArray(new String[0]);
        ArrayAdapter<String> emailAdapter = new ArrayAdapter<>(this, android.R.layout
                .simple_list_item_1, emails);
        emailToField.setAdapter(emailAdapter);

        //manage Save button
        saveButton = (Button) findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveButtonOnClick(config);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //set the fist WHEN block
        if (whenBlockList.size() == 0) {
            addWhenBlock();
        }
    }

    protected synchronized void invokeHomeActivity(String message) {
        Intent intent = new Intent(getBaseContext(), HomeActivity.class);
        intent.putExtra(HomeActivity.SNACKBAR_MESSAGE, message);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    /**
     * Helper method for populating the list of attribute in the WHEN dropdown
     * @param config
     * @return
     */
    private List<String> getAttributeList(Configuration config) {
        List<String> list = new ArrayList<>();
        list.add("Time");
        if (config.getOptions().streamingMedia ||
                config.getConfigType() == Configuration.ConfigType.PET_CAM ||
                config.getConfigType() == Configuration.ConfigType.SECURITY_CAM) {
            list.add("Video stream");
        }
        if (config.getOptions().location) {
            list.add("Location");
            list.add("Speed");
        }
        if (config.getOptions().linearAcceleration)
            list.add("Acceleration");
        if (config.getOptions().light)
            list.add("Light");
        if (config.getOptions().proximity)
            list.add("Proximity");
        if (config.getOptions().magneticField)
            list.add("Magnetic field");
        if (config.getOptions().temperature)
            list.add("Temperature");
        return list;
    }


    /**
     * Code for dynamically updating UI to add 1 more WHEN block
     */
    private void addWhenBlock() {
        //inflate the new view
        final View newView = getLayoutInflater().inflate(R.layout.rules_when_block, null);

        //find the WHEN spinner and set its adapter
        WhenSpinner whenSpinner = (WhenSpinner) newView.findViewById(R.id.when_spinner);
        whenSpinner.setAdapter(whenSpinnerAdapter);

        //add the new view to the list
        whenBlockList.add(newView);
        final int position = whenBlockList.size() - 1;

        //set the view listeners for place and time pickers
        whenSpinner.setOnItemSelectedListener(this);
        newView.findViewById(R.id.location_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationButtonOnClick(whenBlockList.indexOf(newView));
            }
        });
        newView.findViewById(R.id.between_field_start).setOnClickListener(new View
                .OnClickListener() {
            @Override
            public void onClick(View v) {
                betweenFieldOnClick((TextView) v);
            }
        });
        newView.findViewById(R.id.between_field_end).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                betweenFieldOnClick((TextView) v);
            }
        });
        ((FloatingActionButton) newView.findViewById(R.id.add_fab)).setOnClickListener(new View
                .OnClickListener() {
            @Override
            public void onClick(View v) {
                addWhenBlock();
            }
        });
        ((FloatingActionButton) newView.findViewById(R.id.delete_fab)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeWhenBlock(newView);
            }
        });

        //add the new view to the root
        rootWhenBlockLayout.addView(newView);

        //call onItemSelected for the first item in the WHEN spinner
        onItemSelected(whenSpinner, null, 0, 0);

        //update the UI
        updateUi();
    }

    /**
     * Code for dynamically updating UI to remove an existing WHEN block
     * @param view the WHEN block to remove
     */
    private void removeWhenBlock(View view) {
        //remove the view from the hierarchy
        rootWhenBlockLayout.removeView(view);

        //remove the view from the list
        whenBlockList.remove(view);

        //update the ui
        updateUi();
    }

    /**
     * Helper method for updating UI elements whenever a WHEN block is added or removed
     */
    private void updateUi() {
        for (View view : whenBlockList) {
            TextView andSeparator = (TextView) view.findViewById(R.id.and_seperator);
            FloatingActionButton addFab = (FloatingActionButton) view.findViewById(R.id.add_fab);
            FloatingActionButton spacerFab = (FloatingActionButton) view.findViewById(R.id
                    .invisible_fab);
            FloatingActionButton deleteFab = (FloatingActionButton) view.findViewById(R.id
                    .delete_fab);
            if (whenBlockList.get(whenBlockList.size() - 1) == view) {
                //the last one - hide the AND separator, and activate the AND fab button
                andSeparator.setVisibility(View.GONE);
                addFab.setVisibility(View.VISIBLE);
                spacerFab.setVisibility(View.INVISIBLE);
                deleteFab.setVisibility(View.VISIBLE);
                //can't delete the only one
                if (whenBlockList.size() == 1) {
                    deleteFab.setVisibility(View.INVISIBLE);
                }
            }
            else {
                //otherwise, show the AND separator and hide the AND fab button
                andSeparator.setVisibility(View.VISIBLE);
                addFab.setVisibility(View.GONE);
                spacerFab.setVisibility(View.GONE);
                deleteFab.setVisibility(View.VISIBLE);
            }
            view.invalidate();
        }
        whenBlockList.get(0).getRootView().invalidate();

    }


    /********************* INTERFACE LISTENERS ************************************************/


    /***BEGIN OnItemSelectedListener override methods **/

    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

        if (parent instanceof WhenSpinner) {
            String attribute = whenAttributeList.get(position);
            List<String> isList = new ArrayList<>();

            View rootView = (View) parent.getParent().getParent();
            TextView isTextView = (TextView) rootView.findViewById(R.id.is_text_view);
            View roughGroup = rootView.findViewById(R.id.rough_threshold_group);
            View fineGroup = rootView.findViewById(R.id.fine_threshold_group);
            View betweenGroup = rootView.findViewById(R.id.between_fields_group);
            View withinDistanceGroup = rootView.findViewById(R.id.within_distance_group);
            TextView units = (TextView) rootView.findViewById(R.id.fine_threshold_units_label);

            //set the appropriate list members
            switch (attribute) {
                case "Time":
                    isList.add("Between");
                    isTextView.setVisibility(View.VISIBLE);
                    isTextView.setText("IS");
                    roughGroup.setVisibility(View.GONE);
                    fineGroup.setVisibility(View.GONE);
                    betweenGroup.setVisibility(View.VISIBLE);
                    withinDistanceGroup.setVisibility(View.GONE);
                    break;
                case "Video stream":
                    isList.add("Detects motion");
                    isTextView.setVisibility(View.INVISIBLE);
                    roughGroup.setVisibility(View.GONE);
                    fineGroup.setVisibility(View.GONE);
                    betweenGroup.setVisibility(View.GONE);
                    withinDistanceGroup.setVisibility(View.GONE);
                    break;
                case "Proximity":
                    isList.add("Near");
                    isList.add("Far");
                    isTextView.setVisibility(View.VISIBLE);
                    isTextView.setText("IS");
                    roughGroup.setVisibility(View.GONE);
                    fineGroup.setVisibility(View.GONE);
                    betweenGroup.setVisibility(View.GONE);
                    withinDistanceGroup.setVisibility(View.GONE);
                    break;
                case "Location":
                    isList.add("Closer than");
                    isList.add("Farther than");
                    isTextView.setVisibility(View.VISIBLE);
                    roughGroup.setVisibility(View.GONE);
                    fineGroup.setVisibility(View.GONE);
                    betweenGroup.setVisibility(View.GONE);
                    withinDistanceGroup.setVisibility(View.VISIBLE);
                    break;
                case "Speed":
                    isList.add("Faster than");
                    isList.add("Slower than");
                    isTextView.setVisibility(View.VISIBLE);
                    isTextView.setText("IS");
                    roughGroup.setVisibility(View.GONE);
                    fineGroup.setVisibility(View.VISIBLE);
                    betweenGroup.setVisibility(View.GONE);
                    withinDistanceGroup.setVisibility(View.GONE);
                    units.setText("mph");
                    break;
                case "Acceleration":
                    isList.add("Absolute value more than");
                    isList.add("Absolute value less than");
                    isTextView.setVisibility(View.VISIBLE);
                    isTextView.setText("HAS");
                    roughGroup.setVisibility(View.GONE);
                    fineGroup.setVisibility(View.VISIBLE);
                    betweenGroup.setVisibility(View.GONE);
                    withinDistanceGroup.setVisibility(View.GONE);
                    units.setText("m/s²");
                    break;
                case "Magnetic field":
                    isList.add("Absolute value more than");
                    isList.add("Absolute value less than");
                    isTextView.setVisibility(View.VISIBLE);
                    isTextView.setText("HAS");
                    roughGroup.setVisibility(View.GONE);
                    fineGroup.setVisibility(View.VISIBLE);
                    betweenGroup.setVisibility(View.GONE);
                    withinDistanceGroup.setVisibility(View.GONE);
                    units.setText("µT");
                    break;
                case "Light":
                    isList.add("Brighter than");
                    isList.add("Dimmer than");
                    isTextView.setVisibility(View.VISIBLE);
                    isTextView.setText("IS");
                    roughGroup.setVisibility(View.GONE);
                    fineGroup.setVisibility(View.VISIBLE);
                    betweenGroup.setVisibility(View.GONE);
                    withinDistanceGroup.setVisibility(View.GONE);
                    units.setText("lux");
                    break;

            }

            //make a spinner adapter from the list
            final ArrayAdapter<String> isSpinnerAdapter = new ArrayAdapter<>(NewRuleActivity
                    .this, R.layout.spinner_item, isList);

            //set the adapter on the spinner
            ((Spinner) rootView.findViewById(R.id.is_spinner)).setAdapter(isSpinnerAdapter);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    /***END OnItemSelectedListener override methods **/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == PLACE_PICKER_REQUEST) {

//            int position = data.getIntExtra(WHEN_BLOCK_POSITION, -1);
//            if (position < 0 ) {
//                Toast.makeText(this, "failed to fetch intent extra WHEN_BLOCK_POSITION",
//                        Toast.LENGTH_SHORT).show();
//                return;
//            }

        //the list position was used as the requestCode
        int position = requestCode;

        //fetch the WHEN block that triggered the place picker in the first place
        View rootView = whenBlockList.get(position);

//            //set the adapter on the IS spinner for LOCATION
//            List<String> isList = new ArrayList<>();
//            isList.add("Within");
//            final ArrayAdapter<String> isSpinnerAdapter = new ArrayAdapter<>(RuleActivity
//                    .this, R.layout.spinner_item, isList);
//            ((Spinner)rootView.findViewById(R.id.is_spinner)).setAdapter(isSpinnerAdapter);
//
//            //set the UI for location
//            rootView.findViewById(R.id.is_text_view).setVisibility(View.VISIBLE);
//            rootView.findViewById(R.id.rough_threshold_group).setVisibility(View.GONE);
//            rootView.findViewById(R.id.fine_threshold_group).setVisibility(View.GONE);
//            rootView.findViewById(R.id.between_fields_group).setVisibility(View.GONE);
//            rootView.findViewById(R.id.within_distance_group).setVisibility(View.VISIBLE);

        if (resultCode == RESULT_OK) {
            Place place = PlacePicker.getPlace(data, this);
            LatLng latLng = place.getLatLng();
            ((EditText) rootView.findViewById(R.id.lat_field)).setText(String.format("%.6f",
                    latLng.latitude));
            ((EditText) rootView.findViewById(R.id.lng_field)).setText(String.format("%.6f",
                    latLng.longitude));
        }
//        }
    }

    /**
     * code to run in the OnClick handler for a location button
     * @param position the position in the list of the parent WHEN block
     */
    private void locationButtonOnClick(int position) {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            Intent intent = builder.build(NewRuleActivity.this);
            startActivityForResult(intent, position);
        }
        catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
            Toast.makeText(NewRuleActivity.this, "Google Play Services Failure.\nCan't open place" +
                            " " +
                            "picker.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Code for handling clicking of a time start or end field. Invokes the picker.
     * @param clickedView
     */
    private void betweenFieldOnClick(final TextView clickedView) {
        final Calendar calendar = Calendar.getInstance();
        final int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        final int currentMinute = calendar.get(Calendar.MINUTE);
        final boolean is24HrFormat = DateFormat.is24HourFormat(App.getAppContext());

        TimePickerDialog timePickerDialog = new TimePickerDialog(
                this, //context
                new TimePickerDialog.OnTimeSetListener() { //listener
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        String time;
                        String time24HrFormat = String.format("%02d:%02d", hourOfDay, minute);
                        if (is24HrFormat) {
                            time = time24HrFormat;
                        }
                        else {
                            String suffix;
                            //set am or pm
                            if (hourOfDay > 11) {
                                suffix = "pm";
                                hourOfDay = hourOfDay % 12;
                            }
                            else {
                                suffix = "am";
                            }
                            //convert 0 to 12
                            if (hourOfDay == 0)
                                hourOfDay = 12;
                            time = String.format("%d:%02d %s", hourOfDay, minute, suffix);
                        }

                        //figure out which invisible field to set
                        int invisble24hrFieldId;
                        if (clickedView.getId() == R.id.between_field_start) {
                            invisble24hrFieldId = R.id.between_field_start_24hr_val;
                        }
                        else {
                            invisble24hrFieldId = R.id.between_field_end_24hr_val;
                        }
                        //set the invisible field with the 24hr format
                        TextView invisibleTextView = (TextView) ((View) clickedView.getParent())
                                .findViewById(invisble24hrFieldId);
                        invisibleTextView.setText(time24HrFormat);
                        clickedView.setText(time);
                        clickedView.invalidate();
                    }
                },
                currentHour,  //hour to show
                currentMinute, //minute toshow
                is24HrFormat
        );
        timePickerDialog.show();
    }

    /**
     * Code to run when the user saves a new rule
     * @param config
     */
    private void saveButtonOnClick(final Configuration config) {
        //save the email and phone fields to autocomplete
        model.addRuleAutoCompleteEntries(textToField.getText().toString(),
                emailToField.getText().toString());


        //make the rule object
        final Rule rule = generateRule(config);

        //set the rule at the server
        model.api.requestPutRule(rule, new API.ApiResponseListener() {
            @Override
            public void onApiResponse(int responseCode, JSONObject response) {
                //failure - tell the user and unset it
                if (responseCode != 200) {
                    final String[] msg = {"unknown"};
                    try {
                        if (response != null)
                            msg[0] = response.getString("msg");
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //show a failure toast
                            String message = "Failed to set rule at server: " + msg[0];
                            Toast.makeText(App.getAppContext(), message, Toast.LENGTH_LONG).show();

                            //unset the rule before saving it
                            rule.is_set = false;
                        }
                    });
                    setResult(RESULT_CANCELED);
                    finish();

                }
                //add the rule to the config
                final String name = ruleNameField.getText().toString();
                Log.d("NewRuleActivity", "saveButtonOnClick: rule " + name + " JSON: " + rule.toJson());
                config.addRule(rule);
                model.saveConfigsToFile();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(App.getAppContext(), String.format("New rule: %s\nadded to %s", name,
                                config.getName()), Toast.LENGTH_LONG).show();
                    }
                });

                setResult(RESULT_OK);
                finish();
            }
        });

    }


    /**
     * Manages the clicking of a THEN block radio button
     * @param view
     */
    public void onCheckBoxClicked(View view) {
        CheckBox checkBox = (CheckBox) view;
        if (view.getId() == R.id.text_message_checkbox) {
            //enable or disable the field, depending on the check
            View toField = ((View) checkBox.getParent()).findViewById(R.id.text_message_to_field);
            if (checkBox.isChecked()) {
                toField.setEnabled(true);
            }
            else {
                toField.setEnabled(false);
            }
        }
        else if (view.getId() == R.id.email_checkbox) {
            //enable or disable the field, depending on the check
            View toField = ((View) checkBox.getParent()).findViewById(R.id.email_to_field);
            if (checkBox.isChecked()) {
                toField.setEnabled(true);
            }
            else {
                toField.setEnabled(false);
            }
        }
        else if (view.getId() == R.id.notification_checkbox) {
            //enable or disable the field, depending on the check
            View toField = ((View) checkBox.getParent()).findViewById(R.id.notification_to_field);
            if (checkBox.isChecked()) {
                toField.setEnabled(true);
            }
            else {
                toField.setEnabled(false);
            }
        }
        //notification must be checked if the other two are not
        if (!textCheckBox.isChecked() && !emailCheckBox.isChecked()) {
            notificationCheckBox.setChecked(true);
        }
    }

    /**
     * Creates a new Rule object from the New Rule UI and a config object
     * @param config the config object
     * @return the new Rule
     */
    private Rule generateRule(Configuration config) {

        //populate an array of trigger objects from each WHEN block
        Trigger[] triggers = new Trigger[whenBlockList.size()];
        for (int i = 0; i < whenBlockList.size(); i++) {
            triggers[i] = generateTrigger(whenBlockList.get(i));
        }

        //make the action object
        String text = null, email = null;
        if (textCheckBox.isChecked())
            text = textToField.getText().toString();
        if (emailCheckBox.isChecked())
            email = emailToField.getText().toString();
        Action action = new Action(notificationCheckBox.isChecked(), text, email);

        //make the rule
        Rule rule = new Rule(FirebaseInstanceId.getInstance().getToken(),
                config.getDeviceId(),
                config.getConfigId(),
                ruleId,
                ruleNameField.getText().toString(),
                triggers,
                action);

        return rule;
    }

    /**
     * Object representing a trigger (as described in a WHEN block), as part of a Rule object
     * @param whenBlock the view representing the WHEN block
     * @return a new Trigger object
     */
    private Trigger generateTrigger(View whenBlock) {
        Spinner whenSpinner = (Spinner) whenBlock.findViewById(R.id.when_spinner);
        Spinner isSpinner = (Spinner) whenBlock.findViewById(R.id.is_spinner);
        String whenString = ((TextView) whenSpinner.getSelectedView()).getText().toString();
        String isString = ((TextView) isSpinner.getSelectedView()).getText().toString();
        String sensorType, comparison = null, refStartTime = null, refEndTime = null;
        Double value = null, refLat = null, refLng = null;
        switch (whenString) {
            case "Time":
                sensorType = "TIME";
                break;
            case "Video stream":
                sensorType = "VIDEO";
                break;
            case "Location":
                sensorType = "LOCATION";
                break;
            case "Speed":
                sensorType = "SPEED";
                break;
            case "Acceleration":
                sensorType = "TYPE_LINEAR_ACCELERATION";
                break;
            case "Light":
                sensorType = "TYPE_LIGHT";
                break;
            case "Proximity":
                sensorType = "TYPE_PROXIMITY";
                break;
            case "Magnetic field":
                sensorType = "TYPE_MAGNETIC_FIELD";
                break;
            default:
                sensorType = null;
                try {
                    throw new RuntimeException("WTF? Null sensor type?");
                }
                catch (Exception e) {
                    e.printStackTrace();
                    throw e;
                }
        }

        switch (isString) {
            case "Between":
                comparison = "BETWEEN";
                break;
            case "Less than":
            case "Absolute value less than":
            case "Dimmer than":
            case "Slower than":
            case "Near":
            case "Below":
            case "Before":
            case "Closer than":
                comparison = "BELOW";
                break;
            case "More than":
            case "Absolute value more than":
            case "Brighter than":
            case "Faster than":
            case "Far":
            case "Above":
            case "After":
            case "Farther than":
                comparison = "ABOVE";
                break;
            default:
                comparison = null;
        }

        if (sensorType.equals("TIME")) {
            refStartTime = ((TextView) whenBlock.findViewById(R.id.between_field_start_24hr_val))
                    .getText().toString();
            refEndTime = ((TextView) whenBlock.findViewById(R.id.between_field_end_24hr_val))
                    .getText().toString();
        }

        /** Video stream requires no additional values **/
        else if (sensorType.equals("LOCATION")) {
            String dist = ((TextView) whenBlock.findViewById(R.id.miles_value_field)).getText()
                    .toString();
            String lat = ((TextView) whenBlock.findViewById(R.id.lat_field)).getText().toString();
            String lng = ((TextView) whenBlock.findViewById(R.id.lng_field)).getText().toString();
            value = Double.parseDouble(dist);
            refLat = Double.parseDouble(lat);
            refLng = Double.parseDouble(lng);
        }
        else if (sensorType.equals("TYPE_LIGHT") || sensorType.equals("SPEED") ||
                sensorType.equals("TYPE_ACCELERATION") || sensorType.equals("TYPE_MAGNETIC_FIELD")){
            //everything else - fine threshold float field
            String val = ((TextView) whenBlock.findViewById(R.id.fine_threshold_field)).getText
                    ().toString();
            value = Double.parseDouble(val);
        }

        return new Trigger(sensorType, comparison, refStartTime, refEndTime, value, refLat, refLng);
    }
}
