package net.redroid.redroidmaster.rules;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import net.redroid.redroidcorelibrary.API;
import net.redroid.redroidcorelibrary.Configuration;
import net.redroid.redroidcorelibrary.rules.Rule;
import net.redroid.redroidcorelibrary.rules.Trigger;
import net.redroid.redroidmaster.MasterDataModel;
import net.redroid.redroidmaster.R;

import org.json.JSONException;
import org.json.JSONObject;

import static net.redroid.redroidmaster.R.id.is_set_switch;
import static net.redroid.redroidmaster.R.id.rule_name;


/**
 * Activity for displaying a list of current rules
 */
public class RulesActivity extends AppCompatActivity {

    public static final String TAG = "Rules4ctivity";

    //intent identifier for optional snackbar message to show when home activity is invoked
    public static final String SNACKBAR_MESSAGE = "rules_activity_init_snackbar_message";
    public static final String CONFIG = "CONFIG";
    public final int NEW_RULE_ACTIVITY_REQUEST_CODE = 0;

    MasterDataModel model = MasterDataModel.getInstance();
    Activity thisActivity = this;
    RecyclerView recyclerView;
    TextView defaultText;
    Configuration config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rules);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        model.currentActivity = this;

        //find the relevant config
        String configId = getIntent().getStringExtra("configId");
        if (configId == null) {
            finish();
            return;
        }
        config = model.getConfig(configId);

        if (config == null) {
            finish();
            return;
        }

        //capture and set the title of the activity in the title bar
        toolbar.setTitle(String.format("Rules for: %s", config.getName()));

        // set up the back button in the title bar
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        //get UI handles
        recyclerView = (RecyclerView) findViewById(R.id.rule_list);
        defaultText = (TextView) findViewById(R.id.rule_list_default_text);

        //set up the recycler findViewById
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new RuleListAdapter());
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT |
                        ItemTouchHelper.START | ItemTouchHelper.END) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, ViewHolder viewHolder,
                                          ViewHolder
                            target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(ViewHolder viewHolder, int direction) {
                        //remove the rule
                        ((RuleListAdapter) recyclerView.getAdapter()).remove(viewHolder
                                .getAdapterPosition());
                    }
                });
        itemTouchHelper.attachToRecyclerView(recyclerView);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.home_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                invokeNewRuleActivity();
            }
        });

        // in case this activity was invoked by another, check for any snackbar messages to display
        String snackbarMessage = getIntent().getStringExtra(SNACKBAR_MESSAGE);
        if (snackbarMessage != null) {
            Snackbar.make(thisActivity.findViewById(R.id.activity_home),
                    snackbarMessage, Snackbar.LENGTH_LONG).show();
            //ensure that this isn't repeated
            getIntent().removeExtra(SNACKBAR_MESSAGE);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //user pressed back button in action bar
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void invokeNewRuleActivity() {
        Intent intent = new Intent(this, NewRuleActivity.class);
        intent.putExtra("configId", config.getConfigId());
        startActivityForResult(intent, NEW_RULE_ACTIVITY_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case NEW_RULE_ACTIVITY_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    updateDataSet();
                }
                break;
        }
    }

    /**
     * For refreshing the view when something is changed
     */
    void updateDataSet() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                recyclerView.getAdapter().notifyDataSetChanged();
            }
        });
    }

    private void showChangeNameAlertDialog(final Configuration config) {

        final EditText nameField = new EditText(this);
        nameField.setText(config.getName());
        //make an alert
        AlertDialog.Builder builder = new AlertDialog.Builder(RulesActivity.this);
        builder.setView(nameField);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                String name = nameField.getText().toString();
                if (name != null && !name.isEmpty()) {
                    config.setName(name);
                    model.saveConfigsToFile();
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    /**
     * Thanks to https://medium.com/@ipaulpro/drag-and-swipe-with-recyclerview-b9456d2b1aaf
     **/
    /**
     * An implementation of a RecycleView adapter for a rules list
     */
    class RuleListAdapter extends RecyclerView.Adapter<RuleListAdapter.RulesViewHolder> {

        class RulesViewHolder extends RecyclerView.ViewHolder {
            View cardView;
            TextView ruleNameLabel;
            Switch sswitch;
            ImageView infoIcon;

            RulesViewHolder(View v) {
                super(v);
                cardView = v;
                ruleNameLabel = (TextView) v.findViewById(rule_name);
                sswitch = (Switch)v.findViewById(is_set_switch);
                infoIcon = (ImageView)v.findViewById(R.id.info_icon);
            }
        }

        @Override
        public RulesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View cardView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.viewgroup_rule_list_item_card, null);
            return new RulesViewHolder(cardView);
        }

        @Override
        public void onBindViewHolder(RulesViewHolder holder, int position) {
            //set the rule name
            final Rule rule = config.rulesList.get(position);
            if (rule.rule_name.isEmpty())
                holder.cardView.setVisibility(View.GONE);
            holder.ruleNameLabel.setText(rule.rule_name);

            //set the switch
            holder.sswitch.setChecked(rule.is_set);
            holder.sswitch.setOnCheckedChangeListener(new SetButtonOnCheckedChangedListener(rule));

            //set the info icon handler
            holder.infoIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    makeRuleInfoDialog(rule);
                }
            });
        }

        @Override
        public int getItemCount() {
            int count = config.rulesList.size();
            if (count == 0) {
                defaultText.setVisibility(View.VISIBLE);
            }
            else {
                defaultText.setVisibility(View.INVISIBLE);
            }
            return count;
        }

        //called when user swipes the rule card
        void remove(final int position) {
            //get the associated Rule object and its ruleId
            final Rule rule = config.rulesList.get(position);

            //unset the rule at the Redroid server
            model.api.requestDeleteRule(rule.rule_id, new API.ApiResponseListener() {
                @Override
                public void onApiResponse(final int responseCode, final JSONObject response) {
                    if (responseCode != 200) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String message;
                                try {
                                    message = "Failed to delete rule: " + response.getString("msg");
                                }
                                catch (JSONException e) {
                                    e.printStackTrace();
                                    message = String.format("Unknown failure unsetting rule '%s' at server",
                                            rule.rule_name);
                                }
                                Snackbar.make(thisActivity.findViewById(R.id.activity_home), message,
                                        Snackbar.LENGTH_INDEFINITE).show();

                                /**TEMP**/
//                                removeRuleLocalTasks(position, rule.rule_name);
                            }
                        });
                        updateDataSet();
                    }
                    else {
                        removeRuleLocalTasks(position, rule.rule_name);
                    }


                }
            });
        }
    }

    private void removeRuleLocalTasks(final int position, final String name) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                RecyclerView.Adapter adapter = recyclerView.getAdapter();
                //remove the Rule from the data structure
                config.deleteRule(position);
                model.saveConfigsToFile();
                //notify the recycler view
                adapter.notifyItemRemoved(position);
                adapter.notifyItemRangeChanged(position, config.rulesList.size());

                Snackbar.make(thisActivity.findViewById(R.id.activity_home),
                        String.format("Rule deleted: %s", name), Snackbar.LENGTH_SHORT)
                        .show();
            }
        });

    }

    /**
     * Listener for the set/unset check button
     */
    private class SetButtonOnCheckedChangedListener implements CompoundButton.OnCheckedChangeListener {

        Rule rule;
        private SetButtonOnCheckedChangedListener(Rule rule) {
            this.rule = rule;
        }

        @Override
        public void onCheckedChanged(final CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                //set the rule at the server
                model.api.requestPutRule(rule, new API.ApiResponseListener() {
                    @Override
                    public void onApiResponse(int responseCode, JSONObject response) {
                        //failure - tell the user and put it back
                        if (responseCode != 200) {
                            final String[] msg = {"unknown"};
                            try {
                                msg[0] = response.getString("msg");
                            }
                            catch (JSONException e) {
                                e.printStackTrace();
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //set a failure snackbar
                                    String message = "Failed to set rule: " + msg[0];
                                    Snackbar.make(thisActivity.findViewById(R.id.activity_home),
                                            message, Snackbar.LENGTH_LONG).show();

                                    //put the switch back where it started
                                    rule.is_set = false;
                                    buttonView.setOnCheckedChangeListener(null);
                                    buttonView.setChecked(false);
                                    buttonView.setOnCheckedChangeListener(SetButtonOnCheckedChangedListener
                                            .this);
                                }
                            });
                        }
                        else
                        {
                            rule.is_set = true;
                        }
                    }
                });
            } //if isChecked
            //unchecked
            else {
                //unset the rule at the server
                model.api.requestDeleteRule(rule.rule_id, new API.ApiResponseListener() {
                    @Override
                    public void onApiResponse(int responseCode, JSONObject response) {
                        // failure - tell the user
                        if (responseCode != 200) {
                            final String[] msg = {"unknown"};
                            try {
                                msg[0] = response.getString("msg");
                            }
                            catch (JSONException e) {
                                e.printStackTrace();
                            }
                            // set a failure snackbar
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //set the snackbar
                                    String message = "Failed to unset rule: " + msg[0];
                                    Snackbar.make(thisActivity.findViewById(R.id.activity_home),
                                            message, Snackbar.LENGTH_LONG).show();

                                    //put the switch back where it started
                                    rule.is_set = true;
                                    buttonView.setOnCheckedChangeListener(null);
                                    buttonView.setChecked(true);
                                    buttonView.setOnCheckedChangeListener(SetButtonOnCheckedChangedListener
                                            .this);
                                }
                            });

                        }
                        else {
                            rule.is_set = false;
                        }
                    }
                });
            } //else (!isChecked)
        }
    }


    //code for making a confirmation dialog for deleting a configuration from the list
    private void makeRuleInfoDialog(Rule rule) {
        //make a confirmation alert dialog before deleting
        AlertDialog.Builder builder =  new AlertDialog.Builder(RulesActivity.this);
        builder.setMessage(getRuleSummary(rule))
                .setTitle(rule.rule_name + " Summary");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
        finish();
    }

    /**
     * Generates a String summarizing a rule
     * @param rule the rule to summarize
     * @return a technically-precise verbal description in English
     */
    private String getRuleSummary(Rule rule) {
        String out = "WHEN\n";
        for (int i = 0; i < rule.triggers.length; i++) {
            String whenTemplate;
            Trigger trigger = rule.triggers[i];
            switch (trigger.sensor_type) {
                case "TIME":
                    whenTemplate = "%s %s %s %s %s";
                    out += String.format(whenTemplate, "TIME", "is BETWEEN", trigger.ref_start_time, "and", trigger.ref_end_time);
                    break;
                case"VIDEO":
                    whenTemplate = "%s %s";
                    out += String.format(whenTemplate, "VIDEO STREAM", "DETECTS MOTION");
                    break;
                case "SPEED":
                    whenTemplate = "%s %s %s %s %s";
                    out += String.format(whenTemplate, "SPEED", "is" , trigger.comparison.equals("ABOVE")? "FASTER THAN":"SLOWER THAN", trigger.value, "mph");
                    break;
                case "TYPE_LINEAR_ACCELERATION":
                    whenTemplate = "%s %s %s %s %s";
                    out += String.format(whenTemplate, "ACCELERATION", "has absolute value", trigger.comparison.equals("ABOVE")? "MORE THAN":"LESS THAN", trigger.value, "m/s²");
                    break;
                case "TYPE_LIGHT":
                    whenTemplate = "%s %s %s %s %s";
                    out += String.format(whenTemplate, "LIGHT", "is" , trigger.comparison.equals("ABOVE")? "BRIGHTER  THAN":"DIMMER THAN", trigger.value, "lux");
                    break;
                case "TYPE_PROXIMITY":
                    whenTemplate = "%s %s %s";
                    out += String.format(whenTemplate, "PROXIMITY", "is", trigger.comparison.equals("ABOVE")? "FAR":"NEAR");
                    break;
                case "TYPE_MAGNETIC_FIELD":
                    whenTemplate = "%s %s %s %s %s";
                    out += String.format(whenTemplate, "MAGNETIC FIELD", "has absolute value", trigger.comparison.equals("ABOVE")? "MORE THAN":"LESS THAN", trigger.value, "μT");
                    break;
                case "LOCATION":
                    whenTemplate = "%s %s %s %s %s %s %s";
                    out += String.format(whenTemplate, "LOCATION", "is",  trigger.comparison.equals("ABOVE")? "FARTHER THAN":"CLOSER THAN", trigger.value, "miles from", trigger.ref_lat + ",", trigger.ref_lng);
                    break;
            }
            if (i < rule.triggers.length-1) {
                out += "\nand ";
            }
            else {
                out += "\n\n";
            }
        }
        out += "THEN\n";
        if (rule.action.notification) {
            out += "Send NOTIFICATION to THIS DEVICE";
            if (rule.action.text != null || rule.action.email != null)
                out += " and\n";
            else
                out += "\n";
        }
        if (rule.action.text != null) {
            out += String.format("Send SMS to %s", rule.action.text);
            if (rule.action.email != null)
                out += " and\n";
            else
                out += "\n";
        }
        if (rule.action.email != null) {
            out += String.format("Send EMAIL to %s", rule.action.email.toUpperCase());
        }
        return out;
    }
}