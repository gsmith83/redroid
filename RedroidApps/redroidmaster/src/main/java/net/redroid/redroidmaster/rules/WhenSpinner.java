package net.redroid.redroidmaster.rules;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.AppCompatSpinner;
import android.util.AttributeSet;


/**
 * A spinner to be used for the WHEN spinner in a rule
 */
public class WhenSpinner extends AppCompatSpinner {
    public WhenSpinner(Context context) {
        super(context);
    }

    public WhenSpinner(Context context, int mode) {
        super(context, mode);
    }

    public WhenSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WhenSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public WhenSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode) {
        super(context, attrs, defStyleAttr, mode);
    }

    public WhenSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode, Resources
            .Theme popupTheme) {
        super(context, attrs, defStyleAttr, mode, popupTheme);
    }
}
