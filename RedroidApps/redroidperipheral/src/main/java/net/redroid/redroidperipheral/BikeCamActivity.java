package net.redroid.redroidperipheral;

import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.foxdogstudios.peepers.StreamCameraFragment;

import net.redroid.redroidcorelibrary.ApiResponse;
import net.redroid.redroidperipheral.PeripheralAPI.BikeCamApiListener;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * The Peripheral half of the bike cam
 */
public class BikeCamActivity extends AppCompatActivity implements BikeCamApiListener {

    PeripheralDataModel model = PeripheralDataModel.getInstance();

    enum BlinkerState {
        OFF, LEFT, RIGHT
    }

    final int FLASHER_PERIOD = 300; //time in milliseconds for one flash cycle
    final int BLINKER_PERIOD = 5000; //time in milliseconds for one blink cycle
    final String VIDEO_FRAGMENT_ID = "video_fragment";

    View animationContainer, blinkerImage;
    ValueAnimator flasherAnimator;
    boolean flasherOn = false;
    BlinkerState blinkerState = BlinkerState.OFF;
    AnimationDrawable blinkerAnimation;
    FrameLayout videoFrame;
    String ipAddress;
    TextView ipLabel;
    StreamCameraFragment videoFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bike_cam);

        //register the activity with the model for bike cam commands
        model.setBikeCamActivity(this);

        //set activity brightness to maximum
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        layoutParams.screenBrightness = 1F;
        //TODO: disable system autobrightness?

        //get handles to view elements
        animationContainer = findViewById(R.id.animation_container);
        blinkerImage = findViewById(R.id.blinker_image);
        videoFrame = (FrameLayout)findViewById(R.id.video_fragment_container);
        ipLabel = (TextView)findViewById(R.id.ip_address_label);

        //place video stream fragment
        videoFragment = new StreamCameraFragment();
        videoFragment.setIpAddress(model.getIPAddress());
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.video_fragment_container, videoFragment)
                .commit();

        //set up the animators for flasher and blinkers
        flasherAnimator = ValueAnimator.ofInt(0, 2);
        flasherAnimator.setRepeatMode(ValueAnimator.RESTART);
        flasherAnimator.setRepeatCount(ValueAnimator.INFINITE);
        flasherAnimator.setDuration(FLASHER_PERIOD);
        flasherAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                int value = (Integer)animator.getAnimatedValue();

                if (value == 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            animationContainer.setBackgroundColor(Color.RED);
                        }
                    });


                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            animationContainer.setBackgroundColor(Color.BLACK);
                        }
                    });

                }
            }
        });

        //TODO: temporary; move this logic into an API callback to run on command from the master
        animationContainer.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (flasherOn) {
                    flasherOn = false;
                    //turn off the flasher
                    flasherAnimator.cancel();
                    if (blinkerState == BlinkerState.OFF) {
                        animationContainer.setBackgroundColor(Color.BLACK);
                    }
                }
                else {
                    flasherOn = true;
                    //turn on the flasher
                    if (blinkerState == BlinkerState.OFF) {
                        if(!flasherAnimator.isStarted())
                            flasherAnimator.start();
                    }
                }
                return true;
            }
        });

        //register broadcast receiver to detect when the master has disconnected
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int num = intent.getIntExtra("num", -1);
                if (num == 0)
                    finish();
            }
        }, new IntentFilter("numConnected"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        model.setBikeCamActivity(this);
    }


    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    /**
     * set's the flasher state the value of isOn
     * @param isOn flasher state to set
     * @return the state of flasher after method completion
     */
    public boolean setFlasherState(final Boolean isOn) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                flasherOn = isOn;

                if(blinkerState == BlinkerState.OFF) {
                    if(flasherOn) {
                        if(!flasherAnimator.isStarted())
                            flasherAnimator.start();
                    } else {
                        flasherAnimator.cancel();
                        animationContainer.setBackgroundColor(Color.BLACK);
                    }
                }
            }
        });
        return flasherOn;
    }

    private BlinkerState toggleBlinker(final BlinkerState state) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                switch (state) {
                    case RIGHT:
                        blinkerState = BlinkerState.RIGHT;
                        //turn off the flasher
                        if (flasherOn) {
                            flasherAnimator.cancel();
                            animationContainer.setBackgroundColor(Color.BLACK);
                        }
                        //turn on the turn signal
                        blinkerImage.setBackgroundResource(R.drawable.right_blinker_animation);
                        blinkerAnimation = (AnimationDrawable) blinkerImage.getBackground();

                        blinkerImage.setVisibility(View.VISIBLE);
                        blinkerAnimation.start();
                        break;

                    case LEFT:
                        blinkerState = BlinkerState.LEFT;
                        //turn off the flasher
                        if (flasherOn) {
                            flasherAnimator.cancel();
                            animationContainer.setBackgroundColor(Color.BLACK);
                        }
                        //turn on the turn signal
                        blinkerImage.setBackgroundResource(R.drawable.left_blinker_animation);
                        blinkerAnimation = (AnimationDrawable) blinkerImage.getBackground();

                        blinkerImage.setVisibility(View.VISIBLE);
                        blinkerAnimation.start();
                        break;

                    case OFF:
                        blinkerState = BlinkerState.OFF;
                        //turn off the turn signal
                        if (blinkerAnimation != null)
                            blinkerAnimation.stop();
                        blinkerImage.setVisibility(View.INVISIBLE);
                        //turn on the flasher (if applicable)
                        if (flasherOn) {
                            if(!flasherAnimator.isStarted())
                                flasherAnimator.start();
                        }
                        break;
                }
            }});

        return blinkerState;
    }


    /**
     * A method to be called by the network listener for handling incoming API message calls
     * pertinent to this activity. It calls the AnimationWorkerThread after obtaining the correct
     * message type from the API parameters.
     * @param method The HTTP method of the API call
     * @param url the URL of the API call
     * @param json the JSON payload string of the API call
     * @return
     * @throws JSONException
     */
    @Override
    public ApiResponse onBikeCamApiRequest(String method, String url, String json) throws JSONException {
        //unpack the Json string
        json = json.replace("\n", "").replace("\r", "");
        JSONObject jsonObject = new JSONObject(json);

        //make the response object
        String responseJson ="";
        int responseCode = 400;

        //which method?
        if (method.equals("POST")) {

            //which API call?

            if (url.equals("/redroid/bikecam/activity/bikecam")) {
                responseCode = 200;
                responseJson = "";
            }
            // FLASHER
            else if (url.equals("/redroid/bikecam/state/flasher")) {
                boolean is_on = (jsonObject.getBoolean("is_on"));
                if (setFlasherState(is_on) == is_on) {
                    responseCode = 200;
                    responseJson = String.format("{\"is_on\":\"%s\"}",
                            is_on);
                }
                else {
                    responseCode = 400;
                    responseJson = String.format("{\"is_on\":\"%s\"}",
                            !is_on);
                }
            }
            // BLINKER
            else if (url.equals("/redroid/bikecam/state/signal")) {
                String state = ((String)jsonObject.get("signal_state")).toLowerCase();
                BlinkerState blinkerState;
                switch (state) {
                    case "left":
                        blinkerState = BlinkerState.LEFT;
                        break;
                    case "right":
                        blinkerState = BlinkerState.RIGHT;
                        break;
                    case "off":
                        blinkerState = BlinkerState.OFF;
                        break;
                    default:
                        blinkerState = null;
                }
                if(blinkerState != null && toggleBlinker(blinkerState) == blinkerState) {
                    responseCode = 200;
                    responseJson = String.format("{\"signal_state\":\"%s\"}",
                            state.toUpperCase());
                }
                else if (blinkerState == null) {
                    responseCode = 400;
                    responseJson = "{\"msg\":\"invalid blinker state value\"}";
                }
                else {
                    responseCode = 400;
                    responseJson = String.format("{\"signal_state\":\"%s\"}",
                            blinkerState.toString().toUpperCase());
                }
            }
            else {
                responseCode = 400;
                responseJson = "{\"msg\":\"request not recognized\"}";
            }
        }
        return new ApiResponse(responseCode, responseJson);
    }
}