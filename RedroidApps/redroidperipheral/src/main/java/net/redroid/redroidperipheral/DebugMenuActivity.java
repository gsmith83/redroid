package net.redroid.redroidperipheral;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import net.redroid.redroidperipheral.audio.AudioStreamingActivity;
import net.redroid.redroidperipheral.video.VideoStreamingActivity;
import net.redroid.redroidperipheral.debug.TestSerializationActivity;

public class DebugMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button startYaseaButton = (Button) findViewById(R.id.start_yasea);
        Button startAudioButton = (Button) findViewById(R.id.start_audio);
        Button testSerializationButton = (Button) findViewById(R.id.start_test);

        startYaseaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invokeVideoStreamingActivity();
            }
        });

        startAudioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invokeAudioStreamingActivity();
            }
        });

        testSerializationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invokeTestSerialization();
            }
        });
    }

    public void invokeVideoStreamingActivity(){
        startActivity(new Intent(getBaseContext(), VideoStreamingActivity.class));
        finish();
    }

    public void invokeAudioStreamingActivity(){
        startActivity(new Intent(getBaseContext(), AudioStreamingActivity.class));
        finish();
    }

    public void invokeTestSerialization(){
        startActivity(new Intent(getBaseContext(), TestSerializationActivity.class));
        finish();
    }

}
