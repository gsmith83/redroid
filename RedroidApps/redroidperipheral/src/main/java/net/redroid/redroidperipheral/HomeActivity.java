package net.redroid.redroidperipheral;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.sun.mail.smtp.SMTPTransport;
import net.redroid.redroidcorelibrary.ApiResponse;
import net.redroid.redroidcorelibrary.App;
import net.redroid.redroidcorelibrary.Network.ConnectionException;
import net.redroid.redroidperipheral.network.PeripheralNetwork;
import net.redroid.redroidperipheral.network.PeripheralP2PNetwork;
import net.redroid.redroidperipheral.network.PeripheralServerNetwork;
import net.redroid.redroidperipheral.network.PeripheralWifiNetwork;
import net.redroid.redroidperipheral.video.YaseaFragment;
import org.json.JSONException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeoutException;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import static android.view.View.GONE;


/**
 * The primary screen for the Peripheral.
 */
public class HomeActivity extends AppCompatActivity implements ListAdapter,
        PeripheralAPI.BikeCamApiListener, PeripheralAPI.RtmpStreamListener {

    private static final long LOCATION_INTERVAL = 10000;
    private LocationListener locationListener;
    private LocationRequest locationRequest;
    private GoogleApiClient locationApiClient;

    PeripheralDataModel model = PeripheralDataModel.getInstance();
    TextView deviceIdLabel, deviceIdText, securityTokenLabel, securityTokenText, ipAddressLabel,
            ipAddressText, serverUrlLabel, connectedDevicesLabel;
    Button currentSelectedButton;
    ImageView qrCode, streamingIcon;
    final static int SERVER_URL_UPDATE_REQUEST = 1;
    MenuItem disconnectMenuItem, recordMenuItem, stopRecordingMenuItem, startLocationLoggingMenuItem,
            stopLocationLoggingMenuItem;
    final Object buttonLock = new Object();
    YaseaFragment yaseaFragment;
    final Object yaseaFragmentLock = new Object();
    ConnectivityReceiver connectivityReceiver;
    PowerManager.WakeLock wakeLock;
    final int[] originalTimeout = new int[1];
    final int[] originalKeyLightValue = new int[1];
    boolean blackedOut = false;
    Bitmap qrBitmapWithWifi, qrBitmapNoWifi;
    String qrStringWithWifi;
    Boolean isBikeCam;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //initialize the API objecgt
        model.setApi();

        //register the activity with the model for bike cam commands
        model.setBikeCamActivity(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "To be implemented: Add new network config alias",
                        Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });

        //fetch handles to views
        ListView listView = (ListView) findViewById(R.id.network_config_listview);
        listView.setAdapter(this);
        deviceIdLabel = (TextView) findViewById(R.id.home_device_id_label);
        deviceIdText = (TextView) findViewById(R.id.home_device_id_text);
        securityTokenLabel = (TextView) findViewById(R.id.home_security_token_label);
        securityTokenText = (TextView) findViewById(R.id.home_security_token_text);
        ipAddressLabel = (TextView) findViewById(R.id.home_ip_address_label);
        ipAddressText = (TextView) findViewById(R.id.home_ip_address_text);
        serverUrlLabel = (TextView) findViewById(R.id.home_server_url);
        connectedDevicesLabel = (TextView) findViewById(R.id.connected_devices);
        qrCode = (ImageView)findViewById(R.id.qr_code);
        streamingIcon = (ImageView)findViewById(R.id.streaming_icon);


        //set QR code click listener
        qrCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showQrPopup();
            }
        });
        //display QR code
        setQrCode();

        // Display device ID, security token, and server URL
        deviceIdText.setText(model.getDeviceID());
        securityTokenText.setText(model.getSecurityToken());
        serverUrlLabel.setText(getResources().
                getString(R.string.server_url_peripheral_label, model.getConnectionServerUrl()));

        //register local broadcast receiver to show number of connected sensors
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int num = intent.getIntExtra("num", -1);
                if (num != -2) //-2 device count signifies disconnection
                    connectedDevicesLabel.setText("Connected Devices: " + num);
                if (model.network instanceof PeripheralP2PNetwork && num == 0) {
                    disconnect("p2p partner disconnected");
                }
                else if (model.network instanceof PeripheralServerNetwork) {
                    disconnect("Server disconnected");
                }
            }
        }, new IntentFilter("numConnected"));


        //set up the wakelock
        wakeLock =  ((PowerManager)getSystemService(Context.POWER_SERVICE))
                .newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,  "screenWakeLock");

        wakeLock.acquire();

        //capture the screen timeout setting
        try {
            originalTimeout[0] = Settings.System.getInt(getContentResolver(),
                    Settings.System.SCREEN_OFF_TIMEOUT);
        } catch (Settings.SettingNotFoundException e) {
            originalTimeout[0] = 300000;
            e.printStackTrace();
        }

        //capture the button key light value
        try {
            originalKeyLightValue[0] = Settings.System.getInt(getContentResolver(),
                    "button_key_light");
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
    }


    /**
     * Blacks out the peripheral screen without putting the activity to sleep
     */
    private void blackoutScreen() {

        blackedOut = true;

        //activate the blackout overlay
        final TextView overlay = (TextView)findViewById(R.id.activity_overlay);
        overlay.setVisibility(View.VISIBLE);

        getTheme().applyStyle(R.style.AppThemeBlack, true);

        //hide the status bar and soft keys
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);

        //hide the action bar
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.hide();

        //reduce the size of the streaming icon
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)streamingIcon.getLayoutParams();
        layoutParams.width = (int)(10 * Resources.getSystem().getDisplayMetrics().density); //10 dp
        layoutParams.height = (int)(10 * Resources.getSystem().getDisplayMetrics().density);
        streamingIcon.setLayoutParams(layoutParams);
        streamingIcon.invalidate();


        //dim the hardware button backlight
        try {
            Settings.System.putInt(getApplicationContext().getContentResolver(), "button_key_light", 0);
        } catch (Exception e) {
            e.printStackTrace();
        }


//        //activate the screen timeout in 1 second
//        Settings.System.putInt(getContentResolver(),Settings.System.SCREEN_OFF_TIMEOUT, 1);

        //deactivate screen timeout
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //set long click to un-blackout
        overlay.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                unBlackoutScreen();
                return true;
            }
        });

        //disable other click gestures
        overlay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                overlay.setText(getResources().getString(R.string.long_press_to_un_blackout));
                overlay.invalidate();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                overlay.setText("");
                            }
                        });
                    }
                }).start();
                return false;
            }
        });
        overlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    /**
     * Shows an expanded overlay of the QR code
     */
    private void showQrPopup() {
        ImageView qrImage = new ImageView(this);
        if (model.network != null)
            qrImage.setImageBitmap(qrBitmapWithWifi);
        else
            qrImage.setImageBitmap(qrBitmapNoWifi);
        final Dialog dialog = new AlertDialog.Builder(HomeActivity.this, R.style.AppThemeTransparentDialog)
                .setMessage("")
                .setTitle("")
                .setView(qrImage)
                .create();

        qrImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
    }

    /**
     * Reverses screen blackout set by blackoutScreen()
     */
    private void unBlackoutScreen() {

        blackedOut = false;
        //de-activate the blackout overlay
        final View overlay = findViewById(R.id.activity_overlay);
        overlay.setVisibility(View.GONE);

        //remove the gesture listeners
        overlay.setOnGenericMotionListener(null);
        overlay.setOnLongClickListener(null);


        //restore hardware button backlight
        Settings.System.putInt(getApplicationContext().getContentResolver(), "button_key_light",
                originalKeyLightValue[0]);
//                5000);


        //show the status bar
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);

        //show the action bar
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.show();

        //restore the size of the streaming icon
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)streamingIcon.getLayoutParams();
        layoutParams.width = (int)(40 * Resources.getSystem().getDisplayMetrics().density); //40dp
        layoutParams.height = (int)(40 * Resources.getSystem().getDisplayMetrics().density);
        streamingIcon.setLayoutParams(layoutParams);
        streamingIcon.invalidate();

//        //restore the screen timeout
//        Settings.System.putInt(getContentResolver(),Settings.System.SCREEN_OFF_TIMEOUT,
//                originalTimeout[0]);
        //reactivate screen timeout
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Settings.System.putInt(getContentResolver(),Settings.System.SCREEN_OFF_TIMEOUT,
                originalTimeout[0]);
    }


    /**
     * Starts the RTMP video stream
     * @param audio whether to use audio
     * @param video whether to use video
     */
    private void startRTMPStreaming(boolean audio, boolean video) {
        synchronized (yaseaFragmentLock) {
            // load shared configuration for yasea
            model.rtmpStreamUrl = model.getRtmpServerUrl() + model.getDeviceID() + "_";

            SharedPreferences sp = this.getSharedPreferences("Yasea", this.MODE_PRIVATE);
            SharedPreferences.Editor edit = sp.edit();
            edit.clear();
            edit.putString("rtmpUrl", model.rtmpStreamUrl);
            edit.putString("audioOnly", (audio && !video) + "");
            edit.commit();

            //make the yasea fragment
            yaseaFragment = new YaseaFragment();

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.yasea_video_fragment_frame, yaseaFragment)
                    .commit();
            updateUiOnStreamStart();
        }
    }

    @Override
    protected void onStart() {
        //destroy the yaseaFragment if it still exists
        if (yaseaFragment != null)
            onRtmpStreamStartStopRequest(false, false);
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        model.setBikeCamActivity(this);
        model.setRtmpStreamListener(this);

        if (blackedOut) {
            //re-hide the status bar
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);

            //re-dim the hardware button backlight
            try {
                Settings.System.putInt(getApplicationContext().getContentResolver(),
                        "button_key_light", 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //register the broadcast receiver to detect loss of connectivity
        connectivityReceiver = new ConnectivityReceiver();
    }


    @Override
    protected void onPause() {
        super.onPause();
        //unregister the broadcast receiver to detect loss of connectivity
        if (connectivityReceiver != null) {
            try {unregisterReceiver(connectivityReceiver);} catch(Exception e){e.printStackTrace();}
        }

        //un-dim the hardware button backlight
        try {
            Settings.System.putInt(getApplicationContext().getContentResolver(),
                    "button_key_light", originalKeyLightValue[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        disconnectMenuItem = menu.findItem(R.id.disconnect_menu_item);
        stopRecordingMenuItem = menu.findItem(R.id.stop_recording_menu_item);
        recordMenuItem = menu.findItem(R.id.record_menu_item);
        startLocationLoggingMenuItem = menu.findItem(R.id.start_location_logging_menu_item);
        stopLocationLoggingMenuItem = menu.findItem(R.id.stop_location_logging_menu_item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.connection_server_url_menu_item:
                showConnectionServerUrlDialog();
                break;
            case R.id.video_server_url_menu_item:
                showVideoServerUrlDialog();
                break;
            case R.id.disconnect_menu_item:
                synchronized (buttonLock) {
                    disconnect("Disconnected");
                    item.setVisible(false);
                }
                break;
            case R.id.blackout_menu_item:
                blackoutScreen();
                break;
            case R.id.exit_menu_item:
                disconnect("");
                System.exit(0);
                break;
            case R.id.record_menu_item:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        onRtmpStreamStartStopRequest(true, true);
                    }
                }).start();
                break;
            case R.id.stop_recording_menu_item:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        onRtmpStreamStartStopRequest(false, false);
                    }
                }).start();
                break;
            case R.id.start_location_logging_menu_item:
                showLocationLoggingEmailDialog();
                startLocationLoggingMenuItem.setVisible(false);
                stopLocationLoggingMenuItem.setVisible(true);
                break;
            case R.id.stop_location_logging_menu_item:
                //stop logging location data
                LocationServices.FusedLocationApi.removeLocationUpdates(locationApiClient, locationListener);
                //send email
                new AsyncTask<Void, Void, Void>(){
                    File fileToSend = null;
                    SMTPTransport t = null;
                    @Override
                    protected Void doInBackground(Void... params) {
                        Properties props = System.getProperties();
                        props.put("mail.smtps.host","smtp.gmail.com");
                        props.put("mail.smtps.auth","true");
                        Session session = Session.getInstance(props, null);
                        Message msg = new MimeMessage(session);
                        try {
                            msg.setFrom(new InternetAddress("location_services@redroid.net"));
                            msg.setRecipients(Message.RecipientType.TO,
                                    InternetAddress.parse(model.getLocationLogEmail(), false));
                            msg.setSubject("Location Log Data");
                            File fileToSend = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/location_log.csv");
                            // create string to set file text
                            StringBuilder sb = new StringBuilder();
                            BufferedReader reader = new BufferedReader(new FileReader(fileToSend));
                            String str = null;
                            while((str = reader.readLine()) != null){
                                sb.append(str + "\n");
                            }

                            msg.setText(sb.toString());
                            msg.setFileName(fileToSend.getName());
                            msg.setHeader("X-Mailer", "Redroid Location Logging");
                            msg.setSentDate(new Date());
                            t = (SMTPTransport) session.getTransport("smtps");
                            t.connect("smtp.gmail.com", "redroid.uofu@gmail.com", "redroidpassword");
                            t.sendMessage(msg, msg.getAllRecipients());
//                            System.out.println("Response: " + t.getLastServerResponse());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(HomeActivity.this, "Location log sent to " +
                                            model.getLocationLogEmail(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                        finally{
                            if(fileToSend != null)
                                fileToSend.delete();
                            if(t != null) {
                                try {
                                    t.close();
                                }
                                catch(Exception e){}
                            }
                        }

                        return null;
                    }
                }.execute();

                startLocationLoggingMenuItem.setVisible(true);
                stopLocationLoggingMenuItem.setVisible(false);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Helper method for updating UI elements upon RTMP stream stopping
     */
    private void updateUiOnStreamStop() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //show the record menu item
                recordMenuItem.setVisible(true);
                //hide the stop menu item
                stopRecordingMenuItem.setVisible(false);
                //hide the streaming icon
                streamingIcon.setVisibility(View.GONE);
            }
        });

    }

    /**
     * Helper mtehod for updating UI elements upon RTMP stream starting
     */
    private void updateUiOnStreamStart()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //hide the record menu item
                recordMenuItem.setVisible(false);
                //show the stop menu item
                stopRecordingMenuItem.setVisible(true);
                //show the streaming icon
                streamingIcon.setVisibility(View.VISIBLE);
            }
        });

    }

    /**
     * ListAdapter method overrides
     ****/
    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEnabled(int position) {
        switch (position) {
            case 0:
                return true;
            case 1:
                return true;
            case 2:
                return true;
            default:
                return false;
        }
    }

    @Override
    public int getCount() {
//        return 6;
        return 3;
    }


    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = getLayoutInflater()
                    .inflate(R.layout.viewgroup_network_config_alias_button, null);
        }

        // fetch the sub-views
        ImageView trashCan = (ImageView) convertView.findViewById(R.id.trash_can);
        final Button button = (Button) convertView.findViewById(R.id.config_name_button);
        final PeripheralNetwork[] network = {null};

        if (position == 0) {
            button.setText(R.string.wifi_direct_p2p);
            trashCan.getLayoutParams().width = 0;
            trashCan.setOnClickListener(null);
            button.setEnabled(true);
            network[0] = new PeripheralWifiNetwork();
            isBikeCam = true;
        }
        if (position == 1) {
            button.setText(R.string.wifi);
            trashCan.getLayoutParams().width = 0;
            trashCan.setOnClickListener(null);
            button.setEnabled(true);
            network[0] = new PeripheralWifiNetwork();
        }
        if(position == 2){
            trashCan.getLayoutParams().width = 0;
            trashCan.setOnClickListener(null);
            button.setText("Redroid Server");
            button.setEnabled(true);
            network[0] = new PeripheralServerNetwork();
        }


        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //check server url
                if (network[0] instanceof PeripheralServerNetwork && model.getConnectionServerUrl().isEmpty()) {
                    divertToServerUrlActivity();
                }

                synchronized (buttonLock) {
                    if (currentSelectedButton == button)
                        return;

                    //tentatively set to pending init color
                    button.getBackground().setColorFilter(getResources().getColor(R.color.colorInitPending_Button), PorterDuff.Mode.MULTIPLY);

                    //make disconnect available in the menu
                    disconnectMenuItem.setVisible(true);

                    HandlerThread handlerThread = new HandlerThread("OnReceive Handler Thread");
                    handlerThread.start();
                    Looper looper = handlerThread.getLooper();
                    Handler handler = new Handler(looper);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            synchronized (buttonLock) {
                                if (network[0] instanceof PeripheralP2PNetwork) {
                                    //async version necessary for P2P framework initialization
                                    initializeP2PNetwork((PeripheralP2PNetwork)network[0],
                                            new PeripheralP2PNetwork.InitSuccessListener() {
                                                @Override
                                                public void onInitSuccess() {
                                                    HomeActivity.this.registerReceiver(connectivityReceiver,
                                                            new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            //deselect the currently selected mode, if any
                                                            updateDeselectedView(currentSelectedButton, "");
                                                            //now the new one
                                                            updateSelectedView(button);
                                                            Snackbar.make(HomeActivity.this.findViewById(R.id.activity_home),
                                                                    "P2P ready for connection", Snackbar.LENGTH_LONG).show();
                                                        }
                                                    });
                                                }

                                                @Override
                                                public void onInitFailure(String msg) {
                                                    model.teardownNetwork();
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            updateDeselectedView(button, "");
                                                        }
                                                    });
                                                }
                                            });
                                }
                                else {
                                    boolean initSuccess = initializeNetwork(network[0]);
                                    if (initSuccess) {
                                        HomeActivity.this.registerReceiver(connectivityReceiver,
                                                new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                //deselect the currently selected mode, if any
                                                updateDeselectedView(currentSelectedButton, "");
                                                //now the new one
                                                updateSelectedView(button);
                                                disconnectMenuItem.setVisible(true);
                                            }
                                        });

                                        Snackbar.make(HomeActivity.this.findViewById(R.id.activity_home),
                                                "Started Peripheral API Server", Snackbar.LENGTH_LONG).show();
                                    } else { //return to default color
                                        model.teardownNetwork();
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                updateDeselectedView(button, "");
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    });
                }
            }
        });
        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
    }

    private void invokeBikeCamActivity() {
        Intent BikeCamActivityIntent = new Intent(getBaseContext(), BikeCamActivity.class);
        startActivity(BikeCamActivityIntent);
    }

    private void invokeServerUrlActivity() {
        if (model.network != null && model.network instanceof PeripheralServerNetwork) {
            Snackbar.make(this.findViewById(R.id.content_home),
                    getResources().getString(R.string.server_already_running),
                    Snackbar.LENGTH_LONG).show();
            return;
        }
        Intent intent = new Intent(getBaseContext(), ServerUrlActivity.class);
        startActivityForResult(intent, SERVER_URL_UPDATE_REQUEST);
    }


    /**
     * The purpose of the following 2 methods is in case the server url has not been set yet,
     * the user will be forced to the server url activity to set it. If they return having set it
     * successfully, this will trigger a snackbar message informing the user that the url was
     * updated.
     */
    private void divertToServerUrlActivity() {

        //make an alert dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setMessage(getResources().getString(R.string.update_server_url_msg))
                .setTitle(getResources().getString(R.string.server_url_not_set_dialog_title));
        builder.setPositiveButton(R.string.set_server_url, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
//                invokeServerUrlActivity();
                showConnectionServerUrlDialog();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SERVER_URL_UPDATE_REQUEST) {
            if (resultCode != RESULT_OK) {
                return;
            }
            Snackbar.make(this.findViewById(R.id.content_home),
                    getResources().getString(R.string.url_updated),
                    Snackbar.LENGTH_SHORT).show();
            serverUrlLabel.setText(getResources().
                    getString(R.string.server_url_peripheral_label, model.getConnectionServerUrl()));

        }
    }


    /**
     * Helper methods for intializing the network objects upon connection
     * @param network Network object
     * @param initSuccessListener listener to be called on init success
     */
    private void initializeP2PNetwork(PeripheralP2PNetwork network,
                                      PeripheralP2PNetwork.InitSuccessListener initSuccessListener) {
        try {
            model.initializeP2PNetwork(network, initSuccessListener);
        } catch (Exception e) {
            initSuccessListener.onInitFailure(e.getMessage());
            e.printStackTrace();
        }
    }

    private boolean initializeNetwork(PeripheralNetwork network) {
        try {
            model.initializeNetwork(network);
            if (network instanceof PeripheralServerNetwork) {
                String success = ((PeripheralServerNetwork) network).connectToServer();
                if (success != null) {
                    throw new ConnectionException(success);
                }
                else {
                    ((PeripheralServerNetwork) network).startApiServer();
                }
            }
        }
        catch (TimeoutException e) {
            String msg = "";
            if (network instanceof PeripheralWifiNetwork) {
                msg = getResources().getString(R.string.wifi_not_connected_msg);
            }
            else if (network instanceof PeripheralServerNetwork) {
                msg = getResources().getString(R.string.connection_attemp_timed_out);
            }
            Snackbar.make(HomeActivity.this.findViewById(R.id.content_home),
                    msg, Snackbar.LENGTH_LONG).show();
            return false;
        }
        catch (ConnectionException e) {
            Snackbar.make(HomeActivity.this.findViewById(R.id.content_home),
                    e.getMessage(), Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    /**
     * Helper method which updates the view for the selected network mode
     */

    private void updateSelectedView(Button button) {
//        if (model.network instanceof PeripheralWifiNetwork)) {
        if (model.network instanceof PeripheralWifiNetwork &&
                !isBikeCam) {
            ipAddressLabel.setVisibility(View.VISIBLE);
            ipAddressText.setText(model.network.localIpAddress);
            ipAddressText.setVisibility(View.VISIBLE);
            connectedDevicesLabel.setVisibility(View.VISIBLE);

        }
        else if (isBikeCam) {
            setQrCode();
//            ipAddressLabel.setText(String.format("%s: %s",
//                    getResources().getString(R.string.ip_address),
//                    model.network.localIpAddress));
//            ipAddressLabel.setVisibility(View.VISIBLE);
//            connectedDevicesLabel.setVisibility(View.VISIBLE);
        }
        button.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryFaded),
                PorterDuff.Mode.MULTIPLY);
        button.setEnabled(false);
        currentSelectedButton = button;
    }

    //updates the view for the disconnectd network mode
    private void updateDeselectedView(Button button, String msg) {
        if (button == null) return;
        ipAddressLabel.setVisibility(GONE);
        ipAddressText.setVisibility(GONE);
        connectedDevicesLabel.setVisibility(GONE);
        button.getBackground().clearColorFilter();
        button.setEnabled(true);
        if (!msg.isEmpty()) {
            Snackbar.make(HomeActivity.this.findViewById(R.id.content_home),
                    msg, Snackbar.LENGTH_SHORT).show();
        }
    }

    /**
     * For disconnecting from a particular network config.  Cleans up the UI and network state,
     * including stopping any rtmp streasm.
     * @param msg an optional snackbar message
     */
    private void disconnect(String msg) {
        model.teardownNetwork();
        updateDeselectedView(currentSelectedButton, msg);
        currentSelectedButton = null;
        teardownRtmp();
        setQrCode();
    }

    /**
     * For stopping an RTMP stream and resetting states
     */
    private void teardownRtmp() {
        synchronized (yaseaFragmentLock) {
            if (yaseaFragment != null) {
                getSupportFragmentManager().beginTransaction().remove(yaseaFragment).commit();
                yaseaFragment = null;
            }
            model.rtmpStreamUrl = null;
            updateUiOnStreamStop();
        }
    }

    /**
     * creates and sets the QR code
     */
    private void setQrCode() {
        String qrString;
        if (model.network == null) {
            //set the string for 2 fields
            qrString = String.format("%s\n%s", model.getDeviceID(), model.getSecurityToken());
            //set and return if the right bitmap already exists
            if (qrBitmapNoWifi != null) {
                qrCode.setImageBitmap(qrBitmapNoWifi);
                return;
            }
        }

        else {
            //set the string for 3 fields
            qrString = String.format("%s\n%s\n%s", model.getDeviceID(),
                    model.getSecurityToken(), model.getIPAddress());
            //set and return if the right bitmap already exists
            if (qrBitmapWithWifi != null && qrStringWithWifi.equals(qrString)) {
                qrCode.setImageBitmap(qrBitmapWithWifi);
                return;
            }
        }

        //thanks to http://stackoverflow.com/a/25283174/7407559
        QRCodeWriter writer = new QRCodeWriter();
        Bitmap bitmap;
        try {
            //create the qr bitmap
            BitMatrix bitMatrix = writer.encode(qrString, BarcodeFormat.QR_CODE, 500, 500);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bitmap.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.parseColor("#fafafa"));
                }
            }
            //set it
            qrCode.setImageBitmap(bitmap);

            //cache it for later use
            if (model.network == null) {
                qrBitmapNoWifi = bitmap;
            } else {
                qrBitmapWithWifi = bitmap;
                qrStringWithWifi = qrString;
            }
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    /**
     * Starts the bike cam activity if an API request 301 "start_bike_cam" is received
     * @param method the HTTP method associated with the incoming API command
     * @param url    the URL string associated with the incoming API command
     * @param json   the JSON string payload of the incoming API command
     * @return an ApiResponse object
     * @throws JSONException
     */
    @Override
    public ApiResponse onBikeCamApiRequest(String method, String url, String json) throws JSONException {
        String responseJson;
        int responseCode;
        boolean success = false;
        if (method.equals("POST") && url.equals("/redroid/bikecam/activity/bikecam")) {
            responseCode = 200;
            responseJson = "";
            success = true;
        } else {
            responseCode = 400;
            responseJson = "{\"msg\":\"invalid request\"}";
        }
        if (success)
            invokeBikeCamActivity();
        return new ApiResponse(responseCode, responseJson);
    }

    /**
     * For responding to changes in RTMP streaming from the Network or API classes. A value of false
     * for both parameters means that the stream should be stopped.
     * @param audio whether audio is requested in the stream
     * @param video whether video is requested in the stream
     * @return the url of the stream; null if stopped
     */
    @Override
    public String onRtmpStreamStartStopRequest(boolean audio, boolean video) {
        synchronized (yaseaFragmentLock) {
            //for starting the stream
            if (yaseaFragment == null && (audio || video)) {
                startRTMPStreaming(audio, video);
            }
            //for stopping the stream
            else if (!audio && !video) {
                teardownRtmp();
            }
            //return the url, regardless
            return model.rtmpStreamUrl;
        }
    }

    /**
     * For handling a request for the RTMP stream url
     * @return
     */
    @Override
    public String onRtmpStreamUrlRequest() {
        synchronized (yaseaFragmentLock) {
            return model.rtmpStreamUrl;
        }
    }

    /**
     * Detects when connectivity is lost.
     * Thanks to http://stackoverflow.com/a/12894153/7407559
     */
    public class ConnectivityReceiver extends BroadcastReceiver {


        @Override
        public void onReceive(Context context, Intent intent) {
            final WifiManager wifiManager = (WifiManager)context.getSystemService(WIFI_SERVICE);
            final NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);

            if (networkInfo == null)
                return;

            //handle wifi p2p
            if (model.network instanceof PeripheralP2PNetwork) {
                if (!wifiManager.isWifiEnabled()) {
                    disconnect("Wifi disabled");
                }
                else
                    return;

            }

            //handle plain wifi
            else if (model.network instanceof PeripheralWifiNetwork &&
                    networkInfo.getType() != ConnectivityManager.TYPE_WIFI) {
                disconnect("Connectivity lost");
            }

            //complete disconnection
            if (!networkInfo.isConnected()) {
                disconnect("Connectivity lost");
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (!blackedOut)
            super.onBackPressed();
        //disabled if blacked out
    }

    /**
     * Shows a dialog for setting the connection server URL
     */
    private void showConnectionServerUrlDialog() {
        final EditText urlField = new EditText(this);
        urlField.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        urlField.setText(model.getConnectionServerUrl());
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setView(urlField)
                .setTitle("Connection server URL")
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        model.setConnectionServerUrl(urlField.getText().toString());
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Shows a dialog for setting the RTMP server URL
     */
    private void showVideoServerUrlDialog() {
        final EditText urlField = new EditText(this);
        urlField.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        urlField.setText(model.getRtmpServerUrl());
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setView(urlField)
                .setTitle("Video server URL")
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        model.setRtmpServerUrl(urlField.getText().toString());
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Shows a dialog for setting an email address for location logging
     */
    private void showLocationLoggingEmailDialog() {
        final AutoCompleteTextView emailField = new AutoCompleteTextView(this);
        emailField.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        if (model.getLocationLogEmail() != null)
            emailField.setText(model.getLocationLogEmail());
        String[] emails = (String[]) model.emailAddressesAutoCompleteList.toArray(new String[0]);
        ArrayAdapter<String> emailAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, emails);
        emailField.setAdapter(emailAdapter);
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setView(emailField)
                .setTitle("Set Location Log Email Destination")
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String email = emailField.getText().toString();
                        model.setLocationLogEmail(email);
                        new LogLocation().start();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private String makeCsvRow(Location location) {
        return String.format("%f,%f,%f,%f,%f,%f,%d\n",
                location.getLatitude(),
                location.getLongitude(),
                location.getAltitude(),
                location.getBearing(),
                location.getSpeed(),
                location.getAccuracy(),
                location.getTime()
        );
    }

    private class LogLocation extends Thread{

        @Override
        public void run(){
            //see if location services are on
            LocationManager locationManager = (LocationManager) App.getAppContext().getSystemService
                    (Context.LOCATION_SERVICE);
            boolean gps_enabled = true;
            boolean network_enabled = true;

            try {
                gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            }
            catch (Exception e) {
                e.printStackTrace();
            }

            if (!gps_enabled && !network_enabled) {
                System.err.println("Location services not available.");
            }

            //prepare the LocationRequest object
            locationRequest = new LocationRequest();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setFastestInterval(LOCATION_INTERVAL);
            locationRequest.setInterval(LOCATION_INTERVAL);

            final File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "location_log.csv");
            final BufferedWriter[] fos = {null};
            try {
                fos[0] = new BufferedWriter(new FileWriter(file.getAbsolutePath(), false));
                fos[0].write("latitude,longitude,altitude,bearing,speed,accuracy,time\n");
            }
            catch (Exception e) {
                e.printStackTrace();
            }


            //prepare the LocationListener
            locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    new AsyncTask<String, Void, Void>() {
                        @Override
                        protected Void doInBackground(String... params) {

                            try {
//                                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "location_log.csv");
//                                if(!file.exists()) {
//                                    file.createNewFile();
//                                    fos.write("latitude,longitude,altitude,bearing,speed,accuracy,time");
//                                }
                                fos[0].write(params[0] + "\n");
                                fos[0].flush();
                            }
                            catch(Exception e){
                                e.printStackTrace();
                            }
                            finally{
                                if(fos[0] != null)
                                    try {
                                        fos[0].close();
                                    } catch(Exception e){}
                            }
                            return null;
                        }
                    }.execute(makeCsvRow(location));
                }
            };

            //add LocationServices API
            GoogleApiClient.Builder builder = new GoogleApiClient.Builder(App.getAppContext());
            builder.addApi(LocationServices.API);
            locationApiClient = builder.build();
            locationApiClient.blockingConnect();

            LocationServices.FusedLocationApi.requestLocationUpdates(locationApiClient,
                    locationRequest, locationListener, Looper.getMainLooper());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(HomeActivity.this, "Location logging started", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}