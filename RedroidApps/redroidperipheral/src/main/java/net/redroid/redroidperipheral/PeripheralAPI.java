package net.redroid.redroidperipheral;


import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.jwetherell.motion_detection.OnMotionDetectionListener;

import net.ossrs.yasea.SrsCameraView;
import net.redroid.redroidcorelibrary.API;
import net.redroid.redroidcorelibrary.ApiResponse;
import net.redroid.redroidcorelibrary.App;
import net.redroid.redroidcorelibrary.FeatureList;
import net.redroid.redroidcorelibrary.Network;
import net.redroid.redroidcorelibrary.RedroidUtil;
import net.redroid.redroidcorelibrary.sensor.SensorList;
import net.redroid.redroidperipheral.network.PeripheralServerNetwork;
import net.redroid.redroidperipheral.network.PeripheralWifiNetwork;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static android.content.ContentValues.TAG;
import static net.redroid.redroidcorelibrary.API.Method.POST;
import static net.redroid.redroidcorelibrary.API.Method.PUT;
import static net.redroid.redroidcorelibrary.RedroidUtil.makeSimpleJson;


/**
 * Contains methods for performing redroid network communication API calls on a Redroid peripheral.
 * Extends the AbstractAPI class in the Redroid Core Library, which contains methods common to
 * both Master and Peripheral, including helper methods utilized by both.
 */
public class PeripheralAPI extends net.redroid.redroidcorelibrary.API {
    private PeripheralDataModel model;
    private final GoogleApiClient[] locationApiClient = {null};



    public PeripheralAPI(PeripheralDataModel model) {
        this.model = model;
    }


    public ApiResponse dispatchRequest(String remoteHost, String method, String url,
                                       String json) throws JSONException {
        return dispatchRequest(remoteHost, method, url, json, null);
    }


    public ApiResponse dispatchRequest(String remoteHost, String method, String url,
                                       String json, WifiConnectedDevice device)
            throws JSONException {
        //unpack the Json string
        if (json != null) {
            json = json.replace("\n", "").replace("\r", "");
        }
        JSONObject jsonObject = null;
        if (json != null && !json.equals("")) {
            jsonObject = new JSONObject(json);
        }

        //declare the response object variables in advance
        String responseJson = "";
        int responseCode = 400;
        ApiResponse response = null; //todo: be sure this is actually set in all paths

        //parse the url into the stem and the argument
        String urlStem = url;
        String urlArg = "";
        // this one API call has a two part argument deliminted by slashes
        if (method.equals("DELETE") && url.startsWith("/redroid/master/register/sensor_events")) {
            String[] urlParts = url.split("/");
            urlStem = "";
            for (int i = 0; i < urlParts.length - 2; i++) {
                if (!urlParts[i].isEmpty())
                    urlStem += "/" + urlParts[i];
            }
            urlArg = "/" + urlParts[urlParts.length - 2] + "/" + urlParts[urlParts.length - 1];
        }
        //otherwise, these have a one-part argument delimited by the last slash in the url
        if (url.startsWith("/redroid/master/register/camera_specs") ||
                url.startsWith("/redroid/master/register/sensor_events") ||
                (method.equals("DELETE") && url.startsWith
                        ("/redroid/server/register/motion_detection")) ||
                (method.equals("DELETE") && url.startsWith("/redroid/master/rule"))) {
            String[] urlParts = url.split("/");
            urlStem = "";
            for (int i = 0; i < urlParts.length - 1; i++) {
                if (!urlParts[i].isEmpty())
                    urlStem += "/" + urlParts[i];
            }
            urlArg = urlParts[urlParts.length - 1];
        }

        //which method?
        if (method.equals("GET")) {
            switch (urlStem) {
                case "/redroid/master/ping":
                    response = respondGetPing();
                    break;
                case "/redroid/master/request/sensor_list":
                    response = respondGetSensorList();
                    break;
                case "/redroid/master/request/feature_list":
                    response = respondGetFeatureList();
                    break;
                case ("/redroid/master/streaming/rtmp/video"):
                case ("/redroid/master/streaming/rtmp/video_audio"):
                case ("/redroid/master/streaming/rtmp/audio"):
                    response = respondGetRTMP(urlStem);
                    break;
                case ("/redroid/master/request/last_location"):
                    response = respondGetLastLocation();
                    break;
                case ("/redroid/master/request/battery"):
                    response = respondGetBattery();
                    break;
                default:
                    break;

            }
            //TODO fill this in for GET API methods
        }
        else if (method.equals("POST")) {
            switch (urlStem) {
                case "/redroid/bikecam/state/flasher":
                case "/redroid/bikecam/state/signal":
                case "/redroid/bikecam/activity/bikecam":
                    response = respondPostBikeCamCommand(method, url, json);
                    break;
                case "/redroid/master/rtmp/state":
                    response = respondPostRtmpState(jsonObject);
                    break;
            }
        }
        else if (method.equals("PUT")) {
            switch (urlStem) {
                case "/redroid/master/register/sensor_events":
                    if (model.network instanceof PeripheralWifiNetwork) {
                        response = respondPutRegisterSensorEvents(device, urlArg,
                                jsonObject);
                    }
                    else {
                        response = respondPutRegisterSensorEvents(urlArg,
                                jsonObject);
                    }
                    break;
                case "/redroid/master/register/location_events":
                    if (model.network instanceof PeripheralWifiNetwork) {
                        response = respondPutRegisterLocationEvent(device, urlArg, jsonObject);
                    }
                    else {
                        response = respondPutRegisterLocationEvent(null, urlArg, jsonObject);
                    }
                    break;
                case "/redroid/server/register/motion_detection":
                    response = respondPutRegisterMotionDection(jsonObject);
                    break;
                case "/redroid/master/rule":
                    response = respondPutDeleteRule();
                    break;
                default:
                    break;
            }
        }

        else if (method.equals("DELETE")) {
            Log.d(TAG, "dispatchRequest: DELETE: urlStem: " + urlStem);
            switch (urlStem) {
                case "/redroid/server/motion_detection":
                    response = respondDeleteRegisterDection();
                    break;
                case "/redroid/master/rule":
                    response = respondPutDeleteRule();
                    break;

                //warning
                /**
                 * This DELETED method not currently used, and changes have been made since
                 * testing. If it
                 * is ever used in the future, it should be done cautiously
                 **/
                case "/redroid/master/register/sensor_events":
                    String args[] = urlArg.split("/");
                    String sensorType = "";
                    if (args.length > 1) {
                        sensorType = args[1];
                    }
                    String requestIdString = "xxx";
                    if (args.length > 2) {
                        requestIdString = args[args.length - 1];
                    }
                    int requestId;
                    try {
                        requestId = Integer.parseInt(requestIdString);

                        if (model.network instanceof PeripheralWifiNetwork) {
                            response = respondDeleteSensorEventRegistration(device, sensorType,
                                    requestId);
                        }
                        else {
                            response = respondDeleteSensorEventRegistration(sensorType, requestId);
                        }
                    }
                    catch (NumberFormatException e) {
                        response = new ApiResponse(400, "{\"msg\":\"malformed request\"}");
                    }
                    break;
                default:
                    response = new ApiResponse(400, "{\"msg\":\"invalid request\"}");
                    break;

            }
        }
        return response;
    }

    /**
     * Creates a response for a PING request to confirm a still-open connection
     *
     * @return an APIResponse object containing the response code response body
     */
    ApiResponse respondGetPing() {
        return new ApiResponse(200, null);
    }


    /**
     * API 001
     * Creates a response for a master handshake request part 1 "hello".
     *
     * @param requestBody the JSON payload of the request
     * @param sessionId   the session ID assigned by the network object for the current connection
     * @return an APIResponse object containing the response code response body
     */
    public ApiResponse respondPostRedroidMasterHello(final String requestBody, final String
            sessionId) {
        int responseCode;
        String responseBody;

        try {
            JSONObject json = new JSONObject(requestBody);
            String deviceId = json.getString("device_id");
            //failure?
            if (!model.getDeviceID().equals(deviceId)) {
                responseCode = 401;
                responseBody = "{\"msg\":\"Device ID does not match\"}";
            }
            //success -- prepare session ID response body
            else {
                responseCode = 200;
                responseBody = String.format(Locale.US, "{\"session_id\":%s}", sessionId);
            }
        }
        catch (JSONException e) {
            Log.e(TAG, "Hello handshake 1: json content invalid");
            responseCode = 400;
            responseBody = "{\"msg\":\"Invalid sensors in handshake request: can't get " +
                    "device_id\"}";
        }
        return new ApiResponse(responseCode, responseBody);
    }

    /**
     * API 002
     * Creates a response for a master handshake request part 2 "auth".
     *
     * @param requestBody the JSON payload of the request
     * @param sessionId   the session ID assigned by the network object for the current connection
     * @return an APIResponse object containing the response code and possibly body, if 4xx
     */
    public ApiResponse respondPostRedroidMasterAuth(final String requestBody, final String
            sessionId) {
        int responseCode;
        String responseBody;

        try {
            JSONObject json = new JSONObject(requestBody);
            String sessionIdCandidate = json.getString("session_id");
            String securityToken = json.getString("security_token");

            //failure?
            if (!sessionId.equals(sessionIdCandidate)) {
                responseCode = 401;
                responseBody = "{\"msg\":\"Invalid session id\"}";
            }
            else if (!("" + model.getSecurityToken()).equals(securityToken)) {
                responseCode = 401;
                responseBody = "{\"msg\":\"Invalid security token\"}";
            }

            //success -- set success status
            else {
                responseCode = 200;
                responseBody = "";
            }
        }
        catch (JSONException e) {
            Log.d(TAG, "run: handshake json content invalid");
            responseCode = 400;
            responseBody = "{\"msg\":\"Invalid sensors in auth handshake request: "
                    + "Can't get session id and/or security token\"}";
        }
        return new ApiResponse(responseCode, responseBody);
    }

    private ApiResponse respondGetRTMP(String url) {
        // generate a unique url to post stream to

        String[] s = url.split("/");

        // try to fetch the streaming URL
        String rtmpURL = model.getRtmpStreamListener().onRtmpStreamUrlRequest();
        if (rtmpURL != null)
            return new ApiResponse(200, String.format("{\"url\":\"%s\"}", rtmpURL));
        else
            return new ApiResponse(404, "{\"msg\":\"Not streaming 404\"}");

    }

    private ApiResponse respondPostRtmpState(JSONObject jsonObject) {
        boolean isStreaming = false;
        try {
            isStreaming = jsonObject.getBoolean("is_streaming");
        }
        catch (JSONException e) {
            e.printStackTrace();
            return new ApiResponse(400, "{\"msg\":\"invalid json\"}");
        }
        String rtmpURL = model.getRtmpStreamListener().onRtmpStreamStartStopRequest(isStreaming,
                isStreaming);
        if (isStreaming) {
            if (rtmpURL != null)
                return new ApiResponse(200, String.format("{\"url\":\"%s\"}", rtmpURL));
            else
                return new ApiResponse(400, "\"msg\":\"RTMP stream failed to start\"");
        }
        else {
            return new ApiResponse(200, String.format("{\"url\":\"%s\"}", rtmpURL));
        }
    }

    /**
     * API 004
     * Connects to a Redroid connection server
     *
     * @param listener contains a callback to be executed upon receipt of a response
     */
    public void requestPostRedroidPeripheralConnectApi(final String serverUrl,
                                                       final BufferedReader socketReader,
                                                       final BufferedWriter socketWriter,
                                                       final ApiResponseListener listener) {

        String[] keys = {"device_id", "security_token"};
        String[] values = {model.getDeviceID(), model.getSecurityToken()};
        String url = "/redroid/peripheral/connect/api";
        String requestBody = "";
        try {
            requestBody = makeSimpleJson(keys, values);
        }
        catch (JSONException e) {
            Log.e(TAG, "requestPostRedroidPeripheralConnectApi: " + e.getMessage());
        }
        String remoteHost = String.format("%s:%d", serverUrl, Network.SERVER_API_PORT);

        //send the HTTP POST request
        getHttpResponseAsync(remoteHost, socketReader, socketWriter, POST, url, requestBody,
                listener);
    }

    /**
     * API 005
     * Connects to a Redroid connection server
     *
     * @param listener contains a callback to be executed upon receipt of a response
     */
    public void requestPostRedroidPeripheralConnectEvent(final String serverUrl,
                                                         final BufferedReader socketReader,
                                                         final BufferedWriter socketWriter,
                                                         final ApiResponseListener listener) {

        String[] keys = {"device_id"};
        String[] values = {model.getDeviceID()};
        String url = "/redroid/peripheral/connect/event";
        String requestBody = "";
        try {
            requestBody = makeSimpleJson(keys, values);
        }
        catch (JSONException e) {
            Log.e(TAG, "requestPostRedroidPeripheralConnectEvent: " + e.getMessage());
        }
        String remoteHost = String.format("%s:%d", serverUrl, Network.SERVER_API_PORT);

        //send the HTTP POST request
        getHttpResponseAsync(remoteHost, socketReader, socketWriter, POST, url, requestBody,
                listener);
    }


    /**
     * API 201
     * Returns an API response containing a JSON representation of the sensors on this device
     *
     * @return the ApiResponse
     */
    private ApiResponse respondGetSensorList() {
        SensorList list = new SensorList();
        return new ApiResponse(200, list.toJSONString());
    }

    /**
     * API 202
     * Returns an API response containing a JSON representation of the camera and location features
     * on this device
     *
     * @return the ApiResponse
     */
    private ApiResponse respondGetFeatureList() {
        FeatureList list = new FeatureList();
        return new ApiResponse(200, list.toJson());
    }

    /**
     * API 204
     * Returns a location JSON for the last known location
     *
     * @return the ApiResponse
     */
    private ApiResponse respondGetLastLocation() {
        final Object locationLock = new Object();
        final Location[] location = {null};
        final Boolean[] locationFixComplete = {false};

        //create the connection callbacks for getLastLocation()
        final GoogleApiClient.ConnectionCallbacks connectionCallbacks = new GoogleApiClient
                .ConnectionCallbacks() {
            @Override
            public void onConnected(@Nullable Bundle bundle) {
                if (locationApiClient[0] != null) {
                    location[0] = LocationServices.FusedLocationApi
                            .getLastLocation(locationApiClient[0]); /**this is the heart of this
                     object**/
                    locationFixComplete[0] = true;
                    synchronized (locationLock) {
                        locationFixComplete[0] = true;
                        locationLock.notify();
                    }
                }
            }

            @Override
            public void onConnectionSuspended(int i) {
                locationFixComplete[0] = true;
            }
        };

        //if locationApiClient doesn't exist yet, prepare it and pass in the callbacks
        if (locationApiClient[0] == null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronized (locationLock) {
                        prepareLocationApiClient();
                        locationApiClient[0].registerConnectionCallbacks(connectionCallbacks);
                        locationApiClient[0].connect();
                    }
                }
            }).start();
        }
        //otherwise, just pass in the callbacks
        else {
            synchronized (locationLock) {
                locationApiClient[0].registerConnectionCallbacks(connectionCallbacks);
                locationApiClient[0].connect();
            }
        }

        //block until location is ready
        synchronized (locationLock) {
            while (!locationFixComplete[0]) {
                try {
                    locationLock.wait();
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        //clean up the client
        locationApiClient[0].disconnect();
        locationApiClient[0].unregisterConnectionCallbacks(connectionCallbacks);


        //build the JSON body from the location object
        if (location[0] != null) {
            String body = makeLocationJson(location[0]);
            if (body != null) {
                return new ApiResponse(200, body);
            }
            else {
                return new ApiResponse(500, "{\"msg\":\"oops\"}");
            }
        }
        else {
            return new ApiResponse(503, "{\"msg\":\"location service failed\"}");
        }
    }


    /**
     * API 206
     * Returns an API response containing a JSON representation of the battery sate
     * on this device
     *
     * @return the ApiResponse
     */
    private ApiResponse respondGetBattery() {
        float fLevel;
        boolean isCharging;

        //what's the current level? thanks to http://stackoverflow.com/a/15746919/7407559
        Intent batteryIntent = App.getAppContext().registerReceiver(null, new IntentFilter(Intent
                .ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        // Error checking that probably isn't needed but I added just in case.
        if (level == -1 || scale == -1) {
            fLevel = 50.0f;
        }

        else {
            fLevel = ((float) level / (float) scale) * 100.0f;
        }

        // Are we charging / charged?
        int status = batteryIntent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;

        // How are we charging?  thanks to
//        int chargePlug = batteryIntent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
//        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
//        boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;

        String[] keys = {"level", "isCharging"};
        Object[] values = {fLevel, isCharging};
        String responseJson;
        try {
            responseJson = RedroidUtil.makeSimpleJson(keys, values);
        }
        catch (JSONException e) {
            e.printStackTrace();
            return new ApiResponse(400, String.format("{\"msg\":\"%s\"}", e.getMessage()));
        }

        return new ApiResponse(200, responseJson);
    }


    /**
     * API 301 302 and 303
     * For handling requests to set bike cam state
     *
     * @param method      the HTTP method
     * @param url         the HTTP url, ostensibly either /redroid/bikecam/activity/bikecam
     *                    /redroid/bikecam/state/signal or /redroid/bikecam/state/flasher
     * @param requestJson the JSON body of the request
     */
    public ApiResponse respondPostBikeCamCommand(final String method, final String url,
                                                 final String requestJson) {

        //pass the arguments to the BikeCamApiListener residing in the model
        try {
            return model.getBikeCamApiListener().onBikeCamApiRequest(method, url, requestJson);
        }
        catch (JSONException e) {
            Log.e(TAG, "respondPostBikeCamCommand: " + e.getMessage());
            return new ApiResponse(400, "\"msg\":\"invalid JSON\"");
        }
    }


    /**
     * API 401 - SERVER VERSION
     * For handling requests to register for sensor events by a server connected device. The sensor
     * request is registered with the Redroid sensor framework
     *
     * @param urlArg     the second part of the url indicating the sensor type
     * @param jsonObject contains the request JSON
     * @returnon
     */
    private ApiResponse respondPutRegisterSensorEvents(String urlArg, JSONObject jsonObject) {
        final int requestId;
        final int samplingPeriod;
        boolean onChangeOnly;
        final PeripheralDataModel model = PeripheralDataModel.getInstance();
        final PeripheralServerNetwork network = (PeripheralServerNetwork) model.network;
        final Object syncLock = new Object();

        //get sensor type constant from the lookup table
        final int sensorType = RedroidUtil.getSensorTypeInt(urlArg);
        if (sensorType < 0) {
            String responseJson = "{\"msg\":\"invalid sensor type\"}";
            return new ApiResponse(400, responseJson);
        }

        //get json args from the request json
        try {
            requestId = jsonObject.getInt("request_id");
            samplingPeriod = jsonObject.getInt("sampling_period");
            onChangeOnly = jsonObject.getBoolean("on_change_only"); //TODO: use this?
        }
        catch (JSONException e) {
            //failure: return 400 with error message
            Log.e(TAG, "respondPutRegisterSensorEvents: " + e.getMessage());
            String responseJson = String.format("{\"msg\":\"%s\"}",
                    e.getMessage());
            return new ApiResponse(400, responseJson);
        }

        // MANAGE THE EVENT REGISTRATION REQUEST

        // initialize the event socket
        String failMsg = initializeServerEventSocket();
        boolean success = (failMsg == null);
        if (success) {
            registerEventListener(requestId, network, sensorType, samplingPeriod);
            return new ApiResponse(200, "");
        }
        else {
            return new ApiResponse(400, failMsg);
        }
    }

    //helper method for doing the task of registering a sensor event listener
    private void registerEventListener(final int requestId, PeripheralServerNetwork network,
                                       final int sensorType, final int samplingPeriod) {
        //make a sensor event listener for this request
        SensorEventListener eventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                handleSensorEvent(requestId, event, this);
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }
        };

        //save the listener in the listener list
        network.addSensorEventRegistration(eventListener, sensorType, requestId);

        //get the sensor and register a SensorEventListener
        SensorManager sensorManager = (SensorManager) App.getAppContext().getSystemService(Context.
                SENSOR_SERVICE);
        final Sensor sensor = sensorManager.getDefaultSensor(sensorType);

        //what to do when an event is triggered for this sensor
        sensorManager.registerListener(eventListener, sensor, samplingPeriod);
    }


    /**
     * SERVER VERSION
     * [FOR SERVER] handle a sensor event for a particular request id. Unregisters the
     * calling listener from the SensorManager if the response is not 200 OK.
     *
     * @param requestId the request id associated with the request
     * @param event     the event that was triggered
     * @param listener  the listener that called this handler (in case it needs to be unregistered)
     */
    private void handleSensorEvent(final int requestId, final SensorEvent event,
                                   final SensorEventListener listener) {
        final PeripheralServerNetwork serverNetwork = (PeripheralServerNetwork) model.network;
        try {

            //send the API notification request
            BufferedReader eventSocketReader = serverNetwork.getEventSocketReader();
            BufferedWriter eventSocketWriter = serverNetwork.getEventSocketWriter();
            String remoteHost = model.network.getRemoteIpAddress() + ":" + Network
                    .SERVER_EVENT_PORT;
            requestPutSensorEvent(remoteHost, eventSocketReader, eventSocketWriter, requestId,
                    event, new ApiResponseListener() {
                        @Override
                        public void onApiResponse(int responseCode, JSONObject response) {
                            if (responseCode != 200) {
                                serverNetwork.removeAndUnregisterSensorEventListener(
                                        event.sensor.getType(), requestId);
                            }
                        }
                    });
        }
        catch (Exception e) {
            if (serverNetwork != null && event != null)
                serverNetwork.removeAndUnregisterSensorEventListener(event.sensor.getType(),
                        requestId);
        }
    }


    /**
     * API 401 - WIFI VERSION
     * For handling requests to register for sensor events by a WiFi connected device. It registers
     * the request in network.ConnectedDevices, and if the first time this sensor type has been
     * requested, the sensor request is registered with the Redroid sensor framework
     *
     * @param device     the object representing the requesting device in the network object
     * @param urlArg     the second part of the url indicating the sensor type
     * @param jsonObject represents the request body
     * @returnon
     */
    private ApiResponse respondPutRegisterSensorEvents(final WifiConnectedDevice device,
                                                       String urlArg,
                                                       JSONObject jsonObject) {
        final int requestId;
        final int samplingPeriod;
        boolean onChangeOnly;
        final PeripheralDataModel model = PeripheralDataModel.getInstance();

        //prepare response objects
        ApiResponse response;
        JSONObject responsJsonObject = new JSONObject();

        //get sensor type constant from the lookup table
        final int sensorType = RedroidUtil.getSensorTypeInt(urlArg);
        if (sensorType < 0) {
            String responseJson = "{\"msg\":\"invalid sensor type\"}";
            return new ApiResponse(400, responseJson);
        }

        //get json args from the request json
        try {
            requestId = jsonObject.getInt("request_id");
            samplingPeriod = jsonObject.getInt("sampling_period");
            onChangeOnly = jsonObject.getBoolean("on_change_only"); //TODO: use this?
        }
        catch (JSONException e) {
            //failure: return 400 with error message
            Log.e(TAG, "respondPutRegisterSensorEvents: " + e.getMessage());
            String responseJson = String.format("{\"msg\":\"%s\"}",
                    e.getMessage());
            return new ApiResponse(400, responseJson);
        }

        // MANAGE THE EVENT REGISTRATION REQUEST
        synchronized (PeripheralWifiNetwork.deviceSyncLock) {

            //start the device's event socket

            //make a sensor even listener for this request
            SensorEventListener eventListener = new SensorEventListener() {
                @Override
                public void onSensorChanged(SensorEvent event) {
                    handleSensorEvent(device, requestId, event, this);
                }

                @Override
                public void onAccuracyChanged(Sensor sensor, int accuracy) {
                }
            };

            //register the listener with the device
            device.registerSensor(sensorType, eventListener);

            //get the sensor and register a SensorEventListener
            SensorManager sensorManager = (SensorManager) App.getAppContext().getSystemService
                    (Context.
                    SENSOR_SERVICE);
            final Sensor sensor = sensorManager.getDefaultSensor(sensorType);

            //what to do when an event is triggered for this sensor
            sensorManager.registerListener(eventListener, sensor, samplingPeriod);

        }
        //send response to initial request
        return new ApiResponse(200, "");
    }

    /**
     * WIFI VERSION
     * handle a sensor event for a particular wifi-connected device and a particular request id.
     * Unregisters the calling listener from the SensorManager if the response is not 200 OK.
     *
     * @param device    the device that asked for the event notificaigton
     * @param requestId the request id associated with the request
     * @param event     the event that was triggered
     * @param listener  the listener that called this handler (in case it needs to be unregistered)
     */
    private void handleSensorEvent(final WifiConnectedDevice device, final int requestId,
                                   final SensorEvent event, final SensorEventListener listener) {
        try {
            synchronized (PeripheralWifiNetwork.deviceSyncLock) {
                //send the API notification request
                String remoteHost = device.eventSocket.getRemoteSocketAddress()
                        .toString().replace("/", "");
                requestPutSensorEvent(remoteHost, device.eventSocketReader,
                        device.eventSocketWriter, requestId, event, new ApiResponseListener() {
                            @Override
                            public void onApiResponse(int responseCode, JSONObject response) {
                                if (responseCode != 200) {
                                    device.unregisterSensor(event.sensor.getType());
                                }
                            }
                        });
            }
        }
        catch (Exception e) {
            //unregister this listener if any instance fails
            device.unregisterSensor(event.sensor.getType());
        }

    }


    /**
     * API 402 - Location. Handles both WIFI and Server cases in one implementation, unlike sensors
     * For handling requests to register for sensor events by a server connected device. The sensor
     * request is registered with the Redroid sensor framework
     *
     * @param urlArg     the second part of the url indicating the sensor type
     * @param jsonObject contains the request JSON
     * @returnon
     */
    private ApiResponse respondPutRegisterLocationEvent(final WifiConnectedDevice device,
                                                        String urlArg, JSONObject jsonObject) {
        final int requestId;
        final int samplingPeriod;
        boolean onChangeOnly;
        final PeripheralDataModel model = PeripheralDataModel.getInstance();

        //see if location services are on
        LocationManager locationManager = (LocationManager) App.getAppContext().getSystemService
                (Context.LOCATION_SERVICE);
        boolean gps_enabled = true;
        boolean network_enabled = true;

        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }
        catch (Exception e) {
        }

        if (!gps_enabled && !network_enabled) {
            return new ApiResponse(503, "{\"msg\":\"location services not enabled\"}");
        }

        //initialize the event socket
        if (model.network instanceof PeripheralServerNetwork) {
            String failMsg = initializeServerEventSocket();
            boolean success = (failMsg == null);
            if (!success) {
                return new ApiResponse(400, failMsg);
            }
        }

        //get json args from the request json
        try {
            requestId = jsonObject.getInt("request_id");
            samplingPeriod = jsonObject.getInt("sampling_period");
            onChangeOnly = jsonObject.getBoolean("on_change_only"); //TODO: use this?
        }
        catch (JSONException e) {
            //failure: return 400 with error message
            Log.e(TAG, "402 respondPutRegisterLocationEvent: " + e.getMessage());
            String responseJson = String.format("{\"msg\":\"%s\"}",
                    e.getMessage());
            return new ApiResponse(400, responseJson);
        }

        // MANAGE THE EVENT REGISTRATION REQUEST

        //prepare the LocationRequest object
        final LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY); //todo: make this
        // setable??
        locationRequest.setFastestInterval(samplingPeriod / 1000);
        locationRequest.setInterval(samplingPeriod / 1000);

        //prepare the LocationListener
        final LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                //call the handler to send the event to the master
                handleLocationEvent(device, requestId, location, this);
            }
        };

        //prepare the connectionCallbacks and pass in the listener
        GoogleApiClient.ConnectionCallbacks connectionCallbacks = new GoogleApiClient
                .ConnectionCallbacks() {
            @Override
            public void onConnected(@Nullable Bundle bundle) {
                LocationServices.FusedLocationApi.requestLocationUpdates(locationApiClient[0],
                        locationRequest, locationListener);
            }

            @Override
            public void onConnectionSuspended(int i) {
                Log.d(TAG, "onConnectionSuspended: ");
            }
        };

        //register the location listener with the device
        if (device != null) {
            device.setLocationListener(locationListener);
        }
        else {
            ((PeripheralServerNetwork) model.network).setLocationListener(locationListener);
        }

        //prepare the locationApiClient object
        prepareLocationApiClient();

        //set the callbacks with the client and connect
        locationApiClient[0].registerConnectionCallbacks(connectionCallbacks);
        locationApiClient[0].connect();

        //send response to initial request
        return new ApiResponse(200, "");
    }

    /**
     * handle a location event for a particular wifi-connected device or server and a particular
     * request id. Unregisters the calling listener from the ApiClient if the response is not 200
     * OK.
     *
     * @param device    the device that asked for the event notificaigton
     * @param requestId the request id associated with the request
     * @param location  the event that was triggered
     * @param listener  the listener that called this handler (in case it needs to be unregistered)
     */
    private void handleLocationEvent(final WifiConnectedDevice device, final int requestId,
                                     final Location location, final LocationListener listener) {

        Socket eventSocket;
        BufferedReader eventSocketReader;
        BufferedWriter eventSocketWriter;
        try {
            if (device == null) {
                PeripheralServerNetwork network = (PeripheralServerNetwork) model.network;
                eventSocket = network.getEventSocket();
                eventSocketReader = network.getEventSocketReader();
                eventSocketWriter = network.getEventSocketWriter();
            }
            else {
                eventSocket = device.eventSocket;
                eventSocketReader = device.eventSocketReader;
                eventSocketWriter = device.eventSocketWriter;
            }


            synchronized (PeripheralWifiNetwork.deviceSyncLock) {
                //send the API notification request
                String remoteHost = eventSocket.getRemoteSocketAddress()
                        .toString().replace("/", "");

                //make the API call to send the event with the device's event socket
                requestPutLocationEvent(remoteHost, eventSocketReader, eventSocketWriter, requestId,
                        location, new ApiResponseListener() {

                            //what to do with the response -- unregister the listener on failure
                            @Override
                            public void onApiResponse(int responseCode, JSONObject response) {
                                if (responseCode != 200) {
                                    synchronized (PeripheralWifiNetwork.deviceSyncLock) {
                                        LocationServices.FusedLocationApi
                                                .removeLocationUpdates(locationApiClient[0],
                                                        listener);
                                        if (device != null) {
                                            device.removeLocationListener();
                                        }
                                        else {
                                            PeripheralServerNetwork network =
                                                    (PeripheralServerNetwork) model.network;
                                            if (network != null)
                                                network.removeLocationListener();
                                        }
                                    }
                                }
                            }
                        });
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            //unregister listener
            synchronized (PeripheralWifiNetwork.deviceSyncLock) {
                LocationServices.FusedLocationApi.removeLocationUpdates(locationApiClient[0],
                        listener);
                if (device != null) {
                    device.removeLocationListener();
                }
                else {
                    try {
                        ((PeripheralServerNetwork) model.network).removeLocationListener();
                    }
                    catch (Exception x) {
                    }
                }
            }
        }
    }


    /**
     * API 403 - Responds to a server's request to register for motion detection events. Sets
     * the OnMotionDetected callback with the yasea module (if not set) and returns 200 OK for
     * success.
     *
     * @param jsonObject the request payload containing the requestId
     * @return the ApiResponse object
     */
    private ApiResponse respondPutRegisterMotionDection(JSONObject jsonObject) {
        //unpack the requestId
        final int requestId[] = {0};
        if (jsonObject != null) {
            try {
                requestId[0] = jsonObject.getInt("request_id");
            }
            catch (JSONException e) {
                e.printStackTrace();
                return new ApiResponse(400, "{\"msg\",\"invalid JSON. Expected request_id.\"}");
            }
        }

        //initialize the event socket
        String failMsg = initializeServerEventSocket();
        boolean success = (failMsg == null);
        if (!success) {
            return new ApiResponse(400, failMsg);
        }

        //set the listener in yasea
        model.srsCameraView.setMotionListener(new OnMotionDetectionListener() {
            @Override
            public void onMotionDetected(byte[] jpegImageData) {
                final PeripheralServerNetwork serverNetwork = (PeripheralServerNetwork) model
                        .network;
                try {
                    //send the API notification request
                    BufferedReader eventSocketReader = serverNetwork.getEventSocketReader();
                    BufferedWriter eventSocketWriter = serverNetwork.getEventSocketWriter();
                    String remoteHost = model.network.getRemoteIpAddress() + ":" + Network
                            .SERVER_EVENT_PORT;


                    //notify the server
                    requestPutMotionDetection(remoteHost, eventSocketReader, eventSocketWriter,
                            requestId[0], jpegImageData, new ApiResponseListener() {
                        @Override
                        public void onApiResponse(int responseCode, JSONObject response) {
                            //success? unregister the motion detection callback
                            if (responseCode == 200) {
                                model.srsCameraView.setMotionListener(null);
                            }
                            //failure -- log it and keep trying
                            else {
                                String msg = "unknown";
                                try {
                                    msg = response.getString("msg");
                                }
                                catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Log.e(TAG, "onApiResponse: bad response on Motion Detection: " +
                                        msg);
                            }
                        }
                    });
                }
                catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException("something went wrong in the motion detected " +
                            "callback");
                }
            }
        });
        //respond
        return new ApiResponse(200, null);
    }

    /**
     * API 404,604 - FOR TESTING ONLY- simulates a server's response to a rule setting request
     *
     * @return the ApiResponse object
     */
    private ApiResponse respondPutDeleteRule() {
        return new ApiResponse(200, null);
    }

    /**
     * API 502
     * Sends a location event notification to the master device(s)
     *
     * @param requestId the id assigned by the master when initially requesting the notificaiton(s)
     * @param location  the Location object containing the location data
     * @return true for success, false for failure
     */
    private boolean requestPutLocationEvent(String remoteHost,
                                            BufferedReader socketReader,
                                            BufferedWriter socketWriter,
                                            int requestId,
                                            Location location,
                                            API.ApiResponseListener listener) {
        //craft http PUT request
        String url = String.format("/redroid/peripheral/location_event/%d", requestId);

        String body = makeLocationJson(location);
        if (body == null)
            return false;

        //send the HTTP PUT request
        getHttpResponseAsync(remoteHost, socketReader, socketWriter, PUT, url, body, listener);
        return true;
    }


    private void prepareLocationApiClient() {
        synchronized (locationApiClient) {
            if (locationApiClient[0] != null)
                return;

            //add LocationServices API
            GoogleApiClient.Builder builder = new GoogleApiClient.Builder(App.getAppContext());
            builder.addApi(LocationServices.API);
//            builder.addOnConnectionFailedListener(new GoogleApiClient
// .OnConnectionFailedListener() {
//                @Override
//                public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//                    synchronized (locationApiClient) {
//                        locationApiClient.notify();
//                    }
//                }
//            });
            locationApiClient[0] = builder.build();
        }
    }

    private String makeLocationJson(Location location) {
        String[] keys = {"latitude", "longitude", "altitude", "bearing", "speed", "accuracy",
                "time"};
        Object[] values = {
                location.getLatitude(),
                location.getLongitude(),
                location.getAltitude(),
                (double) location.getBearing(),
                (double) location.getSpeed(),
                (double) location.getAccuracy(),
                location.getTime()
        };
        String body = "";
        try {
            body = RedroidUtil.makeSimpleJson(keys, values);
            return body;
        }
        catch (JSONException e) {
            Log.e(TAG, "PeripheralApi.makeLocationJson(): " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }


    /**


     * @return true for success, false for failure
     */

    /**
     * API 503 - NON JPEG VERSION - sends a motion detection event to the server
     *
     * @param requestId the request ID associated with the motion detection request from the server
     * @param listener  the API response listener
     * @return true for success, false for failure
     */
    private boolean requestPutMotionDetection(String remoteHost,
                                              BufferedReader socketReader,
                                              BufferedWriter socketWriter,
                                              int requestId,
                                              ApiResponseListener listener) {
        String url = String.format("/redroid/peripheral/motion_detection/%d", requestId);
        getHttpResponseAsync(remoteHost, socketReader, socketWriter, PUT, url, "", listener);
        return true;
    }

    /**
     * API 503 - JPEG VERSION - sends a motion detection event to the server, including a jpeg image
     *
     * @param requestId     the request ID associated with the motion detection request from the
     *                      server
     * @param jpegImageData a byte array containing the image to be converted to JPEG and sent
     * @param listener      the API response listener
     * @return true for success, false for failure
     */
    public boolean requestPutMotionDetection(final String remoteHost,
                                              final BufferedReader socketReader,
                                              final BufferedWriter socketWriter,
                                              final int requestId,
                                              final byte[] jpegImageData,
                                              final ApiResponseListener listener) {

        //make the url + filename for the JPEG server upload
        String date = new SimpleDateFormat("yyyy_MM_dd_H_mm_ss", Locale.US)
                .format(Calendar.getInstance().getTime());
        final String imageUrl = String.format("%s/motion_detect_%s.jpg", "http://redroid.net/images", date);
        //send the JPEG to the web server
        sendJpeg(jpegImageData, imageUrl, new OnJpegReadyListener() {
            @Override
            public void onJpegReady(int responseCode) {
                //make the url
                String url = "/redroid/peripheral/motion_detection";

                //craft the json
                String json = "";
                if (responseCode == 200) {
                    json = String.format("{\"image\":\"%s\"}", imageUrl);
                }

                getHttpResponseAsync(remoteHost, socketReader, socketWriter, PUT, url, json,
                        listener);
            }
        });
        return true;
    }


    /**
     * API 601
     * Deregisters a request for event notifications from a server
     *
     * @param sensorType the sensor type associated with the original request
     * @param requestId  the request id associated with the original request
     * @return an ApiResponse object
     */
    private ApiResponse respondDeleteSensorEventRegistration(String sensorType, int requestId) {

        int sensorTypeInt = RedroidUtil.getSensorTypeInt(sensorType);
        //check sensor type4
        if (sensorTypeInt < 0) {
            String responseJson = "{\"msg\":\"invalid sensor type\"}";
            return new ApiResponse(400, responseJson);
        }

        //unregister the sensor event listener
        ((PeripheralServerNetwork) model.network).removeAndUnregisterSensorEventListener
                (sensorTypeInt,
                requestId);

        Log.d("SensorEvents", "Peripheral deregistered events for sensor "
                + sensorType);
        //send response to initial request
        return new ApiResponse(200, "");
    }

    /**
     * API 601
     * Deregisters a request for event notifications from a WiFI connected device
     *
     * @param device     the device deregistering the event
     * @param sensorType the sensor type associated with the original request
     * @param requestId  the request id associated with the original request
     * @return an ApiResponse object
     */
    private ApiResponse respondDeleteSensorEventRegistration(final WifiConnectedDevice device,
                                                             String sensorType, int requestId) {

        int sensorTypeInt = RedroidUtil.getSensorTypeInt(sensorType);
        //check sensor type
        if (sensorTypeInt < 0) {
            String responseJson = "{\"msg\":\"invalid sensor type\"}";
            return new ApiResponse(400, responseJson);
        }

        //unregister the sensor event listener
        device.unregisterSensor(sensorTypeInt);

        Log.d("SensorEvents", "Peripheral unregistered events for sensor "
                + sensorType);
        //send response to initial request
        return new ApiResponse(200, "");
    }

    /**
     * API 603 - Responds to a server's request to unregister for motion detection events. Sets
     * the OnMotionDetectedListener to null and returns 200 OK for success.
     *
     * @return
     */
    private ApiResponse respondDeleteRegisterDection() {
        //clear the listener in yasea
        model.srsCameraView.setMotionListener(null);
        //respond
        return new ApiResponse(200, null);
    }

    /**
     * API 501
     * Sends a sensor event notification to the master device(s)
     *
     * @param requestId the id assigned by the master when initially requesting the notificaiton(s)
     * @param event     the SensorEvent object containing the event sensors
     * @return true for success, false for failure
     */
    private boolean requestPutSensorEvent(String remoteHost,
                                          BufferedReader socketReader,
                                          BufferedWriter socketWriter,
                                          int requestId,
                                          SensorEvent event,
                                          API.ApiResponseListener listener) {
        //craft http PUT request
        JSONObject requestJsonObject = new JSONObject();
        String url = String.format("/redroid/peripheral/sensor_event/%d", requestId);

        try {
            requestJsonObject.put("accuracy", event.accuracy);
            requestJsonObject.put("sensor_type", event.sensor.getType());
            requestJsonObject.put("time_stamp", event.timestamp);
            JSONArray jsonArray = API.getJSONArray(event.values);
            requestJsonObject.put("values", jsonArray);
        }
        catch (JSONException e) {
            Log.e(TAG, "onSensorChanged: " + e.getMessage());
            return false;
        }
        String body = requestJsonObject.toString();

        //send the HTTP PUT request
        getHttpResponseAsync(remoteHost, socketReader, socketWriter, PUT, url, body, listener);
        return true;
    }


    /**
     * A generic helper method for making an API call from a Redroid peripheral to notify of an
     * event.
     *
     * @param url                 the API url
     * @param apiResponseListener the listener containing code to execute on response receipt
     */
    public void getHttpResponseAsync(final String remoteHost,
                                     final BufferedReader socketReader,
                                     final BufferedWriter socketWriter,
                                     final Method method,
                                     final String url,
                                     final String body,
                                     final API.ApiResponseListener apiResponseListener) {


        AsyncTask<Object, Void, Void> task = new AsyncTask<Object, Void, Void>() {
            @Nullable
            @Override
            protected Void doInBackground(Object[] params) {

                int responseCode;
                String jsonResponse = null;
                JSONObject jsonObject;
                Network network = PeripheralDataModel.getInstance().network;

                //transact with server
                Pair<Integer, String> response = null;
                try {
                    response = Network.getHttpResponse(remoteHost, socketReader,
                            socketWriter, method, url, body);
                }
                catch (Network.ConnectionException e) {
                    network.teardown();
                }

                if (response != null) {
                    responseCode = response.first;
                    jsonResponse = response.second;
                }
                else {
                    responseCode = -1;
                    jsonResponse = "{\"msg\":\"Unable to connect to host. Check your IP address" +
                            ".\"}";
                }

                //create the return object from the response string
                jsonObject = null;
                try {
                    if (jsonResponse != null && !jsonResponse.equals("")) {
                        jsonObject = new JSONObject(jsonResponse);
                    }
                }
                catch (JSONException e) {
                    Log.e(TAG, "JSON exception: " + e);
                    try {
                        jsonObject = new JSONObject("{\"msg\": \"unknown failure\"}");
                    }
                    catch (JSONException j) {
                    }
                }

                //call the listener and pass the results
                if (apiResponseListener != null) {
                    apiResponseListener.onApiResponse(responseCode, jsonObject);
                }
                return null;
            }
        };
        task.execute();
    }


    /**
     * A generic helper method for making an API call from a Redroid peripheral to notify of a
     * motion detection event and send a JPG
     *
     * @param url                 the API url
     * @param apiResponseListener the listener containing code to execute on response receipt
     */
    public void getHttpResponseAsyncJpeg(final String remoteHost,
                                         final BufferedReader socketReader,
                                         final OutputStream outputStream,
                                         final String url,
                                         final byte[] jpegImageData,
                                         final API.ApiResponseListener apiResponseListener) {


        AsyncTask<Object, Void, Void> task = new AsyncTask<Object, Void, Void>() {
            @Nullable
            @Override
            protected Void doInBackground(Object[] params) {

                int responseCode;
                String jsonResponse = null;
                JSONObject jsonObject;
                Network network = PeripheralDataModel.getInstance().network;

                //transact with server
                Pair<Integer, String> response = null;
                try {
                    response = Network.getHttpResponseJpeg(remoteHost, socketReader,
                            outputStream, url, jpegImageData);
                }
                catch (Network.ConnectionException e) {
                    network.teardown();
                }

                if (response != null) {
                    responseCode = response.first;
                    jsonResponse = response.second;
                }
                else {
                    responseCode = -1;
                    jsonResponse = "{\"msg\":\"Unable to connect to host. Check your IP address" +
                            ".\"}";
                }

                //create the return object from the response string
                jsonObject = null;
                try {
                    if (jsonResponse != null && !jsonResponse.equals("")) {
                        jsonObject = new JSONObject(jsonResponse);
                    }
                }
                catch (JSONException e) {
                    Log.e(TAG, "JSON exception: " + e);
                    try {
                        jsonObject = new JSONObject("{\"msg\": \"unknown failure\"}");
                    }
                    catch (JSONException j) {
                    }
                }

                //call the listener and pass the results
                if (apiResponseListener != null) {
                    apiResponseListener.onApiResponse(responseCode, jsonObject);
                }
                return null;
            }
        };
        task.execute();
    }

    /**
     * A listener for a peripheral Activity, such as HomeActivity and BikeCamActivity, for
     * handling incoming API
     * requests pertaining to a bike cam
     */
    public interface BikeCamApiListener {

        /**
         * For handling incoming API commands pertinent to the particular activity.
         *
         * @param method the HTTP method associated with the incoming API command
         * @param url    the URL string associated with the incoming API command
         * @param json   the JSON string payload of the incoming API command
         * @return An ApiResponse object containing the response code and JSON string
         * @throws JSONException if there are any errors caused by packing or unpacking a JSONObject
         */
        ApiResponse onBikeCamApiRequest(String method, String url, String json) throws
                JSONException;
    }


    /**
     * A listener for requests to invoke an RTMP stream, for handling incoming API
     * requests pertaining to a bike cam
     */
    public interface RtmpStreamListener {

        /**
         * For handling incoming API commands to start or stop an RTMP stream
         *
         * @return String containing RTMP stream url
         */
        String onRtmpStreamStartStopRequest(boolean audio, boolean video);

        /**
         * For handling incoming API commands to fetch a URL for an onging stream.
         *
         * @return String containing RTMP stream url, or null if stream is not ongoing
         */
        String onRtmpStreamUrlRequest();

    }

    /**
     * Sends a JPEG (of a captured motion detection BMP) to the Redroid web server for hosting
     * @param jpegImageData the jpeg data
     * @param imageUrl the url for the POST request, including file name
     * @param listener indicates completion
     */
    void sendJpeg(final byte[] jpegImageData, final String imageUrl, final OnJpegReadyListener
            listener) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //send the JPEG to Scott's server
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPut httpPut = new HttpPut(imageUrl);
                    ByteArrayEntity byteArrayEntity = new ByteArrayEntity(jpegImageData);
                    byteArrayEntity.setContentType("image/jpeg");
                    httpPut.setEntity(byteArrayEntity);
                    HttpResponse response = httpClient.execute(httpPut);
                    listener.onJpegReady(response.getStatusLine().getStatusCode());
                }
                catch (IOException e) {
                    e.printStackTrace();
                    listener.onJpegReady(400);
                }
            }
        }).start();
    }


    /**
     * If no event socket is established yet, makes one and sends the connect API call to the server.
     * If already established, returns null. If successful, reutrns null. If fails, returns a string
     * containing a failure message
     * @return null on success, failure message on failure.
     */
    private synchronized String initializeServerEventSocket() {
        final String[] msg = {null};
        final Boolean[] success = {null};
        final Object syncLock = new Object();
        final PeripheralServerNetwork network = (PeripheralServerNetwork)model.network;
        if (network.getEventSocket() == null) {
            //set the event socket
            network.setEventSocket();

            //call API call 005. The return value of the current method depends on this result.
            requestPostRedroidPeripheralConnectEvent(network.getRemoteIpAddress(),
                    network.getEventSocketReader(), network.getEventSocketWriter(),
                    new ApiResponseListener() {
                        @Override
                        public void onApiResponse(int responseCode, JSONObject response) {
                            //check the result
                            if (responseCode == 200) {
                                success[0] = true;
                            }
                            else {
                                //failure - retrieve the error msg and reset socket to null
                                try {
                                    msg[0] = String.format("{\"msg\":\"%s\"}", response.getString("msg"));
                                }
                                catch (JSONException e) {
                                    e.printStackTrace();
                                    msg[0] = "{\"msg\":\"JSON error when retrieving failure message\"}";
                                }
                                success[0] = false;
                                network.nullifyEventSocket();
                            }
                            //signal ready
                            synchronized (syncLock) {
                                syncLock.notify();
                            }
                        }
                    }//new apiResponseListener
            ); //requestPost...
        } //if
        //socket already initialized -- just register the event and move on
        else {
            success[0] = true;
        }

        //wait until ready
        while (success[0] == null) {
            synchronized (syncLock) {
                try {
                    syncLock.wait();
                }
                catch (InterruptedException e) {
                }
            }
        }
        return msg[0];
    }

    interface OnJpegReadyListener {
        void onJpegReady(int responseCode);
    }
}
