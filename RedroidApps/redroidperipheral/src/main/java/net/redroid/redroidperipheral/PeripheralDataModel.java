package net.redroid.redroidperipheral;


import android.graphics.Bitmap;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.ossrs.yasea.SrsCameraView;
import net.redroid.redroidcorelibrary.App;
import net.redroid.redroidcorelibrary.DataModel;
import net.redroid.redroidcorelibrary.Network;
import net.redroid.redroidperipheral.PeripheralAPI.BikeCamApiListener;
import net.redroid.redroidperipheral.PeripheralAPI.RtmpStreamListener;
import net.redroid.redroidcorelibrary.RedroidUtil;
import net.redroid.redroidperipheral.network.PeripheralNetwork;
import net.redroid.redroidperipheral.network.PeripheralP2PNetwork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.concurrent.TimeoutException;
import static android.content.ContentValues.TAG;

/**
 * A singleton comprising the data model for an instance of Redrodi Peripheral
 */
public class PeripheralDataModel extends DataModel{

    static final boolean DEBUG = true;
    private static PeripheralDataModel instance;  // application context
    protected String deviceID = null;
    private String securityToken = null;
    private BikeCamApiListener bikeCamApiListener;
    private RtmpStreamListener rtmpStreamListener;
    public PeripheralAPI api;
    Bitmap qrBitmap, qrBitmapNoWifi;
    public SrsCameraView srsCameraView;
    private String locationLogEmail;

    //    private String rtmpServerUrl = "rtmp://redroid-video.westus.cloudapp.azure.com/live/";
    private String rtmpServerUrl = "rtmp://redroid.net/live/";
    public String rtmpStreamUrl = null;
    public HashSet<String> emailAddressesAutoCompleteList;


    public static PeripheralDataModel getInstance(){
        if (instance == null) {
            instance = new PeripheralDataModel();
        }
        return instance;
    }

    /**
     * Private constructor
     */
    private PeripheralDataModel(){
        setDeviceID();
        loadConnectionServerUrlFromFile();
        if (!loadSecurityTokenFromFile()) {
            setSecurityToken();  //normally to be run only the first time
            saveSecurityTokenToFile();
        }
        loadVideoServerUrlFromFile();
        loadEmailAutoCompleteListFromFile();
    }


    void setApi() {
        this.api = new PeripheralAPI(this);
    }


    private void setDeviceID(){

        this.deviceID = RedroidUtil.hexStringToBase64(Settings.Secure.getString(App.getAppContext().getContentResolver(), Settings.Secure.ANDROID_ID).substring(0, 12));

    }

    public String getDeviceID(){
        if(deviceID != null)
            return deviceID;
        else
            return null;
    }

    String getSecurityToken(){
        if(securityToken != null)
            return securityToken;
        else
            return null;
    }

    private void setSecurityToken(){
        this.securityToken = RedroidUtil.generateSecurityToken();
    }

    void setIPAddress(String ipAddress) {
        network.localIpAddress = ipAddress;
    }

    String getIPAddress() {
        return network.localIpAddress;
    }


    /**
     * To be set by the bike cam whenever invoked. Allows the API object to find the bike cam.
     * @param currentActivity
     */
    void setBikeCamActivity(BikeCamApiListener currentActivity) {
        this.bikeCamApiListener = currentActivity;
    }


    /**
     * Sets the activity currenlty listening for an RTMP stream.
     * @param currentActivity The current activity
     */
    void setRtmpStreamListener(RtmpStreamListener currentActivity) {
        this.rtmpStreamListener = currentActivity;
    }


    public BikeCamApiListener getBikeCamApiListener() {
        return bikeCamApiListener;
    }

    public RtmpStreamListener getRtmpStreamListener() {
        return rtmpStreamListener;
    }

    /**
     * A Method for saving the security token to file
     */
    private void saveSecurityTokenToFile() {
        BufferedWriter writer;
        try {
            File securityTokenFile = new File(App.getAppContext().getFilesDir(), "auth.dat");
            writer = new BufferedWriter(new FileWriter(securityTokenFile, false));
            writer.write(this.securityToken);
            writer.newLine();
            writer.flush();
            writer.close();
        } catch (IOException e) {
            String msg = App.getAppContext().getResources()
                    .getString(R.string.save_security_token_ioexception_msg, e.getMessage());
            Log.e(TAG, msg);
            Toast.makeText(App.getAppContext(), msg, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * A method for refreshing the security token from file
     */
    private boolean loadSecurityTokenFromFile() {
        try {
            File securityTokenFile = new File(App.getAppContext().getFilesDir(), "auth.dat");
            if (securityTokenFile.exists()) {
                BufferedReader reader = new BufferedReader(new FileReader(securityTokenFile));
                this.securityToken = reader.readLine();
                reader.close();
            }
            else {
                return false;
            }
            return true;
        }
        catch (IOException e) {
            String msg = App.getAppContext().getResources()
                    .getString(R.string.load_security_token_ioexception_msg, e.getMessage());
            Toast.makeText(App.getAppContext(), msg, Toast.LENGTH_LONG).show();
            return false;
        }
    }

    /**
     * A helper method for intializing a network object
     * @param newNetwork the network object
     * @throws TimeoutException if takes too long
     * @throws Network.ConnectionException if fails otherwise
     */
    void initializeNetwork(PeripheralNetwork newNetwork) throws TimeoutException,
            Network.ConnectionException {

        // current network not compatible?

        //clean up the old one
        teardownNetwork();

        //update with the new one
        network = newNetwork;

        // initialize
        try {
            network.init();
        }
        catch (TimeoutException | Network.ConnectionException e) {
            teardownNetwork(); //reverse the work done so far
            throw e; //pass the exception up to the caller
        }
        // otherwise, you're good already -- do nothing
    }

    /**
     * The aysn P2P version of intializeNetwork
     * @param newNetwork the network object
     * @param initSuccessListener the async listener
     * @throws TimeoutException if takes too long
     * @throws Network.ConnectionException if fails otherwise
     */
    void initializeP2PNetwork(PeripheralP2PNetwork newNetwork,
                              PeripheralP2PNetwork.InitSuccessListener initSuccessListener)
            throws TimeoutException, Network.ConnectionException {

        //clean up the old one
        teardownNetwork();

        //update with the new one
        network = newNetwork;

        // initialize
        try {
            newNetwork.init(initSuccessListener);
        }
        catch (TimeoutException | Network.ConnectionException e) {
            teardownNetwork(); //reverse the work done so far
            throw e; //pass the exception up to the caller
        }
        // otherwise, you're good already -- do nothing
    }

    /**
     * Helper method for tearing down network object
     */
    public void teardownNetwork() {
        if (network != null) {
            network.teardown();
            network = null;
        }
    }
    public String getRtmpServerUrl() {
        return rtmpServerUrl;
    }

    public void setRtmpServerUrl(String rtmpServerUrl) {
        this.rtmpServerUrl = rtmpServerUrl;
        saveVideoServerUrlToFile();
    }

    /**
     * Method for saving the server url to file
     */
    protected void saveVideoServerUrlToFile() {
        BufferedWriter writer;
        try {
            File serverUrlFile = new File(App.getAppContext().getFilesDir(), "rtmp_server_url.dat");
            writer = new BufferedWriter(new FileWriter(serverUrlFile, false));
            writer.write(rtmpServerUrl);
            writer.newLine();
            writer.flush();
            writer.close();
        } catch (IOException e) {
            String msg = "An error occurred when writing the video server URL to file: " +
                    e.getMessage();
            Log.e(TAG, msg);
            Toast.makeText(App.getAppContext(), msg, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Method for refreshing the config store from file
     */
    protected void loadVideoServerUrlFromFile() {
        try {
            File rtmpServerUrlFile = new File(App.getAppContext().getFilesDir(), "rtmp_server_url.dat");
            if (rtmpServerUrlFile.exists()) {
                BufferedReader reader = new BufferedReader(new FileReader(rtmpServerUrlFile));
                this.rtmpServerUrl = reader.readLine();
                reader.close();
            }
        }
        catch (IOException e) {
            String msg = "An error occurred when loading the video server URL from from file: " +
                    e.getMessage();
            Toast.makeText(App.getAppContext(), msg, Toast.LENGTH_LONG).show();
        }
    }

    public void setLocationLogEmail(String email) {
        locationLogEmail = email;
        emailAddressesAutoCompleteList.add(email);
        saveEmailAutoCompleteListToFile();
    }

    public String getLocationLogEmail() {
        return locationLogEmail;
    }

    /**
     * Method for saving the auto complete lists to file
     */
    private void saveEmailAutoCompleteListToFile() {
        Gson gson = new Gson();
        if (emailAddressesAutoCompleteList == null)
            return;
        String json = gson.toJson(emailAddressesAutoCompleteList);

        try {
            File listsFile = new File(App.getAppContext().getFilesDir(), "email_list.dat");
            BufferedWriter writer = new BufferedWriter(new FileWriter(listsFile, false));
            writer.write(json);
            writer.newLine();
            writer.flush();
            writer.close();

        } catch (IOException e) {
            String msg = "An error occurred when writing the peripheral email list to file: " +
                    e.getMessage();
            Log.e(TAG, msg);
            Toast.makeText(App.getAppContext(), msg, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Method for refreshing auto complete string lists from file
     */
    private void loadEmailAutoCompleteListFromFile() {

        try {
            //open the file and set a reader
            File listsFile = new File(App.getAppContext().getFilesDir(), "email_list.dat");
            if (!listsFile.exists()) {
                this.emailAddressesAutoCompleteList= new HashSet<>();
            }
            else {
                BufferedReader reader = new BufferedReader(new FileReader(listsFile));
                Type EmailAdddresListType = new TypeToken<HashSet<String>>(){}.getType();
                //read in the contents to a json object
                String json = reader.readLine();
                this.emailAddressesAutoCompleteList = new Gson().fromJson(json, EmailAdddresListType);
                reader.close();
            }
        }
        catch (IOException e) {
            String msg = "An error occurred when loading the email auto complete list from from file: " +
                    e.getMessage();
            Toast.makeText(App.getAppContext(), msg, Toast.LENGTH_LONG).show();
            if (emailAddressesAutoCompleteList == null) {
                this.emailAddressesAutoCompleteList = new HashSet<>();
            }
        }
    }
}

