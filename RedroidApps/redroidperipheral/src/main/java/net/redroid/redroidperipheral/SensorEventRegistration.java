package net.redroid.redroidperipheral;

import android.hardware.SensorEventListener;

/**
 * Represents info about a single instance of a single sensor type being registered with the
 * sensor manager
 */
public class SensorEventRegistration {
    public SensorEventListener listener;
    public int requestId;
    public int sensorType;

    public SensorEventRegistration(SensorEventListener listener, int sensorType, int requestId) {
        this.listener = listener;
        this.requestId = requestId;
        this.sensorType = sensorType;
    }
}