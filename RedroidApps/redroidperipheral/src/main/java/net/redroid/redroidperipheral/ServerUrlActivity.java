package net.redroid.redroidperipheral;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import net.redroid.redroidcorelibrary.RedroidUtil;
import net.redroid.redroidperipheral.network.PeripheralServerNetwork;

import java.util.concurrent.TimeoutException;

/**
 * Activity for setting the server url
 */
public class ServerUrlActivity extends AppCompatActivity implements View.OnClickListener {

    final String RESULT = "result";
    AppCompatButton testButton, cancelButton, okButton;
    ImageView successIcon;
    TextView successMsg;
    EditText urlField;
    PeripheralDataModel model = PeripheralDataModel.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_url);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set the action bar title
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.server_url);
        }

        //fetch handles to the UI views
        testButton = (AppCompatButton)findViewById(R.id.test_button);
        cancelButton = (AppCompatButton)findViewById(R.id.cancel_button);
        okButton = (AppCompatButton)findViewById(R.id.OK_button);
        successIcon = (ImageView)findViewById(R.id.success_icon);
        successMsg = (TextView)findViewById(R.id.success_msg);
        urlField = (EditText)findViewById(R.id.url_field);

        //set url field text
        String url = model.getConnectionServerUrl();
        urlField.setText(url);
        if (!url.equals("")) {
            testButton.setEnabled(true);
        }
        urlField.addTextChangedListener(textWatcher);

        //set listeners
        testButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        okButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        String newUrl = urlField.getText().toString();
        if (v == testButton) {
            boolean success;

            //disabled the test button until someone changes the url again
            testButton.setEnabled(false);


            //close the on-screen keyboard
            RedroidUtil.hideKeyboard(this);

            //test connection with given server url
            //make a temporary server network
            PeripheralServerNetwork network = new PeripheralServerNetwork();
            network.setServerIpAddress(newUrl);

            //connect and perform handshake
            String msg = null; //msg contains failure result, or null on success
            try {
                msg = network.connectToServer();
                success = (msg == null);
            } catch (TimeoutException e) {
                e.printStackTrace();
                msg = getResources().getString(R.string.connection_attemp_timed_out);
                success = false;
            }

            //clean up the temporary network
            network.teardown();
            network = null;

            //success: enable the OK button
            if (success) {
                okButton.setEnabled(true);
            }

            //failure: change the success icon and message to failure
            else {
                successIcon.setImageResource(R.drawable.x_red_24dp);
                successMsg.setText(msg);
            }

            //activate the icon and message in either case
            successIcon.setVisibility(View.VISIBLE);
            successMsg.setVisibility(View.VISIBLE);

            //put focus on the field
            urlField.setSelectAllOnFocus(true);
            urlField.requestFocus();
            urlField.setSelectAllOnFocus(false);
        }
        else if (v == cancelButton){
            finish();
        }
        else if (v == okButton) {
            String oldUrl = model.getConnectionServerUrl();
            model.setConnectionServerUrl(newUrl);

            //prepare a snackbar message for after returning if the URL was actually changed
            if (!oldUrl.equals(newUrl)) {
                String result = getResources().getString(R.string.url_updated);
                Intent returnIntent = new Intent();
                returnIntent.putExtra(RESULT, result);
                setResult(RESULT_OK);
            }
            finish();
        }
    }

    /**
     * TextWatcher object to watch the three text fields in the activity and activate the
     * Finish button when the fields are properly populated
     */
    private final TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable s) {

            //disable the OK button
            okButton.setEnabled(false);

            //hide the success msg and icon
            successIcon.setVisibility(View.INVISIBLE);
            successMsg.setVisibility(View.INVISIBLE);

            //check to make sure the url field is non-empty
            String url = urlField.getText().toString();
            //TODO: check that no illegal characters were added to IDs

            if (!url.equals("")) {
                //enable the test button
                testButton.setEnabled(true);
            }
            else {
                testButton.setEnabled(false);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}
    };
}
