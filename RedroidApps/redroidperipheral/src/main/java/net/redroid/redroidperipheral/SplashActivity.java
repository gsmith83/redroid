package net.redroid.redroidperipheral;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

/**
 * Startup activity for Redroid Peripheral, showing the logo
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setContentView(R.layout.activity_splash);

        //make a countdown timer for splash timeout
        long TIME = 1000;
        final CountDownTimer timer = new CountDownTimer(TIME, TIME) {
            public void onFinish() {
                invokeHomeActivity();
            }

            @Override
            public void onTick(long millisUntilFinished) {
                //nothing
            }
        };

        //start the timer
        timer.start();

        //set a listener to close the splash screen when touched
        LinearLayout view = (LinearLayout)findViewById(R.id.activity_splash);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                invokeHomeActivity();
            }
        });
        Button debugMenuButton = (Button)findViewById(R.id.debug_menu);
        debugMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timer.cancel();
                invokeDebugMenuActivity();
            }
        });

    }

    private void invokeDebugMenuActivity(){
        startActivity(new Intent(getBaseContext(), DebugMenuActivity.class));
        finish();
    }

    private void invokeHomeActivity()  {
        Intent textReadingIntent = new Intent(getBaseContext(), HomeActivity.class);
        startActivity(textReadingIntent);
        finish(); //so you can't go back to the splash screen
    }
}
