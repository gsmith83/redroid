package net.redroid.redroidperipheral;

import android.content.Context;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.google.android.gms.location.LocationListener;

import net.redroid.redroidcorelibrary.App;
import net.redroid.redroidcorelibrary.DataModel;
import net.redroid.redroidperipheral.network.PeripheralWifiNetwork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.HashMap;

import static android.content.ContentValues.TAG;
import static net.redroid.redroidcorelibrary.Network.SERVER_EVENT_PORT;


/**
 * This class represents a single master device connected to a peripheral. It is used to store
 * metadata about connections within the peripheral's sensors model.
 */
public class WifiConnectedDevice {
    public Socket eventSocket;
    BufferedReader eventSocketReader;
    public BufferedWriter eventSocketWriter;
    public String sessionId;
    private String ipAddress;
    boolean streamsRtmp = false; //todo: this is never changed to true. Something's not right...

    //sensorType, listener
    private HashMap<Integer, SensorEventListener> registeredSensors = new HashMap<>();
    //todo: this ^ does not reference requestId at all, unlike in the serve version. Fix it.

    //requestId, listener
    private LocationListener locationListener;


    public WifiConnectedDevice(String ipAddress) {
        DataModel model = PeripheralDataModel.getInstance();
        sessionId = "" + ((PeripheralWifiNetwork) model.network).generateSessionId();
        this.ipAddress = ipAddress;
    }

    public String getSessionId() {
        return sessionId;
    }

    void registerSensor(int sensorType, SensorEventListener eventListener) {
        synchronized (PeripheralWifiNetwork.deviceSyncLock) {
            registeredSensors.put(sensorType, eventListener);
            Log.d("SensorEvents", "Peripheral registered sensor for host " + ipAddress);
        }
        startEventSocket();
    }

    void unregisterSensor(int sensorType) {
        synchronized (PeripheralWifiNetwork.deviceSyncLock) {
            //unregister the type event listener
            ((SensorManager)App.getAppContext().getSystemService(Context.SENSOR_SERVICE))
                    .unregisterListener(registeredSensors.get(sensorType));

            //remove the entry from the list
            registeredSensors.remove(sensorType);
        }
    }

    public boolean isRegistered(int sensorType) {
        return registeredSensors.containsKey(sensorType);
    }


    void setLocationListener(LocationListener listener) {
        locationListener = listener;
        startEventSocket();
    }

    void removeLocationListener() {
        locationListener = null;
    }


    public Socket getEventSocket() {
        return eventSocket;
    }
    
    private void startEventSocket() {
        if (eventSocket == null) {
            try {
                eventSocket = new Socket(ipAddress, SERVER_EVENT_PORT);
                eventSocketReader = new BufferedReader(
                        new InputStreamReader(eventSocket.getInputStream()));
                eventSocketWriter = new BufferedWriter(
                        new OutputStreamWriter(eventSocket.getOutputStream()));
            } catch (IOException e) {
                Log.e(TAG, "getMasterApiSocket: " + e.getMessage());
            }
        }
        
    }

    public boolean streamsRtmp() {
        return streamsRtmp;
    }
}