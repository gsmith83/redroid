package net.redroid.redroidperipheral.audio;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import net.redroid.redroidperipheral.video.YaseaFragment;

import net.redroid.redroidperipheral.R;

public class AudioStreamingActivity extends AppCompatActivity {

    YaseaFragment yaseaFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_streaming);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        // response screen rotation event
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // load shared configuration for yasea
        SharedPreferences sp = this.getSharedPreferences("Yasea", this.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.clear();
        edit.putString("rtmpUrl", "rtmp://24.2.102.119/live/testaudio");
        edit.putString("audioOnly", "true");
        edit.commit();



        //place video stream fragment
        yaseaFragment = new YaseaFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.yasea_video_fragment_frame, yaseaFragment)
                .commit();

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                yaseaFragment.switchCamera();
//            }
//        });
    }

}
