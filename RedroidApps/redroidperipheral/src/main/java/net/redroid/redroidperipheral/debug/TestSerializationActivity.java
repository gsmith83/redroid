package net.redroid.redroidperipheral.debug;

import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import net.redroid.redroidcorelibrary.App;
import net.redroid.redroidcorelibrary.sensor.CameraMetaData;
import net.redroid.redroidcorelibrary.sensor.SensorList;
import net.redroid.redroidperipheral.R;


public class TestSerializationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_serialization);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        CameraMetaData cmd = new CameraMetaData("FEATURE_CAMERA");
        CameraMetaData cmd2 = new CameraMetaData("FEATURE_CAMERA_FRONT");
        String s = cmd.toJSONString();
        String s2 = cmd2.toJSONString();

//        SensorList list = new SensorList();
//        String s = list.toJSONString();
//        SensorList list2 = new SensorList(s);
        CameraMetaData cmdo = CameraMetaData.fromJson(s);

        System.out.println();

    }

}
