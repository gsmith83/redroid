package net.redroid.redroidperipheral.network;


import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import net.redroid.redroidcorelibrary.App;
import net.redroid.redroidcorelibrary.Network;

import java.util.concurrent.TimeoutException;

public class PeripheralNetwork extends Network {
    @Override
    public void init() throws TimeoutException, ConnectionException {

    }

    @Override
    public void teardown() {

    }

    void sendNumConnectedBroadcast(int num) {
        Intent intent = new Intent("numConnected");
        intent.putExtra("num", num);
        LocalBroadcastManager.getInstance(App.getAppContext()).sendBroadcast(intent);
    }
}
