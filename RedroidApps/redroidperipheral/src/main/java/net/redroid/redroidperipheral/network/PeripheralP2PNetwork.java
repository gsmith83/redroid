package net.redroid.redroidperipheral.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

import net.redroid.redroidcorelibrary.App;
import net.redroid.redroidcorelibrary.RedroidUtil;
import net.redroid.redroidperipheral.PeripheralDataModel;

//import org.apache.http.conn.util.InetAddressUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.TimeoutException;

import static android.content.Context.WIFI_SERVICE;

public class PeripheralP2PNetwork extends PeripheralWifiNetwork {
    final String TAG = this.getClass().getName();
    final Object wifiCycleLock = new Object();
    final Object discoverPeersLock = new Object();
    final Object createGroupLock = new Object();
    private Boolean success = false;
    private final Object lock = new Object();

    private WifiP2pManager manager;
    private WifiP2pManager.Channel channel;
    private PeripheralReceiver receiver;
    private IntentFilter filter;

    private String deviceName;
    private InitSuccessListener initSuccessListener;

    public void init(final InitSuccessListener initSuccessListener) throws TimeoutException, ConnectionException{
//        wifiInit(); // todo: determine if this requires wifi connection
        this.initSuccessListener = initSuccessListener;

        manager = (WifiP2pManager) App.getAppContext().getSystemService(android.content.Context.WIFI_P2P_SERVICE);
        channel = manager.initialize(App.getAppContext(), Looper.myLooper(), null);
        receiver = new PeripheralReceiver();

        cycleWifi();

        // intent filter
        filter = new IntentFilter();
        filter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        filter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        filter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        filter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
        filter.addAction(WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION);

        // Create handler thread
        HandlerThread handlerThread = new HandlerThread("OnReceive Handler Thread");
        handlerThread.start();
        Looper looper = handlerThread.getLooper();
        Handler handler = new Handler(looper);

        App.getAppContext().registerReceiver(receiver, filter, null, handler);

        final long MAX_WAIT = 10 * 1000;
        final long start = System.currentTimeMillis();

        final WifiP2pManager.ActionListener createGroupListener = new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Create Group successful");
            }

            @Override
            public void onFailure(int reason) {
                Log.d(TAG, "Create Group failed: " + reason);
                long elapsed = System.currentTimeMillis() - start;

                //trigger failure callback if MAX_WAIT exceeded
                if (elapsed > MAX_WAIT) {
                    initSuccessListener.onInitFailure("P2P framework timed out");
                }

                //wait max 500 ms and try again
                synchronized (createGroupLock) {
                    try {
                        createGroupLock.wait(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                manager.createGroup(channel, this);
            }
        };

        final WifiP2pManager.ActionListener discoverPeersListener = new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Discover Peers successful");
            }

            @Override
            public void onFailure(int reason) {
                Log.d(TAG, "Discover peers failed: " + reason);
                long elapsed = System.currentTimeMillis() - start;

                //trigger failure callback if MAX_WAIT exceeded
                if (elapsed > MAX_WAIT) {
                    initSuccessListener.onInitFailure("P2P framework timed out");
                }

                //wait max 500 ms and try again
                synchronized (discoverPeersLock) {
                    try {
                        discoverPeersLock.wait(Math.min(500, MAX_WAIT - elapsed));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                manager.discoverPeers(channel, this);
            }
        };

        // create group
        manager.createGroup(channel, createGroupListener);
        manager.discoverPeers(channel, discoverPeersListener);

        // get ip address
        localIpAddress = tryGetIpV4Address();
    }

    private void cycleWifi() {
        Log.d(TAG, "cycleWifi: cycling");
        final int WIFI_CYCLE_DELAY = 2000;
        WifiManager wifiManager = (WifiManager)App.getAppContext().getSystemService(WIFI_SERVICE);

        long start = System.currentTimeMillis();
        long elapsed = 0;
        //disabled wifi
        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
            boolean done = false;
            while (!done) {
                synchronized (wifiCycleLock) {
                    try {
                        wifiCycleLock.wait(WIFI_CYCLE_DELAY - elapsed);
                        elapsed = System.currentTimeMillis() - start;
                        if (elapsed >= WIFI_CYCLE_DELAY) {
                            done = true;
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace(); // todo: enforce this delay??
                    }
                }
            }
        }

        //enable it again
        wifiManager.setWifiEnabled(true);
//        while(wifiManager.getWifiState() != WifiManager.WIFI_STATE_ENABLED){} //todo: possible freeze point??
    }

    @Override
    public void teardown() {
        // reset device name
        if(deviceName != null)
            setDeviceName(deviceName);

        cycleWifi();

        success = false;
        if(manager != null) {
            manager.removeGroup(channel, new WifiP2pManager.ActionListener() {
                @Override
                public void onSuccess() {
                    Log.d(TAG, "onSuccess: ");
                    success = true;
                    synchronized (lock) {
                        lock.notify();
                    }
                }

                @Override
                public void onFailure(int reason) {
                    Log.d(TAG, "teardown, removeGroup, onFailure: ");
                    success = true;
                    synchronized (lock) {
                        lock.notify();
                    }
                }
            });
        }

        synchronized (lock) {
            while (!success) {
                try {
                    lock.wait();
                } catch (Exception e) {
                }
            }
        }

        try {
            App.getAppContext().unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String tryGetIpV4Address() {
        try {
            final Enumeration<NetworkInterface> en =
                    NetworkInterface.getNetworkInterfaces();

            //fetch the p2p interface
            List<NetworkInterface> list = (List<NetworkInterface>) Collections.list(en);
            NetworkInterface intf = null;
            for (NetworkInterface i : list) {
                String name = i.getDisplayName();
                if (name.contains("p2p")) {
                    intf = i;
                    break;
                }
            }

            //fetch the ip address from the p2p interface
            final Enumeration<InetAddress> enumIpAddr =
                    intf.getInetAddresses();
            while (enumIpAddr.hasMoreElements()) {
                final InetAddress inetAddress = enumIpAddr.nextElement();
                if (!inetAddress.isLoopbackAddress()) {
                    final String addr = inetAddress.getHostAddress().toUpperCase();
                    if (RedroidUtil.isValidIpString(addr)) {
                        return addr;
                    }
                } // if
            } // while
        } // try
        catch (final Exception e) {
            // Ignore
        } // catch
        return null;
    } // tryGetIpV4Address()


    private void setDeviceName(String name) {
        Class[] paramTypes = new Class[3];
        Object arglist[] = new Object[3];

        try {
            paramTypes[0] = WifiP2pManager.Channel.class;
            paramTypes[1] = String.class;
            paramTypes[2] = WifiP2pManager.ActionListener.class;

            // get and store previous device name

            Method setDeviceName = manager.getClass().getMethod("setDeviceName", paramTypes);
            setDeviceName.setAccessible(true);

            arglist[0] = channel;
            arglist[1] = name;
            arglist[2] = new WifiP2pManager.ActionListener() {

                @Override
                public void onSuccess() {
                    Log.d(this.getClass().getName(), "setDeviceName succeeded");
                }

                @Override
                public void onFailure(int reason) {
                    Log.d(this.getClass().getName(), "setDeviceName failed");
                }
            };

            setDeviceName.invoke(manager, arglist);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public class PeripheralReceiver extends BroadcastReceiver {
        final boolean[] serverStarted = {false};

        @Override
        public void onReceive(Context context, Intent intent) {
            String TAG = this.getClass().getName();
            String action = intent.getAction();
            Log.d(TAG, (action + "\n" + intent.toString()));

            switch (action) {
                case WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION:
                    NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
                    Log.d(TAG, "P2P Connection changed. NetworkInfo: " + networkInfo.toString());

                    WifiP2pInfo info = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_INFO);
                    Log.d(TAG, "WifiP2PInfo: " + info.toString());

                    WifiP2pGroup group = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_GROUP);
                    if(group != null)
                        Log.d(TAG, "WifiP2PGroup: " + group.toString());

                    if(info.isGroupOwner && !serverStarted[0]) {
                        localIpAddress = info.groupOwnerAddress.toString().replace("/", "");
                        startServer();
                        serverStarted[0] = true;
                        initSuccessListener.onInitSuccess();
                    }

                    break;
                case WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION:
                    int discoveryState = intent.getIntExtra(WifiP2pManager.EXTRA_DISCOVERY_STATE, -1);
                    Log.d(TAG, "Discovery changed. DiscoverState: " + (discoveryState == 2 ? "Started" : "Stopped"));

                    break;
                case WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION:
                    WifiP2pDeviceList list = (WifiP2pDeviceList) intent.getParcelableExtra(WifiP2pManager.EXTRA_P2P_DEVICE_LIST);
                    if (list != null)
                        Log.d(TAG, "Peers Changed. DeviceList: " + Arrays.asList(list.getDeviceList()).toString());
                    else
                        Log.d(TAG, "Peers Changed. DeviceList is null");

                    break;
                case WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION:
                    int wifiState = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
                    Log.d(TAG, "State changed. wifi state: " + (wifiState == 2 ? "Enabled" : "Disabled"));
                    break;
                case WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION:
                    Log.d(TAG, "This device changed.");

                    if(deviceName == null){
                        WifiP2pDevice device = intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);
                        deviceName = device.deviceName;
                        setDeviceName("Android_" + PeripheralDataModel.getInstance().getDeviceID());
                    }

                    break;
                default:
                    break;
            }
        }
    }

    public interface InitSuccessListener{
        public void onInitSuccess();
        public void onInitFailure(String msg);
    }


}
