package net.redroid.redroidperipheral.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

public class PeripheralReceiver extends BroadcastReceiver {
    private WifiP2pManager.Channel channel;
    private WifiP2pManager manager;
    private Context context;

    protected PeripheralReceiver(Context context, WifiP2pManager.Channel channel, WifiP2pManager manager){
        this.channel = channel;
        this.manager = manager;
        this.context = context;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d(this.getClass().getName(), (action + "\n" + intent.toString()));

        if(action.equals(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION)){
            if(manager == null)
                return;

            NetworkInfo networkInfo = (NetworkInfo)intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
            // update display to show connected status
//            activity.setConnectionStatus(networkInfo.isConnected());
            if(networkInfo.isConnected()){
                Log.d(this.getClass().getName(), "Connected. Starting peripheral server");
            }

        }
    }

}
