package net.redroid.redroidperipheral.network;

import android.content.Context;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.google.android.gms.location.LocationListener;

import net.redroid.redroidcorelibrary.API;
import net.redroid.redroidcorelibrary.ApiResponse;
import net.redroid.redroidcorelibrary.App;
import net.redroid.redroidperipheral.PeripheralDataModel;
import net.redroid.redroidperipheral.SensorEventRegistration;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import static android.content.ContentValues.TAG;
import static java.lang.Math.max;
import static java.lang.Math.min;


public class PeripheralServerNetwork extends PeripheralNetwork {

    private final Object apiSyncLock = new Object();
    private final Object eventSyncLock = new Object();

    private PeripheralDataModel model = PeripheralDataModel.getInstance();
    private Socket apiSocket;
    private BufferedWriter apiSocketWriter;
    private BufferedReader apiSocketReader;

    private Socket eventSocket;
    private BufferedWriter eventSocketWriter;
    private BufferedReader eventSocketReader;
    private List <SensorEventRegistration> sensorEventRegistrations = new ArrayList<>();
    private LocationListener locationListener;

    public void init() throws TimeoutException, ConnectionException {
        super.init();
        serverIpAddress = model.getConnectionServerUrl();
    }

    @Override
    public void teardown() {
        super.teardown();
        try {
            synchronized (apiSyncLock) {
                if (apiSocketReader != null) {
//                    apiSocketReader.close();
                    apiSocketReader = null;
                }
                if (apiSocketWriter != null) {
                    apiSocketWriter.close();
                    apiSocketWriter = null;
                }
                if (apiSocket != null) {
                    apiSocket.close();
                    apiSocket = null;
                }
            }
            synchronized (eventSyncLock) {
                if (eventSocketReader != null) {
                    eventSocketReader.close();
                    eventSocketWriter = null;
                }
                if (eventSocketWriter != null) {
                    eventSocketWriter.close();
                    eventSocketWriter = null;
                }
                if (eventSocket != null) {
                    eventSocket.close();
                    eventSocket = null;
                }
            }


        } catch (IOException e) {
            Log.e(TAG, "teardown: " + e.getMessage());
            e.printStackTrace();
        }

        //unregister all sensor events that remain
        for (SensorEventRegistration reg : sensorEventRegistrations) {
            ((SensorManager)App.getAppContext().getSystemService(Context.SENSOR_SERVICE))
                    .unregisterListener(reg.listener);
        }
        sensorEventRegistrations.clear();
    }


    /**
     * For setting the apiSocket. A thread, as it performs networking tasks, and it is assumed
     * that this will be called from the main thread.
     * @return the socket, or null if creation failed.
     */
    private void setAPISocket() throws TimeoutException {

        final boolean[] finished = {false};

        synchronized (apiSyncLock) {
            if (apiSocket == null) {
                new Thread(new Runnable() {
                    @Override
                    public synchronized void run() {
                        synchronized (apiSyncLock) {
                            try {
                                apiSocket = new Socket();
//                                apiSocket.setSoTimeout(3000);
                                apiSocket.setSoTimeout(10000);
//                                apiSocket.connect(new InetSocketAddress(serverIpAddress,
//                                        SERVER_API_PORT), 3000);
                                apiSocket.connect(new InetSocketAddress(serverIpAddress,
                                        SERVER_API_PORT), 10000);
                                apiSocketReader = new BufferedReader(
                                        new InputStreamReader(apiSocket.getInputStream()));
                                apiSocketWriter = new BufferedWriter(
                                        new OutputStreamWriter(apiSocket.getOutputStream()));

                                //notify main thread of completion
                                finished[0] = true;
                                apiSyncLock.notify();
                            } catch (IOException e) {
                                Log.e(TAG, "setApiSocket: " + e.getMessage());
                                apiSocket = null;
                                finished[0] = true;
                                apiSyncLock.notify();
                            }
                        }
                    }
                }).start();
            }
        }

        //BLOCK UNTIL COMPLETE OR TIMES OUT
        while (!finished[0]) {
            synchronized (apiSyncLock) {
                try {
                    apiSyncLock.wait();
                } catch (InterruptedException e) {}
            }
        }
        if (apiSocketWriter == null) {
            throw new TimeoutException();
        }

    }

    /**
     * For setting the evensocket using lazy initialization. Must be called from a worker thread,
     * as it performs networking tasks.
     */
    public synchronized void setEventSocket() {
        if (eventSocket == null) {
            synchronized (eventSyncLock) {
                try {
                    eventSocket = new Socket();
                    eventSocket.setSoTimeout(SOCKET_READ_TIMEOUT);
                    eventSocket.connect(new InetSocketAddress(serverIpAddress,
                            SERVER_EVENT_PORT), SOCKET_CONNECTION_TIMEOUT);
                    eventSocketReader = new BufferedReader(
                            new InputStreamReader(eventSocket.getInputStream()));
                    eventSocketWriter = new BufferedWriter(
                            new OutputStreamWriter(eventSocket.getOutputStream()));

                    //notify main thread of completion
                    eventSyncLock.notify();
                } catch (IOException e) {
                    Log.e(TAG, "setApiSocket: " + e.getMessage());
                }
            }
        }
    }

    /**
     * For reversing event socket setting if the peripheral/connect/event api call fails
     */
    public void nullifyEventSocket() {
        try {
            eventSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        eventSocket = null;
        eventSocketReader = null;
        eventSocketWriter = null;
    }

    public synchronized Socket getEventSocket() {
        return eventSocket;
    }

    public synchronized BufferedWriter getApiSocketWriter() {

        return apiSocketWriter;
    }

    public synchronized BufferedReader getApiSocketReader() {
        return apiSocketReader;
    }

    public synchronized BufferedWriter getEventSocketWriter() {
        return eventSocketWriter;
    }

    public synchronized BufferedReader getEventSocketReader() {
        return eventSocketReader;
    }

    /**
     * A blocking method which performs the handshake for connecting with a server. If no response
     * is received from the server it will time out after a given interval and return. Returns a
     * message string on failure, or null on success.
     * @return a string containing a failure message, or null if successful.
     */
    public synchronized String connectToServer() throws TimeoutException {

        final Object monitorObject = new Object();
        final int TIMEOUT_INTERVAL = SOCKET_READ_TIMEOUT;

        setAPISocket(); //throws TimeoutException

        //contain the return message
        final String[] out = {null};

        //flag to track completion and success of the handshake phases
        final Boolean[] successFlag = {null};


        //DO HANDSHAKE
        model.api.requestPostRedroidPeripheralConnectApi(serverIpAddress, apiSocketReader, apiSocketWriter,
                new API.ApiResponseListener() {
                    @Override
                    public void onApiResponse(int responseCode, JSONObject response) {
                        //check for failure and notify user of reason
                        if (responseCode != 200) {
                            String msg;
                            try {
                                if (response != null)
                                    msg = (String) response.get("msg");
                                else
                                    msg = "unspecified cause";
                            } catch (JSONException e) {
                                msg = null;
                            }
                            Log.e(TAG, "handshake response contained failure code: " + responseCode);
                            out[0] = ((msg == null) ? "code " + responseCode : msg);

                            //notify parent thread of completion
                            successFlag[0] = false;
                        } else {
//                            try {
//                                //set the rtmp server url from the response
//                                model.setRtmpServerUrl(response.getString("rtmp_server_url"));
//                            } catch (JSONException e) {
//                                out[0] = "Server '200 OK' response did not contain rtmp server url";
//                                successFlag[0] = false;
//                            }
                            successFlag[0] = true;
                        }
                        //notify parent thread of callback completion
                        synchronized (monitorObject) {
                            monitorObject.notify();
                        }
                    }
                });

        //BLOCK UNTIL HANDSHAKE COMPLETE
        //time out after TIMEOUT_INTERVAL milliseconds with no response
        long startTime = System.currentTimeMillis();
        long elapsedTime = 0;
        while (successFlag[0] == null) {
            synchronized (monitorObject) {
                try {
                    //sleep for the remainder of the timeout interval
                    monitorObject.wait(max(TIMEOUT_INTERVAL - elapsedTime, 0));
                } catch (InterruptedException e) {}
            }
            //return early on a timeout
            elapsedTime = System.currentTimeMillis() - startTime;
            if (elapsedTime >= TIMEOUT_INTERVAL && successFlag[0] == null) {
                out[0] = "Handshake timed out on step 1";
                return out[0];
            }
        }

        //complete! now return the result
        return out[0];
    }

    public void startApiServer() {
        PeripheralAPIServer apiServer = new PeripheralAPIServer();
        apiServer.start();
    }

    /**
     * A thread which continuously handles incoming API requests until the socket closes
     */
    private class PeripheralAPIServer extends Thread {
        int responseCode = 200;
        String[] requestParams = null;
        String httpResponse = null;
        ApiResponse response = null;
        String responseBody = "";

        @Override
        public void run() {

            try {
                //start processing API calls
                while (apiSocket != null && !apiSocket.isClosed()) {

                    try {
                        //fetch the request
                        requestParams = getHttpRequestParams(PeripheralServerNetwork.this.apiSocketReader);
                        if (requestParams == null) continue;

                        String remoteIp = apiSocket.getInetAddress().toString()
                                .replace("/", "");
                        //dispatch the request to the proper API method
                        response = model.api.dispatchRequest(remoteIp, requestParams[0],
                                requestParams[1], requestParams[2]);

                        responseCode = response.getCode();

                        //generate the HTTP response string from the result
                        httpResponse = getHttpResponseString(responseCode,
                                (response.getBody() == null ? "" : response.getBody()));

                    } catch (JSONException | NumberFormatException e) {
                        Log.d(this.getClass().getName(), "run: " + e.getMessage());
                        responseCode = 411;
                        responseBody = "{\"msg\":\"malformed request\"}";
                        httpResponse = getHttpResponseString(responseCode, responseBody);
                    }
                    apiSocketWriter.write(httpResponse);
                    apiSocketWriter.flush();
                }
                teardown();
                //tell the home activity that the connection was lost
                sendNumConnectedBroadcast(-2); //-2 is used to signal server disconnect
            } catch (IOException | NullPointerException e) {
                teardown();
                //tell the home activity that the connection was lost
                sendNumConnectedBroadcast(-2); //-2 is used to signal server disconnect
            }
        }
    }

    public void addSensorEventRegistration(SensorEventListener listener, int sensorType, int reqId){
        sensorEventRegistrations.add(new SensorEventRegistration(listener, sensorType, reqId));
    }

    public void removeAndUnregisterSensorEventListener(int sensorType, int reqId){
        ArrayList<SensorEventRegistration> found = new ArrayList<>();
        for (SensorEventRegistration reg : sensorEventRegistrations) {
            if (reg.sensorType == sensorType && reg.requestId == reqId)
                found.add(reg);
        }
        for (SensorEventRegistration reg : found) {
            ((SensorManager)App.getAppContext().getSystemService(Context.SENSOR_SERVICE))
                    .unregisterListener(reg.listener);
            sensorEventRegistrations.remove(reg);
        }
    }

    public void setLocationListener(LocationListener listener) {
        locationListener = listener;
        setEventSocket();
    }
    public void removeLocationListener() {
        locationListener = null;
    }


    public void setServerIpAddress(String address) {
        serverIpAddress = address;
    }
}

