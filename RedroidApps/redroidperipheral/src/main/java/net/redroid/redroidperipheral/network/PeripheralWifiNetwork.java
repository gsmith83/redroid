package net.redroid.redroidperipheral.network;

import android.util.Log;

import net.redroid.redroidcorelibrary.ApiResponse;
import net.redroid.redroidperipheral.PeripheralDataModel;
import net.redroid.redroidperipheral.WifiConnectedDevice;

import org.json.JSONException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeoutException;

import static android.content.ContentValues.TAG;


public class PeripheralWifiNetwork extends PeripheralNetwork {

    public static final Object deviceSyncLock = new Object();

    private final static int MAX_CONNECTED_DEVICES = 2;
    public PeripheralDataModel model = PeripheralDataModel.getInstance();
    private List<WifiConnectedDevice> connectedDevices = new ArrayList<>();
    private ServerSocket apiListenerSocket;
    public List<Socket> apiClientSocketList = new ArrayList<>();


    public void init() throws TimeoutException, ConnectionException {
        localIpAddress = wifiInit();
        startServer();
    }

    @Override
    public void teardown() {
        super.teardown();
        try {
            if (apiListenerSocket != null)
                apiListenerSocket.close();
            for (WifiConnectedDevice device:
                 connectedDevices) {
                if (device.eventSocket != null)
                    device.eventSocket.close();
            }
            connectedDevices.clear();
            sendNumConnectedBroadcast(connectedDevices.size());

            //close all API sockets
            for (Socket client : apiClientSocketList) {
                if (client != null)
                    client.close();
            }
            apiClientSocketList.clear();
        } catch (IOException e) {
            Log.d(TAG, "teardown: " + e.getMessage());
        }

        //todo: deregister all type event listeners -- possibly in root teardown()?
    }

    public void startServer(){
        new PeripheralConnectionServer().start();
    }

    public synchronized boolean addConnectedDevice(WifiConnectedDevice device) {
        synchronized (deviceSyncLock) {
            if ((!(this instanceof PeripheralP2PNetwork) && connectedDevices.size() < MAX_CONNECTED_DEVICES) ||
                    (this instanceof PeripheralP2PNetwork && connectedDevices.size() < 1)) {
                Log.d(TAG, "master device connected");
                connectedDevices.add(device);

                sendNumConnectedBroadcast(connectedDevices.size());
                return true;
            } else
                return false;
        }
    }

    public void removeConnectedDevice(WifiConnectedDevice device) {
        synchronized (deviceSyncLock) {
            try {
                if (device.eventSocket != null) {
                    device.eventSocketWriter.flush();
                    device.eventSocket.close();
                }
            } catch (IOException e) {}
            Log.d(TAG, "master device disconnected");
            connectedDevices.remove(device);
            int size = connectedDevices.size();
            sendNumConnectedBroadcast(size);
        }
    }

    private int countRtmpConnectedDevices() {
        synchronized (deviceSyncLock) {
            int count = 0;
            for (WifiConnectedDevice device : connectedDevices) {
                if (device.streamsRtmp()) {
                    count++;
                }
            }
            return count;
        }
    }



    boolean isRegistered(int sensorType) {
        synchronized (deviceSyncLock) {
            for (WifiConnectedDevice device : connectedDevices) {
                if (device.isRegistered(sensorType))
                    return true;
            }
            return false;
        }
    }

    Iterator<WifiConnectedDevice> getDeviceIterator() {
        synchronized (deviceSyncLock) {
            return connectedDevices.iterator();
        }
    }


    public long generateSessionId() {
        long id = new Random().nextLong();
        boolean success;
        synchronized (deviceSyncLock) {
            do {
                success = true;
                for (WifiConnectedDevice device : connectedDevices) {
                    if (device.sessionId.equals("" + id))
                        success = false;
                }
            } while (!success);
        }
        return id;
    }


    private class PeripheralConnectionServer extends Thread {
        @Override
        public void run(){

            try{
                InetAddress inetAddr = InetAddress.getByName(localIpAddress);
                apiListenerSocket = new ServerSocket();
                apiListenerSocket.setReuseAddress(true);
                apiListenerSocket.bind(new InetSocketAddress(inetAddr, SERVER_API_PORT), MAX_CONNECTED_DEVICES);


                while(true){
                    Socket apiClientSocket = apiListenerSocket.accept();
                    apiClientSocket.setReuseAddress(true);
                    apiClientSocket.setKeepAlive(true);
                    apiClientSocketList.add(apiClientSocket);



                    // spin off thread
                    PeripheralAPIServer server = new PeripheralAPIServer(apiClientSocket);
                    server.start();
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private class PeripheralAPIServer extends Thread{
        Socket clientSocket;
        int responseCode = 200;
        String[] requestParams = null;
        String httpResponse = null;
        ApiResponse response = null;
        BufferedReader reader = null;
        BufferedWriter writer = null;
        String responseBody = "";

        PeripheralAPIServer(Socket clientSocket){
            this.clientSocket = clientSocket;
        }


        @Override
        public void run(){

            //add this connection to the connected devices for now
            String ipAddress = clientSocket.getRemoteSocketAddress()
                    .toString().replace("/", "").split(":")[0];
            WifiConnectedDevice device = new WifiConnectedDevice(ipAddress);
            if (!addConnectedDevice(device)) {
                try {
                    if (clientSocket != null) {
                        clientSocket.close();
                        apiClientSocketList.remove(clientSocket);
                    }
                }
                catch (IOException e){}
                return;
            }


            try {
                reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

                // Get the handshake
                requestParams = getHttpRequestParams(reader);

                Boolean authSuccess = true;

                //PHASE 1: check for the hello handshake request
                if (!requestParams[0].equals("POST") ||
                    !requestParams[1].equals("/redroid/master/connect/api")) {

                    authSuccess = false;

                    responseCode = 401;
                    responseBody = "{\"msg\" : \"Expected handshake not received.\"}";
                }
                //get the API response
                else {
                    response = model.api.respondPostRedroidMasterHello(requestParams[2],
                            device.getSessionId());
                    responseCode = response.getCode();
                    responseBody = response.getBody();
                    if (responseCode != 200) {
                        authSuccess = false;
                    }
                }
                //send the response
                httpResponse = getHttpResponseString(responseCode, responseBody);
                writer.write(httpResponse);
                writer.flush();
                if (!authSuccess) {
                    clientSocket.close();
                    apiClientSocketList.remove(clientSocket);
                    reader.close();
                    removeConnectedDevice(device);
                    return;
                }

                //PHASE 2: check for the auth handshake request

                // Get the auth request
                requestParams = getHttpRequestParams(reader);

                if (!requestParams[0].equals("POST") ||
                    !requestParams[1].equals("/redroid/master/auth")) {

                    authSuccess = false;
                    responseCode = 401;
                }
                //check the session id and security_token
                else {
                    response = model.api.respondPostRedroidMasterAuth(requestParams[2],
                            device.getSessionId());
                    responseCode = response.getCode();
                    responseBody = response.getBody();
                    if (responseCode != 200) {
                        authSuccess = false;
                    }
                }
                //send the response
                httpResponse = getHttpResponseString(responseCode, responseBody);
                writer.write(httpResponse);
                writer.flush();

                //shutdown the session and remove the device from connected sensors if failure
                if (!authSuccess) {
                    clientSocket.close();
                    apiClientSocketList.remove(clientSocket);
                    reader.close();
                    removeConnectedDevice(device);
                    return;
                }

                //start processing API calls
                while(clientSocket != null && !clientSocket.isClosed()) {

                    try {
                        //fetch the request
                        requestParams = getHttpRequestParams(reader);
                        String remoteIp = clientSocket.getInetAddress().toString()
                                .replace("/", "");

                        //dispatch the request to the proper API method
                        response = model.api.dispatchRequest(remoteIp, requestParams[0],
                                requestParams[1], requestParams[2], device);

                        responseCode = response.getCode();

                        //generate the HTTP response string from the result
                        httpResponse = getHttpResponseString(responseCode,
                                (response.getBody() == null? "" : response.getBody()));
                    } catch (JSONException | NumberFormatException e) {
                        Log.d(this.getClass().getName(), "run: " + e.getMessage());
                        responseCode = 411;
                        responseBody = "{\"msg\":\"malformed request\"}";
                        httpResponse = getHttpResponseString(responseCode, responseBody);
                    }
                    writer.write(httpResponse);
                    writer.flush();
                }
                reader.close();
                if (clientSocket != null) {
                    clientSocket.close();
                    apiClientSocketList.remove(clientSocket);
                }
                removeConnectedDevice(device);
            } catch (IOException | NullPointerException e) {
                try {
                    if (clientSocket != null) {
                        clientSocket.close();
                        apiClientSocketList.remove(clientSocket);
                    }
                    if (reader != null)
                        reader.close();
                    removeConnectedDevice(device);
                } catch (IOException i){}
            }
        }
    }



}
