package net.redroid.redroidperipheral.video;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import net.redroid.redroidperipheral.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.ContentValues.TAG;


public class VideoRecorderActivity extends Activity {

    Context context;
    static Camera camera;
    FrameLayout previewFrame;
    CameraPreview preview;
    Button shutterButton;
    MediaRecorder recorder;
    PowerManager.WakeLock wakeLock;
    PowerManager pm;
    Handler uiHandler = new Handler();
    Thread cameraThread;
    int seconds = 0;
    Boolean paused = false;
    SurfaceHolder holder;

    static final int FRONT = 2;
    static final int BACK = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_video_recorder);
        context = getApplicationContext();

        //get UI elements
        previewFrame = (FrameLayout)findViewById(R.id.surfaceFrame);
        shutterButton = (Button)findViewById(R.id.shutter);

        //get a camera instance
        checkCamera();
        camera = getCameraInstance();
        preview = new CameraPreview(this, camera);
        previewFrame.addView(preview);

        //set listeners
        shutterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (shutterButton.getText().equals("●")) {

                    //manage the shutter button apperance
                    shutterButton.setText("■");
                    shutterButton.setTextColor(Color.WHITE);

                    // all the stuff that has to happen to record video from the camera
                    recorder = new MediaRecorder();
                    camera.unlock();
                    recorder.setCamera(camera);
                    recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
                    recorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
                    recorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH ));
                    recorder.setOutputFile(getPath());
                    try {
                        recorder.prepare();
                    } catch (IOException e) {

                        e.printStackTrace();
                    }
                    recorder.start();
                }
                else {
                    shutterButton.setText("●");
                    shutterButton.setTextColor(Color.RED);
                    recorder.stop();
                    recorder.reset();
                    recorder.release();
                    camera.lock();
                }

            }
        });
        //https://developer.android.com/reference/android/media/MediaRecorder.html
        //get media recorder ready to record
        //recorder.setPreviewDisplay()
    }

    @Override
    protected void onStop() {
        super.onStop();
        camera.release();
    }

    private String getPath() {
        File dir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "RedroidPeripheralVideoRecorder");

        // Create the storage directory if it does not exist
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                Toast.makeText(context, "failed to create directory: RedroidPeripheralVideoRecorder", Toast.LENGTH_SHORT).show();
                return null;
            }
        }
        String path = dir.getPath();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        path += File.separator + "VID_" + timeStamp + ".mp4";
        return path;
    }

    private void checkCamera() {
        PackageManager packageManager = context.getPackageManager();
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Toast.makeText(context, "Found a camera!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "No camera :(", Toast.LENGTH_SHORT).show();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            finish();
        }
    }

    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            //Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }



    /** A basic Camera preview class */
    public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {
        private SurfaceHolder mHolder;
        private Camera mCamera;

        public CameraPreview(Context context, Camera camera) {
            super(context);
            mCamera = camera;

            // Install a SurfaceHolder.Callback so we get notified when the
            // underlying surface is created and destroyed.
            mHolder = getHolder();
            mHolder.addCallback(this);
            // deprecated setting, but required on Android versions prior to 3.0
            mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
            holder = mHolder;
        }

        public void surfaceCreated(SurfaceHolder holder) {
            // The Surface has been created, now tell the camera where to draw the preview.
            try {
                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
            } catch (IOException e) {
                Log.d(TAG, "Error setting camera preview: " + e.getMessage());
            }
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            // empty. Take care of releasing the Camera preview in your activity.
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
            // If your preview can change or rotate, take care of those events here.
            // Make sure to stop the preview before resizing or reformatting it.

            if (mHolder.getSurface() == null){
                // preview surface does not exist
                return;
            }

            // stop preview before making changes
            try {
                mCamera.stopPreview();
            } catch (Exception e){
                // ignore: tried to stop a non-existent preview
            }

            // set preview size and make any resize, rotate or
            // reformatting changes here

            // start preview with new settings
            try {
                mCamera.setPreviewDisplay(mHolder);
                mCamera.startPreview();

            } catch (Exception e){
                Log.d(TAG, "Error starting camera preview: " + e.getMessage());
            }
        }
    }

}
