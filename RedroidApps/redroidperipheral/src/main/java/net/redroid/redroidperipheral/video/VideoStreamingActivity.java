package net.redroid.redroidperipheral.video;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import net.redroid.redroidperipheral.R;

public class VideoStreamingActivity extends AppCompatActivity {

    YaseaFragment yaseaFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_streaming);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        // response screen rotation event


        // load shared configuration for yasea
        SharedPreferences sp = this.getSharedPreferences("Yasea", this.MODE_PRIVATE);
        SharedPreferences.Editor edit = sp.edit();
        edit.clear();
        edit.putString("rtmpUrl", "rtmp://24.2.102.119/live/testvideo");
        //edit.putString("rtmpUrl", "rtmp://redroid-video.westus.cloudapp.azure.com/live/testvideo");
        edit.putString("audioOnly", "false");
        edit.commit();



        //place video stream fragment
        yaseaFragment = new YaseaFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.yasea_video_fragment_frame, yaseaFragment)
                .commit();
    }

}
